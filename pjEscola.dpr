program pjEscola;

uses
  Vcl.Forms,
  Aux_Padrao in 'pas\Aux_Padrao.pas' {FrmAux_Padrao},
  cad_usuarios in 'pas\cad_usuarios.pas' {FrmCad_Usuarios},
  cls_usuarios in 'pas\cls_usuarios.pas',
  cls_utils in 'pas\cls_utils.pas',
  cad_situacoes_entidade in 'pas\cad_situacoes_entidade.pas' {FrmCad_Situacoes_Entidade},
  cls_situacoes_entidade in 'pas\cls_situacoes_entidade.pas',
  cad_entidades in 'pas\cad_entidades.pas' {FrmCad_Entidades},
  cls_empresas in 'pas\cls_empresas.pas',
  cls_tabelacodigos in 'pas\cls_tabelacodigos.pas',
  menu in 'pas\menu.pas' {frmMenu},
  login in 'pas\login.pas' {frmLogin},
  cls_usuarios_logins in 'pas\cls_usuarios_logins.pas',
  cls_enderecos in 'pas\cls_enderecos.pas',
  cls_cidades in 'pas\cls_cidades.pas',
  cls_estados in 'pas\cls_estados.pas',
  cls_contatos in 'pas\cls_contatos.pas',
  cad_salas in 'pas\cad_salas.pas' {FrmCad_Salas},
  cls_responsaveis in 'pas\cls_responsaveis.pas',
  cls_salas in 'pas\cls_salas.pas',
  cls_funcionarios in 'pas\cls_funcionarios.pas',
  cad_funcionarios in 'pas\cad_funcionarios.pas' {FrmCad_Funcionarios},
  cls_entidades_tipos in 'pas\cls_entidades_tipos.pas',
  cls_contratos in 'pas\cls_contratos.pas',
  cls_produtos in 'pas\cls_produtos.pas',
  cls_formas_pagamentos_contratos in 'pas\cls_formas_pagamentos_contratos.pas',
  cls_agendamentos in 'pas\cls_agendamentos.pas',
  dm_conexao in 'pas\dm_conexao.pas' {Conexao: TDataModule},
  cls_entidades in 'pas\cls_entidades.pas',
  mov_agendamentos in 'pas\mov_agendamentos.pas' {FrmMov_Agendamentos},
  cls_historicos_agendamentos in 'pas\cls_historicos_agendamentos.pas',
  mov_contas_a_receber in 'pas\mov_contas_a_receber.pas' {FrmMov_Contas_a_Receber},
  cls_contas_a_receber in 'pas\cls_contas_a_receber.pas',
  cls_historicos_cr in 'pas\cls_historicos_cr.pas',
  cls_bancos in 'pas\cls_bancos.pas',
  cad_bancos in 'pas\cad_bancos.pas' {FrmCad_Bancos},
  cls_contas_bancarias in 'pas\cls_contas_bancarias.pas',
  cad_contasbancarias in 'pas\cad_contasbancarias.pas' {FrmCad_ContasBancarias},
  cls_agendamentos_entidade in 'pas\cls_agendamentos_entidade.pas',
  cls_cc_entidade_aulas in 'pas\cls_cc_entidade_aulas.pas',
  aux_backup in 'pas\aux_backup.pas' {FrmAux_Backup},
  cad_contratos in 'pas\cad_contratos.pas' {FrmCad_Contratos},
  cls_historico_contratos in 'pas\cls_historico_contratos.pas',
  Aux_RelAulasProfessorData in 'pas\Aux_RelAulasProfessorData.pas' {FrmAux_RelAulasProfessorData};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
//  Application.ShowMainForm      := False;
  Application.CreateForm(TConexao, Conexao);
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.Run;
end.
