--create user 'user'@'server';
--ALTER USER 'user'@'server' IDENTIFIED WITH mysql_native_password BY ''
--grant all privileges on *.* to 'user'@'server';

create table tabelacodigos(
  tab_nome       varchar(30),
  tab_composicao varchar(30),
  tab_codigo     integer    ,
constraint pk_tabelacodigos primary key (tab_nome, tab_composicao));

create table salas(
  sala_codigo     integer    ,
  sala_emp_codigo integer    ,
  sala_descricao  varchar(60),
constraint pk_salas primary key (sala_codigo, sala_emp_codigo));

create table usuarios(
  usu_codigo integer    ,
  usu_nome   varchar(60),
  usu_login  varchar(20),
  usu_senha  varchar(20),
  usu_nivel  integer    ,
  usu_status integer    ,
constraint pk_usuarios primary key (usu_codigo));

create table estados(
  uf_sigla    varchar(3)  ,
  uf_nome     varchar(150),
  uf_regiao   varchar(3)  ,
  uf_cod_ibge varchar(20) ,
constraint pk_estados primary key (uf_sigla));

create table cidades(
  cid_codigo   integer     ,
  cid_nome     varchar(150),
  cid_uf_sigla varchar(3)  ,
  cid_cod_ibge varchar(20) ,
constraint pk_cidades primary key (cid_codigo),
constraint fk_uf_cidades foreign key (cid_uf_sigla) references estados (uf_sigla));

create table empresas(
  emp_codigo        integer     ,
  emp_razao_social  varchar(150),
  emp_nome_fantasia varchar(150),
  emp_cnpj          varchar(14) ,
  emp_ie            varchar(20) ,
  emp_im            varchar(20) ,
  emp_telefone      varchar(20) ,
  emp_email         varchar(150),  
  emp_data_abertura date        , 
  emp_observacoes   blob        ,
  emp_cep           varchar(20) ,
  emp_logradouro    varchar(150),
  emp_numero        integer     ,
  emp_complemento   varchar(150),
  emp_uf_sigla      varchar(3)  ,
  emp_cid_codigo    integer     ,
  emp_situacao      integer     ,
constraint pk_empresas primary key (emp_codigo),
constraint fk_uf_empresas foreign key (emp_uf_sigla) references estados (uf_sigla),
constraint fk_cidades_empresas foreign key (emp_cid_codigo) references cidades (cid_codigo));

create table situacoes_entidade(
  sit_codigo    integer auto_increment,
  sit_descricao varchar(15),
constraint pk_situacoes_entidade primary key (sit_codigo));

create table entidades(
  ent_codigo          integer     ,
  ent_emp_codigo      integer     ,
  ent_razao_social    varchar(150),
  ent_nome_fantasia   varchar(150),
  ent_tipo_pessoa     integer     ,
  ent_cnpj            varchar(20) ,
  ent_ie              varchar(20) ,
  ent_im              varchar(20) ,
  ent_ir              varchar(20) ,
  ent_suframa         varchar(20) ,
  ent_ramo_codigo     integer     ,
  ent_cpf             varchar(20) ,
  ent_rg              varchar(20) ,
  ent_data_nascimento date        ,
  ent_ocupacao        varchar(50) ,
  ent_estado_civil    varchar(20) ,
  ent_sit_codigo      integer     ,
constraint pk_entidades primary key (ent_codigo, ent_emp_codigo),
constraint fk_empresas_entidades foreign key (ent_emp_codigo) references empresas (emp_codigo),
constraint fk_situacoes_entidades foreign key (ent_sit_codigo) references situacoes_entidade (sit_codigo));

create table usuarios_logins(
  ul_usu_codigo integer    ,
  ul_sequencia  integer    ,
  ul_data_hora  datetime   ,  
  ul_maquina    varchar(30),
constraint pk_usuarios_logins primary key (ul_usu_codigo, ul_sequencia),
constraint fk_logins_usuarios foreign key (ul_usu_codigo) references usuarios (usu_codigo));

create table enderecos(
  end_codigo         integer     ,
  end_ent_codigo     integer     ,
  end_ent_emp_codigo integer     ,
  end_tipo           integer     ,
  end_cep            varchar(20) ,
  end_uf_sigla       varchar(3)  ,
  end_cid_codigo     integer     ,
  end_logradouro     varchar(150),
  end_numero         integer     ,
  end_complemento    varchar(150),
constraint pk_enderecos primary key (end_codigo, end_ent_codigo, end_ent_emp_codigo),
constraint fk_entidades_enderecos foreign key (end_ent_codigo, end_ent_emp_codigo) references entidades (ent_codigo, ent_emp_codigo));

create table contatos(
  con_codigo         integer     ,
  con_ent_codigo     integer     ,
  con_ent_emp_codigo integer     ,
  con_email          varchar(150),
  con_contato        varchar(60) ,
  con_telefone       varchar(25) ,
  con_observacao     varchar(150),
constraint pk_contatos primary key (con_codigo, con_ent_codigo, con_ent_emp_codigo),
constraint fk_entidades_contatos foreign key (con_ent_codigo, con_ent_emp_codigo) references entidades (ent_codigo, ent_emp_codigo));

create table responsaveis(
  res_ent_codigo      integer     ,
  res_ent_emp_codigo  integer     ,
  res_nome            varchar(150),
  res_rg              varchar(20) ,
  res_cpf             varchar(20) ,
  res_data_nascimento date        ,
constraint pk_responsaveis primary key (res_ent_codigo, res_ent_emp_codigo),
constraint fk_entidade_responsaveis foreign key (res_ent_codigo, res_ent_emp_codigo) references entidades (ent_codigo, ent_emp_codigo));

alter table entidades add ent_observacoes blob;

create table funcionarios(
  fun_ent_codigo      integer      ,
  fun_ent_emp_codigo  integer      ,
  fun_ct              varchar(20)  ,
  fun_pis             varchar(20)  ,
  fun_funcao          varchar(150) ,  
  fun_salario         numeric(15,4),
  fun_modo            integer      ,
  fun_situacao        integer      ,
constraint pk_funcionarios primary key (fun_ent_codigo, fun_ent_emp_codigo));

create table entidades_tipos(
  etp_ent_codigo     integer,
  etp_ent_emp_codigo integer,
  etp_tipo           integer,
constraint pk_entidades_tipos primary key (etp_ent_codigo, etp_ent_emp_codigo, etp_tipo),
constraint fk_entidades_tipo foreign key (etp_ent_codigo, etp_ent_emp_codigo) references entidades (ent_codigo, ent_emp_codigo));

create table produtos(
  pro_codigo     integer      ,
  pro_emp_codigo integer      ,
  pro_tipo       integer      ,
  pro_descricao  varchar(150) ,
  pro_unidade    integer      ,
  pro_preco      numeric(15,4),
constraint pk_produtos primary key (pro_codigo, pro_emp_codigo));

create table contratos(
  con_codigo                     integer      ,
  con_ent_codigo                 integer      ,  
  con_ent_emp_codigo             integer      ,
  con_tipo                       integer      ,
  con_pro_codigo                 integer      ,
  con_pro_emp_codigo             integer      ,  
  con_descricao                  varchar(250) ,
  con_ent_codigo_funcionario     integer      ,
  con_ent_emp_codigo_funcionario integer      ,  
  con_valor                      numeric(15,4),
  con_vencimento_pagamento       integer      ,
  con_vencimento_contrato        date         ,
  con_horas_mes                  integer      ,
  con_contrato                   blob         ,
  con_situacao                   integer      ,
constraint pk_contratos primary key (con_codigo, con_ent_codigo, con_ent_emp_codigo),
constraint fk_entidades_contratos foreign key (con_ent_codigo, con_ent_emp_codigo) references entidades (ent_codigo, ent_emp_codigo),
constraint fk_produtos_contratos foreign key (con_pro_codigo, con_pro_emp_codigo) references produtos (pro_codigo, pro_emp_codigo),
constraint fk_funcionarios_contratos foreign key (con_ent_codigo_funcionario, con_ent_emp_codigo_funcionario) references entidades (ent_codigo, ent_emp_codigo));
  
create table formas_pagamentos_contratos(
  fpc_codigo             integer,
  fpc_con_codigo         integer,
  fpc_con_ent_codigo     integer,
  fpc_con_ent_emp_codigo integer,
constraint pk_formas_pagamentos_contratos primary key (fpc_codigo, fpc_con_codigo, fpc_con_ent_codigo, fpc_con_ent_emp_codigo),
constraint fk_contratos_formas_pagamentos foreign key (fpc_con_codigo, fpc_con_ent_codigo, fpc_con_ent_emp_codigo) references contratos (con_codigo, con_ent_codigo, con_ent_emp_codigo));

create table agendamentos(
  agd_codigo             integer      ,
  agd_emp_codigo         integer      ,
  agd_ent_codigo         integer      ,
  agd_ent_emp_codigo     integer      ,
  agd_fun_ent_codigo     integer      ,
  agd_fun_ent_emp_codigo integer      ,
  agd_pro_codigo         integer      ,
  agd_pro_emp_codigo     integer      ,
  agd_sala_codigo        integer      ,
  agd_sala_emp_codigo    integer      ,
  agd_data               date         ,
  agd_hora               varchar(5)   ,
  agd_qtde_horas         numeric(15,4),
  agd_situacao           integer      ,
constraint pk_agendamentos primary key (agd_codigo, agd_emp_codigo),
constraint fk_entidades_agendamentos foreign key (agd_ent_codigo, agd_ent_emp_codigo) references entidades (ent_codigo, ent_emp_codigo),
constraint fk_funcionarios_agendamentos foreign key (agd_fun_ent_codigo, agd_fun_ent_emp_codigo) references entidades (ent_codigo, ent_emp_codigo),
constraint fk_produtos_agendamentos foreign key (agd_pro_codigo, agd_pro_emp_codigo) references produtos (pro_codigo, pro_emp_codigo), 
constraint fk_salas_agendamentos foreign key (agd_sala_codigo, agd_sala_emp_codigo) references salas (sala_codigo, sala_emp_codigo));

delimiter $
CREATE TRIGGER tg_agendamentos_ins BEFORE INSERT
ON agendamentos
FOR EACH ROW  
BEGIN  
  IF (NEW.agd_ent_codigo = 0) THEN
    set NEW.agd_ent_codigo     = NULL;  
    set NEW.agd_ent_emp_codigo = NULL;
  END IF;
  
  IF (NEW.agd_fun_ent_codigo = 0) THEN
    set NEW.agd_fun_ent_codigo     = NULL;  
    set NEW.agd_fun_ent_emp_codigo = NULL;
  END IF;
  
  IF (NEW.agd_pro_codigo = 0) THEN
    set NEW.agd_pro_codigo     = NULL;  
    set NEW.agd_pro_emp_codigo = NULL;
  END IF;
  
  IF (NEW.agd_sala_codigo = 0) THEN
    set NEW.agd_sala_codigo     = NULL;  
    set NEW.agd_sala_emp_codigo = NULL;
  END IF;   
END$

CREATE TRIGGER tg_agendamentos_upd BEFORE UPDATE
ON agendamentos
FOR EACH ROW  
BEGIN  
  IF (NEW.agd_ent_codigo = 0) THEN
    set NEW.agd_ent_codigo     = NULL;  
    set NEW.agd_ent_emp_codigo = NULL;
  END IF;
  
  IF (NEW.agd_fun_ent_codigo = 0) THEN
    set NEW.agd_fun_ent_codigo     = NULL;  
    set NEW.agd_fun_ent_emp_codigo = NULL;
  END IF;
  
  IF (NEW.agd_pro_codigo = 0) THEN
    set NEW.agd_pro_codigo     = NULL;  
    set NEW.agd_pro_emp_codigo = NULL;
  END IF;
  
  IF (NEW.agd_sala_codigo = 0) THEN
    set NEW.agd_sala_codigo     = NULL;  
    set NEW.agd_sala_emp_codigo = NULL;
  END IF;   
END$
delimiter ;

alter table agendamentos add agd_observacoes varchar(200);

create index agendamentos_idx1 on agendamentos (agd_data);

alter table agendamentos add agd_hora_final varchar(5);

create table historicos_agendamentos (
  hagd_codigo         integer not null,
  hagd_emp_codigo     integer not null,
  hagd_agd_codigo     integer         ,
  hagd_agd_emp_codigo integer         ,
  hagd_status         integer         ,
constraint pk_historicos_agendamentos primary key (hagd_codigo, hagd_emp_codigo),
constraint fk_historicos_agendamentos foreign key (hagd_agd_codigo, hagd_agd_emp_codigo) references agendamentos(agd_codigo, agd_emp_codigo));

alter table contratos add con_flag_cr integer default 0;

create table contas_a_receber(
  cr_codigo         integer,
  cr_emp_codigo     integer,
  cr_ent_codigo     integer,
  cr_ent_emp_codigo integer,
  cr_con_codigo     integer,
  cr_data           date   ,
  cr_observacao     blob   ,
  cr_situacao       integer,
constraint pk_contas_a_receber primary key (cr_codigo, cr_emp_codigo),
constraint fk_entidades_cr foreign key (cr_ent_codigo, cr_ent_emp_codigo) references entidades (ent_codigo, ent_emp_codigo),
constraint fk_contratos_cr foreign key (cr_con_codigo) references contratos (con_codigo));

create table historico_cr(
  hist_cr_codigo     integer      ,
  hist_cr_emp_codigo integer      ,
  hist_cr_sequencia  integer      ,
  hist_cr_forma      integer      ,
  hist_cr_valor      numeric(15,4),
  hist_nsu           varchar(50)  ,
  hist_cartao        varchar(4)   ,
  hist_data_pgto     date         ,
constraint pk_historico_cr primary key (hist_cr_codigo, hist_cr_emp_codigo, hist_cr_sequencia));

alter table contas_a_receber add cr_valor numeric(15,4);