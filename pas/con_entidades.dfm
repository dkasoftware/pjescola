inherited FrmAux_Padrao1: TFrmAux_Padrao1
  Caption = 'Consulta de Alunos e Entidades'
  ClientHeight = 461
  ClientWidth = 873
  ExplicitWidth = 879
  ExplicitHeight = 490
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 873
    Height = 442
    ExplicitTop = -6
    ExplicitWidth = 873
    ExplicitHeight = 442
    inherited pnlButtons: TPanel
      Top = 406
      Width = 867
      inherited btnSair: TBitBtn
        Left = 777
      end
    end
    object GBFiltros: TGroupBox
      Left = 3
      Top = 3
      Width = 867
      Height = 86
      Align = alTop
      Caption = 'Filtros'
      TabOrder = 1
      object Label4: TLabel
        Left = 17
        Top = 22
        Width = 39
        Height = 16
        Alignment = taRightJustify
        Caption = 'Cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 23
        Top = 48
        Width = 33
        Height = 16
        Alignment = taRightJustify
        Caption = 'Inicial'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 196
        Top = 48
        Width = 27
        Height = 16
        Alignment = taRightJustify
        Caption = 'Final'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cmb_ent_codigo: TComboBox
        Left = 62
        Top = 18
        Width = 507
        Height = 24
        Style = csDropDownList
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object ed_data_inicial: TDateTimePicker
        Left = 62
        Top = 45
        Width = 120
        Height = 24
        Date = 43852.284125532410000000
        Time = 43852.284125532410000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object ed_data_final: TDateTimePicker
        Left = 229
        Top = 45
        Width = 120
        Height = 24
        Date = 43852.284125532410000000
        Time = 43852.284125532410000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
    end
    object DBGrid1: TDBGrid
      Left = 3
      Top = 89
      Width = 867
      Height = 317
      Align = alClient
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -13
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
  end
  inherited SB: TStatusBar
    Top = 442
    Width = 873
  end
  object FDQuery1: TFDQuery
    Left = 432
    Top = 112
  end
end
