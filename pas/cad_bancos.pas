unit cad_bancos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Vcl.Grids, Vcl.DBGrids, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, cls_utils;

type
  TFrmCad_Bancos = class(TFrmAux_Padrao)
    PC: TPageControl;
    tsConsulta: TTabSheet;
    tsCadastro: TTabSheet;
    dbgBancos: TDBGrid;
    CDSBancos: TClientDataSet;
    PROBancos: TDataSetProvider;
    DSBancos: TDataSource;
    FDQBancos: TFDQuery;
    FDQBancosban_codigo: TIntegerField;
    FDQBancosban_emp_codigo: TIntegerField;
    FDQBancosban_nome: TStringField;
    FDQBancosban_numero: TStringField;
    FDQBancosban_digito: TStringField;
    CDSBancosban_codigo: TIntegerField;
    CDSBancosban_emp_codigo: TIntegerField;
    CDSBancosban_nome: TStringField;
    CDSBancosban_numero: TStringField;
    CDSBancosban_digito: TStringField;
    ed_consulta: TEdit;
    gb_ent_codigo: TGroupBox;
    ed_ban_codigo: TEdit;
    cmb_ban_emp_codigo: TComboBox;
    ed_ban_nome: TEdit;
    ed_ban_numero: TEdit;
    Label1: TLabel;
    ed_ban_digito: TEdit;
    procedure ed_consultaChange(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure dbgBancosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    v_operacao : TOperacaoQuery;

    procedure proc_limpa_campos;
  public
    { Public declarations }
  end;

var
  FrmCad_Bancos: TFrmCad_Bancos;

implementation

{$R *.dfm}

uses dm_conexao, cls_bancos, cls_tabelacodigos, cls_empresas, menu;

procedure TFrmCad_Bancos.btnCancelarClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos;

  tsCadastro.TabVisible := False;
  tsConsulta.TabVisible := True;

  ed_consulta.SetFocus;

  v_operacao := statNavegando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSBancos);

  btnExcluir.Enabled := False;
end;

procedure TFrmCad_Bancos.btnEditarClick(Sender: TObject);
var c_empresa : TEmpresas;
begin
  inherited;
  ed_ban_codigo.Text := CDSBancosban_codigo.AsString;

  try
    c_empresa := TEmpresas.Create;
    cmb_ban_emp_codigo.ItemIndex := cmb_ban_emp_codigo.Items.IndexOf(c_empresa.func_retorna_razao_combo(CDSBancosban_emp_codigo.AsInteger));
  finally
    FreeAndNil(c_empresa);
  end;

  ed_ban_nome.Text   := CDSBancosban_nome.AsString;
  ed_ban_numero.Text := CDSBancosban_numero.AsString;
  ed_ban_digito.Text := CDSBancosban_digito.AsString;

  v_operacao := statEditando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSBancos);

  tsConsulta.TabVisible := False;
  tsCadastro.TabVisible := True;

  ed_ban_numero.SetFocus;
end;

procedure TFrmCad_Bancos.btnGravarClick(Sender: TObject);
var c_bancos : TBancos;
begin
  inherited;
  c_bancos := TBancos.Create;
  try
    try
      Conexao.FDConexao.StartTransaction;

      c_bancos.ban_codigo     := StrToInt(ed_ban_codigo.Text);
      c_bancos.ban_emp_codigo := TEmpresasCombo(cmb_ban_emp_codigo.Items.Objects[cmb_ban_emp_codigo.ItemIndex]).emp_codigo;
      c_bancos.ban_nome       := ed_ban_nome.Text;
      c_bancos.ban_numero     := ed_ban_numero.Text;
      c_bancos.ban_digito     := ed_ban_digito.Text;

      c_bancos.proc_gravar(c_bancos.ban_codigo, c_bancos.ban_emp_codigo);

      Conexao.FDConexao.Commit;
    except
      on E : Exception do begin
        Conexao.FDConexao.Rollback;
        Application.MessageBox(PChar(E.Message), PChar('Aten��o'), MB_OK + MB_ICONINFORMATION);
      end;
    end;
  finally
    FreeAndNil(c_bancos);
  end;

  ed_consultaChange(Self);
  CDSBancos.Locate('ban_codigo', ed_ban_codigo.Text, [loCaseInsensitive]);
  btnCancelar.Click;
end;

procedure TFrmCad_Bancos.btnNovoClick(Sender: TObject);
var c_tabela : TTabelaCodigos;
begin
  inherited;
  c_tabela := TTabelaCodigos.Create;
  try
    try
      Conexao.FDConexao.StartTransaction;

      ed_ban_codigo.Text := c_tabela.func_gerar_codigo('bancos', IntToStr(TEmpresasCombo(cmb_ban_emp_codigo.Items.Objects[cmb_ban_emp_codigo.ItemIndex]).emp_codigo)).ToString;
      cmb_ban_emp_codigo.ItemIndex := cmb_ban_emp_codigo.Items.IndexOf(frmMenu.razao_empresa);

      Conexao.FDConexao.Commit;
    except
      on E : Exception do begin
        Conexao.FDConexao.Rollback;
        Application.MessageBox(PChar(E.Message), PChar(Self.Caption), MB_OK + MB_ICONERROR);
        Exit;
      end;
    end;

    tsConsulta.TabVisible := False;
    tsCadastro.TabVisible := True;

    proc_limpa_campos;

    v_operacao := statInserindo;
    proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSBancos);

    ed_ban_numero.SetFocus;
  finally
    FreeAndNil(c_tabela)
  end;
end;

procedure TFrmCad_Bancos.btnSairClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TFrmCad_Bancos.dbgBancosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if (not Odd(CDSBancos.RecNo)) then begin
    dbgBancos.Canvas.Brush.Color := clSilver;
  end else begin
    dbgBancos.Canvas.Brush.Color := clWindow;
  end;

  if (gdSelected in State) then begin
    dbgBancos.Canvas.Brush.Color := $00FFA74F;
  end;

  dbgBancos.Canvas.FillRect(Rect);
  dbgBancos.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TFrmCad_Bancos.ed_consultaChange(Sender: TObject);
var c_bancos : TBancos;
begin
  inherited;
  c_bancos := TBancos.Create;
  try
    c_bancos.proc_consultar(FDQBancos, CDSBancos, ed_consulta.Text);
  finally
    FreeAndNil(c_bancos);
  end;
end;

procedure TFrmCad_Bancos.FormActivate(Sender: TObject);
var c_empresas  : TEmpresas;
begin
  inherited;
  try
    c_empresas  := TEmpresas.Create;
    c_empresas.proc_carregar_combo(cmb_ban_emp_codigo);
  finally
    FreeAndNil(c_empresas);
  end;

  ed_consulta.Clear;

  btnCancelar.Click;
end;

procedure TFrmCad_Bancos.proc_limpa_campos;
begin
  ed_ban_nome.Clear;
  ed_ban_numero.Clear;
  ed_ban_digito.Clear;
end;

end.
