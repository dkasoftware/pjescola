unit cls_historico_contratos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type
  THistorico_Contratos = Class(TObject)

  private
    { Private declarations }
    f_sequencia          : Integer;
    f_con_codigo         : Integer;
    f_con_ent_codigo     : Integer;
    f_con_ent_emp_codigo : Integer;
    f_inclusao           : TDateTime;
    f_horas              : Double;
    f_notas              : String;
    f_tipo               : Integer; {0 - Inclus�o | 1 -  Renova��o | 2 - D�bito}
  public
    { Public declarations }
    procedure proc_gravar(con_codigo, con_ent_codigo, con_ent_emp_codigo : Integer);
    function func_recuperar(con_codigo, con_ent_codigo, con_ent_emp_codigo : Integer) : Boolean;
  published
    { Published declarations }
    property sequencia          : Integer   read f_sequencia          write f_sequencia;
    property con_codigo         : Integer   read f_con_codigo         write f_con_codigo;
    property con_ent_codigo     : Integer   read f_con_ent_codigo     write f_con_ent_codigo;
    property con_ent_emp_codigo : Integer   read f_con_ent_emp_codigo write f_con_ent_emp_codigo;
    property inclusao           : TDateTime read f_inclusao           write f_inclusao;
    property horas              : Double    read f_horas              write f_horas;
    property notas              : String    read f_notas              write f_notas;
    property tipo               : Integer   read f_tipo               write f_tipo;
  end;

implementation

{ THistorico_Contratos }

function THistorico_Contratos.func_recuperar(con_codigo, con_ent_codigo,
  con_ent_emp_codigo: Integer): Boolean;
begin

end;

procedure THistorico_Contratos.proc_gravar(con_codigo, con_ent_codigo,
  con_ent_emp_codigo: Integer);
var qryAux : TFDQuery;
begin
  qryAux := TFDQuery.Create(nil);
  try
    qryAux.SQL.Add(' insert into historico_contratos values( '+
                   '   :p_sequencia         ,                '+
                   '   :p_con_codigo        ,                '+
                   '   :p_con_ent_codigo    ,                '+
                   '   :p_con_ent_emp_codigo,                '+
                   '   :p_inclusao          ,                '+
                   '   :p_horas             ,                '+
                   '   :p_notas             ,                '+
                   '   :p_tipo              )                ');
    qryAux.ParamByName('p_sequencia').Value              := null;
    qryAux.ParamByName('p_con_codigo').AsInteger         := con_codigo;
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := con_ent_codigo;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := con_ent_emp_codigo;
    qryAux.ParamByName('p_inclusao').AsDateTime          := inclusao;
    qryAux.ParamByName('p_horas').AsFloat                := horas;
    qryAux.ParamByName('p_notas').AsString               := notas;
    qryAux.ParamByName('p_tipo').AsInteger               := tipo;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
