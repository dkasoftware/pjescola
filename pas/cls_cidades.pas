unit cls_cidades;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type TCidades = Class(TObject)
  private
    { Private declarations }
    f_cid_codigo   : Integer;
    f_cid_nome     : String;
    f_cid_uf_sigla : String;
    f_cid_cod_ibge : String;
  public
    { Public declarations }
    procedure proc_gravar(PCodigo : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; Pcodigo : Integer; PNome : String);
    procedure proc_carregar_combo(var PComboBox : TComboBox; PUFSigla : String = '');

    function func_recuperar(PCodigo : Integer) : Boolean;
    function func_retorna_nome_combo(PCodigo : Integer) : String;
  published
    { Published declarations }
    property cid_codigo   : Integer read f_cid_codigo   write f_cid_codigo;
    property cid_nome     : String  read f_cid_nome     write f_cid_nome;
    property cid_uf_sigla : String  read f_cid_uf_sigla write f_cid_uf_sigla;
    property cid_cod_ibge : String  read f_cid_cod_ibge write f_cid_cod_ibge;
  end;

type
  TCidadesCombo = TCidades;

implementation

{ TCidades }

function TCidades.func_recuperar(PCodigo: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                           '+
                   ' from cidades c                     '+
                   ' where c.cid_codigo = :p_cid_codigo ');
    qryAux.ParamByName('p_cid_codigo').AsInteger := PCodigo;
    qryAux.Open;

    Result := (not qryAux.Eof);

    cid_codigo   := qryAux.FieldByName('cid_codigo').AsInteger;
    cid_nome     := qryAux.FieldByName('cid_nome').AsString;
    cid_uf_sigla := qryAux.FieldByName('cid_uf_sigla').AsString;
    cid_cod_ibge := qryAux.FieldByName('cid_cod_ibge').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

function TCidades.func_retorna_nome_combo(PCodigo: Integer): String;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select cid_nome                    '+
                   ' from cidades c                     '+
                   ' where c.cid_codigo = :p_cid_codigo ');
    qryAux.ParamByName('p_cid_codigo').AsInteger := PCodigo;
    qryAux.Open;

    Result := qryAux.FieldByName('cid_nome').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TCidades.proc_carregar_combo(var PComboBox: TComboBox; PUFSigla : String = '');
var qryAux    : TFDQuery;
    c_cidades : TCidadesCombo;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                '+
                   ' from cidades c                          '+
                   ' where c.cid_uf_sigla  = :p_cid_uf_sigla '+
                   '    or :p_cid_uf_sigla = ''''            '+
                   ' order by c.cid_uf_sigla, c.cid_nome     ');
    qryAux.ParamByName('p_cid_uf_sigla').AsString := PUFSigla;
    qryAux.Open;

    proc_limpa_combo(PComboBox);
    while (not qryAux.Eof) do begin
      c_cidades := TCidadesCombo.Create;

      c_cidades.cid_codigo := qryAux.FieldByName('cid_codigo').AsInteger;
      c_cidades.cid_nome   := qryAux.FieldByName('cid_nome').AsString;

      PComboBox.Items.AddObject(c_cidades.cid_nome, c_cidades);

      qryAux.Next;
    end;

    PComboBox.ItemIndex := -1;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TCidades.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; Pcodigo: Integer; PNome: String);
begin

end;

procedure TCidades.proc_gravar(PCodigo: Integer);
begin

end;

end.
