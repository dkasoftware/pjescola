unit cls_produtos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type TProdutos = Class(TObject)
  private
    { Private declarations }
    f_pro_codigo     : Integer;
    f_pro_emp_codigo : Integer;
    f_pro_tipo       : Integer;
    f_pro_descricao  : String;
    f_pro_unidade    : Integer;
    f_pro_preco      : Double;
  public
    { Public declarations }
//    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PComboCampos : TComboBox; PConsulta : String);
//    procedure proc_gravar(PCodigo : Integer);
    procedure proc_carregar_combo(var PComboBox : TComboBox);

    function func_recuperar(PCodigo, PEmpCodigo : Integer) : Boolean;
    function func_retorna_nome_combo(PCodigo, PEmpCodigo : Integer) : String;
  published
    { Published declarations }
    property pro_codigo     : Integer read f_pro_codigo     write f_pro_codigo;
    property pro_emp_codigo : Integer read f_pro_emp_codigo write f_pro_emp_codigo;
    property pro_tipo       : Integer read f_pro_tipo       write f_pro_tipo;
    property pro_descricao  : String  read f_pro_descricao  write f_pro_descricao;
    property pro_unidade    : Integer read f_pro_unidade    write f_pro_unidade;
    property pro_preco      : Double  read f_pro_preco      write f_pro_preco;
  end;

type
  TProdutosCombo = TProdutos;

implementation

{ TProdutos }

function TProdutos.func_recuperar(PCodigo, PEmpCodigo: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from produtos p                            '+
                   ' where p.pro_codigo     = :p_pro_codigo     '+
                   '   and p.pro_emp_codigo = :p_pro_emp_codigo ');
    qryAux.ParamByName('p_pro_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_pro_emp_codigo').AsInteger := PEmpCodigo;
    qryAux.Open;

    Result := (not qryAux.Eof);

    pro_codigo     := qryAux.FieldByName('pro_codigo').AsInteger;
    pro_emp_codigo := qryAux.FieldByName('pro_emp_codigo').AsInteger;
    pro_tipo       := qryAux.FieldByName('pro_tipo').AsInteger;
    pro_descricao  := qryAux.FieldByName('pro_descricao').AsString;
    pro_unidade    := qryAux.FieldByName('pro_unidade').AsInteger;
    pro_preco      := qryAux.FieldByName('pro_preco').AsFloat;
  finally
    FreeAndNil(qryAux);
  end;
end;

function TProdutos.func_retorna_nome_combo(PCodigo, PEmpCodigo: Integer): String;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select pro_descricao                     '+
                   ' from produtos                            '+
                   ' where pro_codigo     = :p_pro_codigo     '+
                   '   and pro_emp_codigo = :p_pro_emp_codigo ');
    qryAux.ParamByName('p_pro_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_pro_emp_codigo').AsInteger := PEmpCodigo;
    qryAux.Open;

    Result := qryAux.FieldByName('pro_descricao').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TProdutos.proc_carregar_combo(var PComboBox: TComboBox);
var qryAux     : TFDQuery;
    c_produtos : TProdutosCombo;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                 '+
                   ' from produtos p          '+
                   ' order by p.pro_descricao ');
    qryAux.Open;

    proc_limpa_combo(PComboBox);
    PComboBox.Items.Add('');
    while (not qryAux.Eof) do begin
      c_produtos := TProdutosCombo.Create;

      c_produtos.pro_codigo     := qryAux.FieldByName('pro_codigo').AsInteger;
      c_produtos.pro_emp_codigo := qryAux.FieldByName('pro_emp_codigo').AsInteger;
      c_produtos.pro_descricao  := qryAux.FieldByName('pro_descricao').AsString;

      PComboBox.Items.AddObject(c_produtos.pro_descricao, c_produtos);

      qryAux.Next;
    end;

    PComboBox.ItemIndex := 0;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
