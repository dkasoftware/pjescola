inherited FrmCad_Usuarios: TFrmCad_Usuarios
  Caption = 'Cadastro de Usu'#225'rios'
  ClientHeight = 472
  ClientWidth = 568
  OnActivate = FormActivate
  ExplicitWidth = 574
  ExplicitHeight = 501
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 568
    Height = 453
    ExplicitWidth = 568
    ExplicitHeight = 453
    object PC: TPageControl [0]
      Left = 3
      Top = 3
      Width = 562
      Height = 414
      ActivePage = tsCadastro
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object tsConsulta: TTabSheet
        Caption = 'Consulta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        object pnlConsulta: TPanel
          Left = 0
          Top = 0
          Width = 554
          Height = 383
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Padding.Left = 3
          Padding.Top = 3
          Padding.Right = 3
          Padding.Bottom = 3
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          object Panel1: TPanel
            Left = 3
            Top = 3
            Width = 548
            Height = 34
            Align = alTop
            BevelInner = bvLowered
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object Label1: TLabel
              Left = 9
              Top = 9
              Width = 38
              Height = 16
              Alignment = taRightJustify
              Caption = 'Buscar'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label2: TLabel
              Left = 423
              Top = 10
              Width = 36
              Height = 16
              Alignment = taRightJustify
              Caption = 'Status'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object cmbCampos: TComboBox
              Left = 54
              Top = 5
              Width = 86
              Height = 24
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'C'#243'digo'
              Items.Strings = (
                'C'#243'digo'
                'Nome'
                'Login')
            end
            object ed_consulta: TEdit
              Left = 142
              Top = 5
              Width = 274
              Height = 24
              CharCase = ecUpperCase
              TabOrder = 1
              OnChange = ed_consultaChange
            end
            object cmbConStatus: TComboBox
              Left = 464
              Top = 6
              Width = 80
              Height = 24
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 2
              Text = 'Ativos'
              OnChange = ed_consultaChange
              Items.Strings = (
                'Ativos'
                'Inativos'
                'Todos')
            end
          end
          object dbgUsuarios: TDBGrid
            Left = 3
            Top = 40
            Width = 548
            Height = 340
            Align = alBottom
            DataSource = DSUsuarios
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentFont = False
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'usu_codigo'
                Title.Alignment = taCenter
                Title.Caption = 'C'#243'd.'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'usu_nome'
                Width = 250
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'usu_login'
                Title.Alignment = taCenter
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'usu_nivel'
                Width = 100
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'usu_status'
                Title.Alignment = taCenter
                Title.Caption = 'S'
                Width = 20
                Visible = True
              end>
          end
        end
      end
      object tsCadastro: TTabSheet
        Caption = 'Cadastro'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object pnlCadastro: TPanel
          Left = 0
          Top = 0
          Width = 554
          Height = 383
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object gbCodigo: TGroupBox
            Left = 5
            Top = -2
            Width = 135
            Height = 50
            Caption = 'C'#243'digo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object ed_usu_codigo: TEdit
              Left = 6
              Top = 18
              Width = 121
              Height = 24
              TabStop = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              NumbersOnly = True
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
            end
          end
          object gbInformacoes: TGroupBox
            Left = 5
            Top = 47
            Width = 544
            Height = 85
            Caption = 'Informa'#231#245'es'
            TabOrder = 1
            object Label3: TLabel
              Left = 22
              Top = 24
              Width = 33
              Height = 16
              Alignment = taRightJustify
              Caption = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label4: TLabel
              Left = 28
              Top = 52
              Width = 27
              Height = 16
              Alignment = taRightJustify
              Caption = 'N'#237'vel'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object cmb_usu_nivel: TComboBox
              Left = 61
              Top = 48
              Width = 200
              Height = 24
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 1
              Text = 'Normal'
              Items.Strings = (
                'Normal'
                'Financeiro'
                'Diretoria'
                'Administrador')
            end
            object ed_usu_nome: TEdit
              Left = 61
              Top = 20
              Width = 473
              Height = 24
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              MaxLength = 60
              ParentFont = False
              TabOrder = 0
            end
          end
          object gbSenha: TGroupBox
            Left = 5
            Top = 131
            Width = 544
            Height = 249
            Caption = 'Acesso'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            object Label5: TLabel
              Left = 25
              Top = 24
              Width = 30
              Height = 16
              Alignment = taRightJustify
              Caption = 'Login'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label6: TLabel
              Left = 19
              Top = 51
              Width = 36
              Height = 16
              Alignment = taRightJustify
              Caption = 'Senha'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label7: TLabel
              Left = 19
              Top = 114
              Width = 463
              Height = 16
              Caption = 
                'A senha deve conter no m'#237'nimo 8 caract'#233'res e no m'#225'ximo 10, poden' +
                'do '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label8: TLabel
              Left = 19
              Top = 130
              Width = 359
              Height = 16
              Caption = 'ou n'#227'o, variar entre Mai'#250'sculas, Min'#250'sculas e n'#250'meros.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label9: TLabel
              Left = 19
              Top = 78
              Width = 36
              Height = 16
              Alignment = taRightJustify
              Caption = 'Repita'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label10: TLabel
              Left = 292
              Top = 51
              Width = 36
              Height = 16
              Alignment = taRightJustify
              Caption = 'Repita'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object ed_usu_login: TEdit
              Left = 61
              Top = 20
              Width = 200
              Height = 24
              CharCase = ecUpperCase
              MaxLength = 10
              TabOrder = 0
            end
            object ed_usu_senha: TEdit
              Left = 61
              Top = 47
              Width = 200
              Height = 24
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              MaxLength = 10
              ParentFont = False
              PasswordChar = '*'
              TabOrder = 1
              OnKeyPress = ed_usu_senhaKeyPress
            end
            object ed_repetir_senha: TEdit
              Left = 61
              Top = 74
              Width = 200
              Height = 24
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              MaxLength = 10
              ParentFont = False
              PasswordChar = '*'
              TabOrder = 2
              OnExit = ed_repetir_senhaExit
            end
            object rg_usu_status: TRadioGroup
              Left = 19
              Top = 176
              Width = 94
              Height = 59
              Caption = 'Status'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Items.Strings = (
                'Ativo'
                'Inativo')
              ParentFont = False
              TabOrder = 3
            end
            object ed_nova_senha: TEdit
              Left = 334
              Top = 47
              Width = 200
              Height = 24
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              MaxLength = 10
              ParentFont = False
              PasswordChar = '*'
              TabOrder = 4
            end
            object btnRestarSenha: TBitBtn
              Left = 334
              Top = 74
              Width = 200
              Height = 24
              Caption = 'Resetar Senha'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6
                A4C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3
                B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFC
                FBC2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3FCFCFCFCFCFCFCFCFCFCFCFCFC
                FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCB3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FEFCFBFEFCFBFEFCFBFEFCFBD8EBD6018A02018A02D8EBD6FEFCFBFEFC
                FBC2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3FCFCFCFCFCFCFCFCFCFCFCFCE0
                E0E0959595959595E0E0E0FCFCFCFCFCFCB3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FEFBF7FEFBF7018A02D8EAD2018A02D8EAD2D8EAD2018A02FEFBF7FEFB
                F7C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3FAFAFAFAFAFA959595DEDEDE95
                9595DEDEDEDEDEDE959595FAFAFAFAFAFAB3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FEF9F4FEF9F4018A02018A02D8E8D0FEF9F4FEF9F4D8E8D0FEF9F4FEF9
                F4C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F9F9F9F9F9F9959595959595DC
                DCDCF9F9F9F9F9F9DCDCDCF9F9F9F9F9F9B3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FEF7F0FEF7F0018A02018A02018A02FEF7F0FEF7F0FEF7F0FEF7F0FEF7
                F0C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F7F7F7F7F7F795959595959595
                9595F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7B3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FEF5ECFEF5ECFEF5ECFEF5ECFEF5EC018A02018A02018A02FEF5ECFEF5
                ECC2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F5F5F5F5F5F5F5F5F5F5F5F5F5
                F5F5959595959595959595F5F5F5F5F5F5B3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FEF3E9FEF3E9D8E3C7FEF3E9FEF3E9D8E3C7018A02018A02FEF3E9FEF3
                E9C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F3F3F3F3F3F3D5D5D5F3F3F3F3
                F3F3D5D5D5959595959595F3F3F3F3F3F3B3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FFF1E5FFF1E5018A02D9E2C3D9E2C3018A02D9E2C3018A02FFF1E5FFF1
                E5C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F2F2F2F2F2F2959595D2D2D2D2
                D2D2959595D2D2D2959595F2F2F2F2F2F2B3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FFF0E2FFF0E2D9E1C1018A02018A02D9E1C1DDCFC2DDCFC2DDCFC2DDCF
                C2C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F0F0F0F0F0F0D1D1D195959595
                9595D1D1D1CFCFCFCFCFCFCFCFCFCFCFCFB3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FFEEDEFFEEDEFFEEDEFFEEDEFFEEDEFFEEDEC5B5A9C3B4A8C2B3A7C1B2
                A6C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3EEEEEEEEEEEEEEEEEEEEEEEEEE
                EEEEEEEEEEB7B7B7B5B5B5B4B4B4B3B3B3B3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FFECDAFFECDAFFECDAFFECDAFFECDAFFECDAB0A296B0A296B0A296B0A2
                96C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3ECECECECECECECECECECECECEC
                ECECECECECA3A3A3A3A3A3A3A3A3A3A3A3B3B3B3FF00FFFF00FFFF00FFFF00FF
                C2A6A4FFEAD7FFEAD7FFEAD7FFEAD7FFEAD7C9B9ACFBF8F4FBF8F4E6DAD9C2A6
                A4FF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3EBEBEBEBEBEBEBEBEBEBEBEBEB
                EBEBBABABAF7F7F7F7F7F7DFDFDFB3B3B3FF00FFFF00FFFF00FFFF00FFFF00FF
                C2A6A4FFE8D3FFE8D3FFE8D3FFE8D3FFE8D3C9B9ACFBF8F4DFCEC7C2A6A4FF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3E9E9E9E9E9E9E9E9E9E9E9E9E9
                E9E9BABABAF7F7F7D3D3D3B3B3B3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                C2A6A4FFE6D0FFE6D0FFE6D0FFE6D0FFE6D0C9B9ACDFCEC7C2A6A4FF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3E7E7E7E7E7E7E7E7E7E7E7E7E7
                E7E7BABABAD3D3D3B3B3B3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4FF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3
                B3B3B3B3B3B3B3B3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
              NumGlyphs = 2
              ParentFont = False
              TabOrder = 5
            end
          end
        end
      end
    end
    inherited pnlButtons: TPanel
      Top = 417
      Width = 562
      TabOrder = 1
      ExplicitTop = 417
      ExplicitWidth = 562
      inherited btnCancelar: TBitBtn
        OnClick = btnCancelarClick
      end
      inherited btnEditar: TBitBtn
        OnClick = btnEditarClick
      end
      inherited btnGravar: TBitBtn
        OnClick = btnGravarClick
      end
      inherited btnNovo: TBitBtn
        OnClick = btnNovoClick
      end
      inherited btnSair: TBitBtn
        Left = 472
        ExplicitLeft = 472
      end
    end
  end
  inherited SB: TStatusBar
    Top = 453
    Width = 568
    ExplicitTop = 453
    ExplicitWidth = 568
  end
  object FDQUsuarios: TFDQuery
    Active = True
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select *'
      'from usuarios')
    Left = 514
    Top = 89
    object FDQUsuariosusu_codigo: TIntegerField
      FieldName = 'usu_codigo'
      Origin = 'usu_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQUsuariosusu_nome: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'usu_nome'
      Origin = 'usu_nome'
      Size = 60
    end
    object FDQUsuariosusu_login: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'usu_login'
      Origin = 'usu_login'
    end
    object FDQUsuariosusu_senha: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'usu_senha'
      Origin = 'usu_senha'
    end
    object FDQUsuariosusu_nivel: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_nivel'
      Origin = 'usu_nivel'
    end
    object FDQUsuariosusu_status: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'usu_status'
      Origin = 'usu_status'
    end
  end
  object CDSUsuarios: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'PROUsuarios'
    Left = 514
    Top = 144
    object CDSUsuariosusu_codigo: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'usu_codigo'
      Origin = 'usu_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CDSUsuariosusu_nome: TStringField
      DisplayLabel = 'Nome'
      FieldName = 'usu_nome'
      Origin = 'usu_nome'
      Size = 60
    end
    object CDSUsuariosusu_login: TStringField
      DisplayLabel = 'Login'
      FieldName = 'usu_login'
      Origin = 'usu_login'
    end
    object CDSUsuariosusu_senha: TStringField
      DisplayLabel = 'Senha'
      FieldName = 'usu_senha'
      Origin = 'usu_senha'
    end
    object CDSUsuariosusu_nivel: TIntegerField
      DisplayLabel = 'N'#237'vel'
      FieldName = 'usu_nivel'
      Origin = 'usu_nivel'
    end
    object CDSUsuariosusu_status: TIntegerField
      DisplayLabel = 'Status'
      FieldName = 'usu_status'
      Origin = 'usu_status'
    end
  end
  object PROUsuarios: TDataSetProvider
    DataSet = FDQUsuarios
    Left = 514
    Top = 117
  end
  object DSUsuarios: TDataSource
    DataSet = CDSUsuarios
    Left = 514
    Top = 172
  end
end
