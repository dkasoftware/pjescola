unit cls_formas_pagamentos_contratos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Classes, DB;

type TFormasPagamentosContratos = Class(TObject)
  private
    { Private declarations }
    f_fpc_codigo             : Integer;
    f_fpc_con_codigo         : Integer;
    f_fpc_con_ent_codigo     : Integer;
    f_fpc_con_ent_emp_codigo : Integer;
  public
    { Public declarations }
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PCodigo, PConCodigo, PConEntCodigo, PConEntEmpCodigo : Integer);
    procedure proc_gravar(PCodigo, PConCodigo, PConEntCodigo, PConEntEmpCodigo : Integer);
    procedure proc_limpar_gravacao(PConCodigo, PConEntCodigo, PConEntEmpCodigo : Integer);

    function func_recuperar(PCodigo, PConCodigo, PConEntCodigo, PConEntEmpCodigo : Integer) : Boolean;
  published
    { Published declarations }
    property fpc_codigo             : Integer read f_fpc_codigo             write f_fpc_codigo;
    property fpc_con_codigo         : Integer read f_fpc_con_codigo         write f_fpc_con_codigo;
    property fpc_con_ent_codigo     : Integer read f_fpc_con_ent_codigo     write f_fpc_con_ent_codigo;
    property fpc_con_ent_emp_codigo : Integer read f_fpc_con_ent_emp_codigo write f_fpc_con_ent_emp_codigo;
  end;

implementation

{ TFormasPagamentosContratos }

function TFormasPagamentosContratos.func_recuperar(PCodigo, PConCodigo,
  PConEntCodigo, PConEntEmpCodigo: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                                 '+
                   ' from formas_pagamentos_contratos                         '+
                   ' where fpc_codigo             = :p_fpc_codigo             '+
                   '   and fpc_con_codigo         = :p_fpc_con_codigo         '+
                   '   and fpc_con_ent_codigo     = :p_fpc_con_ent_codigo     '+
                   '   and fpc_con_ent_emp_codigo = :p_fpc_con_ent_emp_codigo ');
    qryAux.ParamByName('p_fpc_codigo').AsInteger             := PCodigo;
    qryAux.ParamByName('p_fpc_con_codigo').AsInteger         := PConCodigo;
    qryAux.ParamByName('p_fpc_con_ent_codigo').AsInteger     := PConEntCodigo;
    qryAux.ParamByName('p_fpc_con_ent_emp_codigo').AsInteger := PConEntEmpCodigo;
    qryAux.Open;

    Result := (not qryAux.Eof);

    fpc_codigo             := qryAux.FieldByName('fpc_codigo').AsInteger;
    fpc_con_codigo         := qryAux.FieldByName('fpc_con_codigo').AsInteger;
    fpc_con_ent_codigo     := qryAux.FieldByName('fpc_con_ent_codigo').AsInteger;
    fpc_con_ent_emp_codigo := qryAux.FieldByName('fpc_con_ent_emp_codigo').AsInteger;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TFormasPagamentosContratos.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PCodigo, PConCodigo, PConEntCodigo,
  PConEntEmpCodigo: Integer);
begin
  PQuery.Close;
  PClient.Close;

  PQuery.Connection := Conexao.FDConexao;

  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                                                 '+
                 ' from formas_pagamentos_contratos                         '+
                 ' where (fpc_codigo            = :p_fpc_codigo             '+
                 '    or  :p_fpc_codigo         = 0)                        '+
                 '   and (fpc_con_codigo        = :p_fpc_con_codigo         '+
                 '    or  :p_fpc_con_codigo     = 0)                        '+
                 '   and fpc_con_ent_codigo     = :p_fpc_con_ent_codigo     '+
                 '   and fpc_con_ent_emp_codigo = :p_fpc_con_ent_emp_codigo ');
  PQuery.ParamByName('p_fpc_codigo').AsInteger             := PCodigo;
  PQuery.ParamByName('p_fpc_con_codigo').AsInteger         := PConCodigo;
  PQuery.ParamByName('p_fpc_con_ent_codigo').AsInteger     := PConEntCodigo;
  PQuery.ParamByName('p_fpc_con_ent_emp_codigo').AsInteger := PConEntEmpCodigo;
  PQuery.Open;
  PClient.Open;
end;

procedure TFormasPagamentosContratos.proc_gravar(PCodigo, PConCodigo,
  PConEntCodigo, PConEntEmpCodigo: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' insert into formas_pagamentos_contratos ( '+
                   '   fpc_codigo            ,                 '+
                   '   fpc_con_codigo        ,                 '+
                   '   fpc_con_ent_codigo    ,                 '+
                   '   fpc_con_ent_emp_codigo                  '+
                   '  ) values (                               '+
                   '   :p_fpc_codigo            ,              '+
                   '   :p_fpc_con_codigo        ,              '+
                   '   :p_fpc_con_ent_codigo    ,              '+
                   '   :p_fpc_con_ent_emp_codigo)              ');
    qryAux.ParamByName('p_fpc_codigo').AsInteger             := PCodigo;
    qryAux.ParamByName('p_fpc_con_codigo').AsInteger         := PConCodigo;
    qryAux.ParamByName('p_fpc_con_ent_codigo').AsInteger     := PConEntCodigo;
    qryAux.ParamByName('p_fpc_con_ent_emp_codigo').AsInteger := PConEntEmpCodigo;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;


procedure TFormasPagamentosContratos.proc_limpar_gravacao(PConCodigo,
  PConEntCodigo, PConEntEmpCodigo: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' delete from formas_pagamentos_contratos                  '+
                   ' where fpc_con_codigo         = :p_fpc_con_codigo         '+
                   '   and fpc_con_ent_codigo     = :p_fpc_con_ent_codigo     '+
                   '   and fpc_con_ent_emp_codigo = :p_fpc_con_ent_emp_codigo ');
    qryAux.ParamByName('p_fpc_con_codigo').AsInteger         := PConCodigo;
    qryAux.ParamByName('p_fpc_con_ent_codigo').AsInteger     := PConEntCodigo;
    qryAux.ParamByName('p_fpc_con_ent_emp_codigo').AsInteger := PConEntEmpCodigo;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
