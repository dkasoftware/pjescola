unit cls_bancos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type
  TBancos = Class(TObject)

  private
    { Private declarations }
    f_ban_codigo     : Integer;
    f_ban_emp_codigo : Integer;
    f_ban_nome       : String;
    f_ban_numero     : String;
    f_ban_digito     : String;
  public
    { Public declarations }
    procedure proc_gravar(PBanCodigo, PEmpresa : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PNome : String);
    procedure proc_carregar_combo(var PComboBox : TComboBox);

    function func_recuperar(PBanCodigo, PEmpresa : Integer) : Boolean;
    function func_retorna_razao_combo(PBanCodigo, PEmpresa : Integer) : String;
  published
    { Published declarations }
    property ban_codigo     : Integer read f_ban_codigo     write f_ban_codigo;
    property ban_emp_codigo : Integer read f_ban_emp_codigo write f_ban_emp_codigo;
    property ban_nome       : String  read f_ban_nome       write f_ban_nome;
    property ban_numero     : String  read f_ban_numero     write f_ban_numero;
    property ban_digito     : String  read f_ban_digito     write f_ban_digito;
  end;

type
  TBancoCombo = TBancos;

implementation

{ TBancos }

function TBancos.func_recuperar(PBanCodigo, PEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from bancos b                              '+
                   ' where b.ban_codigo     = :p_ban_codigo     '+
                   '   and b.ban_emp_codigo = :p_ban_emp_codigo ');
    qryAux.ParamByName('p_ban_codigo').AsInteger     := PBanCodigo;
    qryAux.ParamByName('p_ban_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    ban_codigo     := qryAux.FieldByName('ban_codigo').AsInteger;
    ban_emp_codigo := qryAux.FieldByName('ban_emp_codigo').AsInteger;
    ban_nome       := qryAux.FieldByName('ban_nome').AsString;
    ban_numero     := qryAux.FieldByName('ban_numero').AsString;
    ban_digito     := qryAux.FieldByName('ban_digito').AsString;
  finally
    FreeAndNil(qryAux);
  end;

end;

function TBancos.func_retorna_razao_combo(PBanCodigo,
  PEmpresa: Integer): String;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from bancos b                              '+
                   ' where b.ban_codigo     = :p_ban_codigo     '+
                   '   and b.ban_emp_codigo = :p_ban_emp_codigo ');
    qryAux.ParamByName('p_ban_codigo').AsInteger     := PBanCodigo;
    qryAux.ParamByName('p_ban_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := qryAux.FieldByName('ban_numero').AsString + ' - ' + qryAux.FieldByName('ban_nome').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TBancos.proc_carregar_combo(var PComboBox: TComboBox);
var qryAux   : TFDQuery;
    c_bancos : TBancoCombo;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select b.*            '+
                   ' from bancos b         '+
                   ' order by b.ban_numero ');
    qryAux.Open;

    proc_limpa_combo(PComboBox);

    PComboBox.Items.Add('');
    while (not qryAux.Eof) do begin
      c_bancos := TBancoCombo.Create;

      c_bancos.ban_codigo     := qryAux.FieldByName('ban_codigo').AsInteger;
      c_bancos.ban_emp_codigo := qryAux.FieldByName('ban_emp_codigo').AsInteger;
      c_bancos.ban_nome       := qryAux.FieldByName('ban_nome').AsString;
      c_bancos.ban_numero     := qryAux.FieldByName('ban_numero').AsString;

      PComboBox.Items.AddObject(c_bancos.ban_numero + ' - ' + c_bancos.ban_nome, c_bancos);

      qryAux.Next;
    end;

    PComboBox.ItemIndex := 0;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TBancos.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PNome : String);
begin
  PQuery.Connection := Conexao.FDConexao;

  PClient.Close;
  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                                  '+
                 ' from bancos b                             '+
                 ' where b.ban_nome like ''%' + PNome + '%'' ');
  PQuery.Open;
  PClient.Open;
end;

procedure TBancos.proc_gravar(PBanCodigo, PEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from bancos b                              '+
                   ' where b.ban_codigo     = :p_ban_codigo     '+
                   '   and b.ban_emp_codigo = :p_ban_emp_codigo ');
    qryAux.ParamByName('p_ban_codigo').AsInteger     := PBanCodigo;
    qryAux.ParamByName('p_ban_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into bancos(  '+
                     '   ban_codigo    ,    '+
                     '   ban_emp_codigo,    '+
                     '   ban_nome      ,    '+
                     '   ban_numero    ,    '+
                     '   ban_digito         '+
                     ' ) values (           '+
                     '   :p_ban_codigo    , '+
                     '   :p_ban_emp_codigo, '+
                     '   :p_ban_nome      , '+
                     '   :p_ban_numero    , '+
                     '   :p_ban_digito    ) ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update bancos                           '+
                     ' set ban_nome   = :p_ban_nome  ,         '+
                     '     ban_numero = :p_ban_numero,         '+
                     '     ban_digito = :p_ban_digito          '+
                     ' where ban_codigo     = p_ban_codigo     '+
                     '   and ban_emp_codigo = p_ban_emp_codigo ');
    end;

    qryAux.ParamByName('p_ban_codigo').AsInteger     := PBanCodigo;
    qryAux.ParamByName('p_ban_emp_codigo').AsInteger := PEmpresa;
    qryAux.ParamByName('p_ban_nome').AsString        := ban_nome;
    qryAux.ParamByName('p_ban_numero').AsString      := ban_numero;
    qryAux.ParamByName('p_ban_digito').AsString      := ban_digito;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
