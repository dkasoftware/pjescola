inherited FrmCad_ContasBancarias: TFrmCad_ContasBancarias
  Caption = 'Contas Banc'#225'rias'
  ClientHeight = 448
  ClientWidth = 1018
  OnActivate = FormActivate
  ExplicitWidth = 1024
  ExplicitHeight = 477
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 1018
    Height = 429
    ExplicitWidth = 1018
    ExplicitHeight = 429
    inherited pnlButtons: TPanel
      Top = 393
      Width = 1012
      ExplicitTop = 393
      ExplicitWidth = 1012
      inherited btnCancelar: TBitBtn
        OnClick = btnCancelarClick
      end
      inherited btnEditar: TBitBtn
        OnClick = btnEditarClick
      end
      inherited btnGravar: TBitBtn
        OnClick = btnGravarClick
      end
      inherited btnNovo: TBitBtn
        OnClick = btnNovoClick
      end
      inherited btnSair: TBitBtn
        Left = 922
        ExplicitLeft = 922
      end
    end
    object PC: TPageControl
      Left = 3
      Top = 3
      Width = 1012
      Height = 390
      ActivePage = tsCadastro
      Align = alClient
      TabOrder = 1
      object tsConsulta: TTabSheet
        Caption = 'Consulta'
        object TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 359
          Align = alClient
          BevelOuter = bvNone
          Padding.Left = 3
          Padding.Top = 3
          Padding.Right = 3
          Padding.Bottom = 3
          ParentBackground = False
          TabOrder = 0
          object TPanel
            Left = 3
            Top = 3
            Width = 998
            Height = 41
            Align = alTop
            BevelInner = bvLowered
            TabOrder = 0
            object lbl_con_pro_codigo: TLabel
              Left = 9
              Top = 13
              Width = 34
              Height = 16
              Alignment = taRightJustify
              Caption = 'Banco'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object TLabel
              Left = 814
              Top = 13
              Width = 45
              Height = 16
              Alignment = taRightJustify
              Caption = 'N'#250'mero'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object TLabel
              Left = 676
              Top = 13
              Width = 45
              Height = 16
              Alignment = taRightJustify
              Caption = 'Ag'#234'ncia'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object cmb_con_bancos: TComboBox
              Left = 49
              Top = 9
              Width = 607
              Height = 24
              Style = csDropDownList
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnClick = cmb_con_bancosClick
            end
            object ed_con_cb_numero: TEdit
              Left = 865
              Top = 9
              Width = 127
              Height = 24
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              MaxLength = 20
              NumbersOnly = True
              ParentFont = False
              TabOrder = 2
              OnChange = ed_con_cb_numeroChange
            end
            object ed_con_cb_agencia_numero: TEdit
              Left = 727
              Top = 9
              Width = 76
              Height = 24
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              MaxLength = 20
              NumbersOnly = True
              ParentFont = False
              TabOrder = 1
              OnChange = ed_con_cb_agencia_numeroChange
            end
          end
          object dbgContasBancarias: TDBGrid
            Left = 3
            Top = 44
            Width = 998
            Height = 312
            Align = alClient
            DataSource = DSContasBancarias
            Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnDrawColumnCell = dbgContasBancariasDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'cb_ag_numero'
                Title.Caption = 'Ag'#234'ncia'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'cb_ag_digito'
                Title.Caption = 'Dig.'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'cb_numero'
                Title.Caption = 'N'#186'. Conta'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'cb_digito'
                Title.Caption = 'Dig.'
                Width = 200
                Visible = True
              end>
          end
        end
      end
      object tsCadastro: TTabSheet
        Caption = 'Cadastro'
        ImageIndex = 1
        object TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 54
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object TGroupBox
            Left = 133
            Top = 0
            Width = 684
            Height = 54
            Align = alLeft
            Caption = 'Banco'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object cmb_cb_ban_codigo: TComboBox
              Left = 11
              Top = 18
              Width = 660
              Height = 24
              Style = csDropDownList
              TabOrder = 0
            end
          end
          object gb_ent_codigo: TGroupBox
            Left = 0
            Top = 0
            Width = 133
            Height = 54
            Align = alLeft
            Caption = 'C'#243'digo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object ed_cb_codigo: TEdit
              Left = 6
              Top = 18
              Width = 121
              Height = 24
              TabStop = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              NumbersOnly = True
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
            end
          end
          object TGroupBox
            Left = 817
            Top = 0
            Width = 187
            Height = 54
            Align = alClient
            Caption = 'Situa'#231#227'o'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            object cmb_cb_ativo: TComboBox
              Left = 9
              Top = 18
              Width = 170
              Height = 24
              Style = csDropDownList
              TabOrder = 0
              Items.Strings = (
                'INATIVO'
                'ATIVO')
            end
          end
        end
        object TPanel
          Left = 0
          Top = 54
          Width = 1004
          Height = 305
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object TLabel
            Left = 22
            Top = 48
            Width = 45
            Height = 16
            Alignment = taRightJustify
            Caption = 'N'#250'mero'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object TLabel
            Left = 22
            Top = 21
            Width = 45
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ag'#234'ncia'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object TLabel
            Left = 305
            Top = 21
            Width = 62
            Height = 16
            Alignment = taRightJustify
            Caption = 'Tipo Conta'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object ed_cb_numero: TEdit
            Left = 73
            Top = 44
            Width = 127
            Height = 24
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 20
            NumbersOnly = True
            ParentFont = False
            TabOrder = 2
          end
          object ed_cb_ag_numero: TEdit
            Left = 73
            Top = 17
            Width = 127
            Height = 24
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 20
            NumbersOnly = True
            ParentFont = False
            TabOrder = 0
          end
          object ed_cb_ag_digito: TEdit
            Left = 202
            Top = 17
            Width = 50
            Height = 24
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 20
            NumbersOnly = True
            ParentFont = False
            TabOrder = 1
          end
          object ed_cb_digito: TEdit
            Left = 202
            Top = 44
            Width = 50
            Height = 24
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            MaxLength = 20
            NumbersOnly = True
            ParentFont = False
            TabOrder = 3
          end
          object cmb_cb_tipo: TComboBox
            Left = 373
            Top = 17
            Width = 350
            Height = 24
            Style = csDropDownList
            TabOrder = 4
            Items.Strings = (
              'CONTA CORRENTE'
              'POUPAN'#199'A')
          end
        end
      end
    end
  end
  inherited SB: TStatusBar
    Top = 429
    Width = 1018
    ExplicitTop = 429
    ExplicitWidth = 1018
  end
  object FDQContasBancarias: TFDQuery
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select cb.*'#10'from contas_bancarias cb'#10'order by cb.cb_numero')
    Left = 471
    Top = 150
    object FDQContasBancariascb_codigo: TIntegerField
      FieldName = 'cb_codigo'
      Origin = 'cb_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQContasBancariascb_ban_codigo: TIntegerField
      FieldName = 'cb_ban_codigo'
      Origin = 'cb_ban_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQContasBancariascb_ban_emp_codigo: TIntegerField
      FieldName = 'cb_ban_emp_codigo'
      Origin = 'cb_ban_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQContasBancariascb_ag_numero: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cb_ag_numero'
      Origin = 'cb_ag_numero'
      Size = 10
    end
    object FDQContasBancariascb_ag_digito: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cb_ag_digito'
      Origin = 'cb_ag_digito'
      Size = 1
    end
    object FDQContasBancariascb_numero: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cb_numero'
      Origin = 'cb_numero'
      Size = 10
    end
    object FDQContasBancariascb_digito: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'cb_digito'
      Origin = 'cb_digito'
      Size = 1
    end
    object FDQContasBancariascb_tipo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'cb_tipo'
      Origin = 'cb_tipo'
    end
    object FDQContasBancariascb_ativo: TBooleanField
      AutoGenerateValue = arDefault
      FieldName = 'cb_ativo'
      Origin = 'cb_ativo'
    end
  end
  object PROContasBancarias: TDataSetProvider
    DataSet = FDQContasBancarias
    Left = 471
    Top = 178
  end
  object CDSContasBancarias: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROContasBancarias'
    Left = 471
    Top = 206
    object CDSContasBancariascb_codigo: TIntegerField
      FieldName = 'cb_codigo'
      Required = True
    end
    object CDSContasBancariascb_ban_codigo: TIntegerField
      FieldName = 'cb_ban_codigo'
      Required = True
    end
    object CDSContasBancariascb_ban_emp_codigo: TIntegerField
      FieldName = 'cb_ban_emp_codigo'
      Required = True
    end
    object CDSContasBancariascb_ag_numero: TStringField
      FieldName = 'cb_ag_numero'
      Size = 10
    end
    object CDSContasBancariascb_ag_digito: TStringField
      FieldName = 'cb_ag_digito'
      Size = 1
    end
    object CDSContasBancariascb_numero: TStringField
      FieldName = 'cb_numero'
      Size = 10
    end
    object CDSContasBancariascb_digito: TStringField
      FieldName = 'cb_digito'
      Size = 1
    end
    object CDSContasBancariascb_tipo: TIntegerField
      FieldName = 'cb_tipo'
    end
    object CDSContasBancariascb_ativo: TBooleanField
      FieldName = 'cb_ativo'
    end
  end
  object DSContasBancarias: TDataSource
    DataSet = CDSContasBancarias
    Left = 471
    Top = 234
  end
end
