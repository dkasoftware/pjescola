inherited FrmCad_Entidades: TFrmCad_Entidades
  Caption = 'Cadastro de Entidades'
  ClientHeight = 574
  ClientWidth = 1042
  OnActivate = FormActivate
  OnDestroy = FormDestroy
  ExplicitWidth = 1048
  ExplicitHeight = 603
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 1042
    Height = 555
    ExplicitWidth = 1042
    ExplicitHeight = 555
    inherited pnlButtons: TPanel
      Top = 519
      Width = 1036
      ExplicitTop = 519
      ExplicitWidth = 1036
      inherited btnCancelar: TBitBtn
        OnClick = btnCancelarClick
      end
      inherited btnEditar: TBitBtn
        OnClick = btnEditarClick
      end
      inherited btnGravar: TBitBtn
        OnClick = btnGravarClick
      end
      inherited btnNovo: TBitBtn
        OnClick = btnNovoClick
      end
      inherited btnSair: TBitBtn
        Left = 946
        ExplicitLeft = 946
      end
      object btnOpcoes: TBitBtn
        Left = 450
        Top = 3
        Width = 90
        Height = 30
        Align = alLeft
        Caption = 'Op'#231#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
          FF00FFFF00FF16A7C416A7C4FF00FFFF00FFFF00FFFF00FF16A7C416A7C4FF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFFF
          00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FF16A7C473D8E677E0EC16A7C416A7C416A7C41BAEC937D5E627C8DC16A7
          C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCDCDCDCE1E1E1BCBCBCBC
          BCBCBCBCBCBFBFBFD6D6D6CDCDCDBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FF16A7C498EAF389E7F174DFEC6DE1ED59D9E850DBEA41D8E833D4E616A7
          C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCEAEAEAE7E7E7E0E0E0E1
          E1E1DBDBDBDBDBDBD8D8D8D6D6D6BCBCBCFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FF16A7C49FEBF48BE4EE6AD3E374D6E56DD5E44CCDDF47D5E637CFE116A7
          C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCEBEBEBE5E5E5D8D8D8DB
          DBDBDADADAD3D3D3D7D7D7D3D3D3BCBCBCFF00FFFF00FFFF00FFFF00FF16A7C4
          16A7C46BD3E387DFEB8EDFEA16A7C416A7C416A7C416A7C461D5E53FD0E224BA
          D216A7C416A7C4FF00FFFF00FFBCBCBCBCBCBCD8D8D8E2E2E2E2E2E2BCBCBCBC
          BCBCBCBCBCBCBCBCD9D9D9D3D3D3C6C6C6BCBCBCBCBCBCFF00FF16A7C450CDE0
          61D4E484E6F07DD8E616A7C4A7A7A7A7A7A7A7A7A7A7A7A716A7C45AD0E145D8
          E82BC5DB22C1D716A7C4BCBCBCD3D3D3D8D8D8E6E6E6DDDDDDBCBCBCC0C0C0C0
          C0C0C0C0C0C0C0C0BCBCBCD6D6D6D9D9D9CDCDCDC9C9C9BCBCBC16A7C462DFEC
          6FE1EE64D5E416A7C4A7A7A7FEFEFEFDFDFDF7F7F7E5E5E5A7A7A716A7C445D2
          E342D8E833D4E616A7C4BCBCBCDFDFDFE1E1E1D9D9D9BCBCBCC0C0C0FEFEFEFD
          FDFDF9F9F9EBEBEBC0C0C0BCBCBCD6D6D6D8D8D8D6D6D6BCBCBC16A7C444CBDE
          65DFEC54CEE016A7C4A7A7A7F9F9F9F9F9F9F4F4F4E5E5E5A7A7A716A7C443CB
          DE4DDAE927BED516A7C4BCBCBCD1D1D1DFDFDFD4D4D4BCBCBCC0C0C0FAFAFAFA
          FAFAF6F6F6EBEBEBC0C0C0BCBCBCD1D1D1DBDBDBC8C8C8BCBCBCFF00FF16A7C4
          47CBDE4DCDDF16A7C4A7A7A7E8E8E8EEEEEEE9E9E9DADADAA7A7A716A7C44CCC
          DF34BDD416A7C4FF00FFFF00FFBCBCBCD1D1D1D3D3D3BCBCBCC0C0C0EDEDEDF2
          F2F2EEEEEEE3E3E3C0C0C0BCBCBCD3D3D3C9C9C9BCBCBCFF00FFFF00FFFF00FF
          16A7C45FDEEC16A7C4A7A7A7BCBCBCD5D5D5D2D2D2B9B9B9A7A7A716A7C471E1
          EE16A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCDEDEDEBCBCBCC0C0C0CDCDCDDF
          DFDFDDDDDDCBCBCBC0C0C0BCBCBCE2E2E2BCBCBCFF00FFFF00FFFF00FFFF00FF
          16A7C455DCEA4FD1E216A7C4A7A7A7A7A7A7A7A7A7A7A7A716A7C470D7E67CE4
          EF16A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCDCDCDCD6D6D6BCBCBCC0C0C0C0
          C0C0C0C0C0C0C0C0BCBCBCDBDBDBE4E4E4BCBCBCFF00FFFF00FFFF00FF16A7C4
          40D7E84CDAE958DDEB52D1E216A7C416A7C416A7C416A7C48BE0EC95E9F286E6
          F078E3EF16A7C4FF00FFFF00FFBCBCBCD8D8D8DBDBDBDDDDDDD6D6D6BCBCBCBC
          BCBCBCBCBCBCBCBCE3E3E3E9E9E9E6E6E6E3E3E3BCBCBCFF00FFFF00FF16A7C4
          33CEE13BD0E216A7C45CDDEB68E0ED74E2EE81E5F08DE8F190E5EF16A7C491E8
          F27BE0EC16A7C4FF00FFFF00FFBCBCBCD2D2D2D3D3D3BCBCBCDDDDDDE0E0E0E3
          E3E3E5E5E5E8E8E8E6E6E6BCBCBCE8E8E8E1E1E1BCBCBCFF00FFFF00FFFF00FF
          16A7C416A7C4FF00FF16A7C416A7C46BE0ED77E3EF16A7C416A7C4FF00FF16A7
          C416A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFBCBCBCBCBCBCE0
          E0E0E3E3E3BCBCBCBCBCBCFF00FFBCBCBCBCBCBCFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FF16A7C462DEEC6EE1ED16A7C4FF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCDF
          DFDFE1E1E1BCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FF16A7C416A7C4FF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBC
          BCBCBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 6
        OnClick = btnOpcoesClick
      end
    end
    object PC: TPageControl
      Left = 3
      Top = 3
      Width = 1036
      Height = 516
      ActivePage = tsConsulta
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object tsConsulta: TTabSheet
        Caption = 'Consulta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        object pnlConsulta: TPanel
          Left = 0
          Top = 0
          Width = 1028
          Height = 485
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Padding.Left = 3
          Padding.Top = 3
          Padding.Right = 3
          Padding.Bottom = 3
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          object dbgEntidades: TDBGrid
            Left = 3
            Top = 37
            Width = 1022
            Height = 445
            Align = alClient
            DataSource = DSEntidades
            GradientEndColor = 16754511
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnDrawColumnCell = dbgEntidadesDrawColumnCell
            OnDblClick = dbgEntidadesDblClick
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ent_codigo'
                Title.Alignment = taCenter
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ent_razao_social'
                Title.Caption = 'Nome'
                Width = 913
                Visible = True
              end>
          end
          object Panel1: TPanel
            Left = 3
            Top = 3
            Width = 1022
            Height = 34
            Align = alTop
            BevelInner = bvLowered
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            DesignSize = (
              1022
              34)
            object Label1: TLabel
              Left = 9
              Top = 9
              Width = 38
              Height = 16
              Alignment = taRightJustify
              Caption = 'Buscar'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label2: TLabel
              Left = 808
              Top = 9
              Width = 36
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Status'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object cmbCampos: TComboBox
              Left = 54
              Top = 5
              Width = 86
              Height = 24
              Style = csDropDownList
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ItemIndex = 1
              ParentFont = False
              TabOrder = 0
              Text = 'Nome'
              Items.Strings = (
                'C'#243'digo'
                'Nome')
            end
            object ed_consulta: TEdit
              Left = 142
              Top = 5
              Width = 651
              Height = 24
              Anchors = [akLeft, akTop, akRight]
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              OnChange = ed_consultaChange
            end
            object cmbConStatus: TComboBox
              Left = 850
              Top = 5
              Width = 167
              Height = 24
              Style = csDropDownList
              Anchors = [akTop, akRight]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              OnChange = ed_consultaChange
            end
          end
        end
      end
      object tsCadastro: TTabSheet
        Caption = 'Cadastro'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object pnlCadastro: TPanel
          Left = 0
          Top = 0
          Width = 1028
          Height = 485
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 1028
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            Padding.Left = 5
            Padding.Right = 5
            TabOrder = 0
            object gb_ent_codigo: TGroupBox
              Left = 5
              Top = 0
              Width = 133
              Height = 49
              Align = alLeft
              Caption = 'C'#243'digo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object ed_ent_codigo: TEdit
                Left = 6
                Top = 18
                Width = 121
                Height = 24
                TabStop = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                NumbersOnly = True
                ParentFont = False
                ReadOnly = True
                TabOrder = 0
              end
            end
            object TGroupBox
              Left = 138
              Top = 0
              Width = 671
              Height = 49
              Align = alLeft
              Caption = 'Empresa'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object cmb_ent_emp_codigo: TComboBox
                Left = 7
                Top = 18
                Width = 654
                Height = 24
                Style = csDropDownList
                TabOrder = 0
              end
            end
            object TGroupBox
              Left = 809
              Top = 0
              Width = 214
              Height = 49
              Align = alClient
              Caption = 'Situa'#231#227'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              object cmb_ent_sit_codigo: TComboBox
                Left = 7
                Top = 18
                Width = 199
                Height = 24
                Style = csDropDownList
                TabOrder = 0
              end
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 49
            Width = 1028
            Height = 119
            Align = alTop
            BevelOuter = bvNone
            Padding.Left = 5
            Padding.Right = 5
            TabOrder = 1
            object rg_ent_tipo_pessoa: TRadioGroup
              Left = 5
              Top = 0
              Width = 81
              Height = 119
              Align = alLeft
              Caption = 'Pessoa'
              ItemIndex = 0
              Items.Strings = (
                'F'#237'sica'
                'Jur'#237'dica')
              TabOrder = 0
              OnClick = rg_ent_tipo_pessoaClick
            end
            object PCTipoPessoa: TPageControl
              Left = 91
              Top = 8
              Width = 932
              Height = 111
              ActivePage = tsFisica
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object tsFisica: TTabSheet
                Caption = 'Pessoa F'#237'sica'
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 924
                  Height = 80
                  Align = alClient
                  BevelOuter = bvNone
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object TLabel
                    Left = 40
                    Top = 7
                    Width = 33
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Nome'
                  end
                  object TLabel
                    Left = 57
                    Top = 32
                    Width = 16
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'RG'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object TLabel
                    Left = 303
                    Top = 32
                    Width = 22
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'CPF'
                  end
                  object TLabel
                    Left = 695
                    Top = 32
                    Width = 66
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Nascimento'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object TLabel
                    Left = 8
                    Top = 57
                    Width = 65
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Estado Civil'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object TLabel
                    Left = 269
                    Top = 57
                    Width = 56
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Ocupa'#231#227'o'
                  end
                  object ed_ent_razao_social_f: TEdit
                    Left = 79
                    Top = 3
                    Width = 834
                    Height = 24
                    CharCase = ecUpperCase
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 150
                    ParentFont = False
                    TabOrder = 0
                  end
                  object ed_ent_rg: TEdit
                    Left = 79
                    Top = 28
                    Width = 170
                    Height = 24
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 20
                    NumbersOnly = True
                    ParentFont = False
                    TabOrder = 1
                  end
                  object ed_ent_data_nascimento: TDateTimePicker
                    Left = 768
                    Top = 28
                    Width = 145
                    Height = 24
                    Date = 43720.996172847220000000
                    Time = 43720.996172847220000000
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 3
                    OnExit = ed_ent_data_nascimentoExit
                  end
                  object cmb_ent_estado_civil: TComboBox
                    Left = 79
                    Top = 53
                    Width = 170
                    Height = 24
                    Style = csDropDownList
                    TabOrder = 4
                    Items.Strings = (
                      'SOLTEIRO'
                      'CASADO'
                      'DIVORCIADO')
                  end
                  object ed_ent_ocupacao: TEdit
                    Left = 331
                    Top = 53
                    Width = 582
                    Height = 24
                    CharCase = ecUpperCase
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 5
                  end
                  object ed_ent_cpf: TMaskEdit
                    Left = 331
                    Top = 28
                    Width = 167
                    Height = 24
                    EditMask = '999.999.999-99;1; '
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 14
                    ParentFont = False
                    TabOrder = 2
                    Text = '   .   .   -  '
                    OnExit = ed_ent_cpfExit
                  end
                end
              end
              object tsJuridica: TTabSheet
                Caption = 'Pessoa Jur'#237'dica'
                ImageIndex = 1
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 924
                  Height = 80
                  Align = alClient
                  BevelOuter = bvNone
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object TLabel
                    Left = 6
                    Top = 7
                    Width = 73
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Raz'#227'o Social'
                  end
                  object TLabel
                    Left = 269
                    Top = 57
                    Width = 11
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'IE'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object TLabel
                    Left = 51
                    Top = 57
                    Width = 28
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'CNPJ'
                  end
                  object TLabel
                    Left = 32
                    Top = 32
                    Width = 48
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Fantasia'
                  end
                  object TLabel
                    Left = 729
                    Top = 57
                    Width = 14
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'IM'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object ed_ent_razao_social_j: TEdit
                    Left = 85
                    Top = 3
                    Width = 834
                    Height = 24
                    CharCase = ecUpperCase
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 150
                    ParentFont = False
                    TabOrder = 0
                  end
                  object ed_ent_ie: TEdit
                    Left = 286
                    Top = 53
                    Width = 170
                    Height = 24
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 20
                    NumbersOnly = True
                    ParentFont = False
                    TabOrder = 3
                  end
                  object ed_ent_cnpj: TMaskEdit
                    Left = 85
                    Top = 53
                    Width = 165
                    Height = 24
                    EditMask = '999.999.999/9999-99;1; '
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 19
                    ParentFont = False
                    TabOrder = 2
                    Text = '   .   .   /    -  '
                  end
                  object ed_ent_nome_fantasia: TEdit
                    Left = 85
                    Top = 28
                    Width = 834
                    Height = 24
                    CharCase = ecUpperCase
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 150
                    ParentFont = False
                    TabOrder = 1
                  end
                  object ed_ent_im: TEdit
                    Left = 749
                    Top = 53
                    Width = 170
                    Height = 24
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 20
                    NumbersOnly = True
                    ParentFont = False
                    TabOrder = 4
                  end
                end
              end
            end
          end
          object TPanel
            Left = 0
            Top = 168
            Width = 1028
            Height = 317
            Align = alClient
            BevelOuter = bvNone
            Padding.Left = 5
            Padding.Top = 5
            Padding.Right = 5
            Padding.Bottom = 5
            TabOrder = 2
            object PCAdicionais: TPageControl
              Left = 5
              Top = 5
              Width = 1018
              Height = 307
              ActivePage = tsContratos
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object tsAdicionais: TTabSheet
                Caption = 'Adicionais'
                ImageIndex = 4
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 1010
                  Height = 276
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Padding.Left = 2
                  Padding.Top = 2
                  Padding.Right = 2
                  Padding.Bottom = 2
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object GroupBox1: TGroupBox
                    Left = 3
                    Top = 3
                    Width = 126
                    Height = 270
                    Align = alLeft
                    Caption = 'Tipo de Pessoa'
                    Padding.Left = 3
                    Padding.Right = 3
                    Padding.Bottom = 3
                    TabOrder = 0
                    object ckl_ent_tipo: TCheckListBox
                      Left = 5
                      Top = 18
                      Width = 116
                      Height = 247
                      Align = alClient
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      Items.Strings = (
                        'CLIENTE'
                        'FORNECEDOR'
                        'FUNCION'#193'RIO'
                        'USU'#193'RIO')
                      ParentFont = False
                      TabOrder = 0
                      OnClick = ckl_ent_tipoClick
                    end
                  end
                end
              end
              object tsEnderecos: TTabSheet
                Caption = 'Endere'#231'os'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 1010
                  Height = 276
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Padding.Left = 2
                  Padding.Top = 2
                  Padding.Right = 2
                  Padding.Bottom = 2
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object TGroupBox
                    Left = 3
                    Top = 3
                    Width = 1004
                    Height = 161
                    Align = alTop
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Padding.Left = 2
                    Padding.Top = 2
                    Padding.Right = 2
                    Padding.Bottom = 2
                    ParentFont = False
                    TabOrder = 0
                    object lbl_end_cep: TLabel
                      Left = 68
                      Top = 41
                      Width = 22
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'CEP'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_end_tipo: TLabel
                      Left = 65
                      Top = 16
                      Width = 25
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Tipo'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_end_uf_sigla: TLabel
                      Left = 287
                      Top = 41
                      Width = 15
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'UF'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_end_cid_codigo: TLabel
                      Left = 401
                      Top = 41
                      Width = 39
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Cidade'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_end_logradouro: TLabel
                      Left = 25
                      Top = 66
                      Width = 65
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Logradouro'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_end_numero: TLabel
                      Left = 911
                      Top = 66
                      Width = 14
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'N'#186
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_end_complemento: TLabel
                      Left = 11
                      Top = 91
                      Width = 79
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Complemento'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object pnlBotoesEndereco: TPanel
                      Left = 4
                      Top = 121
                      Width = 996
                      Height = 36
                      Align = alBottom
                      BevelInner = bvLowered
                      Padding.Left = 1
                      Padding.Top = 1
                      Padding.Right = 1
                      Padding.Bottom = 1
                      TabOrder = 8
                      object btnNovoEnd: TBitBtn
                        Left = 3
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Novo'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF029B100099000599
                          10FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C68C8C8C8A8A8A8D8A8DFF00FFFF00FFFF00FFFF00FFFF00FF
                          26B7FF22B4FF34BDFF64D4FF77DCFF6EDCFF61DDFF67E1FF00990025B2280099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C4C4CACACADBDBDBE0E0E0DF
                          DFDFDFDFDFE2E2E28A8A8A9696968A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF4EC8FF64D4FF9EE8FFB8F0FFA5EEFF8CECFF90EEFF00990044C0460099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D2D2D2DBDBDBEAEAEAF0F0F0EE
                          EEEEEAEAEAECECEC8A8A8AA2A2A28A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF73D6FF61D3FF9AE6FFB5EFFF0A9E1000990000990000990061CE620099
                          00009900009900009900FF00FFFF00FFC6C6C6DDDDDDDADADAE9E9E9F0F0F08D
                          8D8D8A8A8A8A8A8A8A8A8AB0B0B08A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                          26B7FF8CDEFF5ED3FF96E5FFB0EDFF009900AAECABADEEADA0ED9F80E57F61D0
                          6037B63718A618009900FF00FFFF00FFC6C6C6E3E3E3D9D9D9E8E8E8EFEFEF8A
                          8A8AD7D7D7D9D9D9D4D4D4C5C5C5B0B0B09B9B9B9090908A8A8AFF00FFFF00FF
                          26B7FFA5E5FF59D2FF91E4FFABEAFF099E100099000099000099009EEB9F0099
                          00009900009900009900FF00FFFF00FFC6C6C6EAEAEAD8D8D8E7E7E7ECECEC8D
                          8D8D8A8A8A8A8A8A8A8A8AD3D3D38A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                          26B7FFBEECFF5ED4FF8CE3FFA6EAFF85E1FF59D7FF58D6FF009900A6EBA60099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6F0F0F0DADADAE6E6E6ECECECE4
                          E4E4DBDBDBDADADA8A8A8AD6D6D68A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF96DBFFC2EDFFD2F4FFD5F5FFD4F5FFD2F4FFD2F4FF0099009CE79E0099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E4E1E4F0F0F0F6F6F6F6F6F6F6
                          F6F6F6F6F6F6F6F68A8A8AD0D0D08A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF22B2FF2FBDFF4ACBFF58D3FF56D5FF51D8FF57DCFF059D10009900039B
                          1026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C2C4CACACAD3D3D3D9D9D9DA
                          DADADBDBDBDDDDDD8C8C8C8A8A8A8C8C8CC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF47C4FF62D3FF9DE8FFB9F0FFA6EEFF8EECFF92EFFF8FEDFF71E2FF33C3
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D0D0D0DADADAEAEAEAF0F0F0EE
                          EEEEEAEAEAEDEDEDEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF70D5FF60D3FF9AE6FFB6EFFFA0ECFF83E7FF85E9FF82E8FF68DEFF32C1
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6DCDCDCDADADAE9E9E9F0F0F0EC
                          ECECE7E7E7E9E9E9E8E8E8E0E0E0CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF88DCFF5DD2FF96E5FFB1EDFF99E9FF77E2FF76E3FF74E2FF5ED9FF2EBE
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E2E2E2D9D9D9E8E8E8EFEFEFEA
                          EAEAE3E3E3E3E3E3E3E3E3DDDDDDCACACAC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFA1E4FF59D2FF91E3FFACEBFF90E5FF69DDFF68DDFF67DDFF53D3FF29BA
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E9E9E9D8D8D8E6E6E6EDEDEDE7
                          E7E7DFDFDFDFDFDFDFDFDFD8D8D8C8C8C8C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFBAEBFF54D2FF8CE3FFA7E9FF88E2FF5CD7FF5AD7FF58D7FF48CEFF39C0
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EFEFEFD8D8D8E6E6E6EBEBEBE5
                          E5E5DBDBDBDBDBDBDBDBDBD5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFAAE5FFC6F0FFC1F0FFCBF3FFC1F0FFB3ECFFB2ECFFB1ECFFBDEEFF9FE4
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EAEAEAF2F2F2F1F1F1F4F4F4F1
                          F1F1EEEEEEEEEEEEEEEEEEF0F0F0E9E9E9C6C6C6FF00FFFF00FFFF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 0
                        OnClick = btnNovoEndClick
                      end
                      object btnGravarEnd: TBitBtn
                        Left = 93
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Gravar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          006600006600B59A9BB59A9BB59A9BB59A9BB59A9BB59A9BB59A9B0066000066
                          00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8E8E8E8EBEBEBEBEBEBEBEBEBEBE
                          BEBEBEBEBEBEBEBEBEBEBE8686868E8E8EFF00FFFF00FFFF00FFFF00FF006600
                          009900009900E5DEDF006600006600E4E7E7E0E3E6D9DFE0CCC9CC006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB8B8B8AEAEAEECECEC838383838383F0
                          F0F0EEEEEEE8E8E8DADADA7E7E7E9A9A9A8E8E8EFF00FFFF00FFFF00FF006600
                          009900009900E9E2E2006600006600E2E1E3E2E6E8DDE2E4CFCCCF006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ACACACF0F0F0838383838383ED
                          EDEDF0F0F0ECECECDCDCDC7F7F7F9999998E8E8EFF00FFFF00FFFF00FF006600
                          009900009900ECE4E4006600006600DFDDDFE1E6E8E0E5E7D3D0D2006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ABABABF2F2F2838383838383EA
                          EAEAEFEFEFEEEEEEE0E0E07C7C7C9898988E8E8EFF00FFFF00FFFF00FF006600
                          009900009900EFE6E6EDE5E5E5DEDFE0DDDFDFE0E2E0E1E3D6D0D2006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4AAAAAAF4F4F4F3F3F3ECECECEA
                          EAEAECECECECECECE1E1E18585859D9D9D8E8E8EFF00FFFF00FFFF00FF006600
                          0099000099000099000099000099000099000099000099000099000099000099
                          00006600FF00FFFF00FFFF00FF8E8E8EB1B1B1AFAFAFB2B2B2B8B8B8B6B6B6B1
                          B1B1AFAFAFB5B5B5B2B2B2ACACACB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B10099
                          00006600FF00FFFF00FFFF00FF8E8E8EA2A2A2B6B6B6CBCBCBD0D0D0D1D1D1D0
                          D0D0CECECECDCDCDD1D1D1D3D3D3B3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                          DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                          DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FFFF00FF
                          006600F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90066
                          00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 1
                        OnClick = btnGravarEndClick
                      end
                      object btnEditarEnd: TBitBtn
                        Left = 183
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Editar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000130B0000130B00000000000000000000FF00FFFF00FF
                          FF00FF7F331B8833138A37128A3B168A3B16FF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA2A2A2A2A2A2A3A3A3A4A4A4A4
                          A4A4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF84351E
                          8E32107C321FFF00FFFF00FFFF00FF934112853F1EFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFA5A5A5A3A3A3A2A2A2FF00FFFF00FFFF00FFA5
                          A5A5A5A5A5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF35221D35221D
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF8C4219984A13984A13FF00FFFF00
                          FFFF00FFFF00FFFF00FF8F8F8F8F8F8FFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFA5A5A5A7A7A7A7A7A7FF00FFFF00FFFF00FFFF00FFFF00FF35221D1C6378
                          0076A900699A004C88FF00FFFF00FFFF00FFFF00FFFF00FF9A4E15A8590FA55A
                          12A65C14B36810AA62148F8F8FA0A0A0A7A7A7A2A2A29D9D9DFF00FFFF00FFFF
                          00FFFF00FFFF00FFA8A8A8ABABABABABABACACACAFAFAFAEAEAEFF00FF02AAD8
                          00B6EA036C970930940B0983FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFB8B8B8BDBDBDA2A2A2A3A3A39E9E9EFF00FFFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0281AB
                          00BEEE03161707119C0C16980B0C83FF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFA8A8A8BFBFBF858585A5A5A5A5A5A59F9F9FFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0281AB
                          056AF2061C7402000007119C0C17990B0C83FF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFA8A8A8C2C2C2999999808080A5A5A5A5A5A59F
                          9F9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          0B1CBB2555FF061C7402000007119C0C169C0B0C82FF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFB1B1B1D2D2D2999999808080A5A5A5A7
                          A7A79E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF0F26BF2455FF061C7402000007119C0C169C0B0C83FF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3D1D1D1999999808080A5
                          A5A5A7A7A79F9F9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FF1028C22455FF061C7402000007119C0C169C0A0C83FF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB5B5B5D1D1D199999980
                          8080A5A5A5A7A7A79E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FF1129C42454FF061C7402000007119C0B169D0B0C83FF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB5B5B5D1D1D199
                          9999808080A5A5A5A7A7A79F9F9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FF112CC92455FF061C7402000007119C07119C0A0C
                          83FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB8B8B8D1
                          D1D1999999808080A5A5A5A5A5A59E9E9EFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FF132FCD2354FF061C7400000007119C999C
                          D9171895FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBA
                          BABAD1D1D1999999808080A5A5A5E9E9E9A8A8A8FF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF1531D11E4EFF061C74A3A19A7B7B
                          D700018FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFBCBCBCCFCFCF999999DADADAE0E0E09F9F9FFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF1532D2ACBFFF6C76E10000
                          A6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFBCBCBCF4F4F4DFDFDFA6A6A6FF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF2F31A4020EAAFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFFF00FFB5B5B5A8A8A8FF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 2
                        OnClick = btnEditarEndClick
                      end
                      object btnCancelarEnd: TBitBtn
                        Left = 273
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Cancelar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FFFF00FFFF00FF6D3327853C1395440D96450D873D12703425FF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9E9E9E9F9F9FA1
                          A1A1A1A1A19F9F9F9E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF70372AA04B0CCD7727E8AD70F3CCA1F4CDA3E9B176D07C2C7034256F35
                          29FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9F9F9FA4A4A4B6B6B6D0D0D0E0
                          E0E0E1E1E1D2D2D2B8B8B89E9E9E9F9F9FFF00FFFF00FFFF00FFFF00FFFF00FF
                          86411DC0620BF0C292FFFEFAFEFAF7F5E3D1F5E2D0FDF8F4FFFFFDF2C99EC669
                          117B3A21FF00FFFF00FFFF00FFFF00FFA2A2A2ACACACDBDBDBFEFEFEFCFCFCEF
                          EFEFEEEEEEFBFBFBFEFEFEDFDFDFAFAFAFA0A0A0FF00FFFF00FFFF00FF8C451C
                          C16107F7DBBDFFFEFEE0A46ACE6F15C96100C96100CD6D12DE9D5FFDFAF7FAE5
                          CCC6680D6F3528FF00FFFF00FFA4A4A4ABABABE9E9E9FEFEFECCCCCCB2B2B2AA
                          AAAAAAAAAAB1B1B1C9C9C9FCFCFCEFEFEFAEAEAE9E9E9EFF00FFFF00FF8C451C
                          ECBD8BFFFFFFDA8F44C75800C85E00C95F00C95F00C85D00C55600D58433FDFB
                          F8F3CB9F703525FF00FFFF00FFA4A4A4D8D8D8FFFFFFC1C1C1A8A8A8A9A9A9AA
                          AAAAAAAAAAA9A9A9A7A7A7BCBCBCFCFCFCDFDFDF9E9E9EFF00FFA04D10CE7721
                          FFFDFBE8B684CE6600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC65600DFA0
                          61FFFFFFCF7B28703525A5A5A5B6B6B6FEFEFED5D5D5ACACACFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA8A8A8CACACAFFFFFFB8B8B89E9E9EAF5507E5AA6F
                          FFFFFFDD8F3FD87B1CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC85C00CE6E
                          0DFDFAF6E9B175873D11A7A7A7CFCFCFFFFFFFC1C1C1B7B7B7FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA9A9A9B0B0B0FCFCFCD2D2D29F9F9FBB5F0AF0CAA1
                          FCF4EDE19343E08D38FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC95F00CA61
                          00F5E3D0F3CEA495440CABABABDFDFDFF9F9F9C3C3C3C0C0C0FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFAAAAAAAAAAAAEEEEEEE1E1E1A1A1A1C1650FF2CDA6
                          FDF7F0E9A158E89C4EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC95F00CA62
                          00F6E6D4F3CCA194440CADADADE1E1E1FAFAFAC9C9C9C7C7C7FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFAAAAAAABABABF0F0F0E0E0E0A1A1A1C1640DEEBC88
                          FFFFFFF1B77CEFA961FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC85D00CF70
                          10FEFCFAE7AC6D853B13ADADADD8D8D8FFFFFFD5D5D5CDCDCDFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA9A9A9B2B2B2FDFDFDCFCFCF9E9E9EBF6006E5A059
                          FFFDFAFBE0C4F7B877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC75800E2AA
                          71FFFFFECC75206D3428AAAAAAC9C9C9FDFDFDECECECD4D4D4FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA8A8A8CFCFCFFFFFFFB5B5B59E9E9EFF00FFC36204
                          FAD9B8FFFFFFFEDCB9F7B876EFA961E89C4EE08C38D87B1DCE6600DB924AFFFF
                          FFEFC08C6D3428FF00FFFF00FFABABABE8E8E8FFFFFFE9E9E9D4D4D4CDCDCDC7
                          C7C7BFBFBFB7B7B7ACACACC3C3C3FFFFFFDADADA9E9E9EFF00FFFF00FFC36204
                          E79E55FEEBD7FFFFFFFBDFC3F1B87CE9A159E29444DE9142E8B786FFFFFFF6D8
                          B7BE5F066B342CFF00FFFF00FFABABABC8C8C8F2F2F2FFFFFFECECECD5D5D5C9
                          C9C9C3C3C3C2C2C2D6D6D6FFFFFFE7E7E7AAAAAA9F9F9FFF00FFFF00FFFF00FF
                          C6670CE69E55FAD9B6FFFBF6FFFFFFFEF8F2FDF6EFFFFFFFFEF9F2ECB884BE5F
                          09753826FF00FFFF00FFFF00FFFF00FFAEAEAEC8C8C8E7E7E7FCFCFCFFFFFFFB
                          FBFBFAFAFAFFFFFFFBFBFBD6D6D6ABABAB9F9F9FFF00FFFF00FFFF00FFFF00FF
                          FF00FFC06005C06005E49F5AEEBA86F2CAA0F0C599E4A768CC741E8E451A783A
                          27FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFAAAAAAAAAAAAC9C9C9D7D7D7DF
                          DFDFDDDDDDCDCDCDB4B4B4A3A3A3A0A0A0FF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFB65C0AB86012B96113B25A0FA24F0E8E451AFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9ACACACAC
                          ACACA9A9A9A6A6A6A3A3A3FF00FFFF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 3
                        OnClick = btnCancelarEndClick
                      end
                      object btnExcluirEnd: TBitBtn
                        Left = 363
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Excluir'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FFFF00FFFF00FF
                          26B7FF22B4FF34BDFF64D4FF77DCFF6EDCFF61DDFF67E1FF64DFFF44CFFF2FC1
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C4C4CACACADBDBDBE0E0E0DF
                          DFDFDFDFDFE2E2E2E0E0E0D5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF4EC8FF64D4FF9EE8FFB8F0FFA5EEFF8CECFF90EEFF8DEDFF70E1FF33C2
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D2D2D2DBDBDBEAEAEAF0F0F0EE
                          EEEEEAEAEAECECECEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF73D6FF61D3FF9AE6FFB5EFFF9EEBFF2034AB0202930202930202930202
                          93020293020293020293FF00FFFF00FFC6C6C6DDDDDDDADADAE9E9E9F0F0F0EC
                          ECEC888888828282828282828282828282828282828282828282FF00FFFF00FF
                          26B7FF8CDEFF5ED3FF96E5FFB0EDFF97E8FF0A0A987272F37575F46C6CF43E3E
                          E31616C50303AA020293FF00FFFF00FFC6C6C6E3E3E3D9D9D9E8E8E8EFEFEFEA
                          EAEA838383AAAAAAABABABA7A7A7919191868686838383828282FF00FFFF00FF
                          26B7FFA5E5FF59D2FF91E4FFABEAFF8EE4FF1C32AB0A0A980A0A980A0A980A0A
                          980A0A980A0A98020293FF00FFFF00FFC6C6C6EAEAEAD8D8D8E7E7E7ECECECE7
                          E7E7888888838383838383838383838383838383838383828282FF00FFFF00FF
                          26B7FFBEECFF5ED4FF8CE3FFA6EAFF85E1FF59D7FF58D6FF56D6FF46CDFF4CC8
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6F0F0F0DADADAE6E6E6ECECECE4
                          E4E4DBDBDBDADADADADADAD4D4D4D2D2D2C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF96DBFFC2EDFFD2F4FFD5F5FFD4F5FFD2F4FFD2F4FFD2F4FFCEF1FF92DB
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E4E1E4F0F0F0F6F6F6F6F6F6F6
                          F6F6F6F6F6F6F6F6F6F6F6F3F3F3E3E1E3C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF22B2FF2FBDFF4ACBFF58D3FF56D5FF51D8FF57DCFF54DAFF42CEFF2DBE
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C2C4CACACAD3D3D3D9D9D9DA
                          DADADBDBDBDDDDDDDCDCDCD4D4D4CAC8CAC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF47C4FF62D3FF9DE8FFB9F0FFA6EEFF8EECFF92EFFF8FEDFF71E2FF33C3
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D0D0D0DADADAEAEAEAF0F0F0EE
                          EEEEEAEAEAEDEDEDEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF70D5FF60D3FF9AE6FFB6EFFFA0ECFF83E7FF85E9FF82E8FF68DEFF32C1
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6DCDCDCDADADAE9E9E9F0F0F0EC
                          ECECE7E7E7E9E9E9E8E8E8E0E0E0CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF88DCFF5DD2FF96E5FFB1EDFF99E9FF77E2FF76E3FF74E2FF5ED9FF2EBE
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E2E2E2D9D9D9E8E8E8EFEFEFEA
                          EAEAE3E3E3E3E3E3E3E3E3DDDDDDCACACAC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFA1E4FF59D2FF91E3FFACEBFF90E5FF69DDFF68DDFF67DDFF53D3FF29BA
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E9E9E9D8D8D8E6E6E6EDEDEDE7
                          E7E7DFDFDFDFDFDFDFDFDFD8D8D8C8C8C8C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFBAEBFF54D2FF8CE3FFA7E9FF88E2FF5CD7FF5AD7FF58D7FF48CEFF39C0
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EFEFEFD8D8D8E6E6E6EBEBEBE5
                          E5E5DBDBDBDBDBDBDBDBDBD5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFAAE5FFC6F0FFC1F0FFCBF3FFC1F0FFB3ECFFB2ECFFB1ECFFBDEEFF9FE4
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EAEAEAF2F2F2F1F1F1F4F4F4F1
                          F1F1EEEEEEEEEEEEEEEEEEF0F0F0E9E9E9C6C6C6FF00FFFF00FFFF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 4
                        OnClick = btnExcluirEndClick
                      end
                    end
                    object ed_end_cep: TEdit
                      Left = 96
                      Top = 37
                      Width = 170
                      Height = 24
                      CharCase = ecUpperCase
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 1
                    end
                    object cmb_end_tipo: TComboBox
                      Left = 96
                      Top = 12
                      Width = 170
                      Height = 24
                      Style = csDropDownList
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ItemIndex = 0
                      ParentFont = False
                      TabOrder = 0
                      Text = 'PRINCIPAL'
                      Items.Strings = (
                        'PRINCIPAL'
                        'ENTREGA'
                        'COBRAN'#199'A'
                        'OUTROS')
                    end
                    object cmb_end_uf_sigla: TComboBox
                      Left = 308
                      Top = 37
                      Width = 68
                      Height = 24
                      Style = csDropDownList
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 2
                      OnChange = cmb_end_uf_siglaChange
                    end
                    object cmb_end_cid_codigo: TComboBox
                      Left = 446
                      Top = 37
                      Width = 525
                      Height = 24
                      Style = csDropDownList
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 3
                    end
                    object btnAddEnd: TBitBtn
                      Left = 972
                      Top = 37
                      Width = 24
                      Height = 24
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = [fsBold]
                      Glyph.Data = {
                        36060000424D3606000000000000360000002800000020000000100000000100
                        18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                        FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF029B100099000599
                        10FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                        C6C6C6C6C6C6C6C68C8C8C8A8A8A8D8A8DFF00FFFF00FFFF00FFFF00FFFF00FF
                        26B7FF22B4FF34BDFF64D4FF77DCFF6EDCFF61DDFF67E1FF00990025B2280099
                        0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C4C4CACACADBDBDBE0E0E0DF
                        DFDFDFDFDFE2E2E28A8A8A9696968A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FF4EC8FF64D4FF9EE8FFB8F0FFA5EEFF8CECFF90EEFF00990044C0460099
                        0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D2D2D2DBDBDBEAEAEAF0F0F0EE
                        EEEEEAEAEAECECEC8A8A8AA2A2A28A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FF73D6FF61D3FF9AE6FFB5EFFF0A9E1000990000990000990061CE620099
                        00009900009900009900FF00FFFF00FFC6C6C6DDDDDDDADADAE9E9E9F0F0F08D
                        8D8D8A8A8A8A8A8A8A8A8AB0B0B08A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                        26B7FF8CDEFF5ED3FF96E5FFB0EDFF009900AAECABADEEADA0ED9F80E57F61D0
                        6037B63718A618009900FF00FFFF00FFC6C6C6E3E3E3D9D9D9E8E8E8EFEFEF8A
                        8A8AD7D7D7D9D9D9D4D4D4C5C5C5B0B0B09B9B9B9090908A8A8AFF00FFFF00FF
                        26B7FFA5E5FF59D2FF91E4FFABEAFF099E100099000099000099009EEB9F0099
                        00009900009900009900FF00FFFF00FFC6C6C6EAEAEAD8D8D8E7E7E7ECECEC8D
                        8D8D8A8A8A8A8A8A8A8A8AD3D3D38A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                        26B7FFBEECFF5ED4FF8CE3FFA6EAFF85E1FF59D7FF58D6FF009900A6EBA60099
                        0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6F0F0F0DADADAE6E6E6ECECECE4
                        E4E4DBDBDBDADADA8A8A8AD6D6D68A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FF96DBFFC2EDFFD2F4FFD5F5FFD4F5FFD2F4FFD2F4FF0099009CE79E0099
                        0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E4E1E4F0F0F0F6F6F6F6F6F6F6
                        F6F6F6F6F6F6F6F68A8A8AD0D0D08A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FF22B2FF2FBDFF4ACBFF58D3FF56D5FF51D8FF57DCFF059D10009900039B
                        1026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C2C4CACACAD3D3D3D9D9D9DA
                        DADADBDBDBDDDDDD8C8C8C8A8A8A8C8C8CC6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FF47C4FF62D3FF9DE8FFB9F0FFA6EEFF8EECFF92EFFF8FEDFF71E2FF33C3
                        FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D0D0D0DADADAEAEAEAF0F0F0EE
                        EEEEEAEAEAEDEDEDEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FF70D5FF60D3FF9AE6FFB6EFFFA0ECFF83E7FF85E9FF82E8FF68DEFF32C1
                        FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6DCDCDCDADADAE9E9E9F0F0F0EC
                        ECECE7E7E7E9E9E9E8E8E8E0E0E0CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FF88DCFF5DD2FF96E5FFB1EDFF99E9FF77E2FF76E3FF74E2FF5ED9FF2EBE
                        FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E2E2E2D9D9D9E8E8E8EFEFEFEA
                        EAEAE3E3E3E3E3E3E3E3E3DDDDDDCACACAC6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FFA1E4FF59D2FF91E3FFACEBFF90E5FF69DDFF68DDFF67DDFF53D3FF29BA
                        FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E9E9E9D8D8D8E6E6E6EDEDEDE7
                        E7E7DFDFDFDFDFDFDFDFDFD8D8D8C8C8C8C6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FFBAEBFF54D2FF8CE3FFA7E9FF88E2FF5CD7FF5AD7FF58D7FF48CEFF39C0
                        FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EFEFEFD8D8D8E6E6E6EBEBEBE5
                        E5E5DBDBDBDBDBDBDBDBDBD5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                        26B7FFAAE5FFC6F0FFC1F0FFCBF3FFC1F0FFB3ECFFB2ECFFB1ECFFBDEEFF9FE4
                        FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EAEAEAF2F2F2F1F1F1F4F4F4F1
                        F1F1EEEEEEEEEEEEEEEEEEF0F0F0E9E9E9C6C6C6FF00FFFF00FFFF00FFFF00FF
                        FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                        C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FF}
                      NumGlyphs = 2
                      ParentFont = False
                      TabOrder = 4
                    end
                    object ed_end_logradouro: TEdit
                      Left = 96
                      Top = 62
                      Width = 809
                      Height = 24
                      CharCase = ecUpperCase
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 150
                      ParentFont = False
                      TabOrder = 5
                    end
                    object ed_end_numero: TEdit
                      Left = 931
                      Top = 62
                      Width = 65
                      Height = 24
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      NumbersOnly = True
                      ParentFont = False
                      TabOrder = 6
                    end
                    object ed_end_complemento: TEdit
                      Left = 96
                      Top = 87
                      Width = 900
                      Height = 24
                      CharCase = ecUpperCase
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 150
                      ParentFont = False
                      TabOrder = 7
                    end
                  end
                  object TPanel
                    Left = 3
                    Top = 164
                    Width = 1004
                    Height = 109
                    Align = alClient
                    BevelOuter = bvNone
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Padding.Left = 2
                    Padding.Top = 2
                    Padding.Right = 2
                    Padding.Bottom = 2
                    ParentFont = False
                    TabOrder = 1
                    object DBGEnderecos: TDBGrid
                      Left = 2
                      Top = 2
                      Width = 1000
                      Height = 105
                      Align = alClient
                      DataSource = DSEnderecos
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                      ParentFont = False
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -13
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnDrawColumnCell = DBGEnderecosDrawColumnCell
                      OnDblClick = DBGEnderecosDblClick
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'end_descricao_tipo'
                          Title.Caption = 'Tipo'
                          Width = 110
                          Visible = True
                        end
                        item
                          Alignment = taCenter
                          Expanded = False
                          FieldName = 'end_cep'
                          Title.Alignment = taCenter
                          Title.Caption = 'CEP'
                          Width = 75
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'end_uf_sigla'
                          Title.Alignment = taCenter
                          Title.Caption = 'UF'
                          Width = 30
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'cid_nome'
                          Title.Caption = 'Cidade'
                          Width = 300
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'end_logradouro'
                          Title.Caption = 'Logradouro'
                          Width = 380
                          Visible = True
                        end
                        item
                          Alignment = taCenter
                          Expanded = False
                          FieldName = 'end_numero'
                          Title.Alignment = taCenter
                          Title.Caption = 'N'#186
                          Width = 65
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'end_complemento'
                          Title.Caption = 'Complemento'
                          Width = 900
                          Visible = True
                        end>
                    end
                  end
                end
              end
              object tsResponsaveis: TTabSheet
                Caption = 'Respons'#225'vel'
                ImageIndex = 2
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 1010
                  Height = 276
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Padding.Left = 5
                  Padding.Top = 5
                  Padding.Right = 5
                  Padding.Bottom = 5
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object TGroupBox
                    Left = 6
                    Top = 6
                    Width = 998
                    Height = 264
                    Align = alClient
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Padding.Left = 2
                    Padding.Top = 2
                    Padding.Right = 2
                    Padding.Bottom = 2
                    ParentFont = False
                    TabOrder = 0
                    object TLabel
                      Left = 50
                      Top = 18
                      Width = 33
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Nome'
                    end
                    object TLabel
                      Left = 67
                      Top = 43
                      Width = 16
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'RG'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object TLabel
                      Left = 61
                      Top = 68
                      Width = 22
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'CPF'
                    end
                    object TLabel
                      Left = 16
                      Top = 93
                      Width = 66
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Nascimento'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object ed_res_nome: TEdit
                      Left = 90
                      Top = 14
                      Width = 833
                      Height = 24
                      CharCase = ecUpperCase
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 150
                      ParentFont = False
                      TabOrder = 0
                    end
                    object ed_res_rg: TEdit
                      Left = 90
                      Top = 39
                      Width = 169
                      Height = 24
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 20
                      NumbersOnly = True
                      ParentFont = False
                      TabOrder = 1
                    end
                    object ed_res_data_nascimento: TDateTimePicker
                      Left = 90
                      Top = 89
                      Width = 169
                      Height = 24
                      Date = 43720.996172847220000000
                      Time = 43720.996172847220000000
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 3
                    end
                    object ed_res_cpf: TMaskEdit
                      Left = 90
                      Top = 64
                      Width = 169
                      Height = 24
                      EditMask = '999.999.999-99;1; '
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 14
                      ParentFont = False
                      TabOrder = 2
                      Text = '   .   .   -  '
                      OnExit = ed_res_cpfExit
                    end
                  end
                end
              end
              object tsContatos: TTabSheet
                Caption = 'Contatos'
                ImageIndex = 1
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 1010
                  Height = 276
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Padding.Left = 2
                  Padding.Top = 2
                  Padding.Right = 2
                  Padding.Bottom = 2
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object TGroupBox
                    Left = 3
                    Top = 3
                    Width = 1004
                    Height = 161
                    Align = alTop
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Padding.Left = 2
                    Padding.Top = 2
                    Padding.Right = 2
                    Padding.Bottom = 2
                    ParentFont = False
                    TabOrder = 0
                    object lbl_con_email: TLabel
                      Left = 44
                      Top = 40
                      Width = 35
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'e-Mail'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_contato: TLabel
                      Left = 35
                      Top = 15
                      Width = 44
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Contato'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_observacao: TLabel
                      Left = 12
                      Top = 65
                      Width = 67
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Observa'#231#227'o'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_telefone: TLabel
                      Left = 29
                      Top = 90
                      Width = 50
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Telefone'
                    end
                    object pnlBotoesContatos: TPanel
                      Left = 4
                      Top = 121
                      Width = 996
                      Height = 36
                      Align = alBottom
                      BevelInner = bvLowered
                      Padding.Left = 1
                      Padding.Top = 1
                      Padding.Right = 1
                      Padding.Bottom = 1
                      TabOrder = 4
                      object btnNovoCon: TBitBtn
                        Left = 3
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Novo'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF029B100099000599
                          10FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C68C8C8C8A8A8A8D8A8DFF00FFFF00FFFF00FFFF00FFFF00FF
                          26B7FF22B4FF34BDFF64D4FF77DCFF6EDCFF61DDFF67E1FF00990025B2280099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C4C4CACACADBDBDBE0E0E0DF
                          DFDFDFDFDFE2E2E28A8A8A9696968A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF4EC8FF64D4FF9EE8FFB8F0FFA5EEFF8CECFF90EEFF00990044C0460099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D2D2D2DBDBDBEAEAEAF0F0F0EE
                          EEEEEAEAEAECECEC8A8A8AA2A2A28A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF73D6FF61D3FF9AE6FFB5EFFF0A9E1000990000990000990061CE620099
                          00009900009900009900FF00FFFF00FFC6C6C6DDDDDDDADADAE9E9E9F0F0F08D
                          8D8D8A8A8A8A8A8A8A8A8AB0B0B08A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                          26B7FF8CDEFF5ED3FF96E5FFB0EDFF009900AAECABADEEADA0ED9F80E57F61D0
                          6037B63718A618009900FF00FFFF00FFC6C6C6E3E3E3D9D9D9E8E8E8EFEFEF8A
                          8A8AD7D7D7D9D9D9D4D4D4C5C5C5B0B0B09B9B9B9090908A8A8AFF00FFFF00FF
                          26B7FFA5E5FF59D2FF91E4FFABEAFF099E100099000099000099009EEB9F0099
                          00009900009900009900FF00FFFF00FFC6C6C6EAEAEAD8D8D8E7E7E7ECECEC8D
                          8D8D8A8A8A8A8A8A8A8A8AD3D3D38A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                          26B7FFBEECFF5ED4FF8CE3FFA6EAFF85E1FF59D7FF58D6FF009900A6EBA60099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6F0F0F0DADADAE6E6E6ECECECE4
                          E4E4DBDBDBDADADA8A8A8AD6D6D68A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF96DBFFC2EDFFD2F4FFD5F5FFD4F5FFD2F4FFD2F4FF0099009CE79E0099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E4E1E4F0F0F0F6F6F6F6F6F6F6
                          F6F6F6F6F6F6F6F68A8A8AD0D0D08A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF22B2FF2FBDFF4ACBFF58D3FF56D5FF51D8FF57DCFF059D10009900039B
                          1026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C2C4CACACAD3D3D3D9D9D9DA
                          DADADBDBDBDDDDDD8C8C8C8A8A8A8C8C8CC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF47C4FF62D3FF9DE8FFB9F0FFA6EEFF8EECFF92EFFF8FEDFF71E2FF33C3
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D0D0D0DADADAEAEAEAF0F0F0EE
                          EEEEEAEAEAEDEDEDEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF70D5FF60D3FF9AE6FFB6EFFFA0ECFF83E7FF85E9FF82E8FF68DEFF32C1
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6DCDCDCDADADAE9E9E9F0F0F0EC
                          ECECE7E7E7E9E9E9E8E8E8E0E0E0CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF88DCFF5DD2FF96E5FFB1EDFF99E9FF77E2FF76E3FF74E2FF5ED9FF2EBE
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E2E2E2D9D9D9E8E8E8EFEFEFEA
                          EAEAE3E3E3E3E3E3E3E3E3DDDDDDCACACAC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFA1E4FF59D2FF91E3FFACEBFF90E5FF69DDFF68DDFF67DDFF53D3FF29BA
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E9E9E9D8D8D8E6E6E6EDEDEDE7
                          E7E7DFDFDFDFDFDFDFDFDFD8D8D8C8C8C8C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFBAEBFF54D2FF8CE3FFA7E9FF88E2FF5CD7FF5AD7FF58D7FF48CEFF39C0
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EFEFEFD8D8D8E6E6E6EBEBEBE5
                          E5E5DBDBDBDBDBDBDBDBDBD5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFAAE5FFC6F0FFC1F0FFCBF3FFC1F0FFB3ECFFB2ECFFB1ECFFBDEEFF9FE4
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EAEAEAF2F2F2F1F1F1F4F4F4F1
                          F1F1EEEEEEEEEEEEEEEEEEF0F0F0E9E9E9C6C6C6FF00FFFF00FFFF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 0
                        OnClick = btnNovoConClick
                      end
                      object btnGravarCon: TBitBtn
                        Left = 93
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Gravar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          006600006600B59A9BB59A9BB59A9BB59A9BB59A9BB59A9BB59A9B0066000066
                          00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8E8E8E8EBEBEBEBEBEBEBEBEBEBE
                          BEBEBEBEBEBEBEBEBEBEBE8686868E8E8EFF00FFFF00FFFF00FFFF00FF006600
                          009900009900E5DEDF006600006600E4E7E7E0E3E6D9DFE0CCC9CC006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB8B8B8AEAEAEECECEC838383838383F0
                          F0F0EEEEEEE8E8E8DADADA7E7E7E9A9A9A8E8E8EFF00FFFF00FFFF00FF006600
                          009900009900E9E2E2006600006600E2E1E3E2E6E8DDE2E4CFCCCF006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ACACACF0F0F0838383838383ED
                          EDEDF0F0F0ECECECDCDCDC7F7F7F9999998E8E8EFF00FFFF00FFFF00FF006600
                          009900009900ECE4E4006600006600DFDDDFE1E6E8E0E5E7D3D0D2006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ABABABF2F2F2838383838383EA
                          EAEAEFEFEFEEEEEEE0E0E07C7C7C9898988E8E8EFF00FFFF00FFFF00FF006600
                          009900009900EFE6E6EDE5E5E5DEDFE0DDDFDFE0E2E0E1E3D6D0D2006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4AAAAAAF4F4F4F3F3F3ECECECEA
                          EAEAECECECECECECE1E1E18585859D9D9D8E8E8EFF00FFFF00FFFF00FF006600
                          0099000099000099000099000099000099000099000099000099000099000099
                          00006600FF00FFFF00FFFF00FF8E8E8EB1B1B1AFAFAFB2B2B2B8B8B8B6B6B6B1
                          B1B1AFAFAFB5B5B5B2B2B2ACACACB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B10099
                          00006600FF00FFFF00FFFF00FF8E8E8EA2A2A2B6B6B6CBCBCBD0D0D0D1D1D1D0
                          D0D0CECECECDCDCDD1D1D1D3D3D3B3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                          DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                          DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FFFF00FF
                          006600F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90066
                          00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 1
                        OnClick = btnGravarConClick
                      end
                      object btnEditarCon: TBitBtn
                        Left = 183
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Editar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000130B0000130B00000000000000000000FF00FFFF00FF
                          FF00FF7F331B8833138A37128A3B168A3B16FF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA2A2A2A2A2A2A3A3A3A4A4A4A4
                          A4A4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF84351E
                          8E32107C321FFF00FFFF00FFFF00FF934112853F1EFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFA5A5A5A3A3A3A2A2A2FF00FFFF00FFFF00FFA5
                          A5A5A5A5A5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF35221D35221D
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF8C4219984A13984A13FF00FFFF00
                          FFFF00FFFF00FFFF00FF8F8F8F8F8F8FFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFA5A5A5A7A7A7A7A7A7FF00FFFF00FFFF00FFFF00FFFF00FF35221D1C6378
                          0076A900699A004C88FF00FFFF00FFFF00FFFF00FFFF00FF9A4E15A8590FA55A
                          12A65C14B36810AA62148F8F8FA0A0A0A7A7A7A2A2A29D9D9DFF00FFFF00FFFF
                          00FFFF00FFFF00FFA8A8A8ABABABABABABACACACAFAFAFAEAEAEFF00FF02AAD8
                          00B6EA036C970930940B0983FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFB8B8B8BDBDBDA2A2A2A3A3A39E9E9EFF00FFFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0281AB
                          00BEEE03161707119C0C16980B0C83FF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFA8A8A8BFBFBF858585A5A5A5A5A5A59F9F9FFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0281AB
                          056AF2061C7402000007119C0C17990B0C83FF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFA8A8A8C2C2C2999999808080A5A5A5A5A5A59F
                          9F9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          0B1CBB2555FF061C7402000007119C0C169C0B0C82FF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFB1B1B1D2D2D2999999808080A5A5A5A7
                          A7A79E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF0F26BF2455FF061C7402000007119C0C169C0B0C83FF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3D1D1D1999999808080A5
                          A5A5A7A7A79F9F9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FF1028C22455FF061C7402000007119C0C169C0A0C83FF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB5B5B5D1D1D199999980
                          8080A5A5A5A7A7A79E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FF1129C42454FF061C7402000007119C0B169D0B0C83FF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB5B5B5D1D1D199
                          9999808080A5A5A5A7A7A79F9F9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FF112CC92455FF061C7402000007119C07119C0A0C
                          83FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB8B8B8D1
                          D1D1999999808080A5A5A5A5A5A59E9E9EFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FF132FCD2354FF061C7400000007119C999C
                          D9171895FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBA
                          BABAD1D1D1999999808080A5A5A5E9E9E9A8A8A8FF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF1531D11E4EFF061C74A3A19A7B7B
                          D700018FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFBCBCBCCFCFCF999999DADADAE0E0E09F9F9FFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF1532D2ACBFFF6C76E10000
                          A6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFBCBCBCF4F4F4DFDFDFA6A6A6FF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF2F31A4020EAAFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFFF00FFB5B5B5A8A8A8FF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 2
                        OnClick = btnEditarConClick
                      end
                      object btnCancelarCon: TBitBtn
                        Left = 273
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Cancelar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FFFF00FFFF00FF6D3327853C1395440D96450D873D12703425FF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9E9E9E9F9F9FA1
                          A1A1A1A1A19F9F9F9E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF70372AA04B0CCD7727E8AD70F3CCA1F4CDA3E9B176D07C2C7034256F35
                          29FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9F9F9FA4A4A4B6B6B6D0D0D0E0
                          E0E0E1E1E1D2D2D2B8B8B89E9E9E9F9F9FFF00FFFF00FFFF00FFFF00FFFF00FF
                          86411DC0620BF0C292FFFEFAFEFAF7F5E3D1F5E2D0FDF8F4FFFFFDF2C99EC669
                          117B3A21FF00FFFF00FFFF00FFFF00FFA2A2A2ACACACDBDBDBFEFEFEFCFCFCEF
                          EFEFEEEEEEFBFBFBFEFEFEDFDFDFAFAFAFA0A0A0FF00FFFF00FFFF00FF8C451C
                          C16107F7DBBDFFFEFEE0A46ACE6F15C96100C96100CD6D12DE9D5FFDFAF7FAE5
                          CCC6680D6F3528FF00FFFF00FFA4A4A4ABABABE9E9E9FEFEFECCCCCCB2B2B2AA
                          AAAAAAAAAAB1B1B1C9C9C9FCFCFCEFEFEFAEAEAE9E9E9EFF00FFFF00FF8C451C
                          ECBD8BFFFFFFDA8F44C75800C85E00C95F00C95F00C85D00C55600D58433FDFB
                          F8F3CB9F703525FF00FFFF00FFA4A4A4D8D8D8FFFFFFC1C1C1A8A8A8A9A9A9AA
                          AAAAAAAAAAA9A9A9A7A7A7BCBCBCFCFCFCDFDFDF9E9E9EFF00FFA04D10CE7721
                          FFFDFBE8B684CE6600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC65600DFA0
                          61FFFFFFCF7B28703525A5A5A5B6B6B6FEFEFED5D5D5ACACACFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA8A8A8CACACAFFFFFFB8B8B89E9E9EAF5507E5AA6F
                          FFFFFFDD8F3FD87B1CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC85C00CE6E
                          0DFDFAF6E9B175873D11A7A7A7CFCFCFFFFFFFC1C1C1B7B7B7FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA9A9A9B0B0B0FCFCFCD2D2D29F9F9FBB5F0AF0CAA1
                          FCF4EDE19343E08D38FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC95F00CA61
                          00F5E3D0F3CEA495440CABABABDFDFDFF9F9F9C3C3C3C0C0C0FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFAAAAAAAAAAAAEEEEEEE1E1E1A1A1A1C1650FF2CDA6
                          FDF7F0E9A158E89C4EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC95F00CA62
                          00F6E6D4F3CCA194440CADADADE1E1E1FAFAFAC9C9C9C7C7C7FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFAAAAAAABABABF0F0F0E0E0E0A1A1A1C1640DEEBC88
                          FFFFFFF1B77CEFA961FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC85D00CF70
                          10FEFCFAE7AC6D853B13ADADADD8D8D8FFFFFFD5D5D5CDCDCDFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA9A9A9B2B2B2FDFDFDCFCFCF9E9E9EBF6006E5A059
                          FFFDFAFBE0C4F7B877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC75800E2AA
                          71FFFFFECC75206D3428AAAAAAC9C9C9FDFDFDECECECD4D4D4FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA8A8A8CFCFCFFFFFFFB5B5B59E9E9EFF00FFC36204
                          FAD9B8FFFFFFFEDCB9F7B876EFA961E89C4EE08C38D87B1DCE6600DB924AFFFF
                          FFEFC08C6D3428FF00FFFF00FFABABABE8E8E8FFFFFFE9E9E9D4D4D4CDCDCDC7
                          C7C7BFBFBFB7B7B7ACACACC3C3C3FFFFFFDADADA9E9E9EFF00FFFF00FFC36204
                          E79E55FEEBD7FFFFFFFBDFC3F1B87CE9A159E29444DE9142E8B786FFFFFFF6D8
                          B7BE5F066B342CFF00FFFF00FFABABABC8C8C8F2F2F2FFFFFFECECECD5D5D5C9
                          C9C9C3C3C3C2C2C2D6D6D6FFFFFFE7E7E7AAAAAA9F9F9FFF00FFFF00FFFF00FF
                          C6670CE69E55FAD9B6FFFBF6FFFFFFFEF8F2FDF6EFFFFFFFFEF9F2ECB884BE5F
                          09753826FF00FFFF00FFFF00FFFF00FFAEAEAEC8C8C8E7E7E7FCFCFCFFFFFFFB
                          FBFBFAFAFAFFFFFFFBFBFBD6D6D6ABABAB9F9F9FFF00FFFF00FFFF00FFFF00FF
                          FF00FFC06005C06005E49F5AEEBA86F2CAA0F0C599E4A768CC741E8E451A783A
                          27FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFAAAAAAAAAAAAC9C9C9D7D7D7DF
                          DFDFDDDDDDCDCDCDB4B4B4A3A3A3A0A0A0FF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFB65C0AB86012B96113B25A0FA24F0E8E451AFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9ACACACAC
                          ACACA9A9A9A6A6A6A3A3A3FF00FFFF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 3
                        OnClick = btnCancelarConClick
                      end
                      object btnExcluirCon: TBitBtn
                        Left = 363
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Excluir'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FFFF00FFFF00FF
                          26B7FF22B4FF34BDFF64D4FF77DCFF6EDCFF61DDFF67E1FF64DFFF44CFFF2FC1
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C4C4CACACADBDBDBE0E0E0DF
                          DFDFDFDFDFE2E2E2E0E0E0D5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF4EC8FF64D4FF9EE8FFB8F0FFA5EEFF8CECFF90EEFF8DEDFF70E1FF33C2
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D2D2D2DBDBDBEAEAEAF0F0F0EE
                          EEEEEAEAEAECECECEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF73D6FF61D3FF9AE6FFB5EFFF9EEBFF2034AB0202930202930202930202
                          93020293020293020293FF00FFFF00FFC6C6C6DDDDDDDADADAE9E9E9F0F0F0EC
                          ECEC888888828282828282828282828282828282828282828282FF00FFFF00FF
                          26B7FF8CDEFF5ED3FF96E5FFB0EDFF97E8FF0A0A987272F37575F46C6CF43E3E
                          E31616C50303AA020293FF00FFFF00FFC6C6C6E3E3E3D9D9D9E8E8E8EFEFEFEA
                          EAEA838383AAAAAAABABABA7A7A7919191868686838383828282FF00FFFF00FF
                          26B7FFA5E5FF59D2FF91E4FFABEAFF8EE4FF1C32AB0A0A980A0A980A0A980A0A
                          980A0A980A0A98020293FF00FFFF00FFC6C6C6EAEAEAD8D8D8E7E7E7ECECECE7
                          E7E7888888838383838383838383838383838383838383828282FF00FFFF00FF
                          26B7FFBEECFF5ED4FF8CE3FFA6EAFF85E1FF59D7FF58D6FF56D6FF46CDFF4CC8
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6F0F0F0DADADAE6E6E6ECECECE4
                          E4E4DBDBDBDADADADADADAD4D4D4D2D2D2C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF96DBFFC2EDFFD2F4FFD5F5FFD4F5FFD2F4FFD2F4FFD2F4FFCEF1FF92DB
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E4E1E4F0F0F0F6F6F6F6F6F6F6
                          F6F6F6F6F6F6F6F6F6F6F6F3F3F3E3E1E3C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF22B2FF2FBDFF4ACBFF58D3FF56D5FF51D8FF57DCFF54DAFF42CEFF2DBE
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C2C4CACACAD3D3D3D9D9D9DA
                          DADADBDBDBDDDDDDDCDCDCD4D4D4CAC8CAC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF47C4FF62D3FF9DE8FFB9F0FFA6EEFF8EECFF92EFFF8FEDFF71E2FF33C3
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D0D0D0DADADAEAEAEAF0F0F0EE
                          EEEEEAEAEAEDEDEDEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF70D5FF60D3FF9AE6FFB6EFFFA0ECFF83E7FF85E9FF82E8FF68DEFF32C1
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6DCDCDCDADADAE9E9E9F0F0F0EC
                          ECECE7E7E7E9E9E9E8E8E8E0E0E0CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF88DCFF5DD2FF96E5FFB1EDFF99E9FF77E2FF76E3FF74E2FF5ED9FF2EBE
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E2E2E2D9D9D9E8E8E8EFEFEFEA
                          EAEAE3E3E3E3E3E3E3E3E3DDDDDDCACACAC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFA1E4FF59D2FF91E3FFACEBFF90E5FF69DDFF68DDFF67DDFF53D3FF29BA
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E9E9E9D8D8D8E6E6E6EDEDEDE7
                          E7E7DFDFDFDFDFDFDFDFDFD8D8D8C8C8C8C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFBAEBFF54D2FF8CE3FFA7E9FF88E2FF5CD7FF5AD7FF58D7FF48CEFF39C0
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EFEFEFD8D8D8E6E6E6EBEBEBE5
                          E5E5DBDBDBDBDBDBDBDBDBD5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFAAE5FFC6F0FFC1F0FFCBF3FFC1F0FFB3ECFFB2ECFFB1ECFFBDEEFF9FE4
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EAEAEAF2F2F2F1F1F1F4F4F4F1
                          F1F1EEEEEEEEEEEEEEEEEEF0F0F0E9E9E9C6C6C6FF00FFFF00FFFF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 4
                        OnClick = btnExcluirConClick
                      end
                    end
                    object ed_con_email: TEdit
                      Left = 85
                      Top = 36
                      Width = 910
                      Height = 24
                      CharCase = ecLowerCase
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 150
                      ParentFont = False
                      TabOrder = 1
                    end
                    object ed_con_contato: TEdit
                      Left = 85
                      Top = 11
                      Width = 910
                      Height = 24
                      CharCase = ecUpperCase
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 150
                      ParentFont = False
                      TabOrder = 0
                    end
                    object ed_con_observacao: TEdit
                      Left = 85
                      Top = 61
                      Width = 910
                      Height = 24
                      CharCase = ecUpperCase
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 150
                      ParentFont = False
                      TabOrder = 2
                    end
                    object ed_con_telefone: TMaskEdit
                      Left = 85
                      Top = 86
                      Width = 160
                      Height = 24
                      EditMask = '+99 (99) 99999-9999;1; '
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 19
                      ParentFont = False
                      TabOrder = 3
                      Text = '+   (  )      -    '
                    end
                  end
                  object TPanel
                    Left = 3
                    Top = 164
                    Width = 1004
                    Height = 109
                    Align = alClient
                    BevelOuter = bvNone
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Padding.Left = 2
                    Padding.Top = 2
                    Padding.Right = 2
                    Padding.Bottom = 2
                    ParentFont = False
                    TabOrder = 1
                    object DBGContatos: TDBGrid
                      Left = 2
                      Top = 2
                      Width = 1000
                      Height = 105
                      Align = alClient
                      DataSource = DSContatos
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                      ParentFont = False
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -13
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnDrawColumnCell = DBGContatosDrawColumnCell
                      OnDblClick = DBGContatosDblClick
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'con_contato'
                          Title.Caption = 'Contnato'
                          Width = 300
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'con_email'
                          Title.Caption = 'e-Mail'
                          Width = 460
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'con_telefone'
                          Title.Caption = 'Telefone'
                          Width = 200
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'con_observacao'
                          Title.Caption = 'Observa'#231#227'o'
                          Width = 964
                          Visible = True
                        end>
                    end
                  end
                end
              end
              object tsFuncionarios: TTabSheet
                Caption = 'Dados do Funcion'#225'rio'
                ImageIndex = 5
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 1010
                  Height = 276
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Padding.Left = 2
                  Padding.Top = 2
                  Padding.Right = 2
                  Padding.Bottom = 2
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object Label14: TLabel
                    Left = 42
                    Top = 22
                    Width = 16
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'CT'
                  end
                  object Label15: TLabel
                    Left = 307
                    Top = 22
                    Width = 19
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'PIS'
                  end
                  object Label16: TLabel
                    Left = 17
                    Top = 48
                    Width = 41
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Fun'#231#227'o'
                  end
                  object Label17: TLabel
                    Left = 28
                    Top = 101
                    Width = 31
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Modo'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object Label18: TLabel
                    Left = 18
                    Top = 74
                    Width = 40
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Sal'#225'rio'
                  end
                  object Label19: TLabel
                    Left = 10
                    Top = 237
                    Width = 49
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Situa'#231#227'o'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object cmb_fun_modo: TComboBox
                    Left = 66
                    Top = 97
                    Width = 169
                    Height = 24
                    Style = csDropDownList
                    TabOrder = 4
                    Items.Strings = (
                      'DI'#193'RIO'
                      'HORAL'
                      'SEMANAL'
                      'MENSAL')
                  end
                  object cmb_fun_situacao: TComboBox
                    Left = 66
                    Top = 233
                    Width = 169
                    Height = 24
                    Style = csDropDownList
                    TabOrder = 5
                    Items.Strings = (
                      'ATIVO'
                      'INATIVO')
                  end
                  object ed_fun_salario: TDBEdit
                    Left = 66
                    Top = 70
                    Width = 169
                    Height = 24
                    DataField = 'fun_salario'
                    DataSource = DSFuncionarios
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 3
                  end
                  object ed_fun_ct: TEdit
                    Left = 66
                    Top = 18
                    Width = 169
                    Height = 24
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 20
                    NumbersOnly = True
                    ParentFont = False
                    TabOrder = 0
                  end
                  object ed_fun_pis: TEdit
                    Left = 333
                    Top = 18
                    Width = 169
                    Height = 24
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 20
                    NumbersOnly = True
                    ParentFont = False
                    TabOrder = 1
                  end
                  object ed_fun_funcao: TEdit
                    Left = 66
                    Top = 44
                    Width = 927
                    Height = 24
                    CharCase = ecUpperCase
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 150
                    ParentFont = False
                    TabOrder = 2
                  end
                end
              end
              object tsObservacoes: TTabSheet
                Caption = 'Observa'#231#245'es'
                ImageIndex = 3
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 1010
                  Height = 276
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object TGroupBox
                    Left = 1
                    Top = 1
                    Width = 1008
                    Height = 274
                    Align = alClient
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Padding.Left = 2
                    Padding.Top = 2
                    Padding.Right = 2
                    Padding.Bottom = 2
                    ParentFont = False
                    TabOrder = 0
                    object ed_ent_observacoes: TRichEdit
                      Left = 4
                      Top = 4
                      Width = 1000
                      Height = 266
                      Align = alBottom
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      WantReturns = False
                      OnEnter = ed_ent_observacoesEnter
                      OnExit = ed_ent_observacoesExit
                    end
                  end
                end
              end
              object tsContratos: TTabSheet
                Caption = 'Contratos'
                ImageIndex = 6
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 1010
                  Height = 276
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Padding.Left = 2
                  Padding.Top = 2
                  Padding.Right = 2
                  Padding.Bottom = 2
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object TGroupBox
                    Left = 3
                    Top = 3
                    Width = 1004
                    Height = 161
                    Align = alTop
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Padding.Left = 2
                    Padding.Top = 2
                    Padding.Right = 2
                    Padding.Bottom = 2
                    ParentFont = False
                    TabOrder = 0
                    object lbl_con_tipo: TLabel
                      Left = 864
                      Top = 11
                      Width = 25
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Tipo'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_ent_codigo_funcionario: TLabel
                      Left = 43
                      Top = 37
                      Width = 66
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Funcion'#225'rio'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_pro_codigo: TLabel
                      Left = 10
                      Top = 11
                      Width = 99
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Produto / Servi'#231'o'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_descricao: TLabel
                      Left = 54
                      Top = 63
                      Width = 55
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Descri'#231#227'o'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_vencimento_pagamento: TLabel
                      Left = 237
                      Top = 89
                      Width = 94
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Vcto. Pgto. (Dia)'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_valor: TLabel
                      Left = 79
                      Top = 89
                      Width = 30
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Valor'
                    end
                    object lbl_con_vencimento_contrato: TLabel
                      Left = 604
                      Top = 89
                      Width = 68
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Vcto. Contr.'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_horas_mes: TLabel
                      Left = 838
                      Top = 37
                      Width = 51
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'N'#186' Horas'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object lbl_con_situacao: TLabel
                      Left = 840
                      Top = 89
                      Width = 49
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Situa'#231#227'o'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object pnlBotoesContratos: TPanel
                      Left = 4
                      Top = 121
                      Width = 996
                      Height = 36
                      Align = alBottom
                      BevelInner = bvLowered
                      Padding.Left = 1
                      Padding.Top = 1
                      Padding.Right = 1
                      Padding.Bottom = 1
                      TabOrder = 9
                      object btnNovoContr: TBitBtn
                        Left = 3
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Novo'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF029B100099000599
                          10FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C68C8C8C8A8A8A8D8A8DFF00FFFF00FFFF00FFFF00FFFF00FF
                          26B7FF22B4FF34BDFF64D4FF77DCFF6EDCFF61DDFF67E1FF00990025B2280099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C4C4CACACADBDBDBE0E0E0DF
                          DFDFDFDFDFE2E2E28A8A8A9696968A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF4EC8FF64D4FF9EE8FFB8F0FFA5EEFF8CECFF90EEFF00990044C0460099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D2D2D2DBDBDBEAEAEAF0F0F0EE
                          EEEEEAEAEAECECEC8A8A8AA2A2A28A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF73D6FF61D3FF9AE6FFB5EFFF0A9E1000990000990000990061CE620099
                          00009900009900009900FF00FFFF00FFC6C6C6DDDDDDDADADAE9E9E9F0F0F08D
                          8D8D8A8A8A8A8A8A8A8A8AB0B0B08A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                          26B7FF8CDEFF5ED3FF96E5FFB0EDFF009900AAECABADEEADA0ED9F80E57F61D0
                          6037B63718A618009900FF00FFFF00FFC6C6C6E3E3E3D9D9D9E8E8E8EFEFEF8A
                          8A8AD7D7D7D9D9D9D4D4D4C5C5C5B0B0B09B9B9B9090908A8A8AFF00FFFF00FF
                          26B7FFA5E5FF59D2FF91E4FFABEAFF099E100099000099000099009EEB9F0099
                          00009900009900009900FF00FFFF00FFC6C6C6EAEAEAD8D8D8E7E7E7ECECEC8D
                          8D8D8A8A8A8A8A8A8A8A8AD3D3D38A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                          26B7FFBEECFF5ED4FF8CE3FFA6EAFF85E1FF59D7FF58D6FF009900A6EBA60099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6F0F0F0DADADAE6E6E6ECECECE4
                          E4E4DBDBDBDADADA8A8A8AD6D6D68A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF96DBFFC2EDFFD2F4FFD5F5FFD4F5FFD2F4FFD2F4FF0099009CE79E0099
                          0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E4E1E4F0F0F0F6F6F6F6F6F6F6
                          F6F6F6F6F6F6F6F68A8A8AD0D0D08A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF22B2FF2FBDFF4ACBFF58D3FF56D5FF51D8FF57DCFF059D10009900039B
                          1026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C2C4CACACAD3D3D3D9D9D9DA
                          DADADBDBDBDDDDDD8C8C8C8A8A8A8C8C8CC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF47C4FF62D3FF9DE8FFB9F0FFA6EEFF8EECFF92EFFF8FEDFF71E2FF33C3
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D0D0D0DADADAEAEAEAF0F0F0EE
                          EEEEEAEAEAEDEDEDEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF70D5FF60D3FF9AE6FFB6EFFFA0ECFF83E7FF85E9FF82E8FF68DEFF32C1
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6DCDCDCDADADAE9E9E9F0F0F0EC
                          ECECE7E7E7E9E9E9E8E8E8E0E0E0CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF88DCFF5DD2FF96E5FFB1EDFF99E9FF77E2FF76E3FF74E2FF5ED9FF2EBE
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E2E2E2D9D9D9E8E8E8EFEFEFEA
                          EAEAE3E3E3E3E3E3E3E3E3DDDDDDCACACAC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFA1E4FF59D2FF91E3FFACEBFF90E5FF69DDFF68DDFF67DDFF53D3FF29BA
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E9E9E9D8D8D8E6E6E6EDEDEDE7
                          E7E7DFDFDFDFDFDFDFDFDFD8D8D8C8C8C8C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFBAEBFF54D2FF8CE3FFA7E9FF88E2FF5CD7FF5AD7FF58D7FF48CEFF39C0
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EFEFEFD8D8D8E6E6E6EBEBEBE5
                          E5E5DBDBDBDBDBDBDBDBDBD5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFAAE5FFC6F0FFC1F0FFCBF3FFC1F0FFB3ECFFB2ECFFB1ECFFBDEEFF9FE4
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EAEAEAF2F2F2F1F1F1F4F4F4F1
                          F1F1EEEEEEEEEEEEEEEEEEF0F0F0E9E9E9C6C6C6FF00FFFF00FFFF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 0
                        OnClick = btnNovoContrClick
                      end
                      object btnGravarContr: TBitBtn
                        Left = 93
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Gravar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          006600006600B59A9BB59A9BB59A9BB59A9BB59A9BB59A9BB59A9B0066000066
                          00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8E8E8E8EBEBEBEBEBEBEBEBEBEBE
                          BEBEBEBEBEBEBEBEBEBEBE8686868E8E8EFF00FFFF00FFFF00FFFF00FF006600
                          009900009900E5DEDF006600006600E4E7E7E0E3E6D9DFE0CCC9CC006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB8B8B8AEAEAEECECEC838383838383F0
                          F0F0EEEEEEE8E8E8DADADA7E7E7E9A9A9A8E8E8EFF00FFFF00FFFF00FF006600
                          009900009900E9E2E2006600006600E2E1E3E2E6E8DDE2E4CFCCCF006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ACACACF0F0F0838383838383ED
                          EDEDF0F0F0ECECECDCDCDC7F7F7F9999998E8E8EFF00FFFF00FFFF00FF006600
                          009900009900ECE4E4006600006600DFDDDFE1E6E8E0E5E7D3D0D2006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ABABABF2F2F2838383838383EA
                          EAEAEFEFEFEEEEEEE0E0E07C7C7C9898988E8E8EFF00FFFF00FFFF00FF006600
                          009900009900EFE6E6EDE5E5E5DEDFE0DDDFDFE0E2E0E1E3D6D0D2006600037D
                          03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4AAAAAAF4F4F4F3F3F3ECECECEA
                          EAEAECECECECECECE1E1E18585859D9D9D8E8E8EFF00FFFF00FFFF00FF006600
                          0099000099000099000099000099000099000099000099000099000099000099
                          00006600FF00FFFF00FFFF00FF8E8E8EB1B1B1AFAFAFB2B2B2B8B8B8B6B6B6B1
                          B1B1AFAFAFB5B5B5B2B2B2ACACACB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B10099
                          00006600FF00FFFF00FFFF00FF8E8E8EA2A2A2B6B6B6CBCBCBD0D0D0D1D1D1D0
                          D0D0CECECECDCDCDD1D1D1D3D3D3B3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                          DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                          DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                          009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                          00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FFFF00FF
                          006600F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90066
                          00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 1
                        OnClick = btnGravarContrClick
                      end
                      object btnEditarContr: TBitBtn
                        Left = 273
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Editar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000130B0000130B00000000000000000000FF00FFFF00FF
                          FF00FF7F331B8833138A37128A3B168A3B16FF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA2A2A2A2A2A2A3A3A3A4A4A4A4
                          A4A4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF84351E
                          8E32107C321FFF00FFFF00FFFF00FF934112853F1EFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFA5A5A5A3A3A3A2A2A2FF00FFFF00FFFF00FFA5
                          A5A5A5A5A5FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF35221D35221D
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF8C4219984A13984A13FF00FFFF00
                          FFFF00FFFF00FFFF00FF8F8F8F8F8F8FFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFA5A5A5A7A7A7A7A7A7FF00FFFF00FFFF00FFFF00FFFF00FF35221D1C6378
                          0076A900699A004C88FF00FFFF00FFFF00FFFF00FFFF00FF9A4E15A8590FA55A
                          12A65C14B36810AA62148F8F8FA0A0A0A7A7A7A2A2A29D9D9DFF00FFFF00FFFF
                          00FFFF00FFFF00FFA8A8A8ABABABABABABACACACAFAFAFAEAEAEFF00FF02AAD8
                          00B6EA036C970930940B0983FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFB8B8B8BDBDBDA2A2A2A3A3A39E9E9EFF00FFFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0281AB
                          00BEEE03161707119C0C16980B0C83FF00FFFF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFA8A8A8BFBFBF858585A5A5A5A5A5A59F9F9FFF
                          00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF0281AB
                          056AF2061C7402000007119C0C17990B0C83FF00FFFF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFA8A8A8C2C2C2999999808080A5A5A5A5A5A59F
                          9F9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          0B1CBB2555FF061C7402000007119C0C169C0B0C82FF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFB1B1B1D2D2D2999999808080A5A5A5A7
                          A7A79E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF0F26BF2455FF061C7402000007119C0C169C0B0C83FF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3D1D1D1999999808080A5
                          A5A5A7A7A79F9F9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FF1028C22455FF061C7402000007119C0C169C0A0C83FF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB5B5B5D1D1D199999980
                          8080A5A5A5A7A7A79E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FF1129C42454FF061C7402000007119C0B169D0B0C83FF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB5B5B5D1D1D199
                          9999808080A5A5A5A7A7A79F9F9FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FF112CC92455FF061C7402000007119C07119C0A0C
                          83FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFB8B8B8D1
                          D1D1999999808080A5A5A5A5A5A59E9E9EFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FF132FCD2354FF061C7400000007119C999C
                          D9171895FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBA
                          BABAD1D1D1999999808080A5A5A5E9E9E9A8A8A8FF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF1531D11E4EFF061C74A3A19A7B7B
                          D700018FFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFBCBCBCCFCFCF999999DADADAE0E0E09F9F9FFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF1532D2ACBFFF6C76E10000
                          A6FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFBCBCBCF4F4F4DFDFDFA6A6A6FF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF2F31A4020EAAFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                          00FFFF00FFFF00FFB5B5B5A8A8A8FF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 2
                        OnClick = btnEditarContrClick
                      end
                      object btnCancelarContr: TBitBtn
                        Left = 363
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Cancelar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FFFF00FFFF00FF6D3327853C1395440D96450D873D12703425FF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9E9E9E9F9F9FA1
                          A1A1A1A1A19F9F9F9E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF70372AA04B0CCD7727E8AD70F3CCA1F4CDA3E9B176D07C2C7034256F35
                          29FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9F9F9FA4A4A4B6B6B6D0D0D0E0
                          E0E0E1E1E1D2D2D2B8B8B89E9E9E9F9F9FFF00FFFF00FFFF00FFFF00FFFF00FF
                          86411DC0620BF0C292FFFEFAFEFAF7F5E3D1F5E2D0FDF8F4FFFFFDF2C99EC669
                          117B3A21FF00FFFF00FFFF00FFFF00FFA2A2A2ACACACDBDBDBFEFEFEFCFCFCEF
                          EFEFEEEEEEFBFBFBFEFEFEDFDFDFAFAFAFA0A0A0FF00FFFF00FFFF00FF8C451C
                          C16107F7DBBDFFFEFEE0A46ACE6F15C96100C96100CD6D12DE9D5FFDFAF7FAE5
                          CCC6680D6F3528FF00FFFF00FFA4A4A4ABABABE9E9E9FEFEFECCCCCCB2B2B2AA
                          AAAAAAAAAAB1B1B1C9C9C9FCFCFCEFEFEFAEAEAE9E9E9EFF00FFFF00FF8C451C
                          ECBD8BFFFFFFDA8F44C75800C85E00C95F00C95F00C85D00C55600D58433FDFB
                          F8F3CB9F703525FF00FFFF00FFA4A4A4D8D8D8FFFFFFC1C1C1A8A8A8A9A9A9AA
                          AAAAAAAAAAA9A9A9A7A7A7BCBCBCFCFCFCDFDFDF9E9E9EFF00FFA04D10CE7721
                          FFFDFBE8B684CE6600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC65600DFA0
                          61FFFFFFCF7B28703525A5A5A5B6B6B6FEFEFED5D5D5ACACACFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA8A8A8CACACAFFFFFFB8B8B89E9E9EAF5507E5AA6F
                          FFFFFFDD8F3FD87B1CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC85C00CE6E
                          0DFDFAF6E9B175873D11A7A7A7CFCFCFFFFFFFC1C1C1B7B7B7FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA9A9A9B0B0B0FCFCFCD2D2D29F9F9FBB5F0AF0CAA1
                          FCF4EDE19343E08D38FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC95F00CA61
                          00F5E3D0F3CEA495440CABABABDFDFDFF9F9F9C3C3C3C0C0C0FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFAAAAAAAAAAAAEEEEEEE1E1E1A1A1A1C1650FF2CDA6
                          FDF7F0E9A158E89C4EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC95F00CA62
                          00F6E6D4F3CCA194440CADADADE1E1E1FAFAFAC9C9C9C7C7C7FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFAAAAAAABABABF0F0F0E0E0E0A1A1A1C1640DEEBC88
                          FFFFFFF1B77CEFA961FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC85D00CF70
                          10FEFCFAE7AC6D853B13ADADADD8D8D8FFFFFFD5D5D5CDCDCDFFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA9A9A9B2B2B2FDFDFDCFCFCF9E9E9EBF6006E5A059
                          FFFDFAFBE0C4F7B877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC75800E2AA
                          71FFFFFECC75206D3428AAAAAAC9C9C9FDFDFDECECECD4D4D4FFFFFFFFFFFFFF
                          FFFFFFFFFFFFFFFFFFFFFFA8A8A8CFCFCFFFFFFFB5B5B59E9E9EFF00FFC36204
                          FAD9B8FFFFFFFEDCB9F7B876EFA961E89C4EE08C38D87B1DCE6600DB924AFFFF
                          FFEFC08C6D3428FF00FFFF00FFABABABE8E8E8FFFFFFE9E9E9D4D4D4CDCDCDC7
                          C7C7BFBFBFB7B7B7ACACACC3C3C3FFFFFFDADADA9E9E9EFF00FFFF00FFC36204
                          E79E55FEEBD7FFFFFFFBDFC3F1B87CE9A159E29444DE9142E8B786FFFFFFF6D8
                          B7BE5F066B342CFF00FFFF00FFABABABC8C8C8F2F2F2FFFFFFECECECD5D5D5C9
                          C9C9C3C3C3C2C2C2D6D6D6FFFFFFE7E7E7AAAAAA9F9F9FFF00FFFF00FFFF00FF
                          C6670CE69E55FAD9B6FFFBF6FFFFFFFEF8F2FDF6EFFFFFFFFEF9F2ECB884BE5F
                          09753826FF00FFFF00FFFF00FFFF00FFAEAEAEC8C8C8E7E7E7FCFCFCFFFFFFFB
                          FBFBFAFAFAFFFFFFFBFBFBD6D6D6ABABAB9F9F9FFF00FFFF00FFFF00FFFF00FF
                          FF00FFC06005C06005E49F5AEEBA86F2CAA0F0C599E4A768CC741E8E451A783A
                          27FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFAAAAAAAAAAAAC9C9C9D7D7D7DF
                          DFDFDDDDDDCDCDCDB4B4B4A3A3A3A0A0A0FF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFB65C0AB86012B96113B25A0FA24F0E8E451AFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9ACACACAC
                          ACACA9A9A9A6A6A6A3A3A3FF00FFFF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 3
                        OnClick = btnCancelarContrClick
                      end
                      object btnExcluirContr: TBitBtn
                        Left = 453
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Excluir'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FFFF00FFFF00FF
                          26B7FF22B4FF34BDFF64D4FF77DCFF6EDCFF61DDFF67E1FF64DFFF44CFFF2FC1
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C4C4CACACADBDBDBE0E0E0DF
                          DFDFDFDFDFE2E2E2E0E0E0D5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF4EC8FF64D4FF9EE8FFB8F0FFA5EEFF8CECFF90EEFF8DEDFF70E1FF33C2
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D2D2D2DBDBDBEAEAEAF0F0F0EE
                          EEEEEAEAEAECECECEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF73D6FF61D3FF9AE6FFB5EFFF9EEBFF2034AB0202930202930202930202
                          93020293020293020293FF00FFFF00FFC6C6C6DDDDDDDADADAE9E9E9F0F0F0EC
                          ECEC888888828282828282828282828282828282828282828282FF00FFFF00FF
                          26B7FF8CDEFF5ED3FF96E5FFB0EDFF97E8FF0A0A987272F37575F46C6CF43E3E
                          E31616C50303AA020293FF00FFFF00FFC6C6C6E3E3E3D9D9D9E8E8E8EFEFEFEA
                          EAEA838383AAAAAAABABABA7A7A7919191868686838383828282FF00FFFF00FF
                          26B7FFA5E5FF59D2FF91E4FFABEAFF8EE4FF1C32AB0A0A980A0A980A0A980A0A
                          980A0A980A0A98020293FF00FFFF00FFC6C6C6EAEAEAD8D8D8E7E7E7ECECECE7
                          E7E7888888838383838383838383838383838383838383828282FF00FFFF00FF
                          26B7FFBEECFF5ED4FF8CE3FFA6EAFF85E1FF59D7FF58D6FF56D6FF46CDFF4CC8
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6F0F0F0DADADAE6E6E6ECECECE4
                          E4E4DBDBDBDADADADADADAD4D4D4D2D2D2C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF96DBFFC2EDFFD2F4FFD5F5FFD4F5FFD2F4FFD2F4FFD2F4FFCEF1FF92DB
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E4E1E4F0F0F0F6F6F6F6F6F6F6
                          F6F6F6F6F6F6F6F6F6F6F6F3F3F3E3E1E3C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF22B2FF2FBDFF4ACBFF58D3FF56D5FF51D8FF57DCFF54DAFF42CEFF2DBE
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C2C4CACACAD3D3D3D9D9D9DA
                          DADADBDBDBDDDDDDDCDCDCD4D4D4CAC8CAC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF47C4FF62D3FF9DE8FFB9F0FFA6EEFF8EECFF92EFFF8FEDFF71E2FF33C3
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D0D0D0DADADAEAEAEAF0F0F0EE
                          EEEEEAEAEAEDEDEDEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF70D5FF60D3FF9AE6FFB6EFFFA0ECFF83E7FF85E9FF82E8FF68DEFF32C1
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6DCDCDCDADADAE9E9E9F0F0F0EC
                          ECECE7E7E7E9E9E9E8E8E8E0E0E0CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FF88DCFF5DD2FF96E5FFB1EDFF99E9FF77E2FF76E3FF74E2FF5ED9FF2EBE
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E2E2E2D9D9D9E8E8E8EFEFEFEA
                          EAEAE3E3E3E3E3E3E3E3E3DDDDDDCACACAC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFA1E4FF59D2FF91E3FFACEBFF90E5FF69DDFF68DDFF67DDFF53D3FF29BA
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E9E9E9D8D8D8E6E6E6EDEDEDE7
                          E7E7DFDFDFDFDFDFDFDFDFD8D8D8C8C8C8C6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFBAEBFF54D2FF8CE3FFA7E9FF88E2FF5CD7FF5AD7FF58D7FF48CEFF39C0
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EFEFEFD8D8D8E6E6E6EBEBEBE5
                          E5E5DBDBDBDBDBDBDBDBDBD5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                          26B7FFAAE5FFC6F0FFC1F0FFCBF3FFC1F0FFB3ECFFB2ECFFB1ECFFBDEEFF9FE4
                          FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EAEAEAF2F2F2F1F1F1F4F4F4F1
                          F1F1EEEEEEEEEEEEEEEEEEF0F0F0E9E9E9C6C6C6FF00FFFF00FFFF00FFFF00FF
                          FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                          C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 4
                        OnClick = btnExcluirContrClick
                      end
                      object btnGerarCR: TBitBtn
                        Left = 774
                        Top = 3
                        Width = 219
                        Height = 30
                        Align = alRight
                        Caption = 'Gerar Contas '#224' Receber'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FFFF00FF16A7C416A7C4FF00FFFF00FFFF00FFFF00FF16A7C416A7C4FF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFFF
                          00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF16A7C473D8E677E0EC16A7C416A7C416A7C41BAEC937D5E627C8DC16A7
                          C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCDCDCDCE1E1E1BCBCBCBC
                          BCBCBCBCBCBFBFBFD6D6D6CDCDCDBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF16A7C498EAF389E7F174DFEC6DE1ED59D9E850DBEA41D8E833D4E616A7
                          C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCEAEAEAE7E7E7E0E0E0E1
                          E1E1DBDBDBDBDBDBD8D8D8D6D6D6BCBCBCFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF16A7C49FEBF48BE4EE6AD3E374D6E56DD5E44CCDDF47D5E637CFE116A7
                          C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCEBEBEBE5E5E5D8D8D8DB
                          DBDBDADADAD3D3D3D7D7D7D3D3D3BCBCBCFF00FFFF00FFFF00FFFF00FF16A7C4
                          16A7C46BD3E387DFEB8EDFEA16A7C416A7C416A7C416A7C461D5E53FD0E224BA
                          D216A7C416A7C4FF00FFFF00FFBCBCBCBCBCBCD8D8D8E2E2E2E2E2E2BCBCBCBC
                          BCBCBCBCBCBCBCBCD9D9D9D3D3D3C6C6C6BCBCBCBCBCBCFF00FF16A7C450CDE0
                          61D4E484E6F07DD8E616A7C4A7A7A7A7A7A7A7A7A7A7A7A716A7C45AD0E145D8
                          E82BC5DB22C1D716A7C4BCBCBCD3D3D3D8D8D8E6E6E6DDDDDDBCBCBCC0C0C0C0
                          C0C0C0C0C0C0C0C0BCBCBCD6D6D6D9D9D9CDCDCDC9C9C9BCBCBC16A7C462DFEC
                          6FE1EE64D5E416A7C4A7A7A7FEFEFEFDFDFDF7F7F7E5E5E5A7A7A716A7C445D2
                          E342D8E833D4E616A7C4BCBCBCDFDFDFE1E1E1D9D9D9BCBCBCC0C0C0FEFEFEFD
                          FDFDF9F9F9EBEBEBC0C0C0BCBCBCD6D6D6D8D8D8D6D6D6BCBCBC16A7C444CBDE
                          65DFEC54CEE016A7C4A7A7A7F9F9F9F9F9F9F4F4F4E5E5E5A7A7A716A7C443CB
                          DE4DDAE927BED516A7C4BCBCBCD1D1D1DFDFDFD4D4D4BCBCBCC0C0C0FAFAFAFA
                          FAFAF6F6F6EBEBEBC0C0C0BCBCBCD1D1D1DBDBDBC8C8C8BCBCBCFF00FF16A7C4
                          47CBDE4DCDDF16A7C4A7A7A7E8E8E8EEEEEEE9E9E9DADADAA7A7A716A7C44CCC
                          DF34BDD416A7C4FF00FFFF00FFBCBCBCD1D1D1D3D3D3BCBCBCC0C0C0EDEDEDF2
                          F2F2EEEEEEE3E3E3C0C0C0BCBCBCD3D3D3C9C9C9BCBCBCFF00FFFF00FFFF00FF
                          16A7C45FDEEC16A7C4A7A7A7BCBCBCD5D5D5D2D2D2B9B9B9A7A7A716A7C471E1
                          EE16A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCDEDEDEBCBCBCC0C0C0CDCDCDDF
                          DFDFDDDDDDCBCBCBC0C0C0BCBCBCE2E2E2BCBCBCFF00FFFF00FFFF00FFFF00FF
                          16A7C455DCEA4FD1E216A7C4A7A7A7A7A7A7A7A7A7A7A7A716A7C470D7E67CE4
                          EF16A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCDCDCDCD6D6D6BCBCBCC0C0C0C0
                          C0C0C0C0C0C0C0C0BCBCBCDBDBDBE4E4E4BCBCBCFF00FFFF00FFFF00FF16A7C4
                          40D7E84CDAE958DDEB52D1E216A7C416A7C416A7C416A7C48BE0EC95E9F286E6
                          F078E3EF16A7C4FF00FFFF00FFBCBCBCD8D8D8DBDBDBDDDDDDD6D6D6BCBCBCBC
                          BCBCBCBCBCBCBCBCE3E3E3E9E9E9E6E6E6E3E3E3BCBCBCFF00FFFF00FF16A7C4
                          33CEE13BD0E216A7C45CDDEB68E0ED74E2EE81E5F08DE8F190E5EF16A7C491E8
                          F27BE0EC16A7C4FF00FFFF00FFBCBCBCD2D2D2D3D3D3BCBCBCDDDDDDE0E0E0E3
                          E3E3E5E5E5E8E8E8E6E6E6BCBCBCE8E8E8E1E1E1BCBCBCFF00FFFF00FFFF00FF
                          16A7C416A7C4FF00FF16A7C416A7C46BE0ED77E3EF16A7C416A7C4FF00FF16A7
                          C416A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFBCBCBCBCBCBCE0
                          E0E0E3E3E3BCBCBCBCBCBCFF00FFBCBCBCBCBCBCFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FF16A7C462DEEC6EE1ED16A7C4FF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCDF
                          DFDFE1E1E1BCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FF16A7C416A7C4FF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBC
                          BCBCBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 5
                        Visible = False
                        OnClick = btnGerarCRClick
                      end
                      object btnRenovar: TBitBtn
                        Left = 183
                        Top = 3
                        Width = 90
                        Height = 30
                        Align = alLeft
                        Caption = 'Renovar'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6
                          A4C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3
                          B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFCFBFEFC
                          FBC2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3FCFCFCFCFCFCFCFCFCFCFCFCFC
                          FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCB3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FEFCFBFEFCFBFEFCFBFEFCFBD8EBD6018A02018A02D8EBD6FEFCFBFEFC
                          FBC2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3FCFCFCFCFCFCFCFCFCFCFCFCE0
                          E0E0959595959595E0E0E0FCFCFCFCFCFCB3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FEFBF7FEFBF7018A02D8EAD2018A02D8EAD2D8EAD2018A02FEFBF7FEFB
                          F7C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3FAFAFAFAFAFA959595DEDEDE95
                          9595DEDEDEDEDEDE959595FAFAFAFAFAFAB3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FEF9F4FEF9F4018A02018A02D8E8D0FEF9F4FEF9F4D8E8D0FEF9F4FEF9
                          F4C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F9F9F9F9F9F9959595959595DC
                          DCDCF9F9F9F9F9F9DCDCDCF9F9F9F9F9F9B3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FEF7F0FEF7F0018A02018A02018A02FEF7F0FEF7F0FEF7F0FEF7F0FEF7
                          F0C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F7F7F7F7F7F795959595959595
                          9595F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7B3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FEF5ECFEF5ECFEF5ECFEF5ECFEF5EC018A02018A02018A02FEF5ECFEF5
                          ECC2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F5F5F5F5F5F5F5F5F5F5F5F5F5
                          F5F5959595959595959595F5F5F5F5F5F5B3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FEF3E9FEF3E9D8E3C7FEF3E9FEF3E9D8E3C7018A02018A02FEF3E9FEF3
                          E9C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F3F3F3F3F3F3D5D5D5F3F3F3F3
                          F3F3D5D5D5959595959595F3F3F3F3F3F3B3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FFF1E5FFF1E5018A02D9E2C3D9E2C3018A02D9E2C3018A02FFF1E5FFF1
                          E5C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F2F2F2F2F2F2959595D2D2D2D2
                          D2D2959595D2D2D2959595F2F2F2F2F2F2B3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FFF0E2FFF0E2D9E1C1018A02018A02D9E1C1DDCFC2DDCFC2DDCFC2DDCF
                          C2C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3F0F0F0F0F0F0D1D1D195959595
                          9595D1D1D1CFCFCFCFCFCFCFCFCFCFCFCFB3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FFEEDEFFEEDEFFEEDEFFEEDEFFEEDEFFEEDEC5B5A9C3B4A8C2B3A7C1B2
                          A6C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3EEEEEEEEEEEEEEEEEEEEEEEEEE
                          EEEEEEEEEEB7B7B7B5B5B5B4B4B4B3B3B3B3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FFECDAFFECDAFFECDAFFECDAFFECDAFFECDAB0A296B0A296B0A296B0A2
                          96C2A6A4FF00FFFF00FFFF00FFFF00FFB3B3B3ECECECECECECECECECECECECEC
                          ECECECECECA3A3A3A3A3A3A3A3A3A3A3A3B3B3B3FF00FFFF00FFFF00FFFF00FF
                          C2A6A4FFEAD7FFEAD7FFEAD7FFEAD7FFEAD7C9B9ACFBF8F4FBF8F4E6DAD9C2A6
                          A4FF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3EBEBEBEBEBEBEBEBEBEBEBEBEB
                          EBEBBABABAF7F7F7F7F7F7DFDFDFB3B3B3FF00FFFF00FFFF00FFFF00FFFF00FF
                          C2A6A4FFE8D3FFE8D3FFE8D3FFE8D3FFE8D3C9B9ACFBF8F4DFCEC7C2A6A4FF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3E9E9E9E9E9E9E9E9E9E9E9E9E9
                          E9E9BABABAF7F7F7D3D3D3B3B3B3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          C2A6A4FFE6D0FFE6D0FFE6D0FFE6D0FFE6D0C9B9ACDFCEC7C2A6A4FF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3E7E7E7E7E7E7E7E7E7E7E7E7E7
                          E7E7BABABAD3D3D3B3B3B3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4C2A6A4FF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3
                          B3B3B3B3B3B3B3B3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 6
                        OnClick = btnRenovarClick
                      end
                      object btnTransferirHoras: TBitBtn
                        Left = 584
                        Top = 3
                        Width = 190
                        Height = 30
                        Align = alRight
                        Caption = 'Transferir Horas'
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = [fsBold]
                        Glyph.Data = {
                          36060000424D3606000000000000360000002800000020000000100000000100
                          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                          FF00FFFF00FF16A7C416A7C4FF00FFFF00FFFF00FFFF00FF16A7C416A7C4FF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFFF
                          00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF16A7C473D8E677E0EC16A7C416A7C416A7C41BAEC937D5E627C8DC16A7
                          C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCDCDCDCE1E1E1BCBCBCBC
                          BCBCBCBCBCBFBFBFD6D6D6CDCDCDBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF16A7C498EAF389E7F174DFEC6DE1ED59D9E850DBEA41D8E833D4E616A7
                          C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCEAEAEAE7E7E7E0E0E0E1
                          E1E1DBDBDBDBDBDBD8D8D8D6D6D6BCBCBCFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FF16A7C49FEBF48BE4EE6AD3E374D6E56DD5E44CCDDF47D5E637CFE116A7
                          C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCEBEBEBE5E5E5D8D8D8DB
                          DBDBDADADAD3D3D3D7D7D7D3D3D3BCBCBCFF00FFFF00FFFF00FFFF00FF16A7C4
                          16A7C46BD3E387DFEB8EDFEA16A7C416A7C416A7C416A7C461D5E53FD0E224BA
                          D216A7C416A7C4FF00FFFF00FFBCBCBCBCBCBCD8D8D8E2E2E2E2E2E2BCBCBCBC
                          BCBCBCBCBCBCBCBCD9D9D9D3D3D3C6C6C6BCBCBCBCBCBCFF00FF16A7C450CDE0
                          61D4E484E6F07DD8E616A7C4A7A7A7A7A7A7A7A7A7A7A7A716A7C45AD0E145D8
                          E82BC5DB22C1D716A7C4BCBCBCD3D3D3D8D8D8E6E6E6DDDDDDBCBCBCC0C0C0C0
                          C0C0C0C0C0C0C0C0BCBCBCD6D6D6D9D9D9CDCDCDC9C9C9BCBCBC16A7C462DFEC
                          6FE1EE64D5E416A7C4A7A7A7FEFEFEFDFDFDF7F7F7E5E5E5A7A7A716A7C445D2
                          E342D8E833D4E616A7C4BCBCBCDFDFDFE1E1E1D9D9D9BCBCBCC0C0C0FEFEFEFD
                          FDFDF9F9F9EBEBEBC0C0C0BCBCBCD6D6D6D8D8D8D6D6D6BCBCBC16A7C444CBDE
                          65DFEC54CEE016A7C4A7A7A7F9F9F9F9F9F9F4F4F4E5E5E5A7A7A716A7C443CB
                          DE4DDAE927BED516A7C4BCBCBCD1D1D1DFDFDFD4D4D4BCBCBCC0C0C0FAFAFAFA
                          FAFAF6F6F6EBEBEBC0C0C0BCBCBCD1D1D1DBDBDBC8C8C8BCBCBCFF00FF16A7C4
                          47CBDE4DCDDF16A7C4A7A7A7E8E8E8EEEEEEE9E9E9DADADAA7A7A716A7C44CCC
                          DF34BDD416A7C4FF00FFFF00FFBCBCBCD1D1D1D3D3D3BCBCBCC0C0C0EDEDEDF2
                          F2F2EEEEEEE3E3E3C0C0C0BCBCBCD3D3D3C9C9C9BCBCBCFF00FFFF00FFFF00FF
                          16A7C45FDEEC16A7C4A7A7A7BCBCBCD5D5D5D2D2D2B9B9B9A7A7A716A7C471E1
                          EE16A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCDEDEDEBCBCBCC0C0C0CDCDCDDF
                          DFDFDDDDDDCBCBCBC0C0C0BCBCBCE2E2E2BCBCBCFF00FFFF00FFFF00FFFF00FF
                          16A7C455DCEA4FD1E216A7C4A7A7A7A7A7A7A7A7A7A7A7A716A7C470D7E67CE4
                          EF16A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCDCDCDCD6D6D6BCBCBCC0C0C0C0
                          C0C0C0C0C0C0C0C0BCBCBCDBDBDBE4E4E4BCBCBCFF00FFFF00FFFF00FF16A7C4
                          40D7E84CDAE958DDEB52D1E216A7C416A7C416A7C416A7C48BE0EC95E9F286E6
                          F078E3EF16A7C4FF00FFFF00FFBCBCBCD8D8D8DBDBDBDDDDDDD6D6D6BCBCBCBC
                          BCBCBCBCBCBCBCBCE3E3E3E9E9E9E6E6E6E3E3E3BCBCBCFF00FFFF00FF16A7C4
                          33CEE13BD0E216A7C45CDDEB68E0ED74E2EE81E5F08DE8F190E5EF16A7C491E8
                          F27BE0EC16A7C4FF00FFFF00FFBCBCBCD2D2D2D3D3D3BCBCBCDDDDDDE0E0E0E3
                          E3E3E5E5E5E8E8E8E6E6E6BCBCBCE8E8E8E1E1E1BCBCBCFF00FFFF00FFFF00FF
                          16A7C416A7C4FF00FF16A7C416A7C46BE0ED77E3EF16A7C416A7C4FF00FF16A7
                          C416A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFBCBCBCBCBCBCE0
                          E0E0E3E3E3BCBCBCBCBCBCFF00FFBCBCBCBCBCBCFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FF16A7C462DEEC6EE1ED16A7C4FF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCDF
                          DFDFE1E1E1BCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                          FF00FFFF00FFFF00FFFF00FFFF00FF16A7C416A7C4FF00FFFF00FFFF00FFFF00
                          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBC
                          BCBCBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
                        NumGlyphs = 2
                        ParentFont = False
                        TabOrder = 7
                        OnClick = btnTransferirHorasClick
                      end
                    end
                    object cmb_con_tipo: TComboBox
                      Left = 895
                      Top = 7
                      Width = 100
                      Height = 24
                      Style = csDropDownList
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ItemIndex = 0
                      ParentFont = False
                      TabOrder = 5
                      Text = 'HORAL'
                      Items.Strings = (
                        'HORAL'
                        'SEMANAL'
                        'MENSAL'
                        'QUINZENAL')
                    end
                    object cmb_con_ent_codigo_funcionario: TComboBox
                      Left = 115
                      Top = 33
                      Width = 694
                      Height = 24
                      Style = csDropDownList
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 1
                      OnChange = cmb_end_uf_siglaChange
                    end
                    object cmb_con_pro_codigo: TComboBox
                      Left = 115
                      Top = 7
                      Width = 694
                      Height = 24
                      Style = csDropDownList
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                    end
                    object ed_con_descricao: TEdit
                      Left = 115
                      Top = 59
                      Width = 694
                      Height = 24
                      CharCase = ecUpperCase
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 150
                      ParentFont = False
                      TabOrder = 2
                    end
                    object gb_formas_pagamentos: TGroupBox
                      Left = 999
                      Top = 5
                      Width = 7
                      Height = 107
                      Caption = 'Formas de Pagamento'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      Padding.Left = 3
                      Padding.Right = 3
                      Padding.Bottom = 3
                      ParentFont = False
                      TabOrder = 8
                      Visible = False
                      object ckl_formas_pagamentos: TCheckListBox
                        Left = 5
                        Top = 18
                        Width = 5
                        Height = 84
                        Align = alClient
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -13
                        Font.Name = 'Tahoma'
                        Font.Style = []
                        Items.Strings = (
                          'DINHEIRO'
                          'CHEQUE'
                          'C. CR'#201'DITO'
                          'C. D'#201'BITO'
                          'BOLETO'
                          'DEP'#211'SITO'
                          'CREDI'#193'RIO'
                          'PERMUTA')
                        ParentFont = False
                        TabOrder = 0
                        OnClick = ckl_ent_tipoClick
                      end
                    end
                    object ed_con_vencimento_contrato: TDateTimePicker
                      Left = 679
                      Top = 85
                      Width = 130
                      Height = 24
                      Date = 43720.996172847220000000
                      Time = 43720.996172847220000000
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 4
                      OnExit = ed_ent_data_nascimentoExit
                    end
                    object ed_con_horas_mes: TEdit
                      Left = 895
                      Top = 33
                      Width = 100
                      Height = 24
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 4
                      ParentFont = False
                      TabOrder = 6
                      OnKeyPress = ed_con_horas_mesKeyPress
                    end
                    object cmb_con_situacao: TComboBox
                      Left = 895
                      Top = 85
                      Width = 100
                      Height = 24
                      Style = csDropDownList
                      Enabled = False
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ItemIndex = 0
                      ParentFont = False
                      TabOrder = 7
                      Text = 'EM ABERTO'
                      Items.Strings = (
                        'EM ABERTO'
                        'RECEBIDO'
                        'CONCLU'#205'DO')
                    end
                    object ed_con_vencimento_pagamento: TEdit
                      Left = 335
                      Top = 85
                      Width = 65
                      Height = 24
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 2
                      NumbersOnly = True
                      ParentFont = False
                      TabOrder = 3
                    end
                    object ed_con_valor: TEdit
                      Left = 115
                      Top = 85
                      Width = 104
                      Height = 24
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 10
                      OnKeyPress = ed_con_valorKeyPress
                    end
                    object ck_indicacao: TCheckBox
                      Left = 421
                      Top = 88
                      Width = 97
                      Height = 17
                      Caption = 'Indica'#231#227'o'
                      TabOrder = 11
                    end
                  end
                  object TPanel
                    Left = 3
                    Top = 164
                    Width = 1004
                    Height = 109
                    Align = alClient
                    BevelOuter = bvNone
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Padding.Left = 2
                    Padding.Top = 2
                    Padding.Right = 2
                    Padding.Bottom = 2
                    ParentFont = False
                    TabOrder = 1
                    object DBGContratos: TDBGrid
                      Left = 2
                      Top = 2
                      Width = 1000
                      Height = 105
                      Align = alClient
                      DataSource = DSContratos
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                      ParentFont = False
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -13
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      Columns = <
                        item
                          Alignment = taCenter
                          Expanded = False
                          FieldName = 'c_con_tipo'
                          Title.Alignment = taCenter
                          Title.Caption = 'Tipo'
                          Width = 100
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'c_pro_descricao'
                          Title.Caption = 'Produto / Servi'#231'o'
                          Width = 400
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'con_descricao'
                          Title.Caption = 'Descri'#231#227'o do Contrato'
                          Width = 460
                          Visible = True
                        end>
                    end
                  end
                  object pnlTransferir: TPanel
                    Left = 962
                    Top = -13
                    Width = 743
                    Height = 95
                    BevelInner = bvLowered
                    TabOrder = 2
                    Visible = False
                    object Label3: TLabel
                      Left = 26
                      Top = 21
                      Width = 42
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'Destino'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object Label4: TLabel
                      Left = 564
                      Top = 21
                      Width = 51
                      Height = 16
                      Alignment = taRightJustify
                      Caption = 'N'#186' Horas'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                    end
                    object cmbDestinoHoras: TComboBox
                      Left = 74
                      Top = 17
                      Width = 463
                      Height = 24
                      Style = csDropDownList
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      OnChange = cmb_end_uf_siglaChange
                    end
                    object btnConfirmarTransf: TBitBtn
                      Left = 16
                      Top = 52
                      Width = 110
                      Height = 30
                      Caption = 'Confirmar'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = [fsBold]
                      Glyph.Data = {
                        36060000424D3606000000000000360000002800000020000000100000000100
                        18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                        006600006600B59A9BB59A9BB59A9BB59A9BB59A9BB59A9BB59A9B0066000066
                        00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8E8E8E8EBEBEBEBEBEBEBEBEBEBE
                        BEBEBEBEBEBEBEBEBEBEBE8686868E8E8EFF00FFFF00FFFF00FFFF00FF006600
                        009900009900E5DEDF006600006600E4E7E7E0E3E6D9DFE0CCC9CC006600037D
                        03006600FF00FFFF00FFFF00FF8E8E8EB8B8B8AEAEAEECECEC838383838383F0
                        F0F0EEEEEEE8E8E8DADADA7E7E7E9A9A9A8E8E8EFF00FFFF00FFFF00FF006600
                        009900009900E9E2E2006600006600E2E1E3E2E6E8DDE2E4CFCCCF006600037D
                        03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ACACACF0F0F0838383838383ED
                        EDEDF0F0F0ECECECDCDCDC7F7F7F9999998E8E8EFF00FFFF00FFFF00FF006600
                        009900009900ECE4E4006600006600DFDDDFE1E6E8E0E5E7D3D0D2006600037D
                        03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ABABABF2F2F2838383838383EA
                        EAEAEFEFEFEEEEEEE0E0E07C7C7C9898988E8E8EFF00FFFF00FFFF00FF006600
                        009900009900EFE6E6EDE5E5E5DEDFE0DDDFDFE0E2E0E1E3D6D0D2006600037D
                        03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4AAAAAAF4F4F4F3F3F3ECECECEA
                        EAEAECECECECECECE1E1E18585859D9D9D8E8E8EFF00FFFF00FFFF00FF006600
                        0099000099000099000099000099000099000099000099000099000099000099
                        00006600FF00FFFF00FFFF00FF8E8E8EB1B1B1AFAFAFB2B2B2B8B8B8B6B6B6B1
                        B1B1AFAFAFB5B5B5B2B2B2ACACACB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                        009900B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B10099
                        00006600FF00FFFF00FFFF00FF8E8E8EA2A2A2B6B6B6CBCBCBD0D0D0D1D1D1D0
                        D0D0CECECECDCDCDD1D1D1D3D3D3B3B3B38E8E8EFF00FFFF00FFFF00FF006600
                        009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                        00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                        009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                        00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                        009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                        00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                        DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                        009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                        00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                        009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                        00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                        DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                        009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                        00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FFFF00FF
                        006600F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90066
                        00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EFF00FFFF00FFFF00FFFF00FFFF00FF
                        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
                      NumGlyphs = 2
                      ParentFont = False
                      TabOrder = 1
                      OnClick = btnConfirmarTransfClick
                    end
                    object btnCancelarTransf: TBitBtn
                      Left = 126
                      Top = 52
                      Width = 110
                      Height = 30
                      Caption = 'Cancelar'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = [fsBold]
                      Glyph.Data = {
                        36060000424D3606000000000000360000002800000020000000100000000100
                        18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                        FF00FFFF00FFFF00FF6D3327853C1395440D96450D873D12703425FF00FFFF00
                        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9E9E9E9F9F9FA1
                        A1A1A1A1A19F9F9F9E9E9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                        FF00FF70372AA04B0CCD7727E8AD70F3CCA1F4CDA3E9B176D07C2C7034256F35
                        29FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9F9F9FA4A4A4B6B6B6D0D0D0E0
                        E0E0E1E1E1D2D2D2B8B8B89E9E9E9F9F9FFF00FFFF00FFFF00FFFF00FFFF00FF
                        86411DC0620BF0C292FFFEFAFEFAF7F5E3D1F5E2D0FDF8F4FFFFFDF2C99EC669
                        117B3A21FF00FFFF00FFFF00FFFF00FFA2A2A2ACACACDBDBDBFEFEFEFCFCFCEF
                        EFEFEEEEEEFBFBFBFEFEFEDFDFDFAFAFAFA0A0A0FF00FFFF00FFFF00FF8C451C
                        C16107F7DBBDFFFEFEE0A46ACE6F15C96100C96100CD6D12DE9D5FFDFAF7FAE5
                        CCC6680D6F3528FF00FFFF00FFA4A4A4ABABABE9E9E9FEFEFECCCCCCB2B2B2AA
                        AAAAAAAAAAB1B1B1C9C9C9FCFCFCEFEFEFAEAEAE9E9E9EFF00FFFF00FF8C451C
                        ECBD8BFFFFFFDA8F44C75800C85E00C95F00C95F00C85D00C55600D58433FDFB
                        F8F3CB9F703525FF00FFFF00FFA4A4A4D8D8D8FFFFFFC1C1C1A8A8A8A9A9A9AA
                        AAAAAAAAAAA9A9A9A7A7A7BCBCBCFCFCFCDFDFDF9E9E9EFF00FFA04D10CE7721
                        FFFDFBE8B684CE6600FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC65600DFA0
                        61FFFFFFCF7B28703525A5A5A5B6B6B6FEFEFED5D5D5ACACACFFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFA8A8A8CACACAFFFFFFB8B8B89E9E9EAF5507E5AA6F
                        FFFFFFDD8F3FD87B1CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC85C00CE6E
                        0DFDFAF6E9B175873D11A7A7A7CFCFCFFFFFFFC1C1C1B7B7B7FFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFA9A9A9B0B0B0FCFCFCD2D2D29F9F9FBB5F0AF0CAA1
                        FCF4EDE19343E08D38FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC95F00CA61
                        00F5E3D0F3CEA495440CABABABDFDFDFF9F9F9C3C3C3C0C0C0FFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFAAAAAAAAAAAAEEEEEEE1E1E1A1A1A1C1650FF2CDA6
                        FDF7F0E9A158E89C4EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC95F00CA62
                        00F6E6D4F3CCA194440CADADADE1E1E1FAFAFAC9C9C9C7C7C7FFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFAAAAAAABABABF0F0F0E0E0E0A1A1A1C1640DEEBC88
                        FFFFFFF1B77CEFA961FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC85D00CF70
                        10FEFCFAE7AC6D853B13ADADADD8D8D8FFFFFFD5D5D5CDCDCDFFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFA9A9A9B2B2B2FDFDFDCFCFCF9E9E9EBF6006E5A059
                        FFFDFAFBE0C4F7B877FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC75800E2AA
                        71FFFFFECC75206D3428AAAAAAC9C9C9FDFDFDECECECD4D4D4FFFFFFFFFFFFFF
                        FFFFFFFFFFFFFFFFFFFFFFA8A8A8CFCFCFFFFFFFB5B5B59E9E9EFF00FFC36204
                        FAD9B8FFFFFFFEDCB9F7B876EFA961E89C4EE08C38D87B1DCE6600DB924AFFFF
                        FFEFC08C6D3428FF00FFFF00FFABABABE8E8E8FFFFFFE9E9E9D4D4D4CDCDCDC7
                        C7C7BFBFBFB7B7B7ACACACC3C3C3FFFFFFDADADA9E9E9EFF00FFFF00FFC36204
                        E79E55FEEBD7FFFFFFFBDFC3F1B87CE9A159E29444DE9142E8B786FFFFFFF6D8
                        B7BE5F066B342CFF00FFFF00FFABABABC8C8C8F2F2F2FFFFFFECECECD5D5D5C9
                        C9C9C3C3C3C2C2C2D6D6D6FFFFFFE7E7E7AAAAAA9F9F9FFF00FFFF00FFFF00FF
                        C6670CE69E55FAD9B6FFFBF6FFFFFFFEF8F2FDF6EFFFFFFFFEF9F2ECB884BE5F
                        09753826FF00FFFF00FFFF00FFFF00FFAEAEAEC8C8C8E7E7E7FCFCFCFFFFFFFB
                        FBFBFAFAFAFFFFFFFBFBFBD6D6D6ABABAB9F9F9FFF00FFFF00FFFF00FFFF00FF
                        FF00FFC06005C06005E49F5AEEBA86F2CAA0F0C599E4A768CC741E8E451A783A
                        27FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFAAAAAAAAAAAAC9C9C9D7D7D7DF
                        DFDFDDDDDDCDCDCDB4B4B4A3A3A3A0A0A0FF00FFFF00FFFF00FFFF00FFFF00FF
                        FF00FFFF00FFFF00FFB65C0AB86012B96113B25A0FA24F0E8E451AFF00FFFF00
                        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9ACACACAC
                        ACACA9A9A9A6A6A6A3A3A3FF00FFFF00FFFF00FFFF00FFFF00FF}
                      NumGlyphs = 2
                      ParentFont = False
                      TabOrder = 2
                      OnClick = btnCancelarTransfClick
                    end
                    object ed_horas_transf: TEdit
                      Left = 621
                      Top = 17
                      Width = 100
                      Height = 24
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      MaxLength = 4
                      NumbersOnly = True
                      ParentFont = False
                      TabOrder = 3
                    end
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  inherited SB: TStatusBar
    Top = 555
    Width = 1042
    ExplicitTop = 555
    ExplicitWidth = 1042
  end
  object CDSEntidades: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROEntidades'
    Left = 679
    Top = 142
    object CDSEntidadesent_codigo: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ent_codigo'
      Origin = 'ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CDSEntidadesent_emp_codigo: TIntegerField
      DisplayLabel = 'Emp.'
      FieldName = 'ent_emp_codigo'
      Origin = 'ent_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CDSEntidadesent_razao_social: TStringField
      DisplayLabel = 'Raz'#227'o Social'
      FieldName = 'ent_razao_social'
      Origin = 'ent_razao_social'
      Size = 150
    end
    object CDSEntidadesent_nome_fantasia: TStringField
      DisplayLabel = 'Nome Fantasia'
      FieldName = 'ent_nome_fantasia'
      Origin = 'ent_nome_fantasia'
      Size = 150
    end
    object CDSEntidadesent_tipo_pessoa: TIntegerField
      DisplayLabel = 'Tipo Pessoa'
      FieldName = 'ent_tipo_pessoa'
      Origin = 'ent_tipo_pessoa'
    end
    object CDSEntidadesent_cnpj: TStringField
      DisplayLabel = 'CNPJ'
      FieldName = 'ent_cnpj'
      Origin = 'ent_cnpj'
    end
    object CDSEntidadesent_ie: TStringField
      DisplayLabel = 'Insc. Estadual'
      FieldName = 'ent_ie'
      Origin = 'ent_ie'
    end
    object CDSEntidadesent_im: TStringField
      DisplayLabel = 'Insc. Municipal'
      FieldName = 'ent_im'
      Origin = 'ent_im'
    end
    object CDSEntidadesent_ir: TStringField
      DisplayLabel = 'Insc. Rural'
      FieldName = 'ent_ir'
      Origin = 'ent_ir'
    end
    object CDSEntidadesent_suframa: TStringField
      DisplayLabel = 'SUFRAMA'
      FieldName = 'ent_suframa'
      Origin = 'ent_suframa'
    end
    object CDSEntidadesent_ramo_codigo: TIntegerField
      DisplayLabel = 'Ramo Atividade'
      FieldName = 'ent_ramo_codigo'
      Origin = 'ent_ramo_codigo'
    end
    object CDSEntidadesent_cpf: TStringField
      DisplayLabel = 'CPF'
      FieldName = 'ent_cpf'
      Origin = 'ent_cpf'
    end
    object CDSEntidadesent_rg: TStringField
      DisplayLabel = 'RG'
      FieldName = 'ent_rg'
      Origin = 'ent_rg'
    end
    object CDSEntidadesent_data_nascimento: TDateField
      DisplayLabel = 'Nascimento'
      FieldName = 'ent_data_nascimento'
      Origin = 'ent_data_nascimento'
    end
    object CDSEntidadesent_ocupacao: TStringField
      DisplayLabel = 'Ocupa'#231#227'o'
      FieldName = 'ent_ocupacao'
      Origin = 'ent_ocupacao'
      Size = 50
    end
    object CDSEntidadesent_estado_civil: TStringField
      DisplayLabel = 'Estado Civil'
      FieldName = 'ent_estado_civil'
      Origin = 'ent_estado_civil'
    end
    object CDSEntidadesent_sit_codigo: TIntegerField
      DisplayLabel = 'Situacao'
      FieldName = 'ent_sit_codigo'
      Origin = 'ent_sit_codigo'
    end
    object CDSEntidadesent_observacoes: TBlobField
      FieldName = 'ent_observacoes'
      Origin = 'ent_observacoes'
    end
  end
  object PROEntidades: TDataSetProvider
    DataSet = FDQEntidades
    Left = 679
    Top = 114
  end
  object DSEntidades: TDataSource
    DataSet = CDSEntidades
    Left = 679
    Top = 170
  end
  object FDQEntidades: TFDQuery
    SQL.Strings = (
      'select *'
      'from entidades')
    Left = 679
    Top = 86
    object FDQEntidadesent_codigo: TIntegerField
      FieldName = 'ent_codigo'
      Origin = 'ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQEntidadesent_emp_codigo: TIntegerField
      FieldName = 'ent_emp_codigo'
      Origin = 'ent_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQEntidadesent_razao_social: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_razao_social'
      Origin = 'ent_razao_social'
      Size = 150
    end
    object FDQEntidadesent_nome_fantasia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_nome_fantasia'
      Origin = 'ent_nome_fantasia'
      Size = 150
    end
    object FDQEntidadesent_tipo_pessoa: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ent_tipo_pessoa'
      Origin = 'ent_tipo_pessoa'
    end
    object FDQEntidadesent_cnpj: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_cnpj'
      Origin = 'ent_cnpj'
    end
    object FDQEntidadesent_ie: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_ie'
      Origin = 'ent_ie'
    end
    object FDQEntidadesent_im: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_im'
      Origin = 'ent_im'
    end
    object FDQEntidadesent_ir: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_ir'
      Origin = 'ent_ir'
    end
    object FDQEntidadesent_suframa: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_suframa'
      Origin = 'ent_suframa'
    end
    object FDQEntidadesent_ramo_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ent_ramo_codigo'
      Origin = 'ent_ramo_codigo'
    end
    object FDQEntidadesent_cpf: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_cpf'
      Origin = 'ent_cpf'
    end
    object FDQEntidadesent_rg: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_rg'
      Origin = 'ent_rg'
    end
    object FDQEntidadesent_data_nascimento: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'ent_data_nascimento'
      Origin = 'ent_data_nascimento'
    end
    object FDQEntidadesent_ocupacao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_ocupacao'
      Origin = 'ent_ocupacao'
      Size = 50
    end
    object FDQEntidadesent_estado_civil: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_estado_civil'
      Origin = 'ent_estado_civil'
    end
    object FDQEntidadesent_sit_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ent_sit_codigo'
      Origin = 'ent_sit_codigo'
    end
    object FDQEntidadesent_observacoes: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ent_observacoes'
      Origin = 'ent_observacoes'
    end
  end
  object CDSEnderecos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROEnderecos'
    OnCalcFields = CDSEnderecosCalcFields
    Left = 707
    Top = 142
    object CDSEnderecosend_codigo: TIntegerField
      FieldName = 'end_codigo'
      Origin = 'end_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CDSEnderecosend_ent_codigo: TIntegerField
      FieldName = 'end_ent_codigo'
      Origin = 'end_ent_codigo'
    end
    object CDSEnderecosend_ent_emp_codigo: TIntegerField
      FieldName = 'end_ent_emp_codigo'
      Origin = 'end_ent_emp_codigo'
    end
    object CDSEnderecosend_tipo: TIntegerField
      FieldName = 'end_tipo'
      Origin = 'end_tipo'
    end
    object CDSEnderecosend_cep: TStringField
      FieldName = 'end_cep'
      Origin = 'end_cep'
    end
    object CDSEnderecosend_uf_sigla: TStringField
      FieldName = 'end_uf_sigla'
      Origin = 'end_uf_sigla'
      Size = 3
    end
    object CDSEnderecosend_cid_codigo: TIntegerField
      FieldName = 'end_cid_codigo'
      Origin = 'end_cid_codigo'
    end
    object CDSEnderecosend_logradouro: TStringField
      FieldName = 'end_logradouro'
      Origin = 'end_logradouro'
      Size = 150
    end
    object CDSEnderecosend_numero: TIntegerField
      FieldName = 'end_numero'
      Origin = 'end_numero'
    end
    object CDSEnderecosend_complemento: TStringField
      FieldName = 'end_complemento'
      Origin = 'end_complemento'
      Size = 150
    end
    object CDSEnderecosend_descricao_tipo: TStringField
      FieldKind = fkCalculated
      FieldName = 'end_descricao_tipo'
      Size = 15
      Calculated = True
    end
    object CDSEnderecoscid_nome: TStringField
      FieldKind = fkCalculated
      FieldName = 'cid_nome'
      Size = 150
      Calculated = True
    end
  end
  object DSEnderecos: TDataSource
    DataSet = CDSEnderecos
    Left = 707
    Top = 170
  end
  object PROEnderecos: TDataSetProvider
    DataSet = FDQEnderecos
    Left = 707
    Top = 114
  end
  object FDQEnderecos: TFDQuery
    SQL.Strings = (
      'select *'
      'from enderecos')
    Left = 707
    Top = 86
    object FDQEnderecosend_codigo: TIntegerField
      FieldName = 'end_codigo'
      Origin = 'end_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQEnderecosend_ent_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'end_ent_codigo'
      Origin = 'end_ent_codigo'
    end
    object FDQEnderecosend_ent_emp_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'end_ent_emp_codigo'
      Origin = 'end_ent_emp_codigo'
    end
    object FDQEnderecosend_tipo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'end_tipo'
      Origin = 'end_tipo'
    end
    object FDQEnderecosend_cep: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'end_cep'
      Origin = 'end_cep'
    end
    object FDQEnderecosend_uf_sigla: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'end_uf_sigla'
      Origin = 'end_uf_sigla'
      Size = 3
    end
    object FDQEnderecosend_cid_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'end_cid_codigo'
      Origin = 'end_cid_codigo'
    end
    object FDQEnderecosend_logradouro: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'end_logradouro'
      Origin = 'end_logradouro'
      Size = 150
    end
    object FDQEnderecosend_numero: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'end_numero'
      Origin = 'end_numero'
    end
    object FDQEnderecosend_complemento: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'end_complemento'
      Origin = 'end_complemento'
      Size = 150
    end
  end
  object FDQContatos: TFDQuery
    SQL.Strings = (
      'select *'
      'from contatos')
    Left = 735
    Top = 86
    object FDQContatoscon_codigo: TIntegerField
      FieldName = 'con_codigo'
      Origin = 'con_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQContatoscon_ent_codigo: TIntegerField
      FieldName = 'con_ent_codigo'
      Origin = 'con_ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQContatoscon_ent_emp_codigo: TIntegerField
      FieldName = 'con_ent_emp_codigo'
      Origin = 'con_ent_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQContatoscon_email: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'con_email'
      Origin = 'con_email'
      Size = 150
    end
    object FDQContatoscon_contato: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'con_contato'
      Origin = 'con_contato'
      Size = 60
    end
    object FDQContatoscon_telefone: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'con_telefone'
      Origin = 'con_telefone'
      Size = 25
    end
    object FDQContatoscon_observacao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'con_observacao'
      Origin = 'con_observacao'
      Size = 150
    end
  end
  object PROContatos: TDataSetProvider
    DataSet = FDQContatos
    Left = 735
    Top = 114
  end
  object CDSContatos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROContatos'
    Left = 735
    Top = 142
    object CDSContatoscon_codigo: TIntegerField
      FieldName = 'con_codigo'
      Origin = 'con_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CDSContatoscon_ent_codigo: TIntegerField
      FieldName = 'con_ent_codigo'
      Origin = 'con_ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CDSContatoscon_ent_emp_codigo: TIntegerField
      FieldName = 'con_ent_emp_codigo'
      Origin = 'con_ent_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object CDSContatoscon_email: TStringField
      FieldName = 'con_email'
      Origin = 'con_email'
      Size = 150
    end
    object CDSContatoscon_contato: TStringField
      FieldName = 'con_contato'
      Origin = 'con_contato'
      Size = 60
    end
    object CDSContatoscon_telefone: TStringField
      FieldName = 'con_telefone'
      Origin = 'con_telefone'
      Size = 25
    end
    object CDSContatoscon_observacao: TStringField
      FieldName = 'con_observacao'
      Origin = 'con_observacao'
      Size = 150
    end
  end
  object DSContatos: TDataSource
    DataSet = CDSContatos
    Left = 735
    Top = 170
  end
  object FDQTipos: TFDQuery
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select *'
      'from entidades_tipos')
    Left = 763
    Top = 86
    object FDQTiposetp_ent_codigo: TIntegerField
      FieldName = 'etp_ent_codigo'
      Origin = 'etp_ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQTiposetp_ent_emp_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'etp_ent_emp_codigo'
      Origin = 'etp_ent_emp_codigo'
    end
    object FDQTiposetp_tipo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'etp_tipo'
      Origin = 'etp_tipo'
    end
  end
  object CDSTipos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROTipos'
    Left = 763
    Top = 142
    object CDSTiposetp_ent_codigo: TIntegerField
      FieldName = 'etp_ent_codigo'
      Required = True
    end
    object CDSTiposetp_ent_emp_codigo: TIntegerField
      FieldName = 'etp_ent_emp_codigo'
    end
    object CDSTiposetp_tipo: TIntegerField
      FieldName = 'etp_tipo'
    end
  end
  object PROTipos: TDataSetProvider
    DataSet = FDQTipos
    Left = 764
    Top = 114
  end
  object DSTipos: TDataSource
    DataSet = CDSTipos
    Left = 763
    Top = 170
  end
  object FDQFuncionarios: TFDQuery
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select *'
      'from funcionarios')
    Left = 791
    Top = 86
    object FDQFuncionariosfun_ent_codigo: TIntegerField
      FieldName = 'fun_ent_codigo'
      Origin = 'fun_ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQFuncionariosfun_ent_emp_codigo: TIntegerField
      FieldName = 'fun_ent_emp_codigo'
      Origin = 'fun_ent_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQFuncionariosfun_ct: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_ct'
      Origin = 'fun_ct'
    end
    object FDQFuncionariosfun_pis: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_pis'
      Origin = 'fun_pis'
    end
    object FDQFuncionariosfun_funcao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_funcao'
      Origin = 'fun_funcao'
      Size = 150
    end
    object FDQFuncionariosfun_salario: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'fun_salario'
      Origin = 'fun_salario'
      Precision = 15
    end
    object FDQFuncionariosfun_modo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'fun_modo'
      Origin = 'fun_modo'
    end
    object FDQFuncionariosfun_situacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'fun_situacao'
      Origin = 'fun_situacao'
    end
  end
  object CDSFuncionarios: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROFuncionarios'
    BeforePost = CDSFuncionariosBeforePost
    Left = 791
    Top = 142
    object CDSFuncionariosfun_ent_codigo: TIntegerField
      FieldName = 'fun_ent_codigo'
      Required = True
    end
    object CDSFuncionariosfun_ent_emp_codigo: TIntegerField
      FieldName = 'fun_ent_emp_codigo'
      Required = True
    end
    object CDSFuncionariosfun_ct: TStringField
      FieldName = 'fun_ct'
    end
    object CDSFuncionariosfun_pis: TStringField
      FieldName = 'fun_pis'
    end
    object CDSFuncionariosfun_funcao: TStringField
      FieldName = 'fun_funcao'
      Size = 150
    end
    object CDSFuncionariosfun_salario: TBCDField
      FieldName = 'fun_salario'
      Precision = 15
    end
    object CDSFuncionariosfun_modo: TIntegerField
      FieldName = 'fun_modo'
    end
    object CDSFuncionariosfun_situacao: TIntegerField
      FieldName = 'fun_situacao'
    end
  end
  object PROFuncionarios: TDataSetProvider
    DataSet = FDQFuncionarios
    Left = 791
    Top = 114
  end
  object DSFuncionarios: TDataSource
    DataSet = CDSFuncionarios
    Left = 791
    Top = 170
  end
  object FDQContratos: TFDQuery
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select *'
      'from contratos')
    Left = 819
    Top = 86
    object FDQContratoscon_codigo: TIntegerField
      FieldName = 'con_codigo'
      Origin = 'con_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQContratoscon_ent_codigo: TIntegerField
      FieldName = 'con_ent_codigo'
      Origin = 'con_ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQContratoscon_ent_emp_codigo: TIntegerField
      FieldName = 'con_ent_emp_codigo'
      Origin = 'con_ent_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQContratoscon_tipo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'con_tipo'
      Origin = 'con_tipo'
    end
    object FDQContratoscon_pro_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'con_pro_codigo'
      Origin = 'con_pro_codigo'
    end
    object FDQContratoscon_pro_emp_codigo: TIntegerField
      FieldName = 'con_pro_emp_codigo'
    end
    object FDQContratoscon_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'con_descricao'
      Origin = 'con_descricao'
      Size = 250
    end
    object FDQContratoscon_ent_codigo_funcionario: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'con_ent_codigo_funcionario'
      Origin = 'con_ent_codigo_funcionario'
    end
    object FDQContratoscon_ent_emp_codigo_funcionario: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'con_ent_emp_codigo_funcionario'
      Origin = 'con_ent_emp_codigo_funcionario'
    end
    object FDQContratoscon_valor: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'con_valor'
      Origin = 'con_valor'
      Precision = 15
    end
    object FDQContratoscon_vencimento_pagamento: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'con_vencimento_pagamento'
      Origin = 'con_vencimento_pagamento'
    end
    object FDQContratoscon_vencimento_contrato: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'con_vencimento_contrato'
      Origin = 'con_vencimento_contrato'
    end
    object FDQContratoscon_contrato: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'con_contrato'
      Origin = 'con_contrato'
    end
    object FDQContratoscon_situacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'con_situacao'
      Origin = 'con_situacao'
    end
    object FDQContratoscon_flag_cr: TIntegerField
      FieldName = 'con_flag_cr'
    end
    object FDQContratoscon_horas_mes: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'con_horas_mes'
      Origin = 'con_horas_mes'
      Precision = 14
      Size = 2
    end
  end
  object CDSContratos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROContratos'
    AfterScroll = CDSContratosAfterScroll
    OnCalcFields = CDSContratosCalcFields
    Left = 819
    Top = 142
    object CDSContratoscon_codigo: TIntegerField
      FieldName = 'con_codigo'
      Required = True
    end
    object CDSContratoscon_ent_codigo: TIntegerField
      FieldName = 'con_ent_codigo'
      Required = True
    end
    object CDSContratoscon_ent_emp_codigo: TIntegerField
      FieldName = 'con_ent_emp_codigo'
      Required = True
    end
    object CDSContratoscon_tipo: TIntegerField
      FieldName = 'con_tipo'
    end
    object CDSContratoscon_pro_codigo: TIntegerField
      FieldName = 'con_pro_codigo'
    end
    object CDSContratoscon_pro_emp_codigo: TIntegerField
      FieldName = 'con_pro_emp_codigo'
    end
    object CDSContratoscon_descricao: TStringField
      FieldName = 'con_descricao'
      Size = 250
    end
    object CDSContratoscon_ent_codigo_funcionario: TIntegerField
      FieldName = 'con_ent_codigo_funcionario'
    end
    object CDSContratoscon_ent_emp_codigo_funcionario: TIntegerField
      FieldName = 'con_ent_emp_codigo_funcionario'
    end
    object CDSContratoscon_valor: TBCDField
      FieldName = 'con_valor'
      Precision = 15
    end
    object CDSContratoscon_vencimento_pagamento: TIntegerField
      FieldName = 'con_vencimento_pagamento'
    end
    object CDSContratoscon_vencimento_contrato: TDateField
      FieldName = 'con_vencimento_contrato'
    end
    object CDSContratoscon_contrato: TBlobField
      FieldName = 'con_contrato'
    end
    object CDSContratoscon_situacao: TIntegerField
      FieldName = 'con_situacao'
    end
    object CDSContratosc_pro_descricao: TStringField
      FieldKind = fkCalculated
      FieldName = 'c_pro_descricao'
      Size = 150
      Calculated = True
    end
    object CDSContratosc_ent_razao_social: TStringField
      FieldKind = fkCalculated
      FieldName = 'c_ent_razao_social'
      Size = 150
      Calculated = True
    end
    object CDSContratosc_con_tipo: TStringField
      FieldKind = fkCalculated
      FieldName = 'c_con_tipo'
      Size = 15
      Calculated = True
    end
    object CDSContratoscon_flag_cr: TIntegerField
      FieldName = 'con_flag_cr'
    end
    object CDSContratoscon_horas_mes: TBCDField
      FieldName = 'con_horas_mes'
      Precision = 14
      Size = 2
    end
  end
  object PROContratos: TDataSetProvider
    DataSet = FDQContratos
    Left = 819
    Top = 114
  end
  object DSContratos: TDataSource
    DataSet = CDSContratos
    Left = 819
    Top = 170
  end
  object FDQFormasPagamentosContratos: TFDQuery
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select *'
      #10'from formas_pagamentos_contratos')
    Left = 847
    Top = 86
    object FDQFormasPagamentosContratosfpc_codigo: TIntegerField
      FieldName = 'fpc_codigo'
      Origin = 'fpc_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQFormasPagamentosContratosfpc_con_codigo: TIntegerField
      FieldName = 'fpc_con_codigo'
      Origin = 'fpc_con_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQFormasPagamentosContratosfpc_con_ent_codigo: TIntegerField
      FieldName = 'fpc_con_ent_codigo'
      Origin = 'fpc_con_ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQFormasPagamentosContratosfpc_con_ent_emp_codigo: TIntegerField
      FieldName = 'fpc_con_ent_emp_codigo'
      Origin = 'fpc_con_ent_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
  end
  object CDSFormasPagamentosContratos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROFormasPagamentosContratos'
    Left = 847
    Top = 142
    object CDSFormasPagamentosContratosfpc_codigo: TIntegerField
      FieldName = 'fpc_codigo'
      Required = True
    end
    object CDSFormasPagamentosContratosfpc_con_codigo: TIntegerField
      FieldName = 'fpc_con_codigo'
      Required = True
    end
    object CDSFormasPagamentosContratosfpc_con_ent_codigo: TIntegerField
      FieldName = 'fpc_con_ent_codigo'
      Required = True
    end
    object CDSFormasPagamentosContratosfpc_con_ent_emp_codigo: TIntegerField
      FieldName = 'fpc_con_ent_emp_codigo'
      Required = True
    end
  end
  object PROFormasPagamentosContratos: TDataSetProvider
    DataSet = FDQFormasPagamentosContratos
    Left = 847
    Top = 114
  end
  object DSFormasPagamentosContratos: TDataSource
    DataSet = CDSFormasPagamentosContratos
    Left = 847
    Top = 170
  end
  object PopOpcoes: TPopupMenu
    Left = 651
    Top = 86
    object meImprimirSaldoDeAulas: TMenuItem
      Caption = 'Imprimir Listagem de Saldo de Aulas'
      OnClick = meImprimirSaldoDeAulasClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ImprimirSaldoemContratosxSaldoemAulas1: TMenuItem
      Caption = 'Imprimir Saldo em Contratos x Saldo em Aulas'
      OnClick = ImprimirSaldoemContratosxSaldoemAulas1Click
    end
  end
  object Relatorio: TfrxReport
    Version = '4.13.2'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44748.972800914400000000
    ReportOptions.LastChange = 44749.811036354200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 407
    Top = 230
    Datasets = <
      item
        DataSet = frxDBSaldoAulasCont
        DataSetName = 'frxDBSaldoAulasCont'
      end
      item
        DataSet = frxDBSaldoContratos
        DataSetName = 'frxDBSaldoContratos'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 30.236240000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo10: TfrxMemoView
          Align = baCenter
          Left = 0.000231890000000012
          Width = 718.110236220000000000
          Height = 30.236240000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'RELA'#199#195'O CONTRATOS x AULAS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        DataSet = frxDBSaldoContratos
        DataSetName = 'frxDBSaldoContratos'
        RowCount = 0
        object frxDBSaldoContratosDESCRICAO: TfrxMemoView
          Width = 393.070949130000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DESCRICAO'
          DataSet = frxDBSaldoContratos
          DataSetName = 'frxDBSaldoContratos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDBSaldoContratos."DESCRICAO"]')
          ParentFont = False
        end
        object frxDBSaldoContratosVENCIMENTO: TfrxMemoView
          Left = 393.071120000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'VENCIMENTO'
          DataSet = frxDBSaldoContratos
          DataSetName = 'frxDBSaldoContratos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBSaldoContratos."VENCIMENTO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBSaldoContratosVALOR: TfrxMemoView
          Left = 585.827150000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'VALOR'
          DataSet = frxDBSaldoContratos
          DataSetName = 'frxDBSaldoContratos'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBSaldoContratos."VALOR"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBSaldoContratosHORAS: TfrxMemoView
          Left = 506.457020000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'HORAS'
          DataSet = frxDBSaldoContratos
          DataSetName = 'frxDBSaldoContratos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBSaldoContratos."HORAS"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 11.338582677165400000
        Top = 532.913730000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Align = baCenter
          Left = 0.000231890000000012
          Width = 718.110236220000000000
          Height = 11.338582677165400000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'RELA'#199#195'O CONTRATOS x AULAS')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 37.795300000000000000
        Top = 188.976500000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'DESCRI'#199#195'O')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 393.071120000000000000
          Top = 18.897650000000000000
          Width = 113.385875590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 506.457020000000000000
          Top = 18.897650000000000000
          Width = 79.370105590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'HORAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 585.827150000000000000
          Top = 18.897650000000000000
          Width = 132.283525590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'VALOR')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'CONTRATOS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        Height = 37.795275590000000000
        Top = 291.023810000000000000
        Width = 718.110700000000000000
        object Memo7: TfrxMemoView
          Left = 585.827150000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          DataSet = frxDBSaldoContratos
          DataSetName = 'frxDBSaldoContratos'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDBSaldoContratos."VALOR">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 506.457020000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          DataSet = frxDBSaldoContratos
          DataSetName = 'frxDBSaldoContratos'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDBSaldoContratos."HORAS">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 393.071120000000000000
          Width = 113.385875590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 56.692940240000000000
        Top = 71.811070000000000000
        Width = 718.110700000000000000
        object SysMemo1: TfrxSysMemoView
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          ShowHint = False
          AutoWidth = True
          DisplayFormat.FormatStr = 'dddddd'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[DATE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Align = baCenter
          Left = 0.000231889764000015
          Top = 11.338582677165400000
          Width = 718.110236220472000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object SysMemo2: TfrxSysMemoView
          Left = 623.622450000000000000
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'g. [PAGE#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBSaldoContratosENT_RAZAO_SOCIAL: TfrxMemoView
          Left = 68.031540000000000000
          Top = 26.456710000000000000
          Width = 650.079160000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'ENT_RAZAO_SOCIAL'
          DataSet = frxDBSaldoContratos
          DataSetName = 'frxDBSaldoContratos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDBSaldoContratos."ENT_RAZAO_SOCIAL"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Top = 26.456710000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'ALUNO:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header2: TfrxHeader
        Height = 37.795300000000000000
        Top = 351.496290000000000000
        Width = 718.110700000000000000
        object Memo12: TfrxMemoView
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'AGENDA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'PROFESSOR')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 393.071120000000000000
          Top = 18.897650000000000000
          Width = 113.385875590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'DIA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 506.457020000000000000
          Top = 18.897650000000000000
          Width = 79.370105590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'HORAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 585.827150000000000000
          Top = 18.897650000000000000
          Width = 132.283525590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData2: TfrxMasterData
        Height = 18.897650000000000000
        Top = 411.968770000000000000
        Width = 718.110700000000000000
        DataSet = frxDBSaldoAulasCont
        DataSetName = 'frxDBSaldoAulasCont'
        RowCount = 0
        object Memo17: TfrxMemoView
          Left = 585.827150000000000000
          Width = 132.283525590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBSaldoAulasContprofessor: TfrxMemoView
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'professor'
          DataSet = frxDBSaldoAulasCont
          DataSetName = 'frxDBSaldoAulasCont'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDBSaldoAulasCont."professor"]')
          ParentFont = False
          SuppressRepeated = True
          VAlign = vaCenter
        end
        object frxDBSaldoAulasContdia: TfrxMemoView
          Left = 393.071120000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'dia'
          DataSet = frxDBSaldoAulasCont
          DataSetName = 'frxDBSaldoAulasCont'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBSaldoAulasCont."dia"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBSaldoAulasConthoras: TfrxMemoView
          Left = 506.457020000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'horas'
          DataSet = frxDBSaldoAulasCont
          DataSetName = 'frxDBSaldoAulasCont'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBSaldoAulasCont."horas"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer2: TfrxFooter
        Height = 18.897650000000000000
        Top = 453.543600000000000000
        Width = 718.110700000000000000
        object Memo18: TfrxMemoView
          Left = 393.071120000000000000
          Width = 113.385875590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 506.457020000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          DataSet = frxDBSaldoAulasCont
          DataSetName = 'frxDBSaldoAulasCont'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDBSaldoAulasCont."horas">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 585.827150000000000000
          Width = 132.283525590000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object FDQSaldoAulas: TFDQuery
    SQL.Strings = (
      
        'select x.ent_codigo, x.ent_emp_codigo, x.ent_razao_social, '#10'    ' +
        '   '
      '       x.agd_qtde_horas, x.con_horas_mes, '#10'       '
      
        '       (x.con_horas_mes - x.agd_qtde_horas) horas_faltantes,'#10'   ' +
        '    '
      '       x.sit_descricao       '
      
        #10'from (  SELECT e.ent_codigo, e.ent_emp_codigo, e.ent_razao_soci' +
        'al, '#10'               '
      '               se.sit_descricao,'#10#9#9#9'   '
      '               sum(a.agd_qtde_horas) agd_qtde_horas,'#10#9#9#9'   '
      '               (select sum(c.con_horas_mes)'#10#9#9#9#9
      '                from personal_english.contratos c'#10#9#9#9#9
      
        '                where c.con_ent_codigo     = a.agd_ent_codigo'#10#9#9 +
        #9#9'  '
      
        '                  and c.con_ent_emp_codigo = a.agd_ent_emp_codig' +
        'o) con_horas_mes'#10#9#9
      '           FROM agendamentos a'#10#9#9
      
        '           inner join entidades e on e.ent_codigo     = a.agd_en' +
        't_codigo '
      '        and'#10#9#9#9#9#9#9#9#9#9#9#9#9'  '
      
        ' e.ent_emp_codigo = a.agd_ent_emp_codigo'#10#9#9'inner join personal_e' +
        'nglish.situacoes_entidade se on se.sit_codigo = e.ent_sit_codigo' +
        '    '#10'        where a.agd_data < sysdate()'#10#9#9'group by e.ent_codig' +
        'o, e.ent_emp_codigo, e.ent_razao_social, se.sit_descricao  ) x')
    Left = 435
    Top = 230
    object FDQSaldoAulasent_codigo: TIntegerField
      FieldName = 'ent_codigo'
      Origin = 'ent_codigo'
      Required = True
    end
    object FDQSaldoAulasent_emp_codigo: TIntegerField
      FieldName = 'ent_emp_codigo'
      Origin = 'ent_emp_codigo'
      Required = True
    end
    object FDQSaldoAulasent_razao_social: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_razao_social'
      Origin = 'ent_razao_social'
      Size = 150
    end
    object FDQSaldoAulasagd_qtde_horas: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'agd_qtde_horas'
      Origin = 'agd_qtde_horas'
      Precision = 37
      Size = 4
    end
    object FDQSaldoAulascon_horas_mes: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'con_horas_mes'
      Origin = 'con_horas_mes'
      Precision = 32
      Size = 0
    end
    object FDQSaldoAulashoras_faltantes: TFMTBCDField
      AutoGenerateValue = arDefault
      FieldName = 'horas_faltantes'
      Origin = 'horas_faltantes'
      Precision = 38
      Size = 4
    end
    object FDQSaldoAulassit_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sit_descricao'
      Origin = 'sit_descricao'
      Size = 15
    end
  end
  object frxDBSaldoAulas: TfrxDBDataset
    UserName = 'frxDBSaldoAulas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ent_codigo=ent_codigo'
      'ent_emp_codigo=ent_emp_codigo'
      'ent_razao_social=ent_razao_social'
      'agd_qtde_horas=agd_qtde_horas'
      'con_horas_mes=con_horas_mes'
      'horas_faltantes=horas_faltantes'
      'sit_descricao=sit_descricao')
    DataSet = FDQSaldoAulas
    BCDToCurrency = False
    Left = 435
    Top = 258
  end
  object cdsExclusaoContrato: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 623
    Top = 86
    object cdsExclusaoContratocon_codigo: TIntegerField
      FieldName = 'con_codigo'
    end
  end
  object FDQSaldoContratos: TFDQuery
    SQL.Strings = (
      
        'select c.con_descricao descricao, c.con_valor valor,            ' +
        '        '
      
        '       date_format(c.con_vencimento_contrato, '#39'%d/%m/%Y'#39') vencim' +
        'ento, '
      
        '       c.con_horas_mes horas, e.ent_razao_social                ' +
        '        '
      
        'from contratos c                                                ' +
        '        '
      
        'inner join entidades e on e.ent_codigo     = c.con_ent_codigo an' +
        'd       '
      
        '                          e.ent_emp_codigo = c.con_ent_emp_codig' +
        'o       '
      'where c.con_ent_codigo = 235                             '
      
        'order by c.con_vencimento_contrato                              ' +
        '        ')
    Left = 463
    Top = 230
    object FDQSaldoContratosdescricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'descricao'
      Origin = 'con_descricao'
      Size = 250
    end
    object FDQSaldoContratosvalor: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'valor'
      Origin = 'con_valor'
      Precision = 15
    end
    object FDQSaldoContratosvencimento: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'vencimento'
      Origin = 'vencimento'
      ProviderFlags = []
      ReadOnly = True
      Size = 10
    end
    object FDQSaldoContratoshoras: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'horas'
      Origin = 'con_horas_mes'
      Precision = 14
      Size = 2
    end
    object FDQSaldoContratosent_razao_social: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_razao_social'
      Origin = 'ent_razao_social'
      ProviderFlags = []
      ReadOnly = True
      Size = 150
    end
  end
  object frxDBSaldoContratos: TfrxDBDataset
    UserName = 'frxDBSaldoContratos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'descricao=DESCRICAO'
      'valor=VALOR'
      'vencimento=VENCIMENTO'
      'horas=HORAS'
      'ent_razao_social=ENT_RAZAO_SOCIAL')
    DataSet = FDQSaldoContratos
    BCDToCurrency = False
    Left = 463
    Top = 258
  end
  object frxDBSaldoAulasCont: TfrxDBDataset
    UserName = 'frxDBSaldoAulasCont'
    CloseDataSource = False
    FieldAliases.Strings = (
      'professor=professor'
      'dia=dia'
      'horas=horas')
    DataSet = FDQSaldoAulasCont
    BCDToCurrency = False
    Left = 491
    Top = 258
  end
  object FDQSaldoAulasCont: TFDQuery
    SQL.Strings = (
      
        'select e.ent_razao_social professor, date_format(a.agd_data, '#39'%d' +
        '/%m/%Y'#39') dia, '
      
        '       a.agd_qtde_horas horas                                   ' +
        '                '
      
        'from agendamentos_entidade ae                                   ' +
        '                '
      
        'inner join agendamentos a on a.agd_codigo     = ae.agd_codigo an' +
        'd               '
      
        '               a.agd_emp_codigo = ae.agd_emp_codigo             ' +
        '                '
      
        'inner join entidades e on e.ent_codigo     = a.agd_fun_ent_codig' +
        'o and           '
      
        '                          e.ent_emp_codigo = a.agd_fun_ent_emp_c' +
        'odigo           '
      
        'where ae.ent_codigo = 235                                       ' +
        '     '
      
        'order by e.ent_razao_social, a.agd_data                         ' +
        '                ')
    Left = 491
    Top = 230
    object FDQSaldoAulasContprofessor: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'professor'
      Origin = 'professor'
      Size = 150
    end
    object FDQSaldoAulasContdia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'dia'
      Origin = 'dia'
      Size = 10
    end
    object FDQSaldoAulasConthoras: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'horas'
      Origin = 'horas'
      Precision = 15
    end
  end
end
