unit Aux_Padrao;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons, Vcl.ComCtrls;

type
  TFrmAux_Padrao = class(TForm)
    mainPainel: TPanel;
    pnlButtons: TPanel;
    SB: TStatusBar;
    btnExcluir: TBitBtn;
    btnCancelar: TBitBtn;
    btnEditar: TBitBtn;
    btnGravar: TBitBtn;
    btnNovo: TBitBtn;
    btnSair: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ProcessKey(var Msg : TMsg; var Handled : Boolean);
  end;

var
  FrmAux_Padrao: TFrmAux_Padrao;

implementation

{$R *.dfm}

{ TFrmAux_Padrao }

procedure TFrmAux_Padrao.btnSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFrmAux_Padrao.FormCreate(Sender: TObject);
begin
  Application.OnMessage := ProcessKey;

  SB.Panels[0].Text := FormatDateTime('dddddd', Now);
end;

procedure TFrmAux_Padrao.ProcessKey(var Msg: TMsg; var Handled: Boolean);
begin
  if Msg.message = WM_KEYDOWN then begin
    if Msg.wParam = VK_RETURN then begin
      Msg.wParam := VK_CLEAR;

      keybd_event(VK_TAB, 0, 0, 0);
    end;
  end;
end;

end.
