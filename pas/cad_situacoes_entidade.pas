unit cad_situacoes_entidade;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider,
  Data.DB, Datasnap.DBClient, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  Vcl.Grids, Vcl.DBGrids, cls_utils;

type
  TFrmCad_Situacoes_Entidade = class(TFrmAux_Padrao)
    PC: TPageControl;
    tsConsulta: TTabSheet;
    pnlConsulta: TPanel;
    Panel1: TPanel;
    Label1: TLabel;
    cmbCampos: TComboBox;
    ed_consulta: TEdit;
    dbgSituacoes: TDBGrid;
    tsCadastro: TTabSheet;
    pnlCadastro: TPanel;
    gbCodigo: TGroupBox;
    ed_sit_codigo: TEdit;
    FDQSituacoes: TFDQuery;
    CDSSituacoes: TClientDataSet;
    DSSituacoes: TDataSource;
    PROSituacoes: TDataSetProvider;
    FDQSituacoessit_codigo: TFDAutoIncField;
    FDQSituacoessit_descricao: TStringField;
    CDSSituacoessit_codigo: TAutoIncField;
    CDSSituacoessit_descricao: TStringField;
    GroupBox1: TGroupBox;
    ed_sit_descricao: TEdit;
    Label2: TLabel;
    procedure btnNovoClick(Sender: TObject);
    procedure ed_consultaChange(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    v_operacao : TOperacaoQuery;
    procedure proc_limpa_campos;
  public
    { Public declarations }
  end;

var
  FrmCad_Situacoes_Entidade: TFrmCad_Situacoes_Entidade;

implementation

{$R *.dfm}

uses dm_conexao, cls_situacoes_entidade;

procedure TFrmCad_Situacoes_Entidade.btnCancelarClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos;

  tsCadastro.TabVisible := False;
  tsConsulta.TabVisible := True;

  ed_consulta.Clear;
  ed_consulta.SetFocus;

  proc_atualiza_operacao_query(TForm(Self), statNavegando, CDSSituacoes);
end;

procedure TFrmCad_Situacoes_Entidade.btnEditarClick(Sender: TObject);
begin
  inherited;
  tsCadastro.TabVisible := True;
  tsConsulta.TabVisible := False;

  ed_sit_codigo.Text      := CDSSituacoessit_codigo.AsString;
  ed_sit_descricao.Text   := CDSSituacoessit_descricao.AsString;

  ed_sit_descricao.SetFocus;
  ed_sit_descricao.SelectAll;

  v_operacao := statEditando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSSituacoes);
end;

procedure TFrmCad_Situacoes_Entidade.btnGravarClick(Sender: TObject);
var c_situacoes : TSituacoesEntidade;
begin
  inherited;
  try
    c_situacoes := TSituacoesEntidade.Create;

    c_situacoes.sit_codigo    := iif((v_operacao = statInserindo), 0, CDSSituacoessit_codigo.AsInteger);
    c_situacoes.sit_descricao := ed_sit_descricao.Text;

    try
      Conexao.FDConexao.StartTransaction;

      c_situacoes.proc_gravar(c_situacoes.sit_codigo);

      Conexao.FDConexao.Commit;

      btnCancelar.Click;
      CDSSituacoes.Locate('sit_codigo', c_situacoes.sit_codigo, [loCaseInsensitive]);
    except
      on E : Exception do begin
        Conexao.FDConexao.Rollback;
        Application.MessageBox(PChar(E.Message), PChar(Self.Caption), MB_OK + MB_ICONERROR);
        Exit;
      end;
    end;
  finally
    FreeAndNil(c_situacoes);
  end;
end;

procedure TFrmCad_Situacoes_Entidade.btnNovoClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos;

  tsCadastro.TabVisible := True;
  tsConsulta.TabVisible := False;

  ed_sit_descricao.SetFocus;

  v_operacao := statInserindo;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSSituacoes);
end;

procedure TFrmCad_Situacoes_Entidade.ed_consultaChange(Sender: TObject);
var c_situacoes : TSituacoesEntidade;
begin
  inherited;
  try
    c_situacoes := TSituacoesEntidade.Create;
    c_situacoes.proc_consultar(FDQSituacoes, CDSSituacoes, cmbCampos, ed_consulta.Text);
  finally
    FreeAndNil(c_situacoes);
  end;
end;

procedure TFrmCad_Situacoes_Entidade.FormActivate(Sender: TObject);
begin
  inherited;
  btnCancelar.Click;
end;

procedure TFrmCad_Situacoes_Entidade.proc_limpa_campos;
begin
  ed_sit_codigo.Clear;
  ed_sit_descricao.Clear;
end;

end.
