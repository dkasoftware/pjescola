unit cls_contas_a_receber;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type TContasReceber = Class(TObject)
  private
    { Private declarations }
    f_cr_codigo         : Integer;
    f_cr_emp_codigo     : Integer;
    f_cr_ent_codigo     : Integer;
    f_cr_ent_emp_codigo : Integer;
    f_cr_con_codigo     : Integer;
    f_cr_data           : TDateTime;
    f_cr_observacao     : String;
    f_cr_situacao       : Integer;
    f_cr_valor          : Double;
  public
    { Public declarations }
    procedure proc_gravar(PCodigo, PEmpresa : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PEntCodigo, PEntEmpresa, PSituacao : Integer; PDataIni, PDataFin : TDateTime);

    function func_recuperar(PCodigo, PEmpresa : Integer) : Boolean;
  published
    { Published declarations }
    property cr_codigo         : Integer   read f_cr_codigo         write f_cr_codigo;
    property cr_emp_codigo     : Integer   read f_cr_emp_codigo     write f_cr_emp_codigo;
    property cr_ent_codigo     : Integer   read f_cr_ent_codigo     write f_cr_ent_codigo;
    property cr_ent_emp_codigo : Integer   read f_cr_ent_emp_codigo write f_cr_ent_emp_codigo;
    property cr_con_codigo     : Integer   read f_cr_con_codigo     write f_cr_con_codigo;
    property cr_data           : TDateTime read f_cr_data           write f_cr_data;
    property cr_observacao     : String    read f_cr_observacao     write f_cr_observacao;
    property cr_situacao       : Integer   read f_cr_situacao       write f_cr_situacao;
    property cr_valor          : Double    read f_cr_valor          write f_cr_valor;
  end;


implementation

{ TContasReceber }

function TContasReceber.func_recuperar(PCodigo, PEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                 '+
                   ' from contas_a_receber c                  '+
                   ' where c.cr_codigo     = :p_cr_codigo     '+
                   '   and c.cr_emp_codigo = :p_cr_emp_codigo ');
    qryAux.ParamByName('p_cr_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_cr_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    cr_codigo         := qryAux.FieldByName('cr_codigo').AsInteger;
    cr_emp_codigo     := qryAux.FieldByName('cr_emp_codigo').AsInteger;
    cr_ent_codigo     := qryAux.FieldByName('cr_ent_codigo').AsInteger;
    cr_ent_emp_codigo := qryAux.FieldByName('cr_ent_emp_codigo').AsInteger;
    cr_con_codigo     := qryAux.FieldByName('cr_con_codigo').AsInteger;
    cr_data           := qryAux.FieldByName('cr_data').AsDateTime;
    cr_observacao     := qryAux.FieldByName('cr_observacao').AsString;
    cr_situacao       := qryAux.FieldByName('cr_situacao').AsInteger;
    cr_valor          := qryAux.FieldByName('cr_valor').AsFloat;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TContasReceber.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PEntCodigo, PEntEmpresa, PSituacao: Integer;
  PDataIni, PDataFin: TDateTime);
begin
  PQuery.Connection := Conexao.FDConexao;

  PClient.Close;
  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                                   '+
                 ' from contas_a_receber c                    '+
                 ' where date(c.cr_data) >= date(:p_data_ini) '+
                 '   and date(c.cr_data) <= date(:p_data_fin) ');

  if PEntCodigo > 0 then begin
    PQuery.SQL.Add(' and c.cr_ent_codigo     = :p_cr_ent_codigo     '+
                   ' and c.cr_ent_emp_codigo = :p_cr_ent_emp_codigo ');
    PQuery.ParamByName('p_cr_ent_codigo').AsInteger     := PEntCodigo;
    PQuery.ParamByName('p_cr_ent_emp_codigo').AsInteger := PEntEmpresa;
  end;

  case PSituacao of
    0 : PQuery.SQL.Add(' and c.cr_situacao = 0 ');
    1 : PQuery.SQL.Add(' and c.cr_situacao = 1 ');
  end;

  PQuery.ParamByName('p_data_ini').AsDateTime := PDataIni;
  PQuery.ParamByName('p_data_fin').AsDateTime := PDataFin;
  PClient.Open;
end;

procedure TContasReceber.proc_gravar(PCodigo, PEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                 '+
                   ' from contas_a_receber c                  '+
                   ' where c.cr_codigo     = :p_cr_codigo     '+
                   '   and c.cr_emp_codigo = :p_cr_emp_codigo ');
    qryAux.ParamByName('p_cr_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_cr_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into contas_a_receber ( '+
                     '   cr_codigo        ,           '+
                     '   cr_emp_codigo    ,           '+
                     '   cr_ent_codigo    ,           '+
                     '   cr_ent_emp_codigo,           '+
                     '   cr_con_codigo    ,           '+
                     '   cr_data          ,           '+
                     '   cr_observacao    ,           '+
                     '   cr_situacao      ,           '+
                     '   cr_valor                     '+
                     ' ) values (                     '+
                     '   :p_cr_codigo        ,        '+
                     '   :p_cr_emp_codigo    ,        '+
                     '   :p_cr_ent_codigo    ,        '+
                     '   :p_cr_ent_emp_codigo,        '+
                     '   :p_cr_con_codigo    ,        '+
                     '   :p_cr_data          ,        '+
                     '   :p_cr_observacao    ,        '+
                     '   :p_cr_situacao      ,        '+
                     '   :p_cr_valor         )        ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update contas_a_receber                       '+
                     ' set cr_ent_codigo     = :p_cr_ent_codigo    , '+
                     '     cr_ent_emp_codigo = :p_cr_ent_emp_codigo, '+
                     '     cr_con_codigo     = :p_cr_con_codigo    , '+
                     '     cr_data           = :p_cr_data          , '+
                     '     cr_observacao     = :p_cr_observacao    , '+
                     '     cr_situacao       = :p_cr_situacao      , '+
                     '     cr_valor          = :p_cr_valor           '+
                     ' where cr_codigo     = :p_cr_codigo            '+
                     '   and cr_emp_codigo = :p_cr_emp_codigo        ');
    end;

    qryAux.ParamByName('p_cr_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_cr_emp_codigo').AsInteger     := PEmpresa;
    qryAux.ParamByName('p_cr_ent_codigo').AsInteger     := cr_ent_codigo;
    qryAux.ParamByName('p_cr_ent_emp_codigo').AsInteger := cr_ent_emp_codigo;
    qryAux.ParamByName('p_cr_con_codigo').AsInteger     := cr_con_codigo;
    qryAux.ParamByName('p_cr_data').AsDateTime          := cr_data;
    qryAux.ParamByName('p_cr_observacao').AsString      := cr_observacao;
    qryAux.ParamByName('p_cr_situacao').AsInteger       := cr_situacao;
    qryAux.ParamByName('p_cr_valor').AsFloat            := cr_valor;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;

end;

end.
