unit cls_agendamentos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Math, cls_tabelacodigos, cls_cc_entidade_aulas;

type TAgendamentos = Class(TObject)
  private
    { Private declarations }
    f_agd_codigo             : Integer;
    f_agd_emp_codigo         : Integer;
    f_agd_ent_codigo         : Integer;
    f_agd_ent_emp_codigo     : Integer;
    f_agd_fun_ent_codigo     : Integer;
    f_agd_fun_ent_emp_codigo : Integer;
    f_agd_pro_codigo         : Integer;
    f_agd_pro_emp_codigo     : Integer;
    f_agd_sala_codigo        : Integer;
    f_agd_sala_emp_codigo    : Integer;
    f_agd_data               : TDateTime;
    f_agd_hora               : String;
    f_agd_qtde_horas         : Double;
    f_agd_situacao           : Integer;
    f_agd_observacoes        : String;
    f_agd_hora_final         : String;
  public
    { Public declarations }
    procedure proc_gravar(PCodigo, PEmpCodigo : Integer);
    procedure proc_excluir(PCodigo, PEmpCodigo : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PCodigoEmpresa, PCodigoCli, PEmpCodigoCli,
      PCodigoFun, PEmpCodigoFun, PCodigoPro, PEmpCodigoPro, PCodigoSala, PEmpCodigoSala : Integer; PDataIni, PDataFim : TDateTime;
      PNomeCliente : String);
    procedure proc_consultar_agenda(var PQuery : TFDQuery; var PClient : TClientDataSet; PCodigoEmpresa, PCodigoFunc,
        PEmpCodigoFunc : Integer; PData : TDateTime);
    procedure proc_consultar_agenda_dia(var PQuery : TFDQuery; var PClient : TClientDataSet; PCodigoEmpresa: Integer; PData: TDateTime);
    procedure proc_repetir_agendamento(PCodigo, PEmpresa, PModo, PQuantidade : Integer);
    function func_valida_data(var PComboHora : TComboBox; PAgdEmpresa : Integer; PData : TDateTime; PFunCodigo, PFunEmpresa, PSalaCodigo, PSalaEmpresa : Integer) : Boolean;
    function func_recuperar(PCodigo, PEmpresa : Integer) : Boolean;
    function func_recuperar_aluno_professor(PFuncCodigo, PFuncEmpresa, PAlunoCodigo, PAlunoEmpresa : Integer; PDiaHora : TDateTime) : Boolean;
    procedure proc_carrega_agenda_online(var PMemo : TMemo; PData : TDateTime; PHora : String);

  published
    { Published declarations }
    property agd_codigo             : Integer   read f_agd_codigo             write f_agd_codigo;
    property agd_emp_codigo         : Integer   read f_agd_emp_codigo         write f_agd_emp_codigo;
    property agd_ent_codigo         : Integer   read f_agd_ent_codigo         write f_agd_ent_codigo;
    property agd_ent_emp_codigo     : Integer   read f_agd_ent_emp_codigo     write f_agd_ent_emp_codigo;
    property agd_fun_ent_codigo     : Integer   read f_agd_fun_ent_codigo     write f_agd_fun_ent_codigo;
    property agd_fun_ent_emp_codigo : Integer   read f_agd_fun_ent_emp_codigo write f_agd_fun_ent_emp_codigo;
    property agd_pro_codigo         : Integer   read f_agd_pro_codigo         write f_agd_pro_codigo;
    property agd_pro_emp_codigo     : Integer   read f_agd_pro_emp_codigo     write f_agd_pro_emp_codigo;
    property agd_sala_codigo        : Integer   read f_agd_sala_codigo        write f_agd_sala_codigo;
    property agd_sala_emp_codigo    : Integer   read f_agd_sala_emp_codigo    write f_agd_sala_emp_codigo;
    property agd_data               : TDateTime read f_agd_data               write f_agd_data;
    property agd_hora               : String    read f_agd_hora               write f_agd_hora;
    property agd_qtde_horas         : Double    read f_agd_qtde_horas         write f_agd_qtde_horas;
    property agd_situacao           : Integer   read f_agd_situacao           write f_agd_situacao;
    property agd_observacoes        : String    read f_agd_observacoes        write f_agd_observacoes;
    property agd_hora_final         : String    read f_agd_hora_final         write f_agd_hora_final;
  end;

implementation

{ TAgendamentos }

function TAgendamentos.func_recuperar(PCodigo, PEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  qryAux := TFDQuery.Create(nil);
  try
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from agendamentos a                        '+
                   ' where a.agd_codigo     = :p_agd_codigo     '+
                   '   and a.agd_emp_codigo = :p_agd_emp_codigo ');
    qryAux.ParamByName('p_agd_codigo').AsInteger := PCodigo;
    qryAux.ParamByName('p_agd_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := not qryAux.Eof;

    agd_codigo             := qryAux.FieldByName('agd_codigo').AsInteger;
    agd_emp_codigo         := qryAux.FieldByName('agd_emp_codigo').AsInteger;
    agd_ent_codigo         := qryAux.FieldByName('agd_ent_codigo').AsInteger;
    agd_ent_emp_codigo     := qryAux.FieldByName('agd_ent_emp_codigo').AsInteger;
    agd_fun_ent_codigo     := qryAux.FieldByName('agd_fun_ent_codigo').AsInteger;
    agd_fun_ent_emp_codigo := qryAux.FieldByName('agd_fun_ent_emp_codigo').AsInteger;
    agd_pro_codigo         := qryAux.FieldByName('agd_pro_codigo').AsInteger;
    agd_pro_emp_codigo     := qryAux.FieldByName('agd_pro_emp_codigo').AsInteger;
    agd_sala_codigo        := qryAux.FieldByName('agd_sala_codigo').AsInteger;
    agd_sala_emp_codigo    := qryAux.FieldByName('agd_sala_emp_codigo').AsInteger;
    agd_data               := qryAux.FieldByName('agd_data').AsDateTime;
    agd_hora               := qryAux.FieldByName('agd_hora').AsString;
    agd_qtde_horas         := qryAux.FieldByName('agd_qtde_horas').AsFloat;
    agd_situacao           := qryAux.FieldByName('agd_situacao').AsInteger;
    agd_observacoes        := qryAux.FieldByName('agd_observacoes').AsString;
    agd_hora_final         := qryAux.FieldByName('agd_hora_final').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

function TAgendamentos.func_recuperar_aluno_professor(PFuncCodigo, PFuncEmpresa,
  PAlunoCodigo, PAlunoEmpresa: Integer; PDiaHora: TDateTime): Boolean;
var qryAux : TFDQuery;
begin
  qryAux := TFDQuery.Create(nil);
  try
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                                   '+
                   ' from agendamentos a                                        '+
                   ' where a.agd_ent_codigo         = :p_agd_ent_codigo         '+
                   '   and a.agd_ent_emp_codigo     = :p_agd_ent_emp_codigo     '+
                   '   and a.agd_fun_ent_codigo     = :p_agd_fun_ent_codigo     '+
                   '   and a.agd_fun_ent_emp_codigo = :p_agd_fun_ent_emp_codigo '+
                   '   and date(a.agd_data)         = :p_agd_data               '+
                   '   and time(a.agd_hora)         = time(:p_agd_hora)         ');
    qryAux.ParamByName('p_agd_ent_codigo').AsInteger         := PAlunoCodigo;
    qryAux.ParamByName('p_agd_ent_emp_codigo').AsInteger     := PAlunoEmpresa;
    qryAux.ParamByName('p_agd_fun_ent_codigo').AsInteger     := PFuncCodigo;
    qryAux.ParamByName('p_agd_fun_ent_emp_codigo').AsInteger := PFuncEmpresa;
    qryAux.ParamByName('p_agd_data').AsString                := FormatDateTime('yyyy-mm-dd', PDiaHora);
    qryAux.ParamByName('p_agd_hora').AsString                := FormatDateTime('HH:MM:SS', PDiaHora);
    qryAux.Open;

    Result := func_recuperar(qryAux.FieldByName('agd_codigo').AsInteger, qryAux.FieldByName('agd_emp_codigo').AsInteger);
  finally
    FreeAndNil(qryAux);
  end;
end;

function TAgendamentos.func_valida_data(var PComboHora : TComboBox; PAgdEmpresa : Integer; PData: TDateTime; PFunCodigo,
  PFunEmpresa, PSalaCodigo, PSalaEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
    dt_ini : TDateTime;
    dt_fim : TDateTime;
    s_aux1 : String;
    s_sux2 : String;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from agendamentos a                        '+
                   ' where a.agd_emp_codigo = :p_agd_emp_codigo ');

    if PFunCodigo > 0 then begin
      if PSalaCodigo = 0 then begin
        qryAux.SQL.Add(' and a.agd_fun_ent_codigo     = :agd_fun_ent_codigo     '+
                       ' and a.agd_fun_ent_emp_codigo = :agd_fun_ent_emp_codigo '+
                       ' and a.agd_data               = :p_agd_data             ');
        qryAux.ParamByName('p_agd_data').AsDateTime := Trunc(PData);
      end else begin
        qryAux.SQL.Add(' and (a.agd_fun_ent_codigo     = :agd_fun_ent_codigo     '+
                       ' and  a.agd_fun_ent_emp_codigo = :agd_fun_ent_emp_codigo '+
                       ' and  a.agd_data               = :p_agd_data)            ');
        qryAux.ParamByName('p_agd_data').AsDateTime := Trunc(PData);
      end;

      qryAux.ParamByName('agd_fun_ent_codigo').AsInteger     := PFunCodigo;
      qryAux.ParamByName('agd_fun_ent_emp_codigo').AsInteger := PFunEmpresa;
    end;

    if (PSalaCodigo > 0) and (PSalaCodigo < 6) then begin
      if PFunCodigo = 0 then begin
        qryAux.SQL.Add(' and a.agd_sala_codigo     = :p_agd_sala_codigo     '+
                       ' and a.agd_sala_emp_codigo = :p_agd_sala_emp_codigo '+
                       ' and a.agd_data            = :p_agd_data            ');
        qryAux.ParamByName('p_agd_data').AsDateTime := Trunc(PData);
      end else begin
        qryAux.SQL.Add('  or (a.agd_sala_codigo     = :p_agd_sala_codigo     '+
                       ' and  a.agd_sala_emp_codigo = :p_agd_sala_emp_codigo '+
                       ' and  a.agd_data            = :p_agd_data)           ');
        qryAux.ParamByName('p_agd_data').AsDateTime := Trunc(PData);
      end;

      qryAux.ParamByName('p_agd_sala_codigo').AsInteger     := PSalaCodigo;
      qryAux.ParamByName('p_agd_sala_emp_codigo').AsInteger := PSalaEmpresa;
    end;

    qryAux.SQL.Add(' order by a.agd_data ');
    qryAux.ParamByName('p_agd_emp_codigo').AsInteger := PAgdEmpresa;
    qryAux.Open;

    Result := True;
    while (not qryAux.Eof) do begin
      dt_ini := StrToDateTime(qryAux.FieldByName('agd_data').AsString + ' ' + qryAux.FieldByName('agd_hora').AsString);
      dt_fim := StrToDateTime(FormatDateTime('dd/mm/yyyy HH:MM', dt_ini + (qryAux.FieldByName('agd_qtde_horas').AsFloat / 24)));

      repeat
        PComboHora.Items.Delete(PComboHora.Items.IndexOf(FormatDateTime('HH:MM', dt_ini)));

        dt_ini := dt_ini + ((15/60)/24);

        s_aux1 := FormatDateTime('HH:MM', dt_ini);
        s_sux2 := FormatDateTime('HH:MM', dt_fim);

      until s_aux1 = s_sux2;

      qryAux.Next;
    end;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TAgendamentos.proc_carrega_agenda_online(var PMemo: TMemo;
  PData: TDateTime; PHora: String);
var qryAux : TFDQuery;
begin
  qryAux := TFDQuery.Create(nil);
  try
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select a.agd_data, a.agd_hora, p.ent_razao_social prof, e.ent_razao_social aluno  '+
                   ' from agendamentos a                                                               '+
                   ' inner join agendamentos_entidade ae on ae.agd_codigo     = a.agd_codigo and       '+
                   '                                        ae.agd_emp_codigo = a.agd_emp_codigo       '+
                   ' inner join entidades e on e.ent_codigo     = ae.ent_codigo and                    '+
                   '                           e.ent_emp_codigo = ae.ent_emp_codigo                    '+
                   ' inner join entidades p on p.ent_codigo     = a.agd_fun_ent_codigo and             '+
                   '                           p.ent_emp_codigo = a.agd_fun_ent_emp_codigo             '+
                   ' where a.agd_data             = date(:p_data)                                      '+
                   '   and time(:p_hora)         >= time(a.agd_hora)                                   '+
                   '   and time(:p_hora)         <= time(a.agd_hora_final)                             '+
                   '   and a.agd_sala_codigo      = 6                                                  '+
                   '   and a.agd_sala_emp_codigo  = 1                                                  '+
                   ' order by a.agd_hora                                                               ');
    qryAux.ParamByName('p_data').AsDateTime := PData;
    qryAux.ParamByName('p_hora').AsString   := PHora;
    qryAux.Open;

    PMemo.Lines.Clear;
    while not qryAux.Eof do begin
      PMemo.Lines.Add(qryAux.FieldByName('agd_hora').AsString + ' - Prof.: ' + qryAux.FieldByName('prof').AsString + '   ALUNO: ' + qryAux.FieldByName('aluno').AsString);
      PMemo.Lines.Add(' ');
      PMemo.Lines.Add(' ');

      qryAux.Next;
    end;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TAgendamentos.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PCodigoEmpresa, PCodigoCli, PEmpCodigoCli,
  PCodigoFun, PEmpCodigoFun, PCodigoPro, PEmpCodigoPro, PCodigoSala,
  PEmpCodigoSala: Integer; PDataIni, PDataFim: TDateTime;
  PNomeCliente : String);
begin
  PQuery.Connection := Conexao.FDConexao;

  PClient.Close;
  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select ea.ent_razao_social nome_aluno, eb.ent_razao_social nome_professor,   '+
                 '        p.pro_descricao nome_produto, s.sala_descricao descricao_sala,        '+
                 '        a.*                                                                   '+
                 ' from agendamentos a                                                          '+
                 ' inner join agendamentos_entidade ae on ae.agd_codigo     = a.agd_codigo and  '+
                 '                                        ae.agd_emp_codigo = a.agd_emp_codigo  '+
                 ' left outer join entidades ea on ea.ent_codigo     = ae.ent_codigo and        '+
                 '                                 ea.ent_emp_codigo = ae.ent_emp_codigo        '+
                 ' left outer join entidades eb on eb.ent_codigo     = a.agd_fun_ent_codigo and '+
                 '                                 eb.ent_emp_codigo = a.agd_fun_ent_emp_codigo '+
                 ' left outer join produtos p on p.pro_codigo     = a.agd_pro_codigo and        '+
                 '                               p.pro_emp_codigo = a.agd_pro_emp_codigo        '+
                 ' left outer join salas s on s.sala_codigo     = a.agd_sala_codigo and         '+
                 '                            s.sala_emp_codigo = a.agd_sala_emp_codigo         '+
                 ' where a.agd_emp_codigo = :p_agd_emp_codigo ');

  if PCodigoCli > 0 then begin
    PQuery.SQL.Add(' and a.agd_ent_codigo     = :p_agd_ent_codigo     '+
                   ' and a.agd_ent_emp_codigo = :p_agd_ent_emp_codigo ');
    PQuery.ParamByName('p_agd_ent_codigo').AsInteger     := PCodigoCli;
    PQuery.ParamByName('p_agd_ent_emp_codigo').AsInteger := PEmpCodigoCli;
  end;

  if PCodigoFun > 0 then begin
    PQuery.SQL.Add(' and a.agd_fun_ent_codigo     = :p_agd_fun_ent_codigo     '+
                   ' and a.agd_fun_ent_emp_codigo = :p_agd_fun_ent_emp_codigo ');
    PQuery.ParamByName('p_agd_fun_ent_codigo').AsInteger     := PCodigoFun;
    PQuery.ParamByName('p_agd_fun_ent_emp_codigo').AsInteger := PEmpCodigoFun;
  end;

  if PCodigoPro > 0 then begin
    PQuery.SQL.Add(' and a.agd_pro_codigo     = :p_agd_pro_codigo     '+
                   ' and a.agd_pro_emp_codigo = :p_agd_pro_emp_codigo ');
    PQuery.ParamByName('p_agd_pro_codigo').AsInteger     := PCodigoPro;
    PQuery.ParamByName('p_agd_pro_emp_codigo').AsInteger := PEmpCodigoPro;
  end;

  if PCodigoSala > 0 then begin
    PQuery.SQL.Add(' and a.agd_sala_codigo     = :p_agd_sala_codigo     '+
                   ' and a.agd_sala_emp_codigo = :p_agd_sala_emp_codigo ');
    PQuery.ParamByName('p_agd_sala_codigo').AsInteger     := PCodigoSala;
    PQuery.ParamByName('p_agd_sala_emp_codigo').AsInteger := PEmpCodigoSala;
  end;

  if (PDataIni > 0) and (PDataFim = 0) then begin
    PQuery.SQL.Add(' and date(a.agd_data) >= :p_data_ini ');
    PQuery.ParamByName('p_data_ini').AsString := FormatDateTime('yyyy-mm-dd', PDataIni);
  end else begin
    if (PDataIni = 0) and (PDataFim > 0) then begin
      PQuery.SQL.Add(' and date(a.agd_data) = :p_data_fim ');
      PQuery.ParamByName('p_data_fim').AsString := FormatDateTime('yyyy-mm-dd', PDataFim);
    end else begin
      if (PDataIni > 0) and (PDataFim > 0) then begin
        PQuery.SQL.Add(' and date(a.agd_data) >= :p_data_ini '+
                       ' and date(a.agd_data) <= :p_data_fim ');
        PQuery.ParamByName('p_data_ini').AsString := FormatDateTime('yyyy-mm-dd', Trunc(PDataIni));
        PQuery.ParamByName('p_data_fim').AsString := FormatDateTime('yyyy-mm-dd', Trunc(PDataFim));
      end;
    end;
  end;

  if Trim(PNomeCliente) <> '' then begin
    PQuery.SQL.Add(' and exists (select *                                                  '+
                   '             from entidades e                                          '+
                   '             where e.ent_codigo     = ae.ent_codigo                    '+
                   '               and e.ent_emp_codigo = ae.ent_emp_codigo                '+
                   '               and e.ent_razao_social like ''%' + PNomeCliente + '%'') ');

  end;

  PQuery.SQL.Add(' order by a.agd_data, a.agd_hora ');
  PQuery.ParamByName('p_agd_emp_codigo').AsInteger := PCodigoEmpresa;
  PQuery.Open;
  PClient.Open;
end;

procedure TAgendamentos.proc_consultar_agenda(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PCodigoEmpresa, PCodigoFunc,
  PEmpCodigoFunc: Integer; PData: TDateTime);
begin
  PQuery.Connection := Conexao.FDConexao;

  PClient.Close;
  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select x.hora,                                                                                                                                                                                        '+
                 '        case when (select a.agd_fun_ent_codigo                                                                                                                                                         '+
                 '                   from agendamentos a                                                                                                                                                                 '+
                 '                   where date(a.agd_data)           >= :p_agd_data_ini                                                                                                                                 '+
                 '                     and date(a.agd_data)           <= :p_agd_data_fin                                                                                                                                 '+
                 '                     and a.agd_fun_ent_codigo        = :p_agd_fun_ent_codigo                                                                                                                           '+
                 '                     and a.agd_fun_ent_emp_codigo    = :p_agd_fun_ent_emp_codigo                                                                                                                       '+
                 '                     and dayname(date(a.agd_data))   = ''Monday''                                                                                                                                      '+
                 '                     and x.hora                     >= time(a.agd_hora)                                                                                                                                '+
                 '                     and x.hora                     <= time(a.agd_hora_final)) is not null then (select concat(e.ent_razao_social, '' (SALA '', a.agd_sala_codigo, '')'')                              '+
                 '                                                                                                 from agendamentos a                                                                                   '+
                 '                                                                                                 inner join entidades e on e.ent_codigo     = a.agd_ent_codigo and                                     '+
                 '                                                                                                                           e.ent_emp_codigo = a.agd_fun_ent_emp_codigo                                 '+
                 '                                                                                                 inner join contatos c on c.con_ent_codigo     = e.ent_codigo and                                      '+
                 '                                                                                                                          c.con_ent_emp_codigo = e.ent_emp_codigo                                      '+
                 '                                                                                                 where date(a.agd_data)          >= :p_agd_data_ini                                                    '+
                 '                                                                                                   and date(a.agd_data)          <= :p_agd_data_fin                                                    '+
                 '                                                                                                   and a.agd_fun_ent_codigo       = :p_agd_fun_ent_codigo                                              '+
                 '                                                                                                   and a.agd_fun_ent_emp_codigo   = :p_agd_fun_ent_emp_codigo                                          '+
                 '                                                                                                   and dayname(date(a.agd_data))  = ''Monday''                                                         '+
                 '                                                                                                   and x.hora                    >= time(a.agd_hora)                                                   '+
                 '                                                                                                   and x.hora                    <= time(a.agd_hora_final)                                             '+
                 '                                                                                                 limit 1)                                                                                              '+
                 '      end segunda,                                                                                                                                                                                     '+
                 '        case when (select a.agd_fun_ent_codigo                                                                                                                                                         '+
                 '                   from agendamentos a                                                                                                                                                                 '+
                 '                   where date(a.agd_data)           >= :p_agd_data_ini                                                                                                                                 '+
                 '                     and date(a.agd_data)           <= :p_agd_data_fin                                                                                                                                 '+
                 '                     and a.agd_fun_ent_codigo        = :p_agd_fun_ent_codigo                                                                                                                           '+
                 '                     and a.agd_fun_ent_emp_codigo    = :p_agd_fun_ent_emp_codigo                                                                                                                       '+
                 '                     and dayname(date(a.agd_data))   = ''Tuesday''                                                                                                                                     '+
                 '                     and x.hora                     >= time(a.agd_hora)                                                                                                                                '+
                 '                     and x.hora                     <= time(a.agd_hora_final)) is not null then (select concat(e.ent_razao_social, '' (SALA '', a.agd_sala_codigo, '')'')                              '+
                 '                                                                                                 from agendamentos a                                                                                   '+
                 '                                                                                                 inner join entidades e on e.ent_codigo     = a.agd_ent_codigo and                                     '+
                 '                                                                                                                           e.ent_emp_codigo = a.agd_fun_ent_emp_codigo                                 '+
                 '                                                                                                 inner join contatos c on c.con_ent_codigo     = e.ent_codigo and                                      '+
                 '                                                                                                                          c.con_ent_emp_codigo = e.ent_emp_codigo                                      '+
                 '                                                                                                 where date(a.agd_data)         >= :p_agd_data_ini                                                     '+
                 '                                                                                                   and date(a.agd_data)         <= :p_agd_data_fin                                                     '+
                 '                                                                                                   and a.agd_fun_ent_codigo      = :p_agd_fun_ent_codigo                                               '+
                 '                                                                                                   and a.agd_fun_ent_emp_codigo  = :p_agd_fun_ent_emp_codigo                                           '+
                 '                                                                                                   and dayname(date(a.agd_data)) = ''Tuesday''                                                         '+
                 '                                                                                                   and x.hora                    >= time(a.agd_hora)                                                   '+
                 '                                                                                                   and x.hora                    <= time(a.agd_hora_final)                                             '+
                 '                                                                                                 limit 1)                                                                                              '+
                 '      end terca,                                                                                                                                                                                       '+
                 '        case when (select a.agd_fun_ent_codigo                                                                                                                                                         '+
                 '                   from agendamentos a                                                                                                                                                                 '+
                 '                   where date(a.agd_data)           >= :p_agd_data_ini                                                                                                                                 '+
                 '                     and date(a.agd_data)           <= :p_agd_data_fin                                                                                                                                 '+
                 '                     and a.agd_fun_ent_codigo        = :p_agd_fun_ent_codigo                                                                                                                           '+
                 '                     and a.agd_fun_ent_emp_codigo    = :p_agd_fun_ent_emp_codigo                                                                                                                       '+
                 '                     and dayname(date(a.agd_data))   = ''Wednesday''                                                                                                                                   '+
                 '                     and x.hora                     >= time(a.agd_hora)                                                                                                                                '+
                 '                     and x.hora                     <= time(a.agd_hora_final)) is not null then (select concat(e.ent_razao_social, '' (SALA '', a.agd_sala_codigo, '')'')                              '+
                 '                                                                                                 from agendamentos a                                                                                   '+
                 '                                                                                                 inner join entidades e on e.ent_codigo     = a.agd_ent_codigo and                                     '+
                 '                                                                                                                           e.ent_emp_codigo = a.agd_fun_ent_emp_codigo                                 '+
                 '                                                                                                 inner join contatos c on c.con_ent_codigo     = e.ent_codigo and                                      '+
                 '                                                                                                                          c.con_ent_emp_codigo = e.ent_emp_codigo                                      '+
                 '                                                                                                 where date(a.agd_data)         >= :p_agd_data_ini                                                     '+
                 '                                                                                                   and date(a.agd_data)         <= :p_agd_data_fin                                                     '+
                 '                                                                                                   and a.agd_fun_ent_codigo      = :p_agd_fun_ent_codigo                                               '+
                 '                                                                                                   and a.agd_fun_ent_emp_codigo  = :p_agd_fun_ent_emp_codigo                                           '+
                 '                                                                                                   and dayname(date(a.agd_data)) = ''Wednesday''                                                       '+
                 '                                                                                                   and x.hora                    >= time(a.agd_hora)                                                   '+
                 '                                                                                                   and x.hora                    <= time(a.agd_hora_final)                                             '+
                 '                                                                                                 limit 1)                                                                                              '+
                 '      end quarta,                                                                                                                                                                                      '+
                 '        case when (select a.agd_fun_ent_codigo                                                                                                                                                         '+
                 '                   from agendamentos a                                                                                                                                                                 '+
                 '                   where date(a.agd_data)           >= :p_agd_data_ini                                                                                                                                 '+
                 '                     and date(a.agd_data)           <= :p_agd_data_fin                                                                                                                                 '+
                 '                     and a.agd_fun_ent_codigo        = :p_agd_fun_ent_codigo                                                                                                                           '+
                 '                     and a.agd_fun_ent_emp_codigo    = :p_agd_fun_ent_emp_codigo                                                                                                                       '+
                 '                     and dayname(date(a.agd_data))   = ''Thursday''                                                                                                                                    '+
                 '                     and x.hora                     >= time(a.agd_hora)                                                                                                                                '+
                 '                     and x.hora                     <= time(a.agd_hora_final)) is not null then (select concat(e.ent_razao_social, '' (SALA '', a.agd_sala_codigo, '')'')                              '+
                 '                                                                                                 from agendamentos a                                                                                   '+
                 '                                                                                                 inner join entidades e on e.ent_codigo     = a.agd_ent_codigo and                                     '+
                 '                                                                                                                           e.ent_emp_codigo = a.agd_fun_ent_emp_codigo                                 '+
                 '                                                                                                 inner join contatos c on c.con_ent_codigo     = e.ent_codigo and                                      '+
                 '                                                                                                                          c.con_ent_emp_codigo = e.ent_emp_codigo                                      '+
                 '                                                                                                 where date(a.agd_data)         >= :p_agd_data_ini                                                     '+
                 '                                                                                                   and date(a.agd_data)         <= :p_agd_data_fin                                                     '+
                 '                                                                                                   and a.agd_fun_ent_codigo      = :p_agd_fun_ent_codigo                                               '+
                 '                                                                                                   and a.agd_fun_ent_emp_codigo  = :p_agd_fun_ent_emp_codigo                                           '+
                 '                                                                                                   and dayname(date(a.agd_data)) = ''Thursday''                                                        '+
                 '                                                                                                   and x.hora                    >= time(a.agd_hora)                                                   '+
                 '                                                                                                   and x.hora                    <= time(a.agd_hora_final)                                             '+
                 '                                                                                                 limit 1)                                                                                              '+
                 '      end quinta,                                                                                                                                                                                      '+
                 '        case when (select a.agd_fun_ent_codigo                                                                                                                                                         '+
                 '                   from agendamentos a                                                                                                                                                                 '+
                 '                   where date(a.agd_data)           >= :p_agd_data_ini                                                                                                                                 '+
                 '                     and date(a.agd_data)           <= :p_agd_data_fin                                                                                                                                 '+
                 '                     and a.agd_fun_ent_codigo        = :p_agd_fun_ent_codigo                                                                                                                           '+
                 '                     and a.agd_fun_ent_emp_codigo    = :p_agd_fun_ent_emp_codigo                                                                                                                       '+
                 '                     and dayname(date(a.agd_data))   = ''Friday''                                                                                                                                      '+
                 '                     and x.hora                     >= time(a.agd_hora)                                                                                                                                '+
                 '                     and x.hora                     <= time(a.agd_hora_final)) is not null then (select concat(e.ent_razao_social, '' (SALA '', a.agd_sala_codigo, '')'')                              '+
                 '                                                                                                 from agendamentos a                                                                                   '+
                 '                                                                                                 inner join entidades e on e.ent_codigo     = a.agd_ent_codigo and                                     '+
                 '                                                                                                                           e.ent_emp_codigo = a.agd_fun_ent_emp_codigo                                 '+
                 '                                                                                                 inner join contatos c on c.con_ent_codigo     = e.ent_codigo and                                      '+
                 '                                                                                                                          c.con_ent_emp_codigo = e.ent_emp_codigo                                      '+
                 '                                                                                                 where date(a.agd_data)         >= :p_agd_data_ini                                                     '+
                 '                                                                                                   and date(a.agd_data)         <= :p_agd_data_fin                                                     '+
                 '                                                                                                   and a.agd_fun_ent_codigo      = :p_agd_fun_ent_codigo                                               '+
                 '                                                                                                   and a.agd_fun_ent_emp_codigo  = :p_agd_fun_ent_emp_codigo                                           '+
                 '                                                                                                   and dayname(date(a.agd_data)) = ''Friday''                                                          '+
                 '                                                                                                   and x.hora                    >= time(a.agd_hora)                                                   '+
                 '                                                                                                   and x.hora                    <= time(a.agd_hora_final)                                             '+
                 '                                                                                                 limit 1)                                                                                              '+
                 '      end sexta,                                                                                                                                                                                       '+
                 '        case when (select a.agd_fun_ent_codigo                                                                                                                                                         '+
                 '                   from agendamentos a                                                                                                                                                                 '+
                 '                   where date(a.agd_data)           >= :p_agd_data_ini                                                                                                                                 '+
                 '                     and date(a.agd_data)           <= :p_agd_data_fin                                                                                                                                 '+
                 '                     and a.agd_fun_ent_codigo        = :p_agd_fun_ent_codigo                                                                                                                           '+
                 '                     and a.agd_fun_ent_emp_codigo    = :p_agd_fun_ent_emp_codigo                                                                                                                       '+
                 '                     and dayname(date(a.agd_data))   = ''Saturday''                                                                                                                                    '+
                 '                     and x.hora                     >= time(a.agd_hora)                                                                                                                                '+
                 '                     and x.hora                     <= time(a.agd_hora_final)) is not null then (select concat(e.ent_razao_social, '' (SALA '', a.agd_sala_codigo, '')'')                              '+
                 '                                                                                                 from agendamentos a                                                                                   '+
                 '                                                                                                 inner join entidades e on e.ent_codigo     = a.agd_ent_codigo and                                     '+
                 '                                                                                                                           e.ent_emp_codigo = a.agd_fun_ent_emp_codigo                                 '+
                 '                                                                                                 inner join contatos c on c.con_ent_codigo     = e.ent_codigo and                                      '+
                 '                                                                                                                          c.con_ent_emp_codigo = e.ent_emp_codigo                                      '+
                 '                                                                                                 where date(a.agd_data)         >= :p_agd_data_ini                                                     '+
                 '                                                                                                   and date(a.agd_data)         <= :p_agd_data_fin                                                     '+
                 '                                                                                                   and a.agd_fun_ent_codigo      = :p_agd_fun_ent_codigo                                               '+
                 '                                                                                                   and a.agd_fun_ent_emp_codigo  = :p_agd_fun_ent_emp_codigo                                           '+
                 '                                                                                                   and dayname(date(a.agd_data)) = ''Saturday''                                                        '+
                 '                                                                                                   and x.hora                    >= time(a.agd_hora)                                                   '+
                 '                                                                                                   and x.hora                    <= time(a.agd_hora_final)                                             '+
                 '                                                                                                 limit 1)                                                                                              '+
                 '      end sabado,                                                                                                                                                                                      '+
                 '        case when (select a.agd_fun_ent_codigo                                                                                                                                                         '+
                 '                   from agendamentos a                                                                                                                                                                 '+
                 '                   where date(a.agd_data)           >= :p_agd_data_ini                                                                                                                                 '+
                 '                     and date(a.agd_data)           <= :p_agd_data_fin                                                                                                                                 '+
                 '                     and a.agd_fun_ent_codigo        = :p_agd_fun_ent_codigo                                                                                                                           '+
                 '                     and a.agd_fun_ent_emp_codigo    = :p_agd_fun_ent_emp_codigo                                                                                                                       '+
                 '                     and dayname(date(a.agd_data))   = ''Sunday''                                                                                                                                      '+
                 '                     and x.hora                     >= time(a.agd_hora)                                                                                                                                '+
                 '                     and x.hora                     <= time(a.agd_hora_final)) is not null then (select concat(e.ent_razao_social, '' (SALA '', a.agd_sala_codigo, '')'')                              '+
                 '                                                                                                 from agendamentos a                                                                                   '+
                 '                                                                                                 inner join entidades e on e.ent_codigo     = a.agd_ent_codigo and                                     '+
                 '                                                                                                                           e.ent_emp_codigo = a.agd_fun_ent_emp_codigo                                 '+
                 '                                                                                                 inner join contatos c on c.con_ent_codigo     = e.ent_codigo and                                      '+
                 '                                                                                                                          c.con_ent_emp_codigo = e.ent_emp_codigo                                      '+
                 '                                                                                                 where date(a.agd_data)         >= :p_agd_data_ini                                                     '+
                 '                                                                                                   and date(a.agd_data)         <= :p_agd_data_fin                                                     '+
                 '                                                                                                   and a.agd_fun_ent_codigo      = :p_agd_fun_ent_codigo                                               '+
                 '                                                                                                   and a.agd_fun_ent_emp_codigo  = :p_agd_fun_ent_emp_codigo                                           '+
                 '                                                                                                   and dayname(date(a.agd_data)) = ''Sunday''                                                          '+
                 '                                                                                                   and x.hora                    >= time(a.agd_hora)                                                   '+
                 '                                                                                                   and x.hora                    <= time(a.agd_hora_final)                                             '+
                 '                                                                                                 limit 1)                                                                                              '+
                 '      end domingo                                                                                                                                                                                      '+
                 ' from (   select time(''06:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''06:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''06:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''06:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''07:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''07:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''07:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''07:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''08:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''08:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''08:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''08:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''09:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''09:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''09:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''09:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''10:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''10:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''10:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''10:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''11:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''11:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''11:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''11:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''12:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''12:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''12:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''12:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''13:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''13:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''13:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''13:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''14:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''14:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''14:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''14:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''15:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''15:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''15:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''15:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''16:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''16:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''16:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''16:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''17:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''17:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''17:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''17:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''18:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''18:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''18:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''18:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''19:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''19:15'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''19:30'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''19:45'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual union all '+
                 '          select time(''20:00'') hora, date(null) Monday, date(null) Tuesday, date(null) Wednesday, date(null) Thursday, date(null) Friday, date(null) Saturday, date(null) Sunday from dual   ) x     ');
  PQuery.ParamByName('p_agd_data_ini').AsString            := FormatDateTime('yyyy-mm-dd', PData);
  PQuery.ParamByName('p_agd_data_fin').AsString            := FormatDateTime('yyyy-mm-dd', (PData + 7));
  PQuery.ParamByName('p_agd_fun_ent_codigo').AsInteger     := PCodigoFunc;
  PQuery.ParamByName('p_agd_fun_ent_emp_codigo').AsInteger := PEmpCodigoFunc;
  PQuery.Open;
  PClient.Open;
end;

procedure TAgendamentos.proc_consultar_agenda_dia(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PCodigoEmpresa: Integer; PData: TDateTime);
begin
  PQuery.Connection := Conexao.FDConexao;

  PClient.Close;
  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select x.hora,                                                                               '+
                 '        (select concat(al.ent_razao_social, '' (Prof.: '', fu.ent_razao_social, '')'', ''|'', '+
                 '                       fu.ent_codigo, ''|'', fu.ent_emp_codigo, ''|'',                        '+
                 '                       al.ent_codigo, ''|'', al.ent_emp_codigo, ''|'',                        '+
                 '                       time(a.agd_hora))                                                      '+
                 '         from agendamentos a                                                                  '+
                 '         inner join agendamentos_entidade ae on a.agd_codigo     = ae.agd_codigo and          '+
                 '                                                a.agd_emp_codigo = ae.agd_emp_codigo          '+
                 '         left outer join entidades al on al.ent_codigo     = ae.ent_codigo and                '+
                 '                                         al.ent_emp_codigo = ae.ent_emp_codigo                '+
                 '         left outer join entidades fu on fu.ent_codigo     = a.agd_fun_ent_codigo and         '+
                 '                                         fu.ent_emp_codigo = a.agd_fun_ent_emp_codigo         '+
                 '         where a.agd_emp_codigo      = :p_agd_emp_codigo                                      '+
                 '           and a.agd_data            = date(:p_data)                                          '+
                 '               and time(x.hora)     >= time(a.agd_hora)                                       '+
                 '               and time(x.hora)      < time(a.agd_hora_final)                                 '+
                 '               and a.agd_sala_codigo = 1                                                      '+
                 '        limit 1) sala1,                                                                       '+
                 '        (select concat(al.ent_razao_social, '' (Prof.: '', fu.ent_razao_social, '')'', ''|'', '+
                 '                       fu.ent_codigo, ''|'', fu.ent_emp_codigo, ''|'',                        '+
                 '                       al.ent_codigo, ''|'', al.ent_emp_codigo, ''|'',                        '+
                 '                       time(a.agd_hora))                                                      '+
                 '         from agendamentos a                                                                  '+
                 '         inner join agendamentos_entidade ae on a.agd_codigo     = ae.agd_codigo and          '+
                 '                                                a.agd_emp_codigo = ae.agd_emp_codigo          '+
                 '         left outer join entidades al on al.ent_codigo     = ae.ent_codigo and                '+
                 '                                         al.ent_emp_codigo = ae.ent_emp_codigo                '+
                 '         left outer join entidades fu on fu.ent_codigo     = a.agd_fun_ent_codigo and         '+
                 '                                         fu.ent_emp_codigo = a.agd_fun_ent_emp_codigo         '+
                 '         where a.agd_emp_codigo  = :p_agd_emp_codigo                                          '+
                 '           and a.agd_data        = date(:p_data)                                              '+
                 '               and time(x.hora)     >= time(a.agd_hora)                                       '+
                 '               and time(x.hora)      < time(a.agd_hora_final)                                 '+
                 '               and a.agd_sala_codigo = 2                                                      '+
                 '        limit 1) sala2,                                                                       '+
                 '        (select concat(al.ent_razao_social, '' (Prof.: '', fu.ent_razao_social, '')'', ''|'', '+
                 '                       fu.ent_codigo, ''|'', fu.ent_emp_codigo, ''|'',                        '+
                 '                       al.ent_codigo, ''|'', al.ent_emp_codigo, ''|'',                        '+
                 '                       time(a.agd_hora))                                                      '+
                 '         from agendamentos a                                                                  '+
                 '         inner join agendamentos_entidade ae on a.agd_codigo     = ae.agd_codigo and          '+
                 '                                                a.agd_emp_codigo = ae.agd_emp_codigo          '+
                 '         left outer join entidades al on al.ent_codigo     = ae.ent_codigo and                '+
                 '                                         al.ent_emp_codigo = ae.ent_emp_codigo                '+
                 '         left outer join entidades fu on fu.ent_codigo     = a.agd_fun_ent_codigo and         '+
                 '                                         fu.ent_emp_codigo = a.agd_fun_ent_emp_codigo         '+
                 '         where a.agd_emp_codigo  = :p_agd_emp_codigo                                          '+
                 '           and a.agd_data        = date(:p_data)                                              '+
                 '           and time(x.hora)     >= time(a.agd_hora)                                           '+
                 '           and time(x.hora)      < time(a.agd_hora_final)                                     '+
                 '           and a.agd_sala_codigo = 3                                                          '+
                 '        limit 1) sala3,                                                                       '+
                 '        (select concat(al.ent_razao_social, '' (Prof.: '', fu.ent_razao_social, '')'', ''|'', '+
                 '                       fu.ent_codigo, ''|'', fu.ent_emp_codigo, ''|'',                        '+
                 '                       al.ent_codigo, ''|'', al.ent_emp_codigo, ''|'',                        '+
                 '                       time(a.agd_hora))                                                      '+
                 '         from agendamentos a                                                                  '+
                 '         inner join agendamentos_entidade ae on a.agd_codigo     = ae.agd_codigo and          '+
                 '                                                a.agd_emp_codigo = ae.agd_emp_codigo          '+
                 '         left outer join entidades al on al.ent_codigo     = ae.ent_codigo and                '+
                 '                                         al.ent_emp_codigo = ae.ent_emp_codigo                '+
                 '         left outer join entidades fu on fu.ent_codigo     = a.agd_fun_ent_codigo and         '+
                 '                                         fu.ent_emp_codigo = a.agd_fun_ent_emp_codigo         '+
                 '         where a.agd_emp_codigo  = :p_agd_emp_codigo                                          '+
                 '           and a.agd_data        = date(:p_data)                                              '+
                 '           and time(x.hora)     >= time(a.agd_hora)                                           '+
                 '           and time(x.hora)      < time(a.agd_hora_final)                                     '+
                 '           and a.agd_sala_codigo = 4                                                          '+
                 '        limit 1) sala4,                                                                       '+
                 '        (select concat(al.ent_razao_social, '' (Prof.: '', fu.ent_razao_social, '')'', ''|'', '+
                 '                       fu.ent_codigo, ''|'', fu.ent_emp_codigo, ''|'',                        '+
                 '                       al.ent_codigo, ''|'', al.ent_emp_codigo, ''|'',                        '+
                 '                       time(a.agd_hora))                                                      '+
                 '         from agendamentos a                                                                  '+
                 '         inner join agendamentos_entidade ae on a.agd_codigo     = ae.agd_codigo and          '+
                 '                                                a.agd_emp_codigo = ae.agd_emp_codigo          '+
                 '         left outer join entidades al on al.ent_codigo     = ae.ent_codigo and                '+
                 '                                         al.ent_emp_codigo = ae.ent_emp_codigo                '+
                 '         left outer join entidades fu on fu.ent_codigo     = a.agd_fun_ent_codigo and         '+
                 '                                         fu.ent_emp_codigo = a.agd_fun_ent_emp_codigo         '+
                 '         where a.agd_emp_codigo  = :p_agd_emp_codigo                                          '+
                 '           and a.agd_data        = date(:p_data)                                              '+
                 '               and time(x.hora)     >= time(a.agd_hora)                                       '+
                 '               and time(x.hora)      < time(a.agd_hora_final)                                 '+
                 '               and a.agd_sala_codigo = 5                                                      '+
                 '        limit 1) sala5,                                                                       '+
                 '        (select concat(''AULA MARCADA'')                                                      '+
                 '         from agendamentos a                                                                  '+
                 '         inner join agendamentos_entidade ae on a.agd_codigo     = ae.agd_codigo and          '+
                 '                                                a.agd_emp_codigo = ae.agd_emp_codigo          '+
                 '         left outer join entidades al on al.ent_codigo     = ae.ent_codigo and                '+
                 '                                         al.ent_emp_codigo = ae.ent_emp_codigo                '+
                 '         left outer join entidades fu on fu.ent_codigo     = a.agd_fun_ent_codigo and         '+
                 '                                         fu.ent_emp_codigo = a.agd_fun_ent_emp_codigo         '+
                 '         where a.agd_emp_codigo  = :p_agd_emp_codigo                                          '+
                 '           and a.agd_data        = date(:p_data)                                              '+
                 '               and time(x.hora)     >= time(a.agd_hora)                                       '+
                 '               and time(x.hora)      < time(a.agd_hora_final)                                 '+
                 '               and a.agd_sala_codigo = 6                                                      '+
                 '        limit 1) online                                                                       '+
                 ' from (   select time(''06:00'') hora from dual union all                                     '+
                 '          select time(''06:30'') hora from dual union all                                     '+
                 '          select time(''07:00'') hora from dual union all                                     '+
                 '          select time(''07:30'') hora from dual union all                                     '+
                 '          select time(''08:00'') hora from dual union all                                     '+
                 '          select time(''08:30'') hora from dual union all                                     '+
                 '          select time(''09:00'') hora from dual union all                                     '+
                 '          select time(''09:30'') hora from dual union all                                     '+
                 '          select time(''10:00'') hora from dual union all                                     '+
                 '          select time(''10:30'') hora from dual union all                                     '+
                 '          select time(''11:00'') hora from dual union all                                     '+
                 '          select time(''11:30'') hora from dual union all                                     '+
                 '          select time(''12:00'') hora from dual union all                                     '+
                 '          select time(''12:30'') hora from dual union all                                     '+
                 '          select time(''13:00'') hora from dual union all                                     '+
                 '          select time(''13:30'') hora from dual union all                                     '+
                 '          select time(''14:00'') hora from dual union all                                     '+
                 '          select time(''14:30'') hora from dual union all                                     '+
                 '          select time(''15:00'') hora from dual union all                                     '+
                 '          select time(''15:30'') hora from dual union all                                     '+
                 '          select time(''16:00'') hora from dual union all                                     '+
                 '          select time(''16:30'') hora from dual union all                                     '+
                 '          select time(''17:00'') hora from dual union all                                     '+
                 '          select time(''17:30'') hora from dual union all                                     '+
                 '          select time(''18:00'') hora from dual union all                                     '+
                 '          select time(''18:30'') hora from dual union all                                     '+
                 '          select time(''19:00'') hora from dual union all                                     '+
                 '          select time(''19:30'') hora from dual union all                                     '+
                 '          select time(''20:00'') hora from dual  ) x                                          ');
  PQuery.ParamByName('p_agd_emp_codigo').AsInteger := PCodigoEmpresa;
  PQuery.ParamByName('p_data').AsString := FormatDateTime('yyyy-mm-dd', PData);
  PQuery.Open;
  PClient.Open;
end;

procedure TAgendamentos.proc_excluir(PCodigo, PEmpCodigo: Integer);
var qryAux  : TFDQuery;
    qryAux2 : TFDQuery;

    c_cc_entidade_aulas : TCC_entidade_aulas;
begin
  qryAux  := TFDQuery.Create(nil);
  qryAux2 := TFDQuery.Create(nil);

  c_cc_entidade_aulas := TCC_entidade_aulas.Create;

  try
    qryAux.Connection  := Conexao.FDConexao;
    qryAux2.Connection := Conexao.FDConexao;

    qryAux2.SQL.Add(' select a.*, ag.agd_pro_codigo, ag.agd_pro_emp_codigo, ag.agd_qtde_horas '+
                    ' from agendamentos_entidade a                                            '+
                    ' inner join agendamentos ag on ag.agd_codigo     = a.agd_codigo and      '+
                    '                               ag.agd_emp_codigo = a.agd_emp_codigo      '+
                    ' where a.agd_codigo     = :p_agd_codigo                                  '+
                    '   and a.agd_emp_codigo = :p_agd_emp_codigo                              ');
    qryAux2.ParamByName('p_agd_codigo').AsInteger     := PCodigo;
    qryAux2.ParamByName('p_agd_emp_codigo').AsInteger := PEmpCodigo;
    qryAux2.Open;

    while not qryAux2.Eof do begin
      c_cc_entidade_aulas.func_recuperar(qryAux2.FieldByName('ent_codigo').AsInteger, qryAux2.FieldByName('ent_emp_codigo').AsInteger, qryAux2.FieldByName('agd_pro_codigo').AsInteger, qryAux2.FieldByName('agd_pro_emp_codigo').AsInteger);
      c_cc_entidade_aulas.cca_saldo_horas := c_cc_entidade_aulas.cca_saldo_horas + qryAux2.FieldByName('agd_qtde_horas').AsFloat;
      c_cc_entidade_aulas.proc_gravar;

      qryAux2.Next;
    end;

    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Add(' delete from agendamentos_entidade        '+
                   ' where agd_codigo     = :p_agd_codigo     '+
                   '   and agd_emp_codigo = :p_agd_emp_codigo ');
    qryAux.ParamByName('p_agd_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_agd_emp_codigo').AsInteger := PEmpCodigo;
    qryAux.ExecSQL;

    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Add(' delete from agendamentos                 '+
                   ' where agd_codigo     = :p_agd_codigo     '+
                   '   and agd_emp_codigo = :p_agd_emp_codigo ');
    qryAux.ParamByName('p_agd_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_agd_emp_codigo').AsInteger := PEmpCodigo;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
    FreeAndNil(qryAux2);
    FreeAndNil(c_cc_entidade_aulas);
  end;
end;

procedure TAgendamentos.proc_gravar(PCodigo, PEmpCodigo: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from agendamentos a                        '+
                   ' where a.agd_codigo     = :p_agd_codigo     '+
                   '   and a.agd_emp_codigo = :p_agd_emp_codigo ');
    qryAux.ParamByName('p_agd_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_agd_emp_codigo').AsInteger := PEmpCodigo;
    qryAux.Open;

    if qryAux.Eof then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into agendamentos(    '+
                     '   agd_codigo               , '+
                     '   agd_emp_codigo           , '+
                     '   agd_ent_codigo           , '+
                     '   agd_ent_emp_codigo       , '+
                     '   agd_fun_ent_codigo       , '+
                     '   agd_fun_ent_emp_codigo   , '+
                     '   agd_pro_codigo           , '+
                     '   agd_pro_emp_codigo       , '+
                     '   agd_sala_codigo          , '+
                     '   agd_sala_emp_codigo      , '+
                     '   agd_data                 , '+
                     '   agd_hora                 , '+
                     '   agd_qtde_horas           , '+
                     '   agd_situacao             , '+
                     '   agd_observacoes          , '+
                     '   agd_hora_final             '+
                     ' ) values (                   '+
                     '   :p_agd_codigo            , '+
                     '   :p_agd_emp_codigo        , '+
                     '   :p_agd_ent_codigo        , '+
                     '   :p_agd_ent_emp_codigo    , '+
                     '   :p_agd_fun_ent_codigo    , '+
                     '   :p_agd_fun_ent_emp_codigo, '+
                     '   :p_agd_pro_codigo        , '+
                     '   :p_agd_pro_emp_codigo    , '+
                     '   :p_agd_sala_codigo       , '+
                     '   :p_agd_sala_emp_codigo   , '+
                     '   :p_agd_data              , '+
                     '   :p_agd_hora              , '+
                     '   :p_agd_qtde_horas        , '+
                     '   :p_agd_situacao          , '+
                     '   :p_agd_observacoes       , '+
                     '   :p_agd_hora_final        ) ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update agendamentos                                     '+
                     ' set agd_ent_codigo         = :p_agd_ent_codigo        , '+
                     '     agd_ent_emp_codigo     = :p_agd_ent_emp_codigo    , '+
                     '     agd_fun_ent_codigo     = :p_agd_fun_ent_codigo    , '+
                     '     agd_fun_ent_emp_codigo = :p_agd_fun_ent_emp_codigo, '+
                     '     agd_pro_codigo         = :p_agd_pro_codigo        , '+
                     '     agd_pro_emp_codigo     = :p_agd_pro_emp_codigo    , '+
                     '     agd_sala_codigo        = :p_agd_sala_codigo       , '+
                     '     agd_sala_emp_codigo    = :p_agd_sala_emp_codigo   , '+
                     '     agd_data               = :p_agd_data              , '+
                     '     agd_hora               = :p_agd_hora              , '+
                     '     agd_qtde_horas         = :p_agd_qtde_horas        , '+
                     '     agd_situacao           = :p_agd_situacao          , '+
                     '     agd_observacoes        = :p_agd_observacoes       , '+
                     '     agd_hora_final         = :p_agd_hora_final          '+
                     ' where agd_codigo     = :p_agd_codigo                    '+
                     '   and agd_emp_codigo = :p_agd_emp_codigo                ');
    end;

    qryAux.ParamByName('p_agd_codigo').AsInteger             := PCodigo;
    qryAux.ParamByName('p_agd_emp_codigo').AsInteger         := PEmpCodigo;
    qryAux.ParamByName('p_agd_ent_codigo').AsInteger         := agd_ent_codigo;
    qryAux.ParamByName('p_agd_ent_emp_codigo').AsInteger     := agd_ent_emp_codigo;
    qryAux.ParamByName('p_agd_fun_ent_codigo').AsInteger     := agd_fun_ent_codigo;
    qryAux.ParamByName('p_agd_fun_ent_emp_codigo').AsInteger := agd_fun_ent_emp_codigo;
    qryAux.ParamByName('p_agd_pro_codigo').AsInteger         := agd_pro_codigo;
    qryAux.ParamByName('p_agd_pro_emp_codigo').AsInteger     := agd_pro_emp_codigo;
    qryAux.ParamByName('p_agd_sala_codigo').AsInteger        := agd_sala_codigo;
    qryAux.ParamByName('p_agd_sala_emp_codigo').AsInteger    := agd_sala_emp_codigo;
    qryAux.ParamByName('p_agd_data').AsDateTime              := agd_data;
    qryAux.ParamByName('p_agd_hora').AsString                := agd_hora;
    qryAux.ParamByName('p_agd_qtde_horas').AsFloat           := agd_qtde_horas;
    qryAux.ParamByName('p_agd_situacao').AsInteger           := agd_situacao;
    qryAux.ParamByName('p_agd_observacoes').AsString         := agd_observacoes;
    qryAux.ParamByName('p_agd_hora_final').AsString          := agd_hora_final;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TAgendamentos.proc_repetir_agendamento(PCodigo, PEmpresa, PModo, PQuantidade: Integer);
var qryAux : TFDQuery;
    i_nro : Integer;
    c_tabela : TTabelaCodigos;
begin
  func_recuperar(PCodigo, PEmpresa);

  for i_nro := 1 to PQuantidade do begin
    case PModo of
      0 : agd_data := agd_data + (7 * i_nro);
      1 : agd_data := agd_data + (1 * i_nro);
      2 : agd_data := agd_data + (30 * i_nro);
    end;

    c_tabela := TTabelaCodigos.Create;
    try
      proc_gravar(c_tabela.func_gerar_codigo('agendamentos', IntToStr(PEmpresa)), PEmpresa);
    finally
      FreeAndNil(c_tabela);
    end;
  end;
end;

end.
