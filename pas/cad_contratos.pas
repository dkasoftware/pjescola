unit cad_contratos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, dm_conexao,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client;

type
  TFrmCad_Contratos = class(TFrmAux_Padrao)
    PC: TPageControl;
    tsConsulta: TTabSheet;
    pnlConsulta: TPanel;
    dbgEntidades: TDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    ed_consulta: TEdit;
    tsCadastro: TTabSheet;
    pnlCadastro: TPanel;
    FDQAlunos: TFDQuery;
    dsAlunos: TDataSource;
    FDQAlunosent_codigo: TIntegerField;
    FDQAlunosent_emp_codigo: TIntegerField;
    FDQAlunosent_razao_social: TStringField;
    ed_ent_codigo: TEdit;
    ed_ent_emp_codigo: TEdit;
    DBGrid1: TDBGrid;
    procedure ed_consultaChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dbgEntidadesDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmCad_Contratos: TFrmCad_Contratos;

implementation

uses cls_contratos;

{$R *.dfm}

procedure TFrmCad_Contratos.dbgEntidadesDblClick(Sender: TObject);
begin
  inherited;
  //
end;

procedure TFrmCad_Contratos.ed_consultaChange(Sender: TObject);
begin
  inherited;
  FDQAlunos.Filtered := False;
  FDQAlunos.Filter   := '';

  if Trim(ed_consulta.Text) <> '' then begin
    FDQAlunos.Filter   := 'ent_razao_social like ' + QuotedStr('%' + Trim(ed_consulta.Text) + '%');
    FDQAlunos.Filtered := True;
  end;
end;

procedure TFrmCad_Contratos.FormActivate(Sender: TObject);
var c_contratos : TContratos;
begin
  inherited;
  c_contratos := TContratos.Create;
  try
    c_contratos.proc_consultar(FDQAlunos);
  finally
    FreeAndNil(c_contratos);
  end;
end;

procedure TFrmCad_Contratos.FormCreate(Sender: TObject);
begin
  inherited;
  tsCadastro.TabVisible := False;
  tsConsulta.TabVisible := True;
end;

end.
