unit con_entidades;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids;

type
  TFrmAux_Padrao1 = class(TFrmAux_Padrao)
    GBFiltros: TGroupBox;
    Label4: TLabel;
    cmb_ent_codigo: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    ed_data_inicial: TDateTimePicker;
    ed_data_final: TDateTimePicker;
    DBGrid1: TDBGrid;
    FDQuery1: TFDQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAux_Padrao1: TFrmAux_Padrao1;

implementation

{$R *.dfm}

end.
