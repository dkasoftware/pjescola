unit cls_entidades;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Classes, DB;

type TEntidades = Class(TObject)
  private
    { Private declarations }
    f_ent_codigo          : Integer;
    f_ent_emp_codigo      : Integer;
    f_ent_razao_social    : String;
    f_ent_nome_fantasia   : String;
    f_ent_tipo_pessoa     : Integer;
    f_ent_cnpj            : String;
    f_ent_ie              : String;
    f_ent_im              : String;
    f_ent_ir              : String;
    f_ent_suframa         : String;
    f_ent_ramo_codigo     : Integer;
    f_ent_cpf             : String;
    f_ent_rg              : String;
    f_ent_data_nascimento : TDateTime;
    f_ent_ocupacao        : String;
    f_ent_estado_civil    : String;
    f_ent_sit_codigo      : Integer;
    f_ent_observacoes     : TMemoryStream;
  public
    { Public declarations }
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PComboCampos, PComboStatus : TComboBox; PConsulta : String);
    procedure proc_gravar(PCodigo, PEmpresa : Integer);
    procedure proc_carregar_combo(var PComboBox : TComboBox; PTipo : Integer = 2; PFirst : Boolean = False);

    function func_recuperar(PCodigo, PEmpresa : Integer) : Boolean;
    function func_recuperar_observacoes(PCodigo, PEmpresa : Integer) : Boolean;
    function func_retorna_nome_combo(PCodigo, PEmpresa : Integer) : String;
    function func_carregar_busca(var PQuery : TFDQuery) : Boolean;
  published
    { Published declarations }
    property ent_codigo          : Integer       read f_ent_codigo          write f_ent_codigo;
    property ent_emp_codigo      : Integer       read f_ent_emp_codigo      write f_ent_emp_codigo;
    property ent_razao_social    : String        read f_ent_razao_social    write f_ent_razao_social;
    property ent_nome_fantasia   : String        read f_ent_nome_fantasia   write f_ent_nome_fantasia;
    property ent_tipo_pessoa     : Integer       read f_ent_tipo_pessoa     write f_ent_tipo_pessoa;
    property ent_cnpj            : String        read f_ent_cnpj            write f_ent_cnpj;
    property ent_ie              : String        read f_ent_ie              write f_ent_ie;
    property ent_im              : String        read f_ent_im              write f_ent_im;
    property ent_ir              : String        read f_ent_ir              write f_ent_ir;
    property ent_suframa         : String        read f_ent_suframa         write f_ent_suframa;
    property ent_ramo_codigo     : Integer       read f_ent_ramo_codigo     write f_ent_ramo_codigo;
    property ent_cpf             : String        read f_ent_cpf             write f_ent_cpf;
    property ent_rg              : String        read f_ent_rg              write f_ent_rg;
    property ent_data_nascimento : TDateTime     read f_ent_data_nascimento write f_ent_data_nascimento;
    property ent_ocupacao        : String        read f_ent_ocupacao        write f_ent_ocupacao;
    property ent_estado_civil    : String        read f_ent_estado_civil    write f_ent_estado_civil;
    property ent_sit_codigo      : Integer       read f_ent_sit_codigo      write f_ent_sit_codigo;
    property ent_observacoes     : TMemoryStream read f_ent_observacoes     write f_ent_observacoes;
  end;

type
  TEntidadeCombo = TEntidades;

implementation

{ TEntidades }

uses cls_situacoes_entidade;

function TEntidades.func_carregar_busca(var PQuery: TFDQuery): Boolean;
begin
  PQuery.Connection := Conexao.FDConexao;

  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                    '+
                 ' from entidades e            '+
                 ' where e.ent_sit_codigo = 1  '+
                 ' order by e.ent_razao_social ');
  PQuery.Open;

  result := not PQuery.Eof;
end;

function TEntidades.func_recuperar(PCodigo, PEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from entidades e                           '+
                   ' where e.ent_codigo     = :p_ent_codigo     '+
                   '   and e.ent_emp_codigo = :p_ent_emp_codigo ');
    qryAux.ParamByName('p_ent_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_ent_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    ent_codigo          := qryAux.FieldByName('ent_codigo').AsInteger;
    ent_emp_codigo      := qryAux.FieldByName('ent_emp_codigo').AsInteger;
    ent_razao_social    := qryAux.FieldByName('ent_razao_social').AsString;
    ent_nome_fantasia   := qryAux.FieldByName('ent_nome_fantasia').AsString;
    ent_tipo_pessoa     := qryAux.FieldByName('ent_tipo_pessoa').AsInteger;
    ent_cnpj            := qryAux.FieldByName('ent_cnpj').AsString;
    ent_ie              := qryAux.FieldByName('ent_ie').AsString;
    ent_im              := qryAux.FieldByName('ent_im').AsString;
    ent_ir              := qryAux.FieldByName('ent_ir').AsString;
    ent_suframa         := qryAux.FieldByName('ent_suframa').AsString;
    ent_ramo_codigo     := qryAux.FieldByName('ent_ramo_codigo').AsInteger;
    ent_cpf             := qryAux.FieldByName('ent_cpf').AsString;
    ent_rg              := qryAux.FieldByName('ent_rg').AsString;
    ent_data_nascimento := qryAux.FieldByName('ent_data_nascimento').AsDateTime;
    ent_ocupacao        := qryAux.FieldByName('ent_ocupacao').AsString;
    ent_estado_civil    := qryAux.FieldByName('ent_estado_civil').AsString;
    ent_sit_codigo      := qryAux.FieldByName('ent_sit_codigo').AsInteger;
  finally
    FreeAndNil(qryAux);
  end;
end;

function TEntidades.func_recuperar_observacoes(PCodigo,
  PEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from entidades e                           '+
                   ' where e.ent_codigo     = :p_ent_codigo     '+
                   '   and e.ent_emp_codigo = :p_ent_emp_codigo ');
    qryAux.ParamByName('p_ent_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_ent_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    TBlobField(qryAux.FieldByName('ent_observacoes')).SaveToStream(ent_observacoes);
  finally
    FreeAndNil(qryAux);
  end;
end;

function TEntidades.func_retorna_nome_combo(PCodigo, PEmpresa: Integer): String;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select ent_razao_social                  '+
                   ' from entidades                           '+
                   ' where ent_codigo     = :p_ent_codigo     '+
                   '   and ent_emp_codigo = :p_ent_emp_codigo ');
    qryAux.ParamByName('p_ent_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_ent_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := qryAux.FieldByName('ent_razao_social').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TEntidades.proc_carregar_combo(var PComboBox: TComboBox; PTipo : Integer = 2; PFirst : Boolean = False);
var qryAux      : TFDQuery;
    c_entidades : TEntidadeCombo;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select e.*                                                                    '+
                   ' from entidades e                                                              '+
                   ' inner join entidades_tipos et on et.etp_ent_codigo     = e.ent_codigo     and '+
                   '                                  et.etp_ent_emp_codigo = e.ent_emp_codigo and '+
                   '                                  et.etp_tipo           = :p_etp_tipo          '+
                   ' where e.ent_sit_codigo = 1                                                    '+
                   ' order by e.ent_razao_social                                                   ');
    qryAux.ParamByName('p_etp_tipo').AsInteger := PTipo;
    qryAux.Open;

    proc_limpa_combo(PComboBox);

    if (PTipo <> 2) or (PFirst) then begin
      PComboBox.Items.Add('');
    end;

    while (not qryAux.Eof) do begin
      c_entidades := TEntidadeCombo.Create;

      c_entidades.ent_codigo       := qryAux.FieldByName('ent_codigo').AsInteger;
      c_entidades.ent_emp_codigo   := qryAux.FieldByName('ent_emp_codigo').AsInteger;
      c_entidades.ent_razao_social := qryAux.FieldByName('ent_razao_social').AsString;

      PComboBox.Items.AddObject(c_entidades.ent_razao_social, c_entidades);

      qryAux.Next;
    end;

    PComboBox.ItemIndex := 0;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TEntidades.proc_consultar(var PQuery : TFDQuery;
  var PClient : TClientDataSet; PComboCampos, PComboStatus : TComboBox;
  PConsulta : String);
begin
  PQuery.Connection := Conexao.FDConexao;

  PClient.Close;
  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *         '+
                 ' from entidades e '+
                 ' where            ');

  case PComboCampos.ItemIndex of
    0 : PQuery.SQL.Add(' e.ent_codigo like concat(''%'', :p_consulta, ''%'') ');
    1 : PQuery.SQL.Add(' e.ent_razao_social like concat(''%'', :p_consulta, ''%'') ');
  end;

  PQuery.SQL.Add(' and (e.ent_sit_codigo  = :p_ent_sit_codigo '+
                 '  or  :p_ent_sit_codigo = :p_total_status)  ');
  PQuery.ParamByName('p_consulta').AsString        := PConsulta;
  PQuery.ParamByName('p_ent_sit_codigo').AsInteger := TSituacoesCombo(PComboStatus.Items.Objects[PComboStatus.ItemIndex]).sit_codigo;
  PQuery.ParamByName('p_total_status').AsInteger   := PComboStatus.Items.Count - 1;
  PClient.Open;
end;

procedure TEntidades.proc_gravar(PCodigo, PEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                   '+
                   ' from entidades e                           '+
                   ' where e.ent_codigo     = :p_ent_codigo     '+
                   '   and e.ent_emp_codigo = :p_ent_emp_codigo ');
    qryAux.ParamByName('p_ent_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_ent_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into entidades (   '+
                     '   ent_codigo         ,    '+
                     '   ent_emp_codigo     ,    '+
                     '   ent_razao_social   ,    '+
                     '   ent_nome_fantasia  ,    '+
                     '   ent_tipo_pessoa    ,    '+
                     '   ent_cnpj           ,    '+
                     '   ent_ie             ,    '+
                     '   ent_im             ,    '+
                     '   ent_ir             ,    '+
                     '   ent_suframa        ,    '+
                     '   ent_ramo_codigo    ,    '+
                     '   ent_cpf            ,    '+
                     '   ent_rg             ,    '+
                     '   ent_data_nascimento,    '+
                     '   ent_ocupacao       ,    '+
                     '   ent_estado_civil   ,    '+
                     '   ent_sit_codigo     ,    '+
                     '   ent_observacoes         '+
                     '  ) values (               '+
                     '   :p_ent_codigo         , '+
                     '   :p_ent_emp_codigo     , '+
                     '   :p_ent_razao_social   , '+
                     '   :p_ent_nome_fantasia  , '+
                     '   :p_ent_tipo_pessoa    , '+
                     '   :p_ent_cnpj           , '+
                     '   :p_ent_ie             , '+
                     '   :p_ent_im             , '+
                     '   :p_ent_ir             , '+
                     '   :p_ent_suframa        , '+
                     '   :p_ent_ramo_codigo    , '+
                     '   :p_ent_cpf            , '+
                     '   :p_ent_rg             , '+
                     '   :p_ent_data_nascimento, '+
                     '   :p_ent_ocupacao       , '+
                     '   :p_ent_estado_civil   , '+
                     '   :p_ent_sit_codigo     , '+
                     '   :p_ent_observacoes    ) ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update entidades                                  '+
                     ' set ent_razao_social    = :p_ent_razao_social   , '+
                     '     ent_nome_fantasia   = :p_ent_nome_fantasia  , '+
                     '     ent_tipo_pessoa     = :p_ent_tipo_pessoa    , '+
                     '     ent_cnpj            = :p_ent_cnpj           , '+
                     '     ent_ie              = :p_ent_ie             , '+
                     '     ent_im              = :p_ent_im             , '+
                     '     ent_ir              = :p_ent_ir             , '+
                     '     ent_suframa         = :p_ent_suframa        , '+
                     '     ent_ramo_codigo     = :p_ent_ramo_codigo    , '+
                     '     ent_cpf             = :p_ent_cpf            , '+
                     '     ent_rg              = :p_ent_rg             , '+
                     '     ent_data_nascimento = :p_ent_data_nascimento, '+
                     '     ent_ocupacao        = :p_ent_ocupacao       , '+
                     '     ent_estado_civil    = :p_ent_estado_civil   , '+
                     '     ent_sit_codigo      = :p_ent_sit_codigo     , '+
                     '     ent_observacoes     = :p_ent_observacoes      '+
                     ' where ent_codigo     = :p_ent_codigo              '+
                     '   and ent_emp_codigo = :p_ent_emp_codigo          ');
    end;

    qryAux.ParamByName('p_ent_codigo').AsInteger           := PCodigo;
    qryAux.ParamByName('p_ent_emp_codigo').AsInteger       := PEmpresa;
    qryAux.ParamByName('p_ent_razao_social').AsString      := ent_razao_social;
    qryAux.ParamByName('p_ent_nome_fantasia').AsString     := ent_nome_fantasia;
    qryAux.ParamByName('p_ent_tipo_pessoa').AsInteger      := ent_tipo_pessoa;
    qryAux.ParamByName('p_ent_cnpj').AsString              := ent_cnpj;
    qryAux.ParamByName('p_ent_ie').AsString                := ent_ie;
    qryAux.ParamByName('p_ent_im').AsString                := ent_im;
    qryAux.ParamByName('p_ent_ir').AsString                := ent_ir;
    qryAux.ParamByName('p_ent_suframa').AsString           := ent_suframa;
    qryAux.ParamByName('p_ent_ramo_codigo').AsInteger      := ent_ramo_codigo;
    qryAux.ParamByName('p_ent_cpf').AsString               := ent_cpf;
    qryAux.ParamByName('p_ent_rg').AsString                := ent_rg;
    qryAux.ParamByName('p_ent_data_nascimento').AsDateTime := ent_data_nascimento;
    qryAux.ParamByName('p_ent_ocupacao').AsString          := ent_ocupacao;
    qryAux.ParamByName('p_ent_estado_civil').AsString      := ent_estado_civil;
    qryAux.ParamByName('p_ent_sit_codigo').AsInteger       := ent_sit_codigo;
    qryAux.ParamByName('p_ent_observacoes').LoadFromStream(ent_observacoes, ftBlob);
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
