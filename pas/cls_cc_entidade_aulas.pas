unit cls_cc_entidade_aulas;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Math, cls_tabelacodigos;

type TCC_entidade_aulas = Class(TObject)
  private
    { Private declarations }
    f_cca_ent_codigo     : Integer;
    f_cca_ent_emp_codigo : Integer;
    f_cca_pro_codigo     : Integer;
    f_cca_pro_emp_codigo : Integer;
    f_cca_saldo_horas    : Double;
  public
    { Public declarations }
    procedure proc_gravar;

    function func_recuperar(PEntCodigo, PEntEmpCodigo, PProCodigo, PProEmpCodigo : Integer) : Boolean;
  published
    { Published declarations }
    property cca_ent_codigo     : Integer read f_cca_ent_codigo     write f_cca_ent_codigo;
    property cca_ent_emp_codigo : Integer read f_cca_ent_emp_codigo write f_cca_ent_emp_codigo;
    property cca_pro_codigo     : Integer read f_cca_pro_codigo     write f_cca_pro_codigo;
    property cca_pro_emp_codigo : Integer read f_cca_pro_emp_codigo write f_cca_pro_emp_codigo;
    property cca_saldo_horas    : Double  read f_cca_saldo_horas    write f_cca_saldo_horas;
  end;

implementation

{ TCC_entidade_aulas }

function TCC_entidade_aulas.func_recuperar(PEntCodigo, PEntEmpCodigo,
  PProCodigo, PProEmpCodigo: Integer): Boolean;
var qryAux : TFDQuery;
begin
  qryAux := TFDQuery.Create(nil);
  try
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from cc_entidade_aulas c                           '+
                   ' where c.cca_ent_codigo     = :p_cca_ent_codigo     '+
                   '   and c.cca_ent_emp_codigo = :p_cca_ent_emp_codigo '+
                   '   and c.cca_pro_codigo     = :p_cca_pro_codigo     '+
                   '   and c.cca_pro_emp_codigo = :p_cca_pro_emp_codigo ');
    qryAux.ParamByName('p_cca_ent_codigo').AsInteger     := PEntCodigo;
    qryAux.ParamByName('p_cca_ent_emp_codigo').AsInteger := PEntEmpCodigo;
    qryAux.ParamByName('p_cca_pro_codigo').AsInteger     := PProCodigo;
    qryAux.ParamByName('p_cca_pro_emp_codigo').AsInteger := PProEmpCodigo;
    qryAux.Open;

    Result := (not qryAux.Eof);

    cca_ent_codigo     := qryAux.FieldByName('cca_ent_codigo').AsInteger;
    cca_ent_emp_codigo := qryAux.FieldByName('cca_ent_emp_codigo').AsInteger;
    cca_pro_codigo     := qryAux.FieldByName('cca_pro_codigo').AsInteger;
    cca_pro_emp_codigo := qryAux.FieldByName('cca_pro_emp_codigo').AsInteger;
    cca_saldo_horas    := qryAux.FieldByName('cca_saldo_horas').AsFloat;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TCC_entidade_aulas.proc_gravar;
var qryAux : TFDQuery;
begin
  qryAux := TFDQuery.Create(nil);
  try
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add('  replace into cc_entidade_aulas values ( '+
                   '    :p_cca_ent_codigo    ,                '+
                   '    :p_cca_ent_emp_codigo,                '+
                   '    :p_cca_pro_codigo    ,                '+
                   '    :p_cca_pro_emp_codigo,                '+
                   '    :p_cca_saldo_horas   )                ');
    qryAux.ParamByName('p_cca_ent_codigo').AsInteger     := cca_ent_codigo;
    qryAux.ParamByName('p_cca_ent_emp_codigo').AsInteger := cca_ent_emp_codigo;
    qryAux.ParamByName('p_cca_pro_codigo').AsInteger     := cca_pro_codigo;
    qryAux.ParamByName('p_cca_pro_emp_codigo').AsInteger := cca_pro_emp_codigo;
    qryAux.ParamByName('p_cca_saldo_horas').AsFloat      := cca_saldo_horas;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
