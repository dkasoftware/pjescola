inherited FrmMov_Agendamentos: TFrmMov_Agendamentos
  Caption = 'Agendamentos'
  ClientHeight = 591
  ClientWidth = 960
  OnActivate = FormActivate
  ExplicitWidth = 966
  ExplicitHeight = 620
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 960
    Height = 572
    ExplicitWidth = 960
    ExplicitHeight = 572
    inherited pnlButtons: TPanel
      Top = 536
      Width = 954
      ExplicitTop = 536
      ExplicitWidth = 954
      inherited btnExcluir: TBitBtn
        OnClick = btnExcluirClick
      end
      inherited btnCancelar: TBitBtn
        OnClick = btnCancelarClick
      end
      inherited btnEditar: TBitBtn
        OnClick = btnEditarClick
      end
      inherited btnGravar: TBitBtn
        OnClick = btnGravarClick
      end
      inherited btnNovo: TBitBtn
        OnClick = btnNovoClick
      end
      inherited btnSair: TBitBtn
        Left = 864
        ExplicitLeft = 864
      end
    end
    object PC: TPageControl
      Left = 3
      Top = 3
      Width = 954
      Height = 533
      ActivePage = tsCadastro
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object tsConsulta: TTabSheet
        Caption = 'Consulta'
        object TPanel
          Left = 0
          Top = 0
          Width = 946
          Height = 502
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Padding.Left = 3
          Padding.Top = 3
          Padding.Right = 3
          Padding.Bottom = 3
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          object TPanel
            Left = 3
            Top = 3
            Width = 940
            Height = 51
            Align = alTop
            BevelOuter = bvNone
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            DesignSize = (
              940
              51)
            object Label3: TLabel
              Left = 366
              Top = 25
              Width = 84
              Height = 16
              Alignment = taRightJustify
              Caption = 'Cliente / Aluno'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object gbPeriodo: TGroupBox
              Left = 0
              Top = 0
              Width = 353
              Height = 51
              Align = alLeft
              Caption = 'Per'#237'odo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object Label1: TLabel
                Left = 18
                Top = 24
                Width = 33
                Height = 16
                Alignment = taRightJustify
                Caption = 'Inicial'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label2: TLabel
                Left = 191
                Top = 24
                Width = 27
                Height = 16
                Alignment = taRightJustify
                Caption = 'Final'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object ed_data_inicial: TDateTimePicker
                Left = 57
                Top = 21
                Width = 120
                Height = 24
                Date = 43852.284125532410000000
                Time = 43852.284125532410000000
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                OnChange = ed_data_inicialChange
              end
              object ed_data_final: TDateTimePicker
                Left = 224
                Top = 21
                Width = 120
                Height = 24
                Date = 43852.284125532410000000
                Time = 43852.284125532410000000
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                OnChange = ed_data_finalChange
              end
            end
            object ed_consulta: TEdit
              Left = 456
              Top = 21
              Width = 480
              Height = 24
              Anchors = [akLeft, akTop, akRight]
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              OnChange = ed_consultaChange
            end
          end
          object TPanel
            Left = 3
            Top = 54
            Width = 940
            Height = 445
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Padding.Left = 3
            Padding.Top = 3
            Padding.Right = 3
            Padding.Bottom = 3
            ParentFont = False
            TabOrder = 1
            object dbgAgendamentos: TDBGrid
              Left = 4
              Top = 4
              Width = 932
              Height = 437
              Align = alClient
              DataSource = DSAgendamentos
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              ParentFont = False
              PopupMenu = popOpcoesGrid
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -13
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              OnDrawColumnCell = dbgAgendamentosDrawColumnCell
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'dia_da_semana'
                  Title.Alignment = taCenter
                  Title.Caption = 'Dia'
                  Width = 132
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'agd_data'
                  Title.Alignment = taCenter
                  Title.Caption = 'Data'
                  Width = 90
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'agd_hora'
                  Title.Alignment = taCenter
                  Title.Caption = 'Hora'
                  Width = 60
                  Visible = True
                end
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'agd_qtde_horas'
                  Title.Alignment = taCenter
                  Title.Caption = 'H'
                  Width = 30
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'descricao_sala'
                  Title.Caption = 'Sala'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nome_aluno'
                  Title.Caption = 'Cliente / Aluno'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nome_professor'
                  Title.Caption = 'Professor'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'agd_observacoes'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'nome_produto'
                  Visible = True
                end>
            end
          end
        end
      end
      object tsCadastro: TTabSheet
        Caption = 'Movimenta'#231#227'o'
        ImageIndex = 1
        object TPanel
          Left = 0
          Top = 0
          Width = 946
          Height = 502
          Align = alClient
          BevelOuter = bvNone
          Padding.Left = 3
          Padding.Top = 1
          Padding.Right = 3
          Padding.Bottom = 3
          ParentBackground = False
          TabOrder = 0
          object TPanel
            Left = 3
            Top = 1
            Width = 940
            Height = 51
            Align = alTop
            BevelOuter = bvNone
            Padding.Left = 3
            Padding.Top = 1
            Padding.Right = 3
            Padding.Bottom = 3
            TabOrder = 1
            object TGroupBox
              Left = 136
              Top = 1
              Width = 671
              Height = 47
              Align = alLeft
              Caption = 'Empresa'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object cmb_agd_emp_codigo: TComboBox
                Left = 7
                Top = 18
                Width = 654
                Height = 24
                Style = csDropDownList
                TabOrder = 0
              end
            end
            object gb_ent_codigo: TGroupBox
              Left = 3
              Top = 1
              Width = 133
              Height = 47
              Align = alLeft
              Caption = 'C'#243'digo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object ed_agd_codigo: TEdit
                Left = 6
                Top = 18
                Width = 121
                Height = 24
                TabStop = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                NumbersOnly = True
                ParentFont = False
                ReadOnly = True
                TabOrder = 0
              end
            end
          end
          object GroupBox1: TGroupBox
            Left = 3
            Top = 52
            Width = 940
            Height = 447
            Align = alClient
            Caption = 'Agendamento'
            TabOrder = 0
            object TLabel
              Left = 398
              Top = 271
              Width = 42
              Height = 16
              Alignment = taRightJustify
              Caption = 'Hor'#225'rio'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object TLabel
              Left = 372
              Top = 297
              Width = 68
              Height = 16
              Alignment = taRightJustify
              Caption = 'Qtde. Horas'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object lbl_con_ent_codigo_funcionario: TLabel
              Left = 64
              Top = 193
              Width = 66
              Height = 16
              Alignment = taRightJustify
              Caption = 'Funcion'#225'rio'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object lbl_con_pro_codigo: TLabel
              Left = 31
              Top = 167
              Width = 99
              Height = 16
              Alignment = taRightJustify
              Caption = 'Produto / Servi'#231'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object lbl_con_descricao: TLabel
              Left = 75
              Top = 245
              Width = 55
              Height = 16
              Alignment = taRightJustify
              Caption = 'Descri'#231#227'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label4: TLabel
              Left = 81
              Top = 27
              Width = 39
              Height = 16
              Alignment = taRightJustify
              Caption = 'Cliente'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label5: TLabel
              Left = 105
              Top = 219
              Width = 25
              Height = 16
              Alignment = taRightJustify
              Caption = 'Sala'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object cmb_ent_codigo_funcionario: TComboBox
              Left = 136
              Top = 189
              Width = 562
              Height = 24
              Style = csDropDownList
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              OnChange = cmb_ent_codigo_funcionarioChange
            end
            object cmb_pro_codigo: TComboBox
              Left = 136
              Top = 163
              Width = 562
              Height = 24
              Style = csDropDownList
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object ed_agd_observacoes: TEdit
              Left = 136
              Top = 241
              Width = 562
              Height = 24
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              MaxLength = 150
              ParentFont = False
              TabOrder = 3
            end
            object cmb_sala_codigo: TComboBox
              Left = 136
              Top = 215
              Width = 562
              Height = 24
              Style = csDropDownList
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              OnChange = cmb_sala_codigoChange
            end
            object mc_agd_data: TMonthCalendar
              Left = 136
              Top = 267
              Width = 225
              Height = 160
              Date = 43866.612371886570000000
              TabOrder = 4
              OnClick = mc_agd_dataClick
            end
            object cmb_agd_hora: TComboBox
              Left = 446
              Top = 267
              Width = 97
              Height = 24
              Style = csDropDownList
              TabOrder = 5
            end
            object cmb_agd_qtde_hora: TComboBox
              Left = 446
              Top = 293
              Width = 97
              Height = 24
              Style = csDropDownList
              TabOrder = 6
              Items.Strings = (
                '0,25'
                '0,5'
                '0,75'
                '1'
                '1,25'
                '1,5'
                '1,75'
                '2')
            end
            object ed_ent_codigo: TEdit
              Left = 136
              Top = 23
              Width = 562
              Height = 24
              CharCase = ecUpperCase
              TabOrder = 7
              OnChange = ed_ent_codigoChange
            end
            object dbgEntidades: TDBGrid
              Left = 136
              Top = 50
              Width = 562
              Height = 110
              DataSource = dsEntidades
              Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 8
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -13
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              OnDrawColumnCell = dbgEntidadesDrawColumnCell
              Columns = <
                item
                  Expanded = False
                  FieldName = 'ent_razao_social'
                  Title.Caption = 'Nome'
                  Width = 526
                  Visible = True
                end>
            end
            object pnlBuscaEntidades: TPanel
              Left = 704
              Top = 175
              Width = 562
              Height = 224
              BevelInner = bvLowered
              Padding.Left = 3
              Padding.Top = 3
              Padding.Right = 3
              Padding.Bottom = 3
              TabOrder = 9
              Visible = False
              object dbgBuscaEntdades: TDBGrid
                Left = 5
                Top = 5
                Width = 552
                Height = 214
                Align = alClient
                DataSource = dsBuscaEntidades
                Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -13
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                OnDrawColumnCell = dbgBuscaEntdadesDrawColumnCell
                OnDblClick = dbgBuscaEntdadesDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'ent_razao_social'
                    Title.Caption = 'Nome'
                    Width = 517
                    Visible = True
                  end>
              end
            end
            object btnAddEntidade: TBitBtn
              Left = 699
              Top = 23
              Width = 24
              Height = 24
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
                006600006600B59A9BB59A9BB59A9BB59A9BB59A9BB59A9BB59A9B0066000066
                00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8E8E8E8EBEBEBEBEBEBEBEBEBEBE
                BEBEBEBEBEBEBEBEBEBEBE8686868E8E8EFF00FFFF00FFFF00FFFF00FF006600
                009900009900E5DEDF006600006600E4E7E7E0E3E6D9DFE0CCC9CC006600037D
                03006600FF00FFFF00FFFF00FF8E8E8EB8B8B8AEAEAEECECEC838383838383F0
                F0F0EEEEEEE8E8E8DADADA7E7E7E9A9A9A8E8E8EFF00FFFF00FFFF00FF006600
                009900009900E9E2E2006600006600E2E1E3E2E6E8DDE2E4CFCCCF006600037D
                03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ACACACF0F0F0838383838383ED
                EDEDF0F0F0ECECECDCDCDC7F7F7F9999998E8E8EFF00FFFF00FFFF00FF006600
                009900009900ECE4E4006600006600DFDDDFE1E6E8E0E5E7D3D0D2006600037D
                03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4ABABABF2F2F2838383838383EA
                EAEAEFEFEFEEEEEEE0E0E07C7C7C9898988E8E8EFF00FFFF00FFFF00FF006600
                009900009900EFE6E6EDE5E5E5DEDFE0DDDFDFE0E2E0E1E3D6D0D2006600037D
                03006600FF00FFFF00FFFF00FF8E8E8EB4B4B4AAAAAAF4F4F4F3F3F3ECECECEA
                EAEAECECECECECECE1E1E18585859D9D9D8E8E8EFF00FFFF00FFFF00FF006600
                0099000099000099000099000099000099000099000099000099000099000099
                00006600FF00FFFF00FFFF00FF8E8E8EB1B1B1AFAFAFB2B2B2B8B8B8B6B6B6B1
                B1B1AFAFAFB5B5B5B2B2B2ACACACB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                009900B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B1B1D0B10099
                00006600FF00FFFF00FFFF00FF8E8E8EA2A2A2B6B6B6CBCBCBD0D0D0D1D1D1D0
                D0D0CECECECDCDCDD1D1D1D3D3D3B3B3B38E8E8EFF00FFFF00FFFF00FF006600
                009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                009900F9F9F9CDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDCDF9F9F90099
                00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFDCDCDCDCDCDCDCDCDCDC
                DCDCDCDCDCDCDCDCDCDCDCFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FF006600
                009900F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90099
                00006600FF00FFFF00FFFF00FF8E8E8EB3B3B3FFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFB3B3B38E8E8EFF00FFFF00FFFF00FFFF00FF
                006600F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F90066
                00FF00FFFF00FFFF00FFFF00FFFF00FF8E8E8EFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF8E8E8EFF00FFFF00FFFF00FFFF00FFFF00FF
                FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
                00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
              NumGlyphs = 2
              TabOrder = 10
              OnClick = btnAddEntidadeClick
            end
            object btnRetEntidade: TBitBtn
              Left = 699
              Top = 50
              Width = 24
              Height = 24
              Glyph.Data = {
                36060000424D3606000000000000360000002800000020000000100000000100
                18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FFFF00FFFF00FF
                26B7FF22B4FF34BDFF64D4FF77DCFF6EDCFF61DDFF67E1FF64DFFF44CFFF2FC1
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C4C4CACACADBDBDBE0E0E0DF
                DFDFDFDFDFE2E2E2E0E0E0D5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FF4EC8FF64D4FF9EE8FFB8F0FFA5EEFF8CECFF90EEFF8DEDFF70E1FF33C2
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D2D2D2DBDBDBEAEAEAF0F0F0EE
                EEEEEAEAEAECECECEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FF73D6FF61D3FF9AE6FFB5EFFF9EEBFF2034AB0202930202930202930202
                93020293020293020293FF00FFFF00FFC6C6C6DDDDDDDADADAE9E9E9F0F0F0EC
                ECEC888888828282828282828282828282828282828282828282FF00FFFF00FF
                26B7FF8CDEFF5ED3FF96E5FFB0EDFF97E8FF0A0A987272F37575F46C6CF43E3E
                E31616C50303AA020293FF00FFFF00FFC6C6C6E3E3E3D9D9D9E8E8E8EFEFEFEA
                EAEA838383AAAAAAABABABA7A7A7919191868686838383828282FF00FFFF00FF
                26B7FFA5E5FF59D2FF91E4FFABEAFF8EE4FF1C32AB0A0A980A0A980A0A980A0A
                980A0A980A0A98020293FF00FFFF00FFC6C6C6EAEAEAD8D8D8E7E7E7ECECECE7
                E7E7888888838383838383838383838383838383838383828282FF00FFFF00FF
                26B7FFBEECFF5ED4FF8CE3FFA6EAFF85E1FF59D7FF58D6FF56D6FF46CDFF4CC8
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6F0F0F0DADADAE6E6E6ECECECE4
                E4E4DBDBDBDADADADADADAD4D4D4D2D2D2C6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FF96DBFFC2EDFFD2F4FFD5F5FFD4F5FFD2F4FFD2F4FFD2F4FFCEF1FF92DB
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E4E1E4F0F0F0F6F6F6F6F6F6F6
                F6F6F6F6F6F6F6F6F6F6F6F3F3F3E3E1E3C6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FF22B2FF2FBDFF4ACBFF58D3FF56D5FF51D8FF57DCFF54DAFF42CEFF2DBE
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C2C4CACACAD3D3D3D9D9D9DA
                DADADBDBDBDDDDDDDCDCDCD4D4D4CAC8CAC6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FF47C4FF62D3FF9DE8FFB9F0FFA6EEFF8EECFF92EFFF8FEDFF71E2FF33C3
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D0D0D0DADADAEAEAEAF0F0F0EE
                EEEEEAEAEAEDEDEDEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FF70D5FF60D3FF9AE6FFB6EFFFA0ECFF83E7FF85E9FF82E8FF68DEFF32C1
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6DCDCDCDADADAE9E9E9F0F0F0EC
                ECECE7E7E7E9E9E9E8E8E8E0E0E0CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FF88DCFF5DD2FF96E5FFB1EDFF99E9FF77E2FF76E3FF74E2FF5ED9FF2EBE
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E2E2E2D9D9D9E8E8E8EFEFEFEA
                EAEAE3E3E3E3E3E3E3E3E3DDDDDDCACACAC6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FFA1E4FF59D2FF91E3FFACEBFF90E5FF69DDFF68DDFF67DDFF53D3FF29BA
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E9E9E9D8D8D8E6E6E6EDEDEDE7
                E7E7DFDFDFDFDFDFDFDFDFD8D8D8C8C8C8C6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FFBAEBFF54D2FF8CE3FFA7E9FF88E2FF5CD7FF5AD7FF58D7FF48CEFF39C0
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EFEFEFD8D8D8E6E6E6EBEBEBE5
                E5E5DBDBDBDBDBDBDBDBDBD5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                26B7FFAAE5FFC6F0FFC1F0FFCBF3FFC1F0FFB3ECFFB2ECFFB1ECFFBDEEFF9FE4
                FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EAEAEAF2F2F2F1F1F1F4F4F4F1
                F1F1EEEEEEEEEEEEEEEEEEF0F0F0E9E9E9C6C6C6FF00FFFF00FFFF00FFFF00FF
                FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FF}
              NumGlyphs = 2
              TabOrder = 11
            end
          end
        end
      end
    end
  end
  inherited SB: TStatusBar
    Top = 572
    Width = 960
    ExplicitTop = 572
    ExplicitWidth = 960
  end
  object pnlRepetirAgendamento: TPanel
    Left = 722
    Top = 194
    Width = 343
    Height = 143
    BevelInner = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object TLabel
      Left = 31
      Top = 54
      Width = 155
      Height = 16
      Caption = 'Quantas vezes agendadas?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object TLabel
      Left = 96
      Top = 24
      Width = 90
      Height = 16
      Caption = 'Como agendar?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cmbQtdeRepeticao: TComboBox
      Left = 192
      Top = 50
      Width = 127
      Height = 24
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      ParentFont = False
      TabOrder = 1
      Text = '1'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '10')
    end
    object btnRepetirAgendamento: TBitBtn
      Left = 67
      Top = 88
      Width = 100
      Height = 24
      Caption = 'Confirmar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
        FF00FFFF00FF16A7C416A7C4FF00FFFF00FFFF00FFFF00FF16A7C416A7C4FF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFFF
        00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF16A7C473D8E677E0EC16A7C416A7C416A7C41BAEC937D5E627C8DC16A7
        C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCDCDCDCE1E1E1BCBCBCBC
        BCBCBCBCBCBFBFBFD6D6D6CDCDCDBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF16A7C498EAF389E7F174DFEC6DE1ED59D9E850DBEA41D8E833D4E616A7
        C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCEAEAEAE7E7E7E0E0E0E1
        E1E1DBDBDBDBDBDBD8D8D8D6D6D6BCBCBCFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF16A7C49FEBF48BE4EE6AD3E374D6E56DD5E44CCDDF47D5E637CFE116A7
        C4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCEBEBEBE5E5E5D8D8D8DB
        DBDBDADADAD3D3D3D7D7D7D3D3D3BCBCBCFF00FFFF00FFFF00FFFF00FF16A7C4
        16A7C46BD3E387DFEB8EDFEA16A7C416A7C416A7C416A7C461D5E53FD0E224BA
        D216A7C416A7C4FF00FFFF00FFBCBCBCBCBCBCD8D8D8E2E2E2E2E2E2BCBCBCBC
        BCBCBCBCBCBCBCBCD9D9D9D3D3D3C6C6C6BCBCBCBCBCBCFF00FF16A7C450CDE0
        61D4E484E6F07DD8E616A7C4A7A7A7A7A7A7A7A7A7A7A7A716A7C45AD0E145D8
        E82BC5DB22C1D716A7C4BCBCBCD3D3D3D8D8D8E6E6E6DDDDDDBCBCBCC0C0C0C0
        C0C0C0C0C0C0C0C0BCBCBCD6D6D6D9D9D9CDCDCDC9C9C9BCBCBC16A7C462DFEC
        6FE1EE64D5E416A7C4A7A7A7FEFEFEFDFDFDF7F7F7E5E5E5A7A7A716A7C445D2
        E342D8E833D4E616A7C4BCBCBCDFDFDFE1E1E1D9D9D9BCBCBCC0C0C0FEFEFEFD
        FDFDF9F9F9EBEBEBC0C0C0BCBCBCD6D6D6D8D8D8D6D6D6BCBCBC16A7C444CBDE
        65DFEC54CEE016A7C4A7A7A7F9F9F9F9F9F9F4F4F4E5E5E5A7A7A716A7C443CB
        DE4DDAE927BED516A7C4BCBCBCD1D1D1DFDFDFD4D4D4BCBCBCC0C0C0FAFAFAFA
        FAFAF6F6F6EBEBEBC0C0C0BCBCBCD1D1D1DBDBDBC8C8C8BCBCBCFF00FF16A7C4
        47CBDE4DCDDF16A7C4A7A7A7E8E8E8EEEEEEE9E9E9DADADAA7A7A716A7C44CCC
        DF34BDD416A7C4FF00FFFF00FFBCBCBCD1D1D1D3D3D3BCBCBCC0C0C0EDEDEDF2
        F2F2EEEEEEE3E3E3C0C0C0BCBCBCD3D3D3C9C9C9BCBCBCFF00FFFF00FFFF00FF
        16A7C45FDEEC16A7C4A7A7A7BCBCBCD5D5D5D2D2D2B9B9B9A7A7A716A7C471E1
        EE16A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCDEDEDEBCBCBCC0C0C0CDCDCDDF
        DFDFDDDDDDCBCBCBC0C0C0BCBCBCE2E2E2BCBCBCFF00FFFF00FFFF00FFFF00FF
        16A7C455DCEA4FD1E216A7C4A7A7A7A7A7A7A7A7A7A7A7A716A7C470D7E67CE4
        EF16A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCDCDCDCD6D6D6BCBCBCC0C0C0C0
        C0C0C0C0C0C0C0C0BCBCBCDBDBDBE4E4E4BCBCBCFF00FFFF00FFFF00FF16A7C4
        40D7E84CDAE958DDEB52D1E216A7C416A7C416A7C416A7C48BE0EC95E9F286E6
        F078E3EF16A7C4FF00FFFF00FFBCBCBCD8D8D8DBDBDBDDDDDDD6D6D6BCBCBCBC
        BCBCBCBCBCBCBCBCE3E3E3E9E9E9E6E6E6E3E3E3BCBCBCFF00FFFF00FF16A7C4
        33CEE13BD0E216A7C45CDDEB68E0ED74E2EE81E5F08DE8F190E5EF16A7C491E8
        F27BE0EC16A7C4FF00FFFF00FFBCBCBCD2D2D2D3D3D3BCBCBCDDDDDDE0E0E0E3
        E3E3E5E5E5E8E8E8E6E6E6BCBCBCE8E8E8E1E1E1BCBCBCFF00FFFF00FFFF00FF
        16A7C416A7C4FF00FF16A7C416A7C46BE0ED77E3EF16A7C416A7C4FF00FF16A7
        C416A7C4FF00FFFF00FFFF00FFFF00FFBCBCBCBCBCBCFF00FFBCBCBCBCBCBCE0
        E0E0E3E3E3BCBCBCBCBCBCFF00FFBCBCBCBCBCBCFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FF16A7C462DEEC6EE1ED16A7C4FF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCDF
        DFDFE1E1E1BCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FF16A7C416A7C4FF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFBC
        BCBCBCBCBCFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 2
      OnClick = btnRepetirAgendamentoClick
    end
    object btnFecharRepeticao: TBitBtn
      Left = 173
      Top = 88
      Width = 100
      Height = 24
      Caption = 'Fechar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Glyph.Data = {
        36060000424D3606000000000000360000002800000020000000100000000100
        18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FF01079F0313A90418AE0419AE0313A90108A0FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9B9B9B9F9F9FA2
        A2A2A2A2A29F9F9F9B9B9BFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FF01049D041CB10730C00734C40735C50735C50734C30731C1041FB30106
        9EFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9A9A9AA2A2A2A9A9A9AAAAAAAB
        ABABABABABAAAAAAA9A9A9A3A3A39A9A9AFF00FFFF00FFFF00FFFF00FFFF00FF
        0109A1052BC30735C70733C20732C20732C20732C20732C20733C30735C4062D
        BE020CA4FF00FFFF00FFFF00FFFF00FF9C9C9CA9A9A9ABABABA9A9A9A9A9A9A9
        A9A9A9A9A9A9A9A9AAAAAAAAAAAAA8A8A89D9D9DFF00FFFF00FFFF00FF01049B
        052BCA0636D80431CD0027C4032EC10732C20732C20430C10027BF042FC10735
        C4072EBE01069EFF00FFFF00FF9A9A9AABABABB1B1B1ACACACA8A8A8A8A8A8A9
        A9A9A9A9A9A8A8A8A6A6A6A8A8A8AAAAAAA8A8A89A9A9AFF00FFFF00FF031ABA
        0537E70331DD123DD86480E01840CB002CC1022DC00F38C46580D91B43C7052F
        C10735C5051FB3FF00FFFF00FFA5A5A5B6B6B6B2B2B2B5B5B5D5D5D5B2B2B2A6
        A6A6A7A7A7ADADADD3D3D3B2B2B2A9A9A9ABABABA4A4A4FF00FF01049E0430E4
        0436F1002AE45070E9FFFFFFB7C4F10D36CA042DC3A2B2E8FFFFFF6984DA0026
        BE0733C30731C10108A09A9A9AB4B4B4B9B9B9B3B3B3D1D1D1FFFFFFF8F8F8AE
        AEAEA9A9A9EDEDEDFFFFFFD4D4D4A6A6A6AAAAAAA9A9A99B9B9B020FAF0336FA
        0335F80232EE0A35E88CA2F2FFFFFFB4C2F1A9B8EDFFFFFFA7B7E9133AC4052F
        C10732C20734C40313AAA1A1A1BBBBBBBBBBBBB7B7B7B8B8B8E9E9E9FFFFFFF6
        F6F6F2F2F2FFFFFFEFEFEFAEAEAEA9A9A9A9A9A9AAAAAA9F9F9F0619BC1747FE
        093AFC0435F80131F0002BE891A5F4FFFFFFFFFFFFABBAEF062FC5022DC00732
        C20732C20736C50419AEA7A7A7C4C4C4BEBEBEBBBBBBB7B7B7B4B4B4EBEBEBFF
        FFFFFFFFFFF3F3F3AAAAAAA7A7A7A9A9A9A9A9A9ABABABA2A2A20B1DBE4168FE
        1C49FC0335FB0031F90531F2A4B5F7FFFFFFFFFFFFB9C6F20D36D0002CC60732
        C20732C20736C50418ADA9A9A9D3D3D3C5C5C5BCBCBCBABABAB9B9B9F3F3F3FF
        FFFFFFFFFFF9F9F9B0B0B0A9A9A9A9A9A9A9A9A9ABABABA1A1A10613B45B7CFC
        486CFD0133FB113CFBA1B4FEFFFFFFA4B6F892A7F5FFFFFFB6C4F21A41D3042F
        C80732C40734C30212A9A4A4A4DBDBDBD5D5D5BBBBBBC1C1C1F4F4F4FFFFFFF4
        F4F4ECECECFFFFFFF8F8F8B6B6B6ABABABAAAAAAAAAAAA9F9F9F0003A04A6AF3
        8FA6FF1F46FB4C6FFCFFFFFFA7B8FE0733F6002AED8CA2F6FFFFFF627FE70028
        D00734CC0730C300069F9B9B9BD2D2D2EFEFEFC6C6C6D6D6D6FFFFFFF6F6F6BB
        BBBBB6B6B6EBEBEBFFFFFFD6D6D6ACACACADADADAAAAAA9A9A9AFF00FF1A2FCB
        99AFFF8BA2FE214DFB4D71FC0E3DFB0030FB0031F70636F14C6EF1103CE30432
        DB0636D7041CB5FF00FFFF00FFB3B3B3F2F2F2EDEDEDC7C7C7D6D6D6C0C0C0BB
        BBBBB9B9B9B9B9B9D2D2D2B8B8B8B1B1B1B0B0B0A4A4A4FF00FFFF00FF0004A0
        415EECB8C7FF9CAFFD3A5CFC0A3AFB0335FB0335FB0133F9052FF20635EB0537
        E9052CCD00049CFF00FFFF00FF9B9B9BCCCCCCFDFDFDF2F2F2D0D0D0BEBEBEBC
        BCBCBCBCBCBBBBBBB9B9B9B7B7B7B7B7B7ADADAD9A9A9AFF00FFFF00FFFF00FF
        0309A54260ECA9BBFFBDCAFF8EA5FE6483FD5073FC4A6EFD3961FD1444F9042C
        D70109A2FF00FFFF00FFFF00FFFF00FF9E9E9ECDCDCDF8F8F8FFFFFFEEEEEEDF
        DFDFD8D8D8D5D5D5D0D0D0C1C1C1B0B0B09C9C9CFF00FFFF00FFFF00FFFF00FF
        FF00FF0004A01E32CD5876F6859EFE8BA3FF7994FE5376FC234AF0051EC50104
        9CFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9B9B9BB5B5B5D8D8D8EBEBEBED
        EDEDE6E6E6D8D8D8C3C3C3AAAAAA9A9A9AFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FF0004A00917B61022C30D1FC20311B401059FFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF9B9B9BA6A6A6AD
        ADADABABABA3A3A39B9B9BFF00FFFF00FFFF00FFFF00FFFF00FF}
      NumGlyphs = 2
      ParentFont = False
      TabOrder = 3
      OnClick = btnFecharRepeticaoClick
    end
    object cmbComoAgendar: TComboBox
      Left = 192
      Top = 20
      Width = 127
      Height = 24
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemIndex = 0
      ParentFont = False
      TabOrder = 0
      Text = 'SEMANALMENTE'
      Items.Strings = (
        'SEMANALMENTE'
        'DIARIAMENTE'
        'MENSALMENTE')
    end
  end
  object DSAgendamentos: TDataSource
    DataSet = CDSAgendamentos
    Top = 85
  end
  object CDSAgendamentos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROAgendamentos'
    OnCalcFields = CDSAgendamentosCalcFields
    Top = 57
    object CDSAgendamentosagd_codigo: TIntegerField
      FieldName = 'agd_codigo'
      Required = True
    end
    object CDSAgendamentosagd_emp_codigo: TIntegerField
      FieldName = 'agd_emp_codigo'
      Required = True
    end
    object CDSAgendamentosagd_ent_codigo: TIntegerField
      FieldName = 'agd_ent_codigo'
    end
    object CDSAgendamentosagd_ent_emp_codigo: TIntegerField
      FieldName = 'agd_ent_emp_codigo'
    end
    object CDSAgendamentosagd_fun_ent_codigo: TIntegerField
      FieldName = 'agd_fun_ent_codigo'
    end
    object CDSAgendamentosagd_fun_ent_emp_codigo: TIntegerField
      FieldName = 'agd_fun_ent_emp_codigo'
    end
    object CDSAgendamentosagd_pro_codigo: TIntegerField
      FieldName = 'agd_pro_codigo'
    end
    object CDSAgendamentosagd_pro_emp_codigo: TIntegerField
      FieldName = 'agd_pro_emp_codigo'
    end
    object CDSAgendamentosagd_sala_codigo: TIntegerField
      FieldName = 'agd_sala_codigo'
    end
    object CDSAgendamentosagd_sala_emp_codigo: TIntegerField
      FieldName = 'agd_sala_emp_codigo'
    end
    object CDSAgendamentosagd_data: TDateField
      FieldName = 'agd_data'
    end
    object CDSAgendamentosagd_hora: TStringField
      FieldName = 'agd_hora'
      Size = 5
    end
    object CDSAgendamentosagd_qtde_horas: TBCDField
      FieldName = 'agd_qtde_horas'
      Precision = 15
    end
    object CDSAgendamentosagd_situacao: TIntegerField
      FieldName = 'agd_situacao'
    end
    object CDSAgendamentosagd_observacoes: TStringField
      FieldName = 'agd_observacoes'
      Size = 200
    end
    object CDSAgendamentosnome_aluno: TStringField
      DisplayWidth = 150
      FieldName = 'nome_aluno'
      Size = 150
    end
    object CDSAgendamentosnome_professor: TStringField
      DisplayWidth = 150
      FieldName = 'nome_professor'
      Size = 150
    end
    object CDSAgendamentosnome_produto: TStringField
      DisplayWidth = 150
      FieldName = 'nome_produto'
      Size = 150
    end
    object CDSAgendamentosdescricao_sala: TStringField
      FieldName = 'descricao_sala'
      Size = 60
    end
    object CDSAgendamentosdia_da_semana: TStringField
      FieldKind = fkCalculated
      FieldName = 'dia_da_semana'
      Size = 25
      Calculated = True
    end
  end
  object PROAgendamentos: TDataSetProvider
    DataSet = FDQAgendamentos
    Top = 29
  end
  object FDQAgendamentos: TFDQuery
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select *'
      'from agendamentos')
    Top = 1
    object FDQAgendamentosagd_codigo: TIntegerField
      FieldName = 'agd_codigo'
      Origin = 'agd_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQAgendamentosagd_emp_codigo: TIntegerField
      FieldName = 'agd_emp_codigo'
      Origin = 'agd_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQAgendamentosagd_ent_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'agd_ent_codigo'
      Origin = 'agd_ent_codigo'
    end
    object FDQAgendamentosagd_ent_emp_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'agd_ent_emp_codigo'
      Origin = 'agd_ent_emp_codigo'
    end
    object FDQAgendamentosagd_fun_ent_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'agd_fun_ent_codigo'
      Origin = 'agd_fun_ent_codigo'
    end
    object FDQAgendamentosagd_fun_ent_emp_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'agd_fun_ent_emp_codigo'
      Origin = 'agd_fun_ent_emp_codigo'
    end
    object FDQAgendamentosagd_pro_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'agd_pro_codigo'
      Origin = 'agd_pro_codigo'
    end
    object FDQAgendamentosagd_pro_emp_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'agd_pro_emp_codigo'
      Origin = 'agd_pro_emp_codigo'
    end
    object FDQAgendamentosagd_sala_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'agd_sala_codigo'
      Origin = 'agd_sala_codigo'
    end
    object FDQAgendamentosagd_sala_emp_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'agd_sala_emp_codigo'
      Origin = 'agd_sala_emp_codigo'
    end
    object FDQAgendamentosagd_data: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'agd_data'
      Origin = 'agd_data'
    end
    object FDQAgendamentosagd_hora: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'agd_hora'
      Origin = 'agd_hora'
      Size = 5
    end
    object FDQAgendamentosagd_qtde_horas: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'agd_qtde_horas'
      Origin = 'agd_qtde_horas'
      Precision = 15
    end
    object FDQAgendamentosagd_situacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'agd_situacao'
      Origin = 'agd_situacao'
    end
    object FDQAgendamentosagd_observacoes: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'agd_observacoes'
      Origin = 'agd_observacoes'
      Size = 200
    end
    object FDQAgendamentosnome_aluno: TStringField
      FieldName = 'nome_aluno'
      Size = 150
    end
    object FDQAgendamentosnome_professor: TStringField
      FieldName = 'nome_professor'
      Size = 150
    end
    object FDQAgendamentosnome_produto: TStringField
      FieldName = 'nome_produto'
      Size = 150
    end
    object FDQAgendamentosdescricao_sala: TStringField
      FieldName = 'descricao_sala'
      Size = 60
    end
  end
  object popOpcoesGrid: TPopupMenu
    Left = 34
    Top = 4
    object RepetirAgendamento1: TMenuItem
      Caption = 'Repetir Agendamento'
      OnClick = RepetirAgendamento1Click
    end
  end
  object cdsEntidades: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 28
    Top = 57
    object cdsEntidadesent_codigo: TIntegerField
      FieldName = 'ent_codigo'
    end
    object cdsEntidadesent_emp_codigo: TIntegerField
      FieldName = 'ent_emp_codigo'
    end
    object cdsEntidadesent_razao_social: TStringField
      FieldName = 'ent_razao_social'
      Size = 150
    end
  end
  object dsEntidades: TDataSource
    DataSet = cdsEntidades
    OnDataChange = dsEntidadesDataChange
    Left = 28
    Top = 85
  end
  object FDQBuscaEntidades: TFDQuery
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select *'
      'from entidades')
    Left = 56
    Top = 57
    object FDQBuscaEntidadesent_codigo: TIntegerField
      FieldName = 'ent_codigo'
      Origin = 'ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBuscaEntidadesent_emp_codigo: TIntegerField
      FieldName = 'ent_emp_codigo'
      Origin = 'ent_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBuscaEntidadesent_razao_social: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_razao_social'
      Origin = 'ent_razao_social'
      Size = 150
    end
    object FDQBuscaEntidadesent_nome_fantasia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_nome_fantasia'
      Origin = 'ent_nome_fantasia'
      Size = 150
    end
    object FDQBuscaEntidadesent_tipo_pessoa: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ent_tipo_pessoa'
      Origin = 'ent_tipo_pessoa'
    end
    object FDQBuscaEntidadesent_cnpj: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_cnpj'
      Origin = 'ent_cnpj'
    end
    object FDQBuscaEntidadesent_ie: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_ie'
      Origin = 'ent_ie'
    end
    object FDQBuscaEntidadesent_im: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_im'
      Origin = 'ent_im'
    end
    object FDQBuscaEntidadesent_ir: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_ir'
      Origin = 'ent_ir'
    end
    object FDQBuscaEntidadesent_suframa: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_suframa'
      Origin = 'ent_suframa'
    end
    object FDQBuscaEntidadesent_ramo_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ent_ramo_codigo'
      Origin = 'ent_ramo_codigo'
    end
    object FDQBuscaEntidadesent_cpf: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_cpf'
      Origin = 'ent_cpf'
    end
    object FDQBuscaEntidadesent_rg: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_rg'
      Origin = 'ent_rg'
    end
    object FDQBuscaEntidadesent_data_nascimento: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'ent_data_nascimento'
      Origin = 'ent_data_nascimento'
    end
    object FDQBuscaEntidadesent_ocupacao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_ocupacao'
      Origin = 'ent_ocupacao'
      Size = 50
    end
    object FDQBuscaEntidadesent_estado_civil: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_estado_civil'
      Origin = 'ent_estado_civil'
    end
    object FDQBuscaEntidadesent_sit_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'ent_sit_codigo'
      Origin = 'ent_sit_codigo'
    end
    object FDQBuscaEntidadesent_observacoes: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'ent_observacoes'
      Origin = 'ent_observacoes'
    end
  end
  object dsBuscaEntidades: TDataSource
    DataSet = FDQBuscaEntidades
    Left = 56
    Top = 85
  end
end
