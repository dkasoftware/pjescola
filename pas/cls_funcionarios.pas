unit cls_funcionarios;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Classes, DB;

type TFuncionarios = Class(TObject)
  private
    { Private declarations }
    f_fun_ent_codigo     : Integer;
    f_fun_ent_emp_codigo : Integer;
    f_fun_ct             : String;
    f_fun_pis            : String;
    f_fun_funcao         : String;
    f_fun_salario        : Double;
    f_fun_modo           : Integer;
    f_fun_situacao       : Integer;
  public
    { Public declarations }
    procedure proc_gravar(PCodigoEntidade, PCodigoEmpresa : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PCodigoEntidade, PCodigoEmpresa : Integer);
    procedure proc_limpar_gravacao(PCodigoEntidade, PCodigoEmpresa : Integer);

  published
    { Published declarations }
    property fun_ent_codigo     : Integer read f_fun_ent_codigo     write f_fun_ent_codigo;
    property fun_ent_emp_codigo : Integer read f_fun_ent_emp_codigo write f_fun_ent_emp_codigo;
    property fun_ct             : String  read f_fun_ct             write f_fun_ct;
    property fun_pis            : String  read f_fun_pis            write f_fun_pis;
    property fun_funcao         : String  read f_fun_funcao         write f_fun_funcao;
    property fun_salario        : Double  read f_fun_salario        write f_fun_salario;
    property fun_modo           : Integer read f_fun_modo           write f_fun_modo;
    property fun_situacao       : Integer read f_fun_situacao       write f_fun_situacao;
  end;

implementation

{ TFuncionarios }

procedure TFuncionarios.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PCodigoEntidade, PCodigoEmpresa : Integer);
begin
  PQuery.Close;
  PClient.Close;

  PQuery.Connection := Conexao.FDConexao;

  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                                         '+
                 ' from funcionarios                                '+
                 ' where fun_ent_codigo     = :p_fun_ent_codigo     '+
                 '   and fun_ent_emp_codigo = :p_fun_ent_emp_codigo '+
                 ' order by fun_ent_codigo                          ');
  PQuery.ParamByName('p_fun_ent_codigo').AsInteger     := PCodigoEntidade;
  PQuery.ParamByName('p_fun_ent_emp_codigo').AsInteger := PCodigoEmpresa;
  PQuery.Open;
  PClient.Open;
end;

procedure TFuncionarios.proc_gravar(PCodigoEntidade, PCodigoEmpresa : Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                         '+
                   ' from funcionarios                                '+
                   ' where fun_ent_codigo     = :p_fun_ent_codigo     '+
                   '   and fun_ent_emp_codigo = :p_fun_ent_emp_codigo ');
    qryAux.ParamByName('p_fun_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_fun_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.Open;

    if qryAux.Eof then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into funcionarios ( '+
                     '   fun_ent_codigo    ,     '+
                     '   fun_ent_emp_codigo,     '+
                     '   fun_ct            ,     '+
                     '   fun_pis           ,     '+
                     '   fun_funcao        ,     '+
                     '   fun_salario       ,     '+
                     '   fun_modo          ,     '+
                     '   fun_situacao            '+
                     ' ) values (                '+
                     '   :p_fun_ent_codigo    ,  '+
                     '   :p_fun_ent_emp_codigo,  '+
                     '   :p_fun_ct            ,  '+
                     '   :p_fun_pis           ,  '+
                     '   :p_fun_funcao        ,  '+
                     '   :p_fun_salario       ,  '+
                     '   :p_fun_modo          ,  '+
                     '   :p_fun_situacao      )  ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update funcionarios                              '+
                     ' set fun_ct       = :p_fun_ct      ,              '+
                     '     fun_pis      = :p_fun_pis     ,              '+
                     '     fun_funcao   = :p_fun_funcao  ,              '+
                     '     fun_salario  = :p_fun_salario ,              '+
                     '     fun_modo     = :p_fun_modo    ,              '+
                     '     fun_situacao = :p_fun_situacao               '+
                     ' where fun_ent_codigo     = :p_fun_ent_codigo     '+
                     '   and fun_ent_emp_codigo = :p_fun_ent_emp_codigo ');
    end;

    qryAux.ParamByName('p_fun_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_fun_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.ParamByName('p_fun_ct').AsString              := fun_ct;
    qryAux.ParamByName('p_fun_pis').AsString             := fun_pis;
    qryAux.ParamByName('p_fun_funcao').AsString          := fun_funcao;
    qryAux.ParamByName('p_fun_salario').AsFloat          := fun_salario;
    qryAux.ParamByName('p_fun_modo').AsInteger           := fun_modo;
    qryAux.ParamByName('p_fun_situacao').AsInteger       := fun_situacao;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TFuncionarios.proc_limpar_gravacao(PCodigoEntidade,
  PCodigoEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' delete from funcionarios                         '+
                   ' where fun_ent_codigo     = :p_fun_ent_codigo     '+
                   '   and fun_ent_emp_codigo = :p_fun_ent_emp_codigo ');
    qryAux.ParamByName('p_fun_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_fun_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
