inherited FrmCad_Bancos: TFrmCad_Bancos
  Caption = 'Cadastro de Bancos'
  ClientHeight = 320
  ClientWidth = 765
  OnActivate = FormActivate
  ExplicitWidth = 771
  ExplicitHeight = 349
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 765
    Height = 301
    ExplicitWidth = 765
    ExplicitHeight = 301
    inherited pnlButtons: TPanel
      Top = 265
      Width = 759
      ExplicitTop = 265
      ExplicitWidth = 759
      inherited btnExcluir: TBitBtn
        Visible = False
      end
      inherited btnCancelar: TBitBtn
        OnClick = btnCancelarClick
      end
      inherited btnEditar: TBitBtn
        OnClick = btnEditarClick
      end
      inherited btnGravar: TBitBtn
        OnClick = btnGravarClick
      end
      inherited btnNovo: TBitBtn
        OnClick = btnNovoClick
      end
      inherited btnSair: TBitBtn
        Left = 669
        ExplicitLeft = 669
      end
    end
    object PC: TPageControl
      Left = 3
      Top = 3
      Width = 759
      Height = 262
      ActivePage = tsConsulta
      Align = alClient
      TabOrder = 1
      object tsConsulta: TTabSheet
        Caption = 'Consulta'
        object TPanel
          Left = 0
          Top = 0
          Width = 751
          Height = 231
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object TPanel
            Left = 0
            Top = 0
            Width = 751
            Height = 45
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object TLabel
              Left = 19
              Top = 15
              Width = 38
              Height = 16
              Alignment = taRightJustify
              Caption = 'Buscar'
            end
            object ed_consulta: TEdit
              Left = 64
              Top = 11
              Width = 673
              Height = 24
              CharCase = ecUpperCase
              TabOrder = 0
              OnChange = ed_consultaChange
            end
          end
          object dbgBancos: TDBGrid
            Left = 0
            Top = 45
            Width = 751
            Height = 186
            Align = alClient
            DataSource = DSBancos
            Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnDrawColumnCell = dbgBancosDrawColumnCell
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ban_numero'
                Title.Alignment = taCenter
                Title.Caption = 'Banco'
                Width = 80
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ban_digito'
                Title.Alignment = taCenter
                Title.Caption = ' '
                Width = 30
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ban_nome'
                Title.Caption = 'Nome'
                Width = 600
                Visible = True
              end>
          end
        end
      end
      object tsCadastro: TTabSheet
        Caption = 'Cadastro'
        ImageIndex = 1
        object TPanel
          Left = 0
          Top = 0
          Width = 751
          Height = 231
          Align = alClient
          BevelOuter = bvNone
          Padding.Left = 3
          Padding.Right = 3
          Padding.Bottom = 3
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 37
            Top = 72
            Width = 45
            Height = 16
            Alignment = taRightJustify
            Caption = 'N'#250'mero'
          end
          object TLabel
            Left = 49
            Top = 101
            Width = 33
            Height = 16
            Alignment = taRightJustify
            Caption = 'Nome'
          end
          object TPanel
            Left = 3
            Top = 0
            Width = 745
            Height = 52
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object gb_ent_codigo: TGroupBox
              Left = 0
              Top = 0
              Width = 133
              Height = 52
              Align = alLeft
              Caption = 'C'#243'digo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object ed_ban_codigo: TEdit
                Left = 6
                Top = 18
                Width = 121
                Height = 24
                TabStop = False
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                NumbersOnly = True
                ParentFont = False
                ReadOnly = True
                TabOrder = 0
              end
            end
            object TGroupBox
              Left = 133
              Top = 0
              Width = 612
              Height = 52
              Align = alClient
              Caption = 'Empresa'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object cmb_ban_emp_codigo: TComboBox
                Left = 8
                Top = 18
                Width = 596
                Height = 24
                Style = csDropDownList
                TabOrder = 0
              end
            end
          end
          object ed_ban_nome: TEdit
            Left = 88
            Top = 97
            Width = 639
            Height = 24
            CharCase = ecUpperCase
            MaxLength = 60
            TabOrder = 3
          end
          object ed_ban_numero: TEdit
            Left = 88
            Top = 68
            Width = 117
            Height = 24
            CharCase = ecUpperCase
            MaxLength = 10
            TabOrder = 1
          end
          object ed_ban_digito: TEdit
            Left = 208
            Top = 68
            Width = 38
            Height = 24
            CharCase = ecUpperCase
            MaxLength = 1
            TabOrder = 2
          end
        end
      end
    end
  end
  inherited SB: TStatusBar
    Top = 301
    Width = 765
    ExplicitTop = 301
    ExplicitWidth = 765
  end
  object CDSBancos: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROBancos'
    Left = 2
    Top = 58
    object CDSBancosban_codigo: TIntegerField
      FieldName = 'ban_codigo'
      Required = True
    end
    object CDSBancosban_emp_codigo: TIntegerField
      FieldName = 'ban_emp_codigo'
      Required = True
    end
    object CDSBancosban_nome: TStringField
      FieldName = 'ban_nome'
      Size = 60
    end
    object CDSBancosban_numero: TStringField
      FieldName = 'ban_numero'
      Size = 10
    end
    object CDSBancosban_digito: TStringField
      FieldName = 'ban_digito'
      Size = 1
    end
  end
  object PROBancos: TDataSetProvider
    DataSet = FDQBancos
    Left = 2
    Top = 30
  end
  object DSBancos: TDataSource
    DataSet = CDSBancos
    Left = 2
    Top = 86
  end
  object FDQBancos: TFDQuery
    SQL.Strings = (
      'select *'#10'from bancos b')
    Left = 2
    Top = 2
    object FDQBancosban_codigo: TIntegerField
      FieldName = 'ban_codigo'
      Origin = 'ban_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBancosban_emp_codigo: TIntegerField
      FieldName = 'ban_emp_codigo'
      Origin = 'ban_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQBancosban_nome: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ban_nome'
      Origin = 'ban_nome'
      Size = 60
    end
    object FDQBancosban_numero: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ban_numero'
      Origin = 'ban_numero'
      Size = 10
    end
    object FDQBancosban_digito: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ban_digito'
      Origin = 'ban_digito'
      Size = 1
    end
  end
end
