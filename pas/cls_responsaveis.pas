unit cls_responsaveis;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type TResponsaveis = Class(TObject)
  private
    { Private declarations }
    f_res_ent_codigo      : Integer;
    f_res_ent_emp_codigo  : Integer;
    f_res_nome            : String;
    f_res_rg              : String;
    f_res_cpf             : String;
    f_res_data_nascimento : TDateTime;
  public
    { Public declarations }
    procedure proc_gravar(PCodigoEntidade, PCodigoEmpresa : Integer);

    function func_recuperar(PCodigoEntidade, PCodigoEmpresa : Integer) : Boolean;
  published
    { Published declarations }
    property res_ent_codigo      : Integer   read f_res_ent_codigo      write f_res_ent_codigo;
    property res_ent_emp_codigo  : Integer   read f_res_ent_emp_codigo  write f_res_ent_emp_codigo;
    property res_nome            : String    read f_res_nome            write f_res_nome;
    property res_rg              : String    read f_res_rg              write f_res_rg;
    property res_cpf             : String    read f_res_cpf             write f_res_cpf;
    property res_data_nascimento : TDateTime read f_res_data_nascimento write f_res_data_nascimento;
  end;

implementation

{ TResponsaveis }

function TResponsaveis.func_recuperar(PCodigoEntidade,
  PCodigoEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from responsaveis r                                '+
                   ' where r.res_ent_codigo     = :p_res_ent_codigo     '+
                   '   and r.res_ent_emp_codigo = :p_res_ent_emp_codigo ');
    qryAux.ParamByName('p_res_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_res_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    res_ent_codigo      := qryAux.FieldByName('res_ent_codigo').AsInteger;
    res_ent_emp_codigo  := qryAux.FieldByName('res_ent_emp_codigo').AsInteger;
    res_nome            := qryAux.FieldByName('res_nome').AsString;
    res_rg              := qryAux.FieldByName('res_rg').AsString;
    res_cpf             := qryAux.FieldByName('res_cpf').AsString;
    res_data_nascimento := qryAux.FieldByName('res_data_nascimento').AsDateTime;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TResponsaveis.proc_gravar(PCodigoEntidade,
  PCodigoEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from responsaveis r                                '+
                   ' where r.res_ent_codigo     = :p_res_ent_codigo     '+
                   '   and r.res_ent_emp_codigo = :p_res_ent_emp_codigo ');
    qryAux.ParamByName('p_res_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_res_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into responsaveis (  '+
                     '   res_ent_codigo     ,      '+
                     '   res_ent_emp_codigo ,      '+
                     '   res_nome           ,      '+
                     '   res_rg             ,      '+
                     '   res_cpf            ,      '+
                     '   res_data_nascimento       '+
                     '  ) values (                 '+
                     '   :p_res_ent_codigo     ,   '+
                     '   :p_res_ent_emp_codigo ,   '+
                     '   :p_res_nome           ,   '+
                     '   :p_res_rg             ,   '+
                     '   :p_res_cpf            ,   '+
                     '   :p_res_data_nascimento)   ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update responsaveis                               '+
                     ' set res_nome            = :p_res_nome           , '+
                     '     res_rg              = :p_res_rg             , '+
                     '     res_cpf             = :p_res_cpf            , '+
                     '     res_data_nascimento = :p_res_data_nascimento  '+
                     ' where res_ent_codigo     = :p_res_ent_codigo      '+
                     '   and res_ent_emp_codigo = :p_res_ent_emp_codigo  ');
    end;

    qryAux.ParamByName('p_res_ent_codigo').AsInteger       := PCodigoEntidade;
    qryAux.ParamByName('p_res_ent_emp_codigo').AsInteger   := PCodigoEmpresa;
    qryAux.ParamByName('p_res_nome').AsString              := res_nome;
    qryAux.ParamByName('p_res_rg').AsString                := res_rg;
    qryAux.ParamByName('p_res_cpf').AsString               := res_cpf;
    qryAux.ParamByName('p_res_data_nascimento').AsDateTime := res_data_nascimento;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
