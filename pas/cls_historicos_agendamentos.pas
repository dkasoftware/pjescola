unit cls_historicos_agendamentos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Classes, DB;

type THistoricos_Agendamentos = Class(TObject)
  private
    { Private declarations }
    f_hagd_agd_codigo     : Integer;
    f_hagd_agd_emp_codigo : Integer;
    f_hagd_sequencia      : Integer;
    f_hagd_data           : TDateTime;
    f_hagd_observacao     : String;
  public
    { Public declarations }
    procedure proc_gravar(PAgdCodigo, PAgdEmpresa, PSequencia : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PAgdCodigo, PAgdEmpresa : Integer);

    function func_recuperar(PCodigo, PEmpresa, PSequencia : Integer) : Boolean;
  published
    { Published declarations }
    property hagd_agd_codigo     : Integer   read f_hagd_agd_codigo     write f_hagd_agd_codigo;
    property hagd_agd_emp_codigo : Integer   read f_hagd_agd_emp_codigo write f_hagd_agd_emp_codigo;
    property hagd_sequencia      : Integer   read f_hagd_sequencia      write f_hagd_sequencia;
    property hagd_data           : TDateTime read f_hagd_data           write f_hagd_data;
    property hagd_observacao     : String    read f_hagd_observacao     write f_hagd_observacao;
  end;

implementation

{ THistoricos_Agendamentos }

function THistoricos_Agendamentos.func_recuperar(PCodigo,
  PEmpresa, PSequencia: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                             '+
                   ' from historicos_agendamentos h                       '+
                   ' where h.hagd_agd_codigo     = :p_hagd_agd_codigo     '+
                   '   and h.hagd_agd_emp_codigo = :p_hagd_agd_emp_codigo '+
                   '   and h.hagd_sequencia      = :p_hagd_sequencia      ');
    qryAux.ParamByName('p_hagd_agd_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_hagd_agd_emp_codigo').AsInteger := PEmpresa;
    qryAux.ParamByName('p_hagd_sequencia').AsInteger      := PSequencia;
    qryAux.Open;

    Result := (not qryAux.Eof);

    hagd_agd_codigo     := qryAux.FieldByName('hagd_agd_codigo').AsInteger;
    hagd_agd_emp_codigo := qryAux.FieldByName('hagd_agd_emp_codigo').AsInteger;
    hagd_sequencia      := qryAux.FieldByName('hagd_sequencia').AsInteger;
    hagd_data           := qryAux.FieldByName('hagd_data').AsDateTime;
    hagd_observacao     := qryAux.FieldByName('hagd_observacao').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure THistoricos_Agendamentos.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PAgdCodigo, PAgdEmpresa: Integer);
begin
  PQuery.Close;
  PClient.Close;

  PQuery.Connection := Conexao.FDConexao;

  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                                             '+
                 ' from historicos_agendamentos h                       '+
                 ' where h.hagd_agd_codigo     = :p_hagd_agd_codigo     '+
                 '   and h.hagd_agd_emp_codigo = :p_hagd_agd_emp_codigo ');
  PQuery.ParamByName('p_hagd_agd_codigo').AsInteger     := PAgdCodigo;
  PQuery.ParamByName('p_hagd_agd_emp_codigo').AsInteger := PAgdEmpresa;
  PQuery.Open;
  PClient.Open;
end;

procedure THistoricos_Agendamentos.proc_gravar(PAgdCodigo, PAgdEmpresa,
  PSequencia : Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                             '+
                   ' from historicos_agendamentos h                       '+
                   ' where h.hagd_agd_codigo     = :p_hagd_agd_codigo     '+
                   '   and h.hagd_agd_emp_codigo = :p_hagd_agd_emp_codigo '+
                   '   and h.hagd_sequencia      = :p_hagd_sequencia      ');
    qryAux.ParamByName('p_hagd_agd_codigo').AsInteger     := PAgdCodigo;
    qryAux.ParamByName('p_hagd_agd_emp_codigo').AsInteger := PAgdEmpresa;
    qryAux.ParamByName('p_hagd_sequencia').AsInteger      := PSequencia;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into historicos_agendamentos ( '+
                     '   hagd_agd_codigo    ,                '+
                     '   hagd_agd_emp_codigo,                '+
                     '   hagd_sequencia     ,                '+
                     '   hagd_data          ,                '+
                     '   hagd_observacao                     '+
                     '  ) values (                           '+
                     '   :p_hagd_agd_codigo    ,             '+
                     '   :p_hagd_agd_emp_codigo,             '+
                     '   :p_hagd_sequencia     ,             '+
                     '   :p_hagd_data          ,             '+
                     '   :p_hagd_observacao    )             ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update historicos_agendamentos                     '+
                     ' set hagd_data       = :p_hagd_data      ,          '+
                     '     hagd_observacao = :p_hagd_observacao           '+
                     ' where hagd_agd_codigo     = :p_hagd_agd_codigo     '+
                     '   and hagd_agd_emp_codigo = :p_hagd_agd_emp_codigo '+
                     '   and hagd_sequencia      = :p_hagd_sequencia      ');
    end;

    qryAux.ParamByName('p_hagd_agd_codigo').AsInteger     := hagd_agd_codigo;
    qryAux.ParamByName('p_hagd_agd_emp_codigo').AsInteger := hagd_agd_emp_codigo;
    qryAux.ParamByName('p_hagd_sequencia').AsInteger      := hagd_sequencia;
    qryAux.ParamByName('p_hagd_data').AsDateTime          := hagd_data;
    qryAux.ParamByName('p_hagd_observacao').AsString      := hagd_observacao;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
