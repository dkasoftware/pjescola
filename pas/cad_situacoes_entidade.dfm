inherited FrmCad_Situacoes_Entidade: TFrmCad_Situacoes_Entidade
  Caption = 'Cadastro de Situa'#231#245'es de Entidade'
  ClientHeight = 474
  ClientWidth = 546
  OnActivate = FormActivate
  ExplicitWidth = 552
  ExplicitHeight = 503
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 546
    Height = 455
    ExplicitLeft = 0
    ExplicitTop = 0
    ExplicitWidth = 546
    ExplicitHeight = 455
    inherited pnlButtons: TPanel
      Top = 419
      Width = 540
      ExplicitTop = 419
      ExplicitWidth = 540
      inherited btnExcluir: TBitBtn
        ExplicitLeft = 360
      end
      inherited btnCancelar: TBitBtn
        OnClick = btnCancelarClick
        ExplicitLeft = 270
      end
      inherited btnEditar: TBitBtn
        OnClick = btnEditarClick
        ExplicitLeft = 180
      end
      inherited btnGravar: TBitBtn
        OnClick = btnGravarClick
        ExplicitLeft = 90
      end
      inherited btnNovo: TBitBtn
        OnClick = btnNovoClick
      end
      inherited btnSair: TBitBtn
        Left = 450
        ExplicitLeft = 450
      end
    end
    object PC: TPageControl
      Left = 3
      Top = 3
      Width = 540
      Height = 416
      ActivePage = tsConsulta
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ExplicitTop = 0
      object tsConsulta: TTabSheet
        Caption = 'Consulta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        object pnlConsulta: TPanel
          Left = 0
          Top = 0
          Width = 532
          Height = 385
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Padding.Left = 3
          Padding.Top = 3
          Padding.Right = 3
          Padding.Bottom = 3
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          object Panel1: TPanel
            Left = 3
            Top = 3
            Width = 526
            Height = 34
            Align = alTop
            BevelInner = bvLowered
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object Label1: TLabel
              Left = 9
              Top = 9
              Width = 38
              Height = 16
              Alignment = taRightJustify
              Caption = 'Buscar'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object cmbCampos: TComboBox
              Left = 54
              Top = 5
              Width = 86
              Height = 24
              Style = csDropDownList
              ItemIndex = 0
              TabOrder = 0
              Text = 'C'#243'digo'
              Items.Strings = (
                'C'#243'digo'
                'Descri'#231#227'o')
            end
            object ed_consulta: TEdit
              Left = 142
              Top = 5
              Width = 274
              Height = 24
              CharCase = ecUpperCase
              TabOrder = 1
              OnChange = ed_consultaChange
            end
          end
          object dbgSituacoes: TDBGrid
            Left = 3
            Top = 42
            Width = 526
            Height = 340
            Align = alBottom
            DataSource = DSSituacoes
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentFont = False
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'sit_codigo'
                Title.Alignment = taCenter
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'sit_descricao'
                Width = 441
                Visible = True
              end>
          end
        end
      end
      object tsCadastro: TTabSheet
        Caption = 'Cadastro'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object pnlCadastro: TPanel
          Left = 0
          Top = 0
          Width = 532
          Height = 385
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object gbCodigo: TGroupBox
            Left = 5
            Top = -2
            Width = 135
            Height = 50
            Caption = 'C'#243'digo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object ed_sit_codigo: TEdit
              Left = 6
              Top = 18
              Width = 121
              Height = 24
              TabStop = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              NumbersOnly = True
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
            end
          end
          object GroupBox1: TGroupBox
            Left = 5
            Top = 48
            Width = 522
            Height = 333
            Caption = 'Informa'#231#245'es'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object Label2: TLabel
              Left = 17
              Top = 28
              Width = 55
              Height = 16
              Caption = 'Descri'#231#227'o'
            end
            object ed_sit_descricao: TEdit
              Left = 79
              Top = 24
              Width = 418
              Height = 24
              CharCase = ecUpperCase
              MaxLength = 15
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  inherited SB: TStatusBar
    Top = 455
    Width = 546
    ExplicitLeft = 0
    ExplicitTop = 455
    ExplicitWidth = 546
  end
  object FDQSituacoes: TFDQuery
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select *'
      'from situacoes_entidade')
    Left = 455
    Top = 94
    object FDQSituacoessit_codigo: TFDAutoIncField
      FieldName = 'sit_codigo'
      Origin = 'sit_codigo'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object FDQSituacoessit_descricao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'sit_descricao'
      Origin = 'sit_descricao'
      Size = 15
    end
  end
  object CDSSituacoes: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'PROSituacoes'
    Left = 455
    Top = 150
    object CDSSituacoessit_codigo: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'sit_codigo'
      Origin = 'sit_codigo'
      ProviderFlags = [pfInWhere, pfInKey]
      ReadOnly = True
    end
    object CDSSituacoessit_descricao: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'sit_descricao'
      Origin = 'sit_descricao'
      Size = 15
    end
  end
  object DSSituacoes: TDataSource
    DataSet = CDSSituacoes
    Left = 455
    Top = 178
  end
  object PROSituacoes: TDataSetProvider
    DataSet = FDQSituacoes
    Left = 455
    Top = 122
  end
end
