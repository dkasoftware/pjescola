unit cls_contratos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Classes, DB, cls_tabelacodigos;

type TContratos = Class(TObject)
  private
    { Private declarations }
    f_con_codigo                     : Integer;
    f_con_ent_codigo                 : Integer;
    f_con_ent_emp_codigo             : Integer;
    f_con_tipo                       : Integer;
    f_con_pro_codigo                 : Integer;
    f_con_pro_emp_codigo             : Integer;
    f_con_descricao                  : String;
    f_con_ent_codigo_funcionario     : Integer;
    f_con_ent_emp_codigo_funcionario : Integer;
    f_con_valor                      : Double;
    f_con_vencimento_pagamento       : Integer;
    f_con_vencimento_contrato        : TDateTime;
    f_con_horas_mes                  : Double;
    f_con_contrato                   : TMemoryStream;
    f_con_situacao                   : Integer;
    f_con_flag_cr                    : Integer;
  public
    { Public declarations }
    procedure proc_gravar(PCodigo, PEntCodigo, PEmpresa : Integer);
    procedure proc_excluir(PCodigo, PEntCodigo, PEmpresa : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PConCodigo, PConEntCodigo, PConEntEmpCodigo : Integer); overload;
    procedure proc_consultar(var PQuery : TFDQuery); overload;
    procedure proc_limpar_gravacao(PCodigoEntidade, PCodigoEmpresa : Integer);
    procedure proc_trasnferir_horas(PCodigo, PCodigoEntidade, PCodigoEmpresa, PCodEntidadeDest, PCodEmpresaDest: Integer; PNumHoras : Double);

    function func_recuperar(PCodigo, PEntCodigo, PEmpresa : Integer) : Boolean;
    function func_recuperar_contrato(PCodigo, PEntCodigo, PEmpresa : Integer) : Boolean;
  published
    { Published declarations }
    property con_codigo                     : Integer       read f_con_codigo                     write f_con_codigo;
    property con_ent_codigo                 : Integer       read f_con_ent_codigo                 write f_con_ent_codigo;
    property con_ent_emp_codigo             : Integer       read f_con_ent_emp_codigo             write f_con_ent_emp_codigo;
    property con_tipo                       : Integer       read f_con_tipo                       write f_con_tipo;
    property con_pro_codigo                 : Integer       read f_con_pro_codigo                 write f_con_pro_codigo;
    property con_pro_emp_codigo             : Integer       read f_con_pro_emp_codigo             write f_con_pro_emp_codigo;
    property con_descricao                  : String        read f_con_descricao                  write f_con_descricao;
    property con_ent_codigo_funcionario     : Integer       read f_con_ent_codigo_funcionario     write f_con_ent_codigo_funcionario;
    property con_ent_emp_codigo_funcionario : Integer       read f_con_ent_emp_codigo_funcionario write f_con_ent_emp_codigo_funcionario;
    property con_valor                      : Double        read f_con_valor                      write f_con_valor;
    property con_vencimento_pagamento       : Integer       read f_con_vencimento_pagamento       write f_con_vencimento_pagamento;
    property con_vencimento_contrato        : TDateTime     read f_con_vencimento_contrato        write f_con_vencimento_contrato;
    property con_horas_mes                  : Double        read f_con_horas_mes                  write f_con_horas_mes;
    property con_contrato                   : TMemoryStream read f_con_contrato                   write f_con_contrato;
    property con_situacao                   : Integer       read f_con_situacao                   write f_con_situacao;
    property con_flag_cr                    : Integer       read f_con_flag_cr                    write f_con_flag_cr;
  end;

implementation

{ TContratos }

function TContratos.func_recuperar(PCodigo, PEntCodigo,
  PEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from contratos c                                   '+
                   ' where c.con_codigo         = :p_con_codigo         '+
                   '   and c.con_ent_codigo     = :p_con_ent_codigo     '+
                   '   and c.con_ent_emp_codigo = :p_con_ent_emp_codigo ');
    qryAux.ParamByName('p_con_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PEntCodigo;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    con_codigo                     := qryAux.FieldByName('con_codigo').AsInteger;
    con_ent_codigo                 := qryAux.FieldByName('con_ent_codigo').AsInteger;
    con_ent_emp_codigo             := qryAux.FieldByName('con_ent_emp_codigo').AsInteger;
    con_tipo                       := qryAux.FieldByName('con_tipo').AsInteger;
    con_pro_codigo                 := qryAux.FieldByName('con_pro_codigo').AsInteger;
    con_pro_emp_codigo             := qryAux.FieldByName('con_pro_emp_codigo').AsInteger;
    con_descricao                  := qryAux.FieldByName('con_descricao').AsString;
    con_ent_codigo_funcionario     := qryAux.FieldByName('con_ent_codigo_funcionario').AsInteger;
    con_ent_emp_codigo_funcionario := qryAux.FieldByName('con_ent_emp_codigo_funcionario').AsInteger;
    con_valor                      := qryAux.FieldByName('con_valor').AsFloat;
    con_vencimento_pagamento       := qryAux.FieldByName('con_vencimento_pagamento').AsInteger;
    con_vencimento_contrato        := qryAux.FieldByName('con_vencimento_contrato').AsDateTime;
    con_horas_mes                  := qryAux.FieldByName('con_horas_mes').AsFloat;
    con_situacao                   := qryAux.FieldByName('con_situacao').AsInteger;
    con_flag_cr                    := qryAux.FieldByName('con_flag_cr').AsInteger;
  finally
    FreeAndNil(qryAux);
  end;
end;

function TContratos.func_recuperar_contrato(PCodigo, PEntCodigo,
  PEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from contratos c                                   '+
                   ' where c.con_codigo         = :p_con_codigo         '+
                   '   and c.con_ent_codigo     = :p_con_ent_codigo     '+
                   '   and c.con_ent_emp_codigo = :p_con_ent_emp_codigo ');
    qryAux.ParamByName('p_con_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PEntCodigo;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    TBlobField(qryAux.FieldByName('con_contrato')).SaveToStream(con_contrato);
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TContratos.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PConCodigo, PConEntCodigo,
  PConEntEmpCodigo: Integer);
begin
  PQuery.Close;
  PClient.Close;

  PQuery.Connection := Conexao.FDConexao;

  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                                           '+
                 ' from contratos c                                   '+
                 ' where (c.con_codigo        = :p_con_codigo         '+
                 '    or  :p_con_codigo       = 0)                    '+
                 '   and c.con_ent_codigo     = :p_con_ent_codigo     '+
                 '   and c.con_ent_emp_codigo = :p_con_ent_emp_codigo '+
                 ' order by c.con_codigo                              ');
  PQuery.ParamByName('p_con_codigo').AsInteger         := PConCodigo;
  PQuery.ParamByName('p_con_ent_codigo').AsInteger     := PConEntCodigo;
  PQuery.ParamByName('p_con_ent_emp_codigo').AsInteger := PConEntEmpCodigo;
  PQuery.Open;
  PClient.Open;
end;

procedure TContratos.proc_consultar(var PQuery: TFDQuery);
begin
  PQuery.Close;

  PQuery.Connection := Conexao.FDConexao;

  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select e.ent_codigo, e.ent_emp_codigo, e.ent_razao_social    '+
                 ' from entidades e                                             '+
                 ' where e.ent_sit_codigo = 1                                   '+
                 '    or exists (select *                                       '+
                 '               from contratos c                               '+
                 '               where c.con_ent_codigo     = e.ent_codigo      '+
                 '                 and c.con_ent_emp_codigo = e.ent_emp_codigo) '+
                 ' order by e.ent_razao_social                                  ');
  PQuery.Open;
end;

procedure TContratos.proc_excluir(PCodigo, PEntCodigo, PEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Add(' delete from contas_a_receber                    '+
                   ' where cr_ent_codigo     = :p_con_ent_codigo     '+
                   '   and cr_ent_emp_codigo = :p_con_ent_emp_codigo '+
                   '   and cr_con_codigo     = :p_con_codigo         ');
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PEntCodigo;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PEmpresa;
    qryAux.ParamByName('p_con_codigo').AsInteger         := PCodigo;
    qryAux.ExecSQL;

    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Add(' delete from contratos                            '+
                   ' where con_codigo         = :p_con_codigo         '+
                   '   and con_ent_codigo     = :p_con_ent_codigo     '+
                   '   and con_ent_emp_codigo = :p_con_ent_emp_codigo ');
    qryAux.ParamByName('p_con_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PEntCodigo;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PEmpresa;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TContratos.proc_gravar(PCodigo, PEntCodigo, PEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from contratos c                                   '+
                   ' where c.con_codigo         = :p_con_codigo         '+
                   '   and c.con_ent_codigo     = :p_con_ent_codigo     '+
                   '   and c.con_ent_emp_codigo = :p_con_ent_emp_codigo ');
    qryAux.ParamByName('p_con_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PEntCodigo;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into contratos (              '+
                     '   con_codigo                    ,    '+
                     '   con_ent_codigo                ,    '+
                     '   con_ent_emp_codigo            ,    '+
                     '   con_tipo                      ,    '+
                     '   con_pro_codigo                ,    '+
                     '   con_pro_emp_codigo            ,    '+
                     '   con_descricao                 ,    '+
                     '   con_ent_codigo_funcionario    ,    '+
                     '   con_ent_emp_codigo_funcionario,    '+
                     '   con_valor                     ,    '+
                     '   con_vencimento_pagamento      ,    '+
                     '   con_vencimento_contrato       ,    '+
                     '   con_horas_mes                 ,    '+
//                     '   con_contrato                  ,    '+
                     '   con_situacao                  ,    '+
                     '   con_flag_cr                        '+
                     '  ) values (                          '+
                     '   :p_con_codigo                    , '+
                     '   :p_con_ent_codigo                , '+
                     '   :p_con_ent_emp_codigo            , '+
                     '   :p_con_tipo                      , '+
                     '   :p_con_pro_codigo                , '+
                     '   :p_con_pro_emp_codigo            , '+
                     '   :p_con_descricao                 , '+
                     '   :p_con_ent_codigo_funcionario    , '+
                     '   :p_con_ent_emp_codigo_funcionario, '+
                     '   :p_con_valor                     , '+
                     '   :p_con_vencimento_pagamento      , '+
                     '   :p_con_vencimento_contrato       , '+
                     '   :p_con_horas_mes                 , '+
//                     '   :p_con_contrato                  , '+
                     '   :p_con_situacao                  , '+
                     '   :p_con_flag_cr                   ) ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update contratos                                                        '+
                     ' set con_tipo                       = :p_con_tipo                      , '+
                     '     con_pro_codigo                 = :p_con_pro_codigo                , '+
                     '     con_pro_emp_codigo             = :p_con_pro_emp_codigo            , '+
                     '     con_descricao                  = :p_con_descricao                 , '+
                     '     con_ent_codigo_funcionario     = :p_con_ent_codigo_funcionario    , '+
                     '     con_ent_emp_codigo_funcionario = :p_con_ent_emp_codigo_funcionario, '+
                     '     con_valor                      = :p_con_valor                     , '+
                     '     con_vencimento_pagamento       = :p_con_vencimento_pagamento      , '+
                     '     con_vencimento_contrato        = :p_con_vencimento_contrato       , '+
                     '     con_horas_mes                  = :p_con_horas_mes                 , '+
//                     '     con_contrato                   = :p_con_contrato                  , '+
                     '     con_situacao                   = :p_con_situacao                  , '+
                     '     con_flag_cr                    = :p_con_flag_cr                     '+
                     ' where con_codigo         = :p_con_codigo                                '+
                     '   and con_ent_codigo     = :p_con_ent_codigo                            '+
                     '   and con_ent_emp_codigo = :p_con_ent_emp_codigo                        ');
    end;

    qryAux.ParamByName('p_con_codigo').AsInteger                     := PCodigo;
    qryAux.ParamByName('p_con_ent_codigo').AsInteger                 := PEntCodigo;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger             := PEmpresa;
    qryAux.ParamByName('p_con_tipo').AsInteger                       := con_tipo;
    qryAux.ParamByName('p_con_pro_codigo').AsInteger                 := con_pro_codigo;
    qryAux.ParamByName('p_con_pro_emp_codigo').AsInteger             := con_pro_emp_codigo;
    qryAux.ParamByName('p_con_descricao').AsString                   := con_descricao;
    qryAux.ParamByName('p_con_ent_codigo_funcionario').AsInteger     := con_ent_codigo_funcionario;
    qryAux.ParamByName('p_con_ent_emp_codigo_funcionario').AsInteger := con_ent_emp_codigo_funcionario;
    qryAux.ParamByName('p_con_valor').AsFloat                        := con_valor;
    qryAux.ParamByName('p_con_vencimento_pagamento').AsInteger       := con_vencimento_pagamento;
    qryAux.ParamByName('p_con_vencimento_contrato').AsDateTime       := con_vencimento_contrato;
    qryAux.ParamByName('p_con_horas_mes').AsFloat                    := con_horas_mes;
//    qryAux.ParamByName('p_con_contrato').LoadFromStream(con_contrato, ftBlob);
    qryAux.ParamByName('p_con_situacao').AsInteger                   := con_situacao;
    qryAux.ParamByName('p_con_flag_cr').AsInteger                    := con_flag_cr;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TContratos.proc_limpar_gravacao(PCodigoEntidade,
  PCodigoEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' delete from contratos                            '+
                   ' where con_ent_codigo     = :p_con_ent_codigo     '+
                   '   and con_ent_emp_codigo = :p_con_ent_emp_codigo ');
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TContratos.proc_trasnferir_horas(PCodigo, PCodigoEntidade,
  PCodigoEmpresa, PCodEntidadeDest, PCodEmpresaDest: Integer; PNumHoras : Double);
var c_tabela     : TTabelaCodigos;
    i_con_codigo : integer;
begin
  func_recuperar(PCodigo, PCodigoEntidade, PCodigoEmpresa);

  c_tabela := TTabelaCodigos.Create;
  try
    i_con_codigo := c_tabela.func_gerar_codigo('contratos', IntToStr(PCodEntidadeDest) + '|' + IntToStr(PCodEmpresaDest));
  finally
    FreeAndNil(c_tabela);
  end;

  con_horas_mes := con_horas_mes - PNumHoras;
  proc_gravar(PCodigo, PCodigoEntidade, PCodigoEmpresa);

  con_descricao := '(TRANSF. DE C:' + IntToStr(PCodigoEntidade) + '-' + IntToStr(PCodigoEmpresa) + '/CON:' +  IntToStr(PCodigo) + ')';
  con_horas_mes := PNumHoras;
  proc_gravar(i_con_codigo, PCodEntidadeDest, PCodEmpresaDest);
end;

end.
