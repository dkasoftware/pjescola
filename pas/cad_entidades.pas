unit cad_entidades;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Data.DB, Datasnap.Provider,
  Datasnap.DBClient, Vcl.DBCtrls, Vcl.Mask, cls_utils, Vcl.CheckLst, Vcl.Menus,
  frxClass, frxDBSet;

type
  TFrmCad_Entidades = class(TFrmAux_Padrao)
    PC: TPageControl;
    tsConsulta: TTabSheet;
    pnlConsulta: TPanel;
    dbgEntidades: TDBGrid;
    tsCadastro: TTabSheet;
    pnlCadastro: TPanel;
    CDSEntidades: TClientDataSet;
    PROEntidades: TDataSetProvider;
    DSEntidades: TDataSource;
    FDQEntidades: TFDQuery;
    FDQEntidadesent_codigo: TIntegerField;
    FDQEntidadesent_emp_codigo: TIntegerField;
    FDQEntidadesent_razao_social: TStringField;
    FDQEntidadesent_nome_fantasia: TStringField;
    FDQEntidadesent_tipo_pessoa: TIntegerField;
    FDQEntidadesent_cnpj: TStringField;
    FDQEntidadesent_ie: TStringField;
    FDQEntidadesent_im: TStringField;
    FDQEntidadesent_ir: TStringField;
    FDQEntidadesent_suframa: TStringField;
    FDQEntidadesent_ramo_codigo: TIntegerField;
    FDQEntidadesent_cpf: TStringField;
    FDQEntidadesent_rg: TStringField;
    FDQEntidadesent_data_nascimento: TDateField;
    FDQEntidadesent_ocupacao: TStringField;
    FDQEntidadesent_estado_civil: TStringField;
    FDQEntidadesent_sit_codigo: TIntegerField;
    CDSEntidadesent_codigo: TIntegerField;
    CDSEntidadesent_emp_codigo: TIntegerField;
    CDSEntidadesent_razao_social: TStringField;
    CDSEntidadesent_nome_fantasia: TStringField;
    CDSEntidadesent_tipo_pessoa: TIntegerField;
    CDSEntidadesent_cnpj: TStringField;
    CDSEntidadesent_ie: TStringField;
    CDSEntidadesent_im: TStringField;
    CDSEntidadesent_ir: TStringField;
    CDSEntidadesent_suframa: TStringField;
    CDSEntidadesent_ramo_codigo: TIntegerField;
    CDSEntidadesent_cpf: TStringField;
    CDSEntidadesent_rg: TStringField;
    CDSEntidadesent_data_nascimento: TDateField;
    CDSEntidadesent_ocupacao: TStringField;
    CDSEntidadesent_estado_civil: TStringField;
    CDSEntidadesent_sit_codigo: TIntegerField;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    cmbCampos: TComboBox;
    ed_consulta: TEdit;
    cmbConStatus: TComboBox;
    Panel7: TPanel;
    ed_ent_codigo: TEdit;
    cmb_ent_emp_codigo: TComboBox;
    Panel8: TPanel;
    rg_ent_tipo_pessoa: TRadioGroup;
    PCTipoPessoa: TPageControl;
    tsFisica: TTabSheet;
    ed_ent_razao_social_f: TEdit;
    ed_ent_rg: TEdit;
    ed_ent_data_nascimento: TDateTimePicker;
    cmb_ent_estado_civil: TComboBox;
    ed_ent_ocupacao: TEdit;
    tsJuridica: TTabSheet;
    ed_ent_cpf: TMaskEdit;
    PCAdicionais: TPageControl;
    tsEnderecos: TTabSheet;
    tsContatos: TTabSheet;
    CDSEnderecos: TClientDataSet;
    btnNovoEnd: TBitBtn;
    btnGravarEnd: TBitBtn;
    btnEditarEnd: TBitBtn;
    btnCancelarEnd: TBitBtn;
    btnExcluirEnd: TBitBtn;
    lbl_end_cep: TLabel;
    ed_end_cep: TEdit;
    lbl_end_tipo: TLabel;
    cmb_end_tipo: TComboBox;
    lbl_end_uf_sigla: TLabel;
    cmb_end_uf_sigla: TComboBox;
    cmb_end_cid_codigo: TComboBox;
    lbl_end_cid_codigo: TLabel;
    btnAddEnd: TBitBtn;
    lbl_end_logradouro: TLabel;
    ed_end_logradouro: TEdit;
    lbl_end_numero: TLabel;
    ed_end_numero: TEdit;
    lbl_end_complemento: TLabel;
    ed_end_complemento: TEdit;
    DBGEnderecos: TDBGrid;
    cmb_ent_sit_codigo: TComboBox;
    gb_ent_codigo: TGroupBox;
    DSEnderecos: TDataSource;
    PROEnderecos: TDataSetProvider;
    FDQEnderecos: TFDQuery;
    FDQEnderecosend_codigo: TIntegerField;
    FDQEnderecosend_ent_codigo: TIntegerField;
    FDQEnderecosend_ent_emp_codigo: TIntegerField;
    FDQEnderecosend_tipo: TIntegerField;
    FDQEnderecosend_cep: TStringField;
    FDQEnderecosend_uf_sigla: TStringField;
    FDQEnderecosend_cid_codigo: TIntegerField;
    FDQEnderecosend_logradouro: TStringField;
    FDQEnderecosend_numero: TIntegerField;
    FDQEnderecosend_complemento: TStringField;
    CDSEnderecosend_codigo: TIntegerField;
    CDSEnderecosend_ent_codigo: TIntegerField;
    CDSEnderecosend_ent_emp_codigo: TIntegerField;
    CDSEnderecosend_tipo: TIntegerField;
    CDSEnderecosend_cep: TStringField;
    CDSEnderecosend_uf_sigla: TStringField;
    CDSEnderecosend_cid_codigo: TIntegerField;
    CDSEnderecosend_logradouro: TStringField;
    CDSEnderecosend_numero: TIntegerField;
    CDSEnderecosend_complemento: TStringField;
    CDSEnderecosend_descricao_tipo: TStringField;
    CDSEnderecoscid_nome: TStringField;
    pnlBotoesEndereco: TPanel;
    lbl_con_email: TLabel;
    lbl_con_contato: TLabel;
    lbl_con_observacao: TLabel;
    pnlBotoesContatos: TPanel;
    btnNovoCon: TBitBtn;
    btnGravarCon: TBitBtn;
    btnEditarCon: TBitBtn;
    btnCancelarCon: TBitBtn;
    btnExcluirCon: TBitBtn;
    ed_con_email: TEdit;
    ed_con_contato: TEdit;
    ed_con_observacao: TEdit;
    DBGContatos: TDBGrid;
    lbl_con_telefone: TLabel;
    ed_con_telefone: TMaskEdit;
    FDQContatos: TFDQuery;
    PROContatos: TDataSetProvider;
    CDSContatos: TClientDataSet;
    DSContatos: TDataSource;
    CDSContatoscon_codigo: TIntegerField;
    CDSContatoscon_ent_codigo: TIntegerField;
    CDSContatoscon_ent_emp_codigo: TIntegerField;
    CDSContatoscon_email: TStringField;
    CDSContatoscon_contato: TStringField;
    CDSContatoscon_telefone: TStringField;
    CDSContatoscon_observacao: TStringField;
    FDQContatoscon_codigo: TIntegerField;
    FDQContatoscon_ent_codigo: TIntegerField;
    FDQContatoscon_ent_emp_codigo: TIntegerField;
    FDQContatoscon_email: TStringField;
    FDQContatoscon_contato: TStringField;
    FDQContatoscon_telefone: TStringField;
    FDQContatoscon_observacao: TStringField;
    ed_ent_razao_social_j: TEdit;
    ed_ent_ie: TEdit;
    ed_ent_cnpj: TMaskEdit;
    ed_ent_nome_fantasia: TEdit;
    ed_ent_im: TEdit;
    tsResponsaveis: TTabSheet;
    ed_res_nome: TEdit;
    ed_res_rg: TEdit;
    ed_res_data_nascimento: TDateTimePicker;
    ed_res_cpf: TMaskEdit;
    tsObservacoes: TTabSheet;
    FDQEntidadesent_observacoes: TBlobField;
    CDSEntidadesent_observacoes: TBlobField;
    tsAdicionais: TTabSheet;
    GroupBox1: TGroupBox;
    ckl_ent_tipo: TCheckListBox;
    FDQTipos: TFDQuery;
    CDSTipos: TClientDataSet;
    PROTipos: TDataSetProvider;
    DSTipos: TDataSource;
    FDQTiposetp_ent_codigo: TIntegerField;
    FDQTiposetp_ent_emp_codigo: TIntegerField;
    FDQTiposetp_tipo: TIntegerField;
    CDSTiposetp_ent_codigo: TIntegerField;
    CDSTiposetp_ent_emp_codigo: TIntegerField;
    CDSTiposetp_tipo: TIntegerField;
    tsFuncionarios: TTabSheet;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    cmb_fun_modo: TComboBox;
    Label19: TLabel;
    cmb_fun_situacao: TComboBox;
    FDQFuncionarios: TFDQuery;
    CDSFuncionarios: TClientDataSet;
    PROFuncionarios: TDataSetProvider;
    DSFuncionarios: TDataSource;
    FDQFuncionariosfun_ent_codigo: TIntegerField;
    FDQFuncionariosfun_ent_emp_codigo: TIntegerField;
    FDQFuncionariosfun_ct: TStringField;
    FDQFuncionariosfun_pis: TStringField;
    FDQFuncionariosfun_funcao: TStringField;
    FDQFuncionariosfun_salario: TBCDField;
    FDQFuncionariosfun_modo: TIntegerField;
    FDQFuncionariosfun_situacao: TIntegerField;
    CDSFuncionariosfun_ent_codigo: TIntegerField;
    CDSFuncionariosfun_ent_emp_codigo: TIntegerField;
    CDSFuncionariosfun_ct: TStringField;
    CDSFuncionariosfun_pis: TStringField;
    CDSFuncionariosfun_funcao: TStringField;
    CDSFuncionariosfun_salario: TBCDField;
    CDSFuncionariosfun_modo: TIntegerField;
    CDSFuncionariosfun_situacao: TIntegerField;
    ed_fun_salario: TDBEdit;
    ed_fun_ct: TEdit;
    ed_fun_pis: TEdit;
    ed_fun_funcao: TEdit;
    tsContratos: TTabSheet;
    ed_ent_observacoes: TRichEdit;
    lbl_con_tipo: TLabel;
    lbl_con_ent_codigo_funcionario: TLabel;
    lbl_con_pro_codigo: TLabel;
    lbl_con_descricao: TLabel;
    pnlBotoesContratos: TPanel;
    btnNovoContr: TBitBtn;
    btnGravarContr: TBitBtn;
    btnEditarContr: TBitBtn;
    btnCancelarContr: TBitBtn;
    btnExcluirContr: TBitBtn;
    cmb_con_tipo: TComboBox;
    cmb_con_ent_codigo_funcionario: TComboBox;
    cmb_con_pro_codigo: TComboBox;
    ed_con_descricao: TEdit;
    DBGContratos: TDBGrid;
    ckl_formas_pagamentos: TCheckListBox;
    lbl_con_vencimento_pagamento: TLabel;
    lbl_con_valor: TLabel;
    ed_con_vencimento_contrato: TDateTimePicker;
    lbl_con_vencimento_contrato: TLabel;
    lbl_con_horas_mes: TLabel;
    ed_con_horas_mes: TEdit;
    FDQContratos: TFDQuery;
    CDSContratos: TClientDataSet;
    PROContratos: TDataSetProvider;
    DSContratos: TDataSource;
    gb_formas_pagamentos: TGroupBox;
    FDQFormasPagamentosContratos: TFDQuery;
    CDSFormasPagamentosContratos: TClientDataSet;
    PROFormasPagamentosContratos: TDataSetProvider;
    DSFormasPagamentosContratos: TDataSource;
    lbl_con_situacao: TLabel;
    cmb_con_situacao: TComboBox;
    ed_con_vencimento_pagamento: TEdit;
    FDQContratoscon_codigo: TIntegerField;
    FDQContratoscon_ent_codigo: TIntegerField;
    FDQContratoscon_ent_emp_codigo: TIntegerField;
    FDQContratoscon_tipo: TIntegerField;
    FDQContratoscon_pro_codigo: TIntegerField;
    FDQContratoscon_descricao: TStringField;
    FDQContratoscon_ent_codigo_funcionario: TIntegerField;
    FDQContratoscon_ent_emp_codigo_funcionario: TIntegerField;
    FDQContratoscon_valor: TBCDField;
    FDQContratoscon_vencimento_pagamento: TIntegerField;
    FDQContratoscon_vencimento_contrato: TDateField;
    FDQContratoscon_contrato: TBlobField;
    FDQContratoscon_situacao: TIntegerField;
    CDSContratoscon_codigo: TIntegerField;
    CDSContratoscon_ent_codigo: TIntegerField;
    CDSContratoscon_ent_emp_codigo: TIntegerField;
    CDSContratoscon_tipo: TIntegerField;
    CDSContratoscon_pro_codigo: TIntegerField;
    CDSContratoscon_descricao: TStringField;
    CDSContratoscon_ent_codigo_funcionario: TIntegerField;
    CDSContratoscon_ent_emp_codigo_funcionario: TIntegerField;
    CDSContratoscon_valor: TBCDField;
    CDSContratoscon_vencimento_pagamento: TIntegerField;
    CDSContratoscon_vencimento_contrato: TDateField;
    CDSContratoscon_contrato: TBlobField;
    CDSContratoscon_situacao: TIntegerField;
    CDSContratosc_pro_descricao: TStringField;
    CDSContratosc_ent_razao_social: TStringField;
    CDSContratosc_con_tipo: TStringField;
    FDQFormasPagamentosContratosfpc_codigo: TIntegerField;
    FDQFormasPagamentosContratosfpc_con_codigo: TIntegerField;
    FDQFormasPagamentosContratosfpc_con_ent_codigo: TIntegerField;
    FDQFormasPagamentosContratosfpc_con_ent_emp_codigo: TIntegerField;
    CDSFormasPagamentosContratosfpc_codigo: TIntegerField;
    CDSFormasPagamentosContratosfpc_con_codigo: TIntegerField;
    CDSFormasPagamentosContratosfpc_con_ent_codigo: TIntegerField;
    CDSFormasPagamentosContratosfpc_con_ent_emp_codigo: TIntegerField;
    FDQContratoscon_pro_emp_codigo: TIntegerField;
    CDSContratoscon_pro_emp_codigo: TIntegerField;
    FDQContratoscon_flag_cr: TIntegerField;
    CDSContratoscon_flag_cr: TIntegerField;
    btnGerarCR: TBitBtn;
    btnRenovar: TBitBtn;
    ed_con_valor: TEdit;
    btnOpcoes: TBitBtn;
    PopOpcoes: TPopupMenu;
    meImprimirSaldoDeAulas: TMenuItem;
    Relatorio: TfrxReport;
    FDQSaldoAulas: TFDQuery;
    frxDBSaldoAulas: TfrxDBDataset;
    FDQSaldoAulasent_codigo: TIntegerField;
    FDQSaldoAulasent_emp_codigo: TIntegerField;
    FDQSaldoAulasent_razao_social: TStringField;
    FDQSaldoAulasagd_qtde_horas: TFMTBCDField;
    FDQSaldoAulascon_horas_mes: TFMTBCDField;
    FDQSaldoAulashoras_faltantes: TFMTBCDField;
    FDQSaldoAulassit_descricao: TStringField;
    cdsExclusaoContrato: TClientDataSet;
    cdsExclusaoContratocon_codigo: TIntegerField;
    ck_indicacao: TCheckBox;
    btnTransferirHoras: TBitBtn;
    pnlTransferir: TPanel;
    Label3: TLabel;
    cmbDestinoHoras: TComboBox;
    btnConfirmarTransf: TBitBtn;
    btnCancelarTransf: TBitBtn;
    Label4: TLabel;
    ed_horas_transf: TEdit;
    FDQContratoscon_horas_mes: TBCDField;
    CDSContratoscon_horas_mes: TBCDField;
    N1: TMenuItem;
    ImprimirSaldoemContratosxSaldoemAulas1: TMenuItem;
    FDQSaldoContratos: TFDQuery;
    frxDBSaldoContratos: TfrxDBDataset;
    frxDBSaldoAulasCont: TfrxDBDataset;
    FDQSaldoAulasCont: TFDQuery;
    FDQSaldoContratosdescricao: TStringField;
    FDQSaldoContratosvalor: TBCDField;
    FDQSaldoContratosvencimento: TStringField;
    FDQSaldoContratoshoras: TBCDField;
    FDQSaldoContratosent_razao_social: TStringField;
    FDQSaldoAulasContprofessor: TStringField;
    FDQSaldoAulasContdia: TStringField;
    FDQSaldoAulasConthoras: TBCDField;
    procedure FormActivate(Sender: TObject);
    procedure ed_consultaChange(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure CDSEnderecosCalcFields(DataSet: TDataSet);
    procedure btnNovoEndClick(Sender: TObject);
    procedure btnGravarEndClick(Sender: TObject);
    procedure btnEditarEndClick(Sender: TObject);
    procedure btnCancelarEndClick(Sender: TObject);
    procedure btnExcluirEndClick(Sender: TObject);
    procedure cmb_end_uf_siglaChange(Sender: TObject);
    procedure btnCancelarConClick(Sender: TObject);
    procedure btnExcluirConClick(Sender: TObject);
    procedure btnGravarConClick(Sender: TObject);
    procedure btnEditarConClick(Sender: TObject);
    procedure btnNovoConClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure rg_ent_tipo_pessoaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dbgEntidadesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGContatosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGEnderecosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGEnderecosDblClick(Sender: TObject);
    procedure DBGContatosDblClick(Sender: TObject);
    procedure dbgEntidadesDblClick(Sender: TObject);
    procedure ed_ent_data_nascimentoExit(Sender: TObject);
    procedure ed_ent_observacoesEnter(Sender: TObject);
    procedure ed_ent_observacoesExit(Sender: TObject);
    procedure ed_ent_cpfExit(Sender: TObject);
    procedure ed_res_cpfExit(Sender: TObject);
    procedure ckl_ent_tipoClick(Sender: TObject);
    procedure ed_fun_salarioExit(Sender: TObject);
    procedure CDSContratosCalcFields(DataSet: TDataSet);
    procedure btnNovoContrClick(Sender: TObject);
    procedure btnGravarContrClick(Sender: TObject);
    procedure btnCancelarContrClick(Sender: TObject);
    procedure btnExcluirContrClick(Sender: TObject);
    procedure btnEditarContrClick(Sender: TObject);
    procedure CDSFuncionariosBeforePost(DataSet: TDataSet);
    procedure btnGerarCRClick(Sender: TObject);
    procedure CDSContratosAfterScroll(DataSet: TDataSet);
    procedure btnRenovarClick(Sender: TObject);
    procedure ed_con_valorKeyPress(Sender: TObject; var Key: Char);
    procedure btnOpcoesClick(Sender: TObject);
    procedure meImprimirSaldoDeAulasClick(Sender: TObject);
    procedure btnTransferirHorasClick(Sender: TObject);
    procedure btnCancelarTransfClick(Sender: TObject);
    procedure btnConfirmarTransfClick(Sender: TObject);
    procedure ed_con_horas_mesKeyPress(Sender: TObject; var Key: Char);
    procedure ImprimirSaldoemContratosxSaldoemAulas1Click(Sender: TObject);
  private
    { Private declarations }
    v_operacao           : TOperacaoQuery;
    v_operacao_enderecos : TOperacaoQuery;
    v_operacao_contatos  : TOperacaoQuery;
    v_operacao_contratos : TOperacaoQuery;

    b_novo_contrato : Boolean;

    procedure proc_limpa_campos; Overload;
    procedure proc_limpa_campos(PTipoPessoa : Integer); Overload;
    procedure proc_limpa_campos_endereco(PStatus : Boolean);
    procedure proc_limpa_campos_contato(PStatus : Boolean);
    procedure proc_limpa_campos_contratos(PStatus : Boolean);
  public
    { Public declarations }
  end;

var
  FrmCad_Entidades: TFrmCad_Entidades;

implementation

{$R *.dfm}

uses dm_conexao, cls_empresas, cls_situacoes_entidade, cls_entidades,
  cls_tabelacodigos, menu, cls_cidades, cls_enderecos, cls_estados,
  cls_contatos, cls_responsaveis, cls_entidades_tipos, cls_funcionarios,
  cls_produtos, cls_contratos, cls_formas_pagamentos_contratos,
  cls_contas_a_receber;

procedure TFrmCad_Entidades.btnCancelarClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos;

  tsCadastro.TabVisible := False;
  tsConsulta.TabVisible := True;

  ed_consulta.SetFocus;

  CDSEnderecos.Close;
  FDQEnderecos.Close;

  CDSContatos.Close;
  FDQContatos.Close;

  v_operacao := statNavegando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSEntidades);

  btnExcluir.Enabled := False;
end;

procedure TFrmCad_Entidades.btnCancelarConClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos_contato(False);

  v_operacao_contatos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesContatos, v_operacao_contatos, CDSContatos);

  DBGContatos.Enabled := True;
end;

procedure TFrmCad_Entidades.btnCancelarContrClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos_contratos(False);

  b_novo_contrato := True;

  v_operacao_contratos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesContratos, v_operacao_contratos, CDSContratos);

  DBGContratos.Enabled := True;
end;

procedure TFrmCad_Entidades.btnCancelarEndClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos_endereco(False);

  v_operacao_enderecos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesEndereco, v_operacao_enderecos, CDSEnderecos);

  DBGEnderecos.Enabled := True;
end;

procedure TFrmCad_Entidades.btnCancelarTransfClick(Sender: TObject);
begin
  inherited;
  pnlTransferir.Visible := False;
end;

procedure TFrmCad_Entidades.btnConfirmarTransfClick(Sender: TObject);
var c_contratos : TContratos;
begin
  inherited;
  if cmbDestinoHoras.ItemIndex = 0 then begin
    Application.MessageBox(PChar('Favor informar o cliente destino!'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
    cmbDestinoHoras.SetFocus;
    Exit;
  end;

  if StrToIntDef(ed_horas_transf.Text, 0) = 0 then begin
    Application.MessageBox(PChar('Favor informar o n�mero de horas!'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
    ed_horas_transf.SetFocus;
    Exit;
  end;

  if StrToFloatDef(ed_horas_transf.Text, 0) > CDSContratoscon_horas_mes.AsFloat then begin
    Application.MessageBox(PChar('Favor informar o n�mero de horas menor ou igual ao total do contrato!'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
    ed_horas_transf.SetFocus;
    Exit;
  end;

  if Application.MessageBox(PChar('Deseja realmente transferir as horas?'), PChar('Aten��o'), MB_YESNO + MB_ICONQUESTION) = mrYes then begin
    c_contratos := TContratos.Create;
    try
      try
        Conexao.FDConexao.StartTransaction;
        c_contratos.proc_trasnferir_horas(CDSContratoscon_codigo.AsInteger, CDSContratoscon_ent_codigo.AsInteger, CDSContratoscon_ent_emp_codigo.AsInteger,
                                          TEntidadeCombo(cmbDestinoHoras.Items.Objects[cmbDestinoHoras.ItemIndex]).ent_codigo,
                                          TEntidadeCombo(cmbDestinoHoras.Items.Objects[cmbDestinoHoras.ItemIndex]).ent_emp_codigo,
                                          StrToFloatDef(ed_horas_transf.Text, 0));
        Conexao.FDConexao.Commit;

        Application.MessageBox(PChar('Transfer�ncia realizada com sucesso!'), PChar('Aten��o'), MB_OK + MB_ICONINFORMATION);

        btnCancelarTransf.Click;
      except
        on E : Exception do begin
          Conexao.FDConexao.Rollback;
          Application.MessageBox(PChar(E.Message), PChar('Aten��o'), MB_OK + MB_ICONERROR);
        end;
      end;
    finally
      FreeAndNil(c_contratos);
    end;
  end;
end;

procedure TFrmCad_Entidades.btnEditarClick(Sender: TObject);
var c_empresa           : TEmpresas;
    c_status            : TSituacoesEntidade;
    c_enderecos         : TEnderecos;
    c_contatos          : TContatos;
    c_entidades         : TEntidades;
    c_responsaveis      : TResponsaveis;
    c_ent_tipos         : TEntidadesTipos;
    c_funcionarios      : TFuncionarios;
    c_contratos         : TContratos;
    c_formas_pagamentos : TFormasPagamentosContratos;
begin
  inherited;
  ed_ent_codigo.Text := CDSEntidadesent_codigo.AsString;

  try
    c_empresa := TEmpresas.Create;
    cmb_ent_emp_codigo.ItemIndex := cmb_ent_emp_codigo.Items.IndexOf(c_empresa.func_retorna_razao_combo(CDSEntidadesent_emp_codigo.AsInteger));
  finally
    FreeAndNil(c_empresa);
  end;

  rg_ent_tipo_pessoa.ItemIndex := CDSEntidadesent_tipo_pessoa.AsInteger;
  rg_ent_tipo_pessoaClick(Self);

  case rg_ent_tipo_pessoa.ItemIndex of
    0 : begin
          ed_ent_razao_social_f.Text     := CDSEntidadesent_razao_social.AsString;
          ed_ent_cpf.Text                := CDSEntidadesent_cpf.AsString;
          ed_ent_rg.Text                 := CDSEntidadesent_rg.AsString;
          ed_ent_data_nascimento.Date    := CDSEntidadesent_data_nascimento.AsDateTime;
          ed_ent_data_nascimentoExit(Self);
          ed_ent_ocupacao.Text           := CDSEntidadesent_ocupacao.AsString;
          cmb_ent_estado_civil.ItemIndex := cmb_ent_estado_civil.Items.IndexOf(CDSEntidadesent_estado_civil.AsString);
        end;

    1 : begin
          ed_ent_razao_social_j.Text := CDSEntidadesent_razao_social.AsString;
          ed_ent_nome_fantasia.Text  := CDSEntidadesent_nome_fantasia.AsString;
          ed_ent_cnpj.Text           := CDSEntidadesent_cnpj.AsString;
          ed_ent_ie.Text             := CDSEntidadesent_ie.AsString;
          ed_ent_im.Text             := CDSEntidadesent_im.AsString;
        end;
  end;

  c_entidades := TEntidades.Create;
  c_entidades.ent_observacoes := TMemoryStream.Create;
  try
    if c_entidades.func_recuperar_observacoes(CDSEntidadesent_codigo.AsInteger, CDSEntidadesent_emp_codigo.AsInteger) then begin
      c_entidades.ent_observacoes.Position := 0;
      ed_ent_observacoes.Lines.LoadFromStream(c_entidades.ent_observacoes);
    end;
  finally
    c_entidades.ent_observacoes.Free;
    FreeAndNil(c_entidades);
  end;

  try
    c_status  := TSituacoesEntidade.Create;
    cmb_ent_sit_codigo.ItemIndex := cmb_ent_sit_codigo.Items.IndexOf(c_status.func_retorna_status_combo(CDSEntidadesent_sit_codigo.AsInteger));
  finally
    FreeAndNil(c_status);
  end;

  v_operacao := statEditando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSEntidades);

  c_enderecos := TEnderecos.Create;
  try
    c_enderecos.proc_consultar(FDQEnderecos, CDSEnderecos, StrToInt(ed_ent_codigo.Text), TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo);
  finally
    FreeAndNil(c_enderecos);
  end;

  c_contatos := TContatos.Create;
  try
    c_contatos.proc_consultar(FDQContatos, CDSContatos, StrToInt(ed_ent_codigo.Text), TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo);
  finally
    FreeAndNil(c_contatos);
  end;

  c_responsaveis := TResponsaveis.Create;
  try
    if c_responsaveis.func_recuperar(CDSEntidadesent_codigo.AsInteger, CDSEntidadesent_emp_codigo.AsInteger) then begin
      ed_res_nome.Text            := c_responsaveis.res_nome;
      ed_res_rg.Text              := c_responsaveis.res_rg;
      ed_res_cpf.Text             := c_responsaveis.res_cpf;
      ed_res_data_nascimento.Date := c_responsaveis.res_data_nascimento;
    end;
  finally
    FreeAndNil(c_responsaveis);
  end;

  try
    c_ent_tipos := TEntidadesTipos.Create;
    c_ent_tipos.proc_carrega_check_list(ckl_ent_tipo, CDSEntidadesent_codigo.AsInteger, CDSEntidadesent_emp_codigo.AsInteger);
  finally
    FreeAndNil(c_ent_tipos);
  end;

  try
    c_funcionarios := TFuncionarios.Create;
    c_funcionarios.proc_consultar(FDQFuncionarios, CDSFuncionarios, CDSEntidadesent_codigo.AsInteger, CDSEntidadesent_emp_codigo.AsInteger);

    if (not CDSFuncionarios.Eof) then begin
      ed_fun_ct.Text      := CDSFuncionariosfun_ct.AsString;
      ed_fun_pis.Text     := CDSFuncionariosfun_pis.AsString;
      ed_fun_funcao.Text  := CDSFuncionariosfun_funcao.AsString;
      ed_fun_salario.Text := CDSFuncionariosfun_salario.AsString;

      cmb_fun_modo.ItemIndex     := CDSFuncionariosfun_modo.AsInteger;
      cmb_fun_situacao.ItemIndex := CDSFuncionariosfun_situacao.AsInteger;

      tsFuncionarios.TabVisible := True;
    end;
  finally
    FreeAndNil(c_funcionarios);
  end;

  c_contratos := TContratos.Create;
  try
    c_contratos.proc_consultar(FDQContratos, CDSContratos, 0, CDSEntidadesent_codigo.AsInteger, CDSEntidadesent_emp_codigo.AsInteger);
  finally
    FreeAndNil(c_contratos);
  end;

  c_formas_pagamentos := TFormasPagamentosContratos.Create;
  try
    c_formas_pagamentos.proc_consultar(FDQFormasPagamentosContratos, CDSFormasPagamentosContratos, 0, 0, CDSEntidadesent_codigo.AsInteger, CDSEntidadesent_emp_codigo.AsInteger);
  finally
    FreeAndNil(c_formas_pagamentos);
  end;

  v_operacao_enderecos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesEndereco, v_operacao_enderecos, CDSEnderecos);

  v_operacao_contatos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesContatos, v_operacao_contatos, CDSContatos);

  v_operacao_contratos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesContratos, v_operacao_contratos, CDSContratos);

  proc_limpa_campos_endereco(False);
  proc_limpa_campos_contato(False);
  proc_limpa_campos_contratos(False);

  PCAdicionais.ActivePageIndex := tsEnderecos.TabIndex;

  tsConsulta.TabVisible := False;
  tsCadastro.TabVisible := True;

  rg_ent_tipo_pessoa.SetFocus;

  DBGContratos.Enabled := True;
end;

procedure TFrmCad_Entidades.btnEditarConClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos_contato(True);

  ed_con_email.Text      := CDSContatoscon_email.AsString;
  ed_con_contato.Text    := CDSContatoscon_contato.AsString;
  ed_con_telefone.Text   := CDSContatoscon_telefone.AsString;
  ed_con_observacao.Text := CDSContatoscon_observacao.AsString;

  v_operacao_contatos := statEditando;
  proc_atualiza_operacao_query(pnlBotoesContatos, v_operacao_contatos, CDSContatos);

  DBGContatos.Enabled := False;

  ed_con_contato.SetFocus;
end;

procedure TFrmCad_Entidades.btnEditarContrClick(Sender: TObject);
var c_produtos     : TProdutos;
    c_funcionarios : TEntidades;

    i_nro : Integer;
begin
  inherited;
  if CDSContratoscon_situacao.AsInteger = 2 then begin
    Application.MessageBox(PChar('Contrato conclu�do, n�o poder� ser alterado, favor renovar!'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
    Exit;
  end;

  proc_limpa_campos_contratos(True);

  b_novo_contrato := False;

  try
    c_produtos := TProdutos.Create;
    cmb_con_pro_codigo.ItemIndex := cmb_con_pro_codigo.Items.IndexOf(c_produtos.func_retorna_nome_combo(CDSContratoscon_pro_codigo.AsInteger, CDSContratoscon_pro_emp_codigo.AsInteger));
  finally
    FreeAndNil(c_produtos);
  end;

  try
    c_funcionarios := TEntidades.Create;
    cmb_con_ent_codigo_funcionario.ItemIndex := cmb_con_ent_codigo_funcionario.Items.IndexOf(c_funcionarios.func_retorna_nome_combo(CDSContratoscon_ent_codigo_funcionario.AsInteger, CDSContratoscon_ent_emp_codigo_funcionario.AsInteger));
  finally
    FreeAndNil(c_funcionarios);
  end;

  ed_con_descricao.Text            := CDSContratoscon_descricao.AsString;
  ed_con_vencimento_pagamento.Text := CDSContratoscon_vencimento_pagamento.AsString;
  cmb_con_tipo.ItemIndex           := CDSContratoscon_tipo.AsInteger;
  ed_con_horas_mes.Text            := CDSContratoscon_horas_mes.AsString;
  ed_con_vencimento_contrato.Date  := CDSContratoscon_vencimento_contrato.AsDateTime;
  cmb_con_situacao.ItemIndex       := CDSContratoscon_situacao.AsInteger;
  ed_con_valor.Text                := CDSContratoscon_valor.AsString;

  try
    CDSFormasPagamentosContratos.Filtered := False;
    CDSFormasPagamentosContratos.Filter   := ' fpc_con_codigo         = ' + CDSContratoscon_codigo.AsString     +   ' and ' +
                                             ' fpc_con_ent_codigo     = ' + CDSContratoscon_ent_codigo.AsString +   ' and ' +
                                             ' fpc_con_ent_emp_codigo = ' + CDSContratoscon_ent_emp_codigo.AsString;
    CDSFormasPagamentosContratos.Filtered := True;
    CDSFormasPagamentosContratos.First;

    while not CDSFormasPagamentosContratos.Eof do begin
      ckl_formas_pagamentos.Checked[CDSFormasPagamentosContratosfpc_codigo.AsInteger] := True;

      CDSFormasPagamentosContratos.Next;
    end;
  finally
    CDSFormasPagamentosContratos.Filtered := False;
    CDSFormasPagamentosContratos.Filter   := '';

    CDSFormasPagamentosContratos.First;
  end;

  v_operacao_contratos := statEditando;
  proc_atualiza_operacao_query(pnlBotoesContratos, v_operacao_contratos, CDSContratos);

  DBGContratos.Enabled := False;

  cmb_con_pro_codigo.SetFocus;
end;

procedure TFrmCad_Entidades.btnEditarEndClick(Sender: TObject);
var c_cidades : TCidades;
begin
  inherited;
  proc_limpa_campos_endereco(True);

  cmb_end_tipo.ItemIndex       := CDSEnderecosend_tipo.AsInteger;
  ed_end_cep.Text              := CDSEnderecosend_cep.AsString;
  cmb_end_uf_sigla.ItemIndex   := cmb_end_uf_sigla.Items.IndexOf(CDSEnderecosend_uf_sigla.AsString);
  cmb_end_uf_siglaChange(Self);

  try
    c_cidades := TCidades.Create;
    cmb_end_cid_codigo.ItemIndex := cmb_end_cid_codigo.Items.IndexOf(c_cidades.func_retorna_nome_combo(CDSEnderecosend_cid_codigo.AsInteger));
  finally
    FreeAndNil(c_cidades);
  end;

  ed_end_logradouro.Text  := CDSEnderecosend_logradouro.AsString;
  ed_end_numero.Text      := CDSEnderecosend_numero.AsString;
  ed_end_complemento.Text := CDSEnderecosend_complemento.AsString;

  v_operacao_enderecos := statEditando;
  proc_atualiza_operacao_query(pnlBotoesEndereco, v_operacao_enderecos, CDSEnderecos);

  DBGEnderecos.Enabled := False;

  cmb_end_tipo.SetFocus;
end;

procedure TFrmCad_Entidades.btnExcluirConClick(Sender: TObject);
begin
  inherited;
  CDSContatos.Delete;

  v_operacao_contatos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesContatos, v_operacao_contatos, CDSContatos);
end;

procedure TFrmCad_Entidades.btnExcluirContrClick(Sender: TObject);
begin
  inherited;
  if (CDSContratoscon_situacao.AsInteger = 1) or (CDSContratoscon_situacao.AsInteger = 2) then begin
    Application.MessageBox(PChar('Contrato conclu�do, n�o poder� ser alterado!'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
    Exit;
  end;

  cdsExclusaoContrato.Insert;
  cdsExclusaoContratocon_codigo.AsInteger := CDSContratoscon_codigo.AsInteger;
  cdsExclusaoContrato.Post;

  try
    CDSFormasPagamentosContratos.Filtered := False;
    CDSFormasPagamentosContratos.Filter   := ' fpc_con_codigo         = ' + CDSContratoscon_codigo.AsString     +   ' and ' +
                                             ' fpc_con_ent_codigo     = ' + CDSContratoscon_ent_codigo.AsString +   ' and ' +
                                             ' fpc_con_ent_emp_codigo = ' + CDSContratoscon_ent_emp_codigo.AsString;
    CDSFormasPagamentosContratos.Filtered := True;

    CDSFormasPagamentosContratos.Open;
    CDSFormasPagamentosContratos.First;

    while (not CDSFormasPagamentosContratos.Eof) do begin
      CDSFormasPagamentosContratos.Delete;
    end;
  finally
    CDSFormasPagamentosContratos.Filtered := False;
    CDSFormasPagamentosContratos.Filter   := '';
  end;

  CDSContratos.Delete;

  v_operacao_contratos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesContratos, v_operacao_contratos, CDSContratos);
end;

procedure TFrmCad_Entidades.btnExcluirEndClick(Sender: TObject);
begin
  inherited;
  CDSEnderecos.Delete;

  v_operacao_enderecos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesEndereco, v_operacao_enderecos, CDSEnderecos);
end;

procedure TFrmCad_Entidades.btnGerarCRClick(Sender: TObject);
var c_contas_receber : TContasReceber;
    c_tabela         : TTabelaCodigos;
begin
  inherited;
  try
    CDSContratos.DisableControls;

    CDSContratos.Filter   := 'con_flag_cr = 0';
    CDSContratos.Filtered := True;

    CDSContratos.First;
    while (not CDSContratos.Eof) do begin
      c_contas_receber := TContasReceber.Create;
      try
        c_contas_receber.cr_codigo         := c_tabela.func_gerar_codigo('contas_a_receber', IntToStr(TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo));
        c_contas_receber.cr_emp_codigo     := TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo;
        c_contas_receber.cr_ent_codigo     := StrToInt(ed_ent_codigo.Text);
        c_contas_receber.cr_ent_emp_codigo := TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo;
        c_contas_receber.cr_con_codigo     := CDSContratoscon_codigo.AsInteger;
        c_contas_receber.cr_data           := Date;
        c_contas_receber.cr_observacao     := '';
        c_contas_receber.cr_situacao       := 0;
        c_contas_receber.cr_valor          := CDSContratoscon_valor.AsFloat;

        c_contas_receber.proc_gravar(c_contas_receber.cr_codigo, c_contas_receber.cr_emp_codigo);
      finally
        FreeAndNil(c_contas_receber);
      end;

      CDSContratos.Next;
    end;
  finally
    CDSContratos.Filter   := '';
    CDSContratos.Filtered := False;

    CDSContratos.EnableControls;
  end;
end;

procedure TFrmCad_Entidades.btnGravarClick(Sender: TObject);
var c_entidades         : TEntidades;
    c_enderecos         : TEnderecos;
    c_contatos          : TContatos;
    c_responsaveis      : TResponsaveis;
    c_ent_tipos         : TEntidadesTipos;
    c_funcionarios      : TFuncionarios;
    c_contratos         : TContratos;
    c_formas_pagamentos : TFormasPagamentosContratos;

    i_nro : Integer;
begin
  inherited;
  c_entidades := TEntidades.Create;
  c_entidades.ent_observacoes := TMemoryStream.Create;
  c_enderecos         := TEnderecos.Create;
  c_contatos          := TContatos.Create;
  c_responsaveis      := TResponsaveis.Create;
  c_ent_tipos         := TEntidadesTipos.Create;
  c_funcionarios      := TFuncionarios.Create;
  c_contratos         := TContratos.Create;
  c_formas_pagamentos := TFormasPagamentosContratos.Create;

  try
    try
      Conexao.FDConexao.StartTransaction;

      c_entidades.ent_codigo     := StrToInt(ed_ent_codigo.Text);
      c_entidades.ent_emp_codigo := TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo;

      case rg_ent_tipo_pessoa.ItemIndex of
        0 : begin
              c_entidades.ent_razao_social    := ed_ent_razao_social_f.Text;
              c_entidades.ent_cpf             := ed_ent_cpf.Text;
              c_entidades.ent_rg              := ed_ent_rg.Text;
              c_entidades.ent_data_nascimento := ed_ent_data_nascimento.Date;
              c_entidades.ent_ocupacao        := ed_ent_ocupacao.Text;
              c_entidades.ent_estado_civil    := cmb_ent_estado_civil.Items.Strings[cmb_ent_estado_civil.ItemIndex];
              c_entidades.ent_nome_fantasia   := '';
              c_entidades.ent_cnpj            := '';
              c_entidades.ent_ie              := '';
              c_entidades.ent_im              := '';
            end;

        1 : begin
              c_entidades.ent_razao_social    := ed_ent_razao_social_j.Text;
              c_entidades.ent_cpf             := '';
              c_entidades.ent_rg              := '';
              c_entidades.ent_data_nascimento := 0;
              c_entidades.ent_ocupacao        := '';
              c_entidades.ent_estado_civil    := '';
              c_entidades.ent_nome_fantasia   := ed_ent_nome_fantasia.Text;
              c_entidades.ent_cnpj            := ed_ent_cnpj.Text;
              c_entidades.ent_ie              := ed_ent_ie.Text;
              c_entidades.ent_im              := ed_ent_im.Text;
//              c_entidades.ent_ir              :=
//              c_entidades.ent_suframa         :=
//              c_entidades.ent_ramo_codigo     :=
            end;
      end;

      c_entidades.ent_sit_codigo  := TSituacoesEntidade(cmb_ent_sit_codigo.Items.Objects[cmb_ent_sit_codigo.ItemIndex]).sit_codigo;
      c_entidades.ent_tipo_pessoa := rg_ent_tipo_pessoa.ItemIndex;

      ed_ent_observacoes.Lines.SaveToStream(c_entidades.ent_observacoes);

      c_entidades.proc_gravar(c_entidades.ent_codigo, c_entidades.ent_emp_codigo);

      c_enderecos.proc_limpar_gravacao(c_entidades.ent_codigo, c_entidades.ent_emp_codigo);

      try
        CDSEnderecos.DisableControls;

        CDSEnderecos.First;
        while (not CDSEnderecos.Eof) do begin
          c_enderecos.end_codigo         := CDSEnderecosend_codigo.AsInteger;
          c_enderecos.end_ent_codigo     := CDSEnderecosend_ent_codigo.AsInteger;
          c_enderecos.end_ent_emp_codigo := CDSEnderecosend_ent_emp_codigo.AsInteger;
          c_enderecos.end_tipo           := CDSEnderecosend_tipo.AsInteger;
          c_enderecos.end_cep            := CDSEnderecosend_cep.AsString;
          c_enderecos.end_uf_sigla       := CDSEnderecosend_uf_sigla.AsString;
          c_enderecos.end_cid_codigo     := CDSEnderecosend_cid_codigo.AsInteger;
          c_enderecos.end_logradouro     := CDSEnderecosend_logradouro.AsString;
          c_enderecos.end_numero         := CDSEnderecosend_numero.AsInteger;
          c_enderecos.end_complemento    := CDSEnderecosend_complemento.AsString;

          c_enderecos.proc_gravar(c_enderecos.end_codigo, c_enderecos.end_ent_codigo, c_enderecos.end_ent_emp_codigo);

          CDSEnderecos.Next;
        end;
      finally
        CDSEnderecos.EnableControls;
      end;

      c_contatos.proc_limpar_gravacao(c_entidades.ent_codigo, c_entidades.ent_emp_codigo);

      try
        CDSContatos.DisableControls;

        CDSContatos.First;
        while (not CDSContatos.Eof) do begin
          c_contatos.con_codigo         := CDSContatoscon_codigo.AsInteger;
          c_contatos.con_ent_codigo     := CDSContatoscon_ent_codigo.AsInteger;
          c_contatos.con_ent_emp_codigo := CDSContatoscon_ent_emp_codigo.AsInteger;
          c_contatos.con_email          := CDSContatoscon_email.AsString;
          c_contatos.con_contato        := CDSContatoscon_contato.AsString;
          c_contatos.con_telefone       := CDSContatoscon_telefone.AsString;
          c_contatos.con_observacao     := CDSContatoscon_observacao.AsString;

          c_contatos.proc_gravar(c_contatos.con_codigo, c_contatos.con_ent_codigo, c_contatos.con_ent_emp_codigo);

          CDSContatos.Next;
        end;
      finally
        CDSContatos.EnableControls;
      end;

      c_responsaveis.res_ent_codigo      := c_entidades.ent_codigo;
      c_responsaveis.res_ent_emp_codigo  := c_entidades.ent_emp_codigo;
      c_responsaveis.res_nome            := ed_res_nome.Text;
      c_responsaveis.res_rg              := ed_res_rg.Text;
      c_responsaveis.res_cpf             := ed_res_cpf.Text;
      c_responsaveis.res_data_nascimento := ed_res_data_nascimento.Date;

      c_responsaveis.proc_gravar(c_responsaveis.res_ent_codigo, c_responsaveis.res_ent_emp_codigo);

      c_ent_tipos.proc_limpar_gravacao(c_entidades.ent_codigo, c_entidades.ent_emp_codigo);
      for i_nro := 0 to ckl_ent_tipo.Items.Count -1 do begin
        if ckl_ent_tipo.Checked[i_nro] then begin
          c_ent_tipos.etp_ent_codigo     := c_entidades.ent_codigo;
          c_ent_tipos.etp_ent_emp_codigo := c_entidades.ent_emp_codigo;
          c_ent_tipos.etp_tipo           := i_nro;

          c_ent_tipos.proc_gravar(c_ent_tipos.etp_ent_codigo, c_ent_tipos.etp_ent_emp_codigo, c_ent_tipos.etp_tipo);
        end;
      end;

      c_funcionarios.proc_limpar_gravacao(c_entidades.ent_codigo, c_entidades.ent_emp_codigo);
      if ckl_ent_tipo.Checked[2] {FUNCION�RIO} then begin
        c_funcionarios.fun_ent_codigo     := c_entidades.ent_codigo;
        c_funcionarios.fun_ent_emp_codigo := c_entidades.ent_emp_codigo;
        c_funcionarios.fun_ct             := ed_fun_ct.Text;
        c_funcionarios.fun_pis            := ed_fun_pis.Text;
        c_funcionarios.fun_funcao         := ed_fun_funcao.Text;
        c_funcionarios.fun_salario        := CDSFuncionariosfun_salario.AsFloat;
        c_funcionarios.fun_modo           := cmb_fun_modo.ItemIndex;
        c_funcionarios.fun_situacao       := cmb_fun_situacao.ItemIndex;

        c_funcionarios.proc_gravar(c_entidades.ent_codigo, c_entidades.ent_emp_codigo);
      end;

      cdsExclusaoContrato.First;
      while not cdsExclusaoContrato.Eof do begin
        c_formas_pagamentos.proc_limpar_gravacao(cdsExclusaoContratocon_codigo.AsInteger, StrToInt(ed_ent_codigo.Text), TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo);
        c_contratos.proc_excluir(cdsExclusaoContratocon_codigo.AsInteger, StrToInt(ed_ent_codigo.Text), TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo);

        cdsExclusaoContrato.Next;
      end;

      CDSContratos.First;
      while (not CDSContratos.Eof) do begin
        c_contratos.con_codigo                     := CDSContratoscon_codigo.AsInteger;
        c_contratos.con_ent_codigo                 := CDSContratoscon_ent_codigo.AsInteger;
        c_contratos.con_ent_emp_codigo             := CDSContratoscon_ent_emp_codigo.AsInteger;
        c_contratos.con_tipo                       := CDSContratoscon_tipo.AsInteger;
        c_contratos.con_pro_codigo                 := CDSContratoscon_pro_codigo.AsInteger;
        c_contratos.con_pro_emp_codigo             := CDSContratoscon_pro_emp_codigo.AsInteger;
        c_contratos.con_descricao                  := CDSContratoscon_descricao.AsString;
        c_contratos.con_ent_codigo_funcionario     := CDSContratoscon_ent_codigo_funcionario.AsInteger;
        c_contratos.con_ent_emp_codigo_funcionario := CDSContratoscon_ent_emp_codigo_funcionario.AsInteger;
        c_contratos.con_valor                      := CDSContratoscon_valor.AsFloat;
        c_contratos.con_vencimento_pagamento       := CDSContratoscon_vencimento_pagamento.AsInteger;
        c_contratos.con_vencimento_contrato        := CDSContratoscon_vencimento_contrato.AsDateTime;
        c_contratos.con_horas_mes                  := CDSContratoscon_horas_mes.AsFloat;
//        c_contratos.con_contrato                 := CDSContratoss.con_contrato
        c_contratos.con_situacao                   := CDSContratoscon_situacao.AsInteger;
        c_contratos.con_flag_cr                    := 1;

        c_contratos.proc_gravar(c_contratos.con_codigo, c_contratos.con_ent_codigo, c_contratos.con_ent_emp_codigo);

        c_formas_pagamentos.proc_limpar_gravacao(c_contratos.con_codigo, c_contratos.con_ent_codigo, c_contratos.con_ent_emp_codigo);
        try
          CDSFormasPagamentosContratos.Filtered := False;
          CDSFormasPagamentosContratos.Filter   := ' fpc_con_codigo         = ' + IntToStr(c_contratos.con_codigo)     +   ' and ' +
                                                   ' fpc_con_ent_codigo     = ' + IntToStr(c_contratos.con_ent_codigo) +   ' and ' +
                                                   ' fpc_con_ent_emp_codigo = ' + IntToStr(c_contratos.con_ent_emp_codigo);
          CDSFormasPagamentosContratos.Filtered := True;

          CDSFormasPagamentosContratos.First;

          while (not CDSFormasPagamentosContratos.Eof) do begin
            c_formas_pagamentos.fpc_codigo             := CDSFormasPagamentosContratosfpc_codigo.AsInteger;
            c_formas_pagamentos.fpc_con_codigo         := CDSFormasPagamentosContratosfpc_con_codigo.AsInteger;
            c_formas_pagamentos.fpc_con_ent_codigo     := CDSFormasPagamentosContratosfpc_con_ent_codigo.AsInteger;
            c_formas_pagamentos.fpc_con_ent_emp_codigo := CDSFormasPagamentosContratosfpc_con_ent_emp_codigo.AsInteger;

            c_formas_pagamentos.proc_gravar(c_formas_pagamentos.fpc_codigo, c_formas_pagamentos.fpc_con_codigo, c_formas_pagamentos.fpc_con_ent_codigo, c_formas_pagamentos.fpc_con_ent_emp_codigo);

            CDSFormasPagamentosContratos.Next;
          end;
        finally
          CDSFormasPagamentosContratos.Filtered := False;
          CDSFormasPagamentosContratos.Filter   := '';
        end;


        CDSContratos.Next;
      end;

      btnGerarCR.Click;

      Conexao.FDConexao.Commit;
    except
      on E : Exception do begin
        Conexao.FDConexao.Rollback;
        Application.MessageBox(PChar(E.Message), PChar(Self.Caption), MB_OK + MB_ICONERROR);
        Exit;
      end;
    end;
  finally
    c_entidades.ent_observacoes.Free;
    FreeAndNil(c_entidades);
    FreeAndNil(c_enderecos);
    FreeAndNil(c_contatos);
    FreeAndNil(c_responsaveis);
    FreeAndNil(c_ent_tipos);
    FreeAndNil(c_funcionarios);
    FreeAndNil(c_contratos);
    FreeAndNil(c_formas_pagamentos);
  end;

  ed_consultaChange(Self);
  CDSEntidades.Locate('ent_codigo', ed_ent_codigo.Text, [loCaseInsensitive]);
  btnCancelar.Click;
end;

procedure TFrmCad_Entidades.btnGravarConClick(Sender: TObject);
var c_tabela : TTabelaCodigos;
begin
  inherited;
  c_tabela := TTabelaCodigos.Create;

  try
    CDSContatos.DisableControls;

    if v_operacao_contatos = statInserindo then begin
      CDSContatos.Insert;
      CDSContatoscon_codigo.AsInteger         := c_tabela.func_gerar_codigo('contatos', ed_ent_codigo.Text + '|' + IntToStr(TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo));
      CDSContatoscon_ent_codigo.AsInteger     := StrToInt(ed_ent_codigo.Text);
      CDSContatoscon_ent_emp_codigo.AsInteger := TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo;
    end else begin
      CDSContatos.Edit;
    end;

    CDSContatoscon_email.AsString      := ed_con_email.Text;
    CDSContatoscon_contato.AsString    := ed_con_contato.Text;
    CDSContatoscon_telefone.AsString   := ed_con_telefone.Text;
    CDSContatoscon_observacao.AsString := ed_con_observacao.Text;
    CDSContatos.Post;

    btnCancelarCon.Click;
  finally
    CDSContatos.EnableControls;
  end;
end;

procedure TFrmCad_Entidades.btnGravarContrClick(Sender: TObject);
var c_tabela : TTabelaCodigos;
    i_nro    : Integer;
begin
  inherited;
  if (StrToFloatDef(ed_con_valor.Text, 0) = 0) and (not ck_indicacao.Checked) then begin
    Application.MessageBox(PChar('Valor deve ser maior que 0 (zero).'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
    ed_con_valor.SetFOcus;
    ed_con_valor.SelectAll;
    Exit;
  end;

  c_tabela := TTabelaCodigos.Create;

  try
    CDSContratos.DisableControls;

    if v_operacao_contratos = statInserindo then begin
      if CDSContratos.State <> dsInsert then begin
        CDSContratos.Insert;
      end;

      CDSContratoscon_codigo.AsInteger         := c_tabela.func_gerar_codigo('contratos', ed_ent_codigo.Text + '|' + IntToStr(TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo));
      CDSContratoscon_ent_codigo.AsInteger     := StrToInt(ed_ent_codigo.Text);
      CDSContratoscon_ent_emp_codigo.AsInteger := TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo;
    end else begin
      CDSContratos.Edit;
    end;

    CDSContratoscon_tipo.AsInteger                       := cmb_con_tipo.ItemIndex;
    CDSContratoscon_pro_codigo.AsInteger                 := TProdutosCombo(cmb_con_pro_codigo.Items.Objects[cmb_con_pro_codigo.ItemIndex]).pro_codigo;
    CDSContratoscon_pro_emp_codigo.AsInteger             := TProdutosCombo(cmb_con_pro_codigo.Items.Objects[cmb_con_pro_codigo.ItemIndex]).pro_emp_codigo;
    CDSContratoscon_descricao.AsString                   := ed_con_descricao.Text;
    CDSContratoscon_ent_codigo_funcionario.AsInteger     := TEntidadeCombo(cmb_con_ent_codigo_funcionario.Items.Objects[cmb_con_ent_codigo_funcionario.ItemIndex]).ent_codigo;
    CDSContratoscon_ent_emp_codigo_funcionario.AsInteger := TEntidadeCombo(cmb_con_ent_codigo_funcionario.Items.Objects[cmb_con_ent_codigo_funcionario.ItemIndex]).ent_emp_codigo;
    CDSContratoscon_valor.AsFloat                        := StrToFloatDef(ed_con_valor.Text, 0);
    CDSContratoscon_vencimento_pagamento.AsInteger       := StrToIntDef(ed_con_vencimento_pagamento.Text, 0);
    CDSContratoscon_vencimento_contrato.AsDateTime       := ed_con_vencimento_contrato.Date;
    CDSContratoscon_horas_mes.AsFloat                    := StrToFloatDef(ed_con_horas_mes.Text, 0);
    CDSContratoscon_situacao.AsInteger                   := cmb_con_situacao.ItemIndex;
    CDSContratoscon_flag_cr.AsInteger                    := iif(b_novo_contrato, 0, CDSContratoscon_flag_cr.AsInteger);
    CDSContratos.Post;

    try
      CDSFormasPagamentosContratos.Filtered := False;
      CDSFormasPagamentosContratos.Filter   := ' fpc_con_codigo         = ' + CDSContratoscon_codigo.AsString     +   ' and ' +
                                               ' fpc_con_ent_codigo     = ' + CDSContratoscon_ent_codigo.AsString +   ' and ' +
                                               ' fpc_con_ent_emp_codigo = ' + CDSContratoscon_ent_emp_codigo.AsString;
      CDSFormasPagamentosContratos.Filtered := True;

      while (not CDSFormasPagamentosContratos.Eof) do begin
        CDSFormasPagamentosContratos.Delete;
      end;

      for i_nro := 0 to ckl_formas_pagamentos.Items.Count -1 do begin
        if ckl_formas_pagamentos.Checked[i_nro] then begin
          CDSFormasPagamentosContratos.Insert;

          CDSFormasPagamentosContratosfpc_codigo.AsInteger             := i_nro;
          CDSFormasPagamentosContratosfpc_con_codigo.AsInteger         := CDSContratoscon_codigo.AsInteger;
          CDSFormasPagamentosContratosfpc_con_ent_codigo.AsInteger     := CDSContratoscon_ent_codigo.AsInteger;
          CDSFormasPagamentosContratosfpc_con_ent_emp_codigo.AsInteger := CDSContratoscon_ent_emp_codigo.AsInteger;

          CDSFormasPagamentosContratos.Post;
        end;
      end;
    finally
      CDSFormasPagamentosContratos.Filtered := False;
      CDSFormasPagamentosContratos.Filter   := '';

      CDSFormasPagamentosContratos.First;
    end;

    btnCancelarContr.Click;
  finally
    CDSContratos.EnableControls;
    FreeAndNil(c_tabela);
  end;
end;

procedure TFrmCad_Entidades.btnGravarEndClick(Sender: TObject);
var c_tabela : TTabelaCodigos;
begin
  inherited;
  c_tabela := TTabelaCodigos.Create;

  try
    CDSEnderecos.DisableControls;

    if v_operacao_enderecos = statInserindo then begin
      CDSEnderecos.Insert;
      CDSEnderecosend_codigo.AsInteger         := c_tabela.func_gerar_codigo('enderecos', ed_ent_codigo.Text + '|' + IntToStr(TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo));
      CDSEnderecosend_ent_codigo.AsInteger     := StrToInt(ed_ent_codigo.Text);
      CDSEnderecosend_ent_emp_codigo.AsInteger := TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo;
    end else begin
      CDSEnderecos.Edit;
    end;

    CDSEnderecosend_tipo.AsInteger           := cmb_end_tipo.ItemIndex;
    CDSEnderecosend_cep.AsString             := ed_end_cep.Text;
    CDSEnderecosend_uf_sigla.AsString        := cmb_end_uf_sigla.Items.Strings[cmb_end_uf_sigla.ItemIndex];
    CDSEnderecosend_cid_codigo.AsInteger     := TCidadesCombo(cmb_end_cid_codigo.Items.Objects[cmb_end_cid_codigo.ItemIndex]).cid_codigo;
    CDSEnderecosend_logradouro.AsString      := ed_end_logradouro.Text;
    CDSEnderecosend_numero.AsInteger         := StrToIntDef(ed_end_numero.Text, 0);
    CDSEnderecosend_complemento.AsString     := ed_end_complemento.Text;
    CDSEnderecos.Post;

    btnCancelarEnd.Click;
  finally
    CDSEnderecos.EnableControls;
    FreeAndNil(c_tabela);
  end;
end;

procedure TFrmCad_Entidades.btnNovoClick(Sender: TObject);
var c_entidades         : TEntidades;
    c_tabela            : TTabelaCodigos;
    c_enderecos         : TEnderecos;
    c_contatos          : TContatos;
    c_funcionarios      : TFuncionarios;
    c_contratos         : TContratos;
    c_formas_pagamentos : TFormasPagamentosContratos;
begin
  inherited;
  try
    Conexao.FDConexao.StartTransaction;

    ed_ent_codigo.Text := c_tabela.func_gerar_codigo('entidades', IntToStr(TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo)).ToString;
    cmb_ent_emp_codigo.ItemIndex := cmb_ent_emp_codigo.Items.IndexOf(frmMenu.razao_empresa);

    Conexao.FDConexao.Commit;
  except
    on E : Exception do begin
      Conexao.FDConexao.Rollback;
      Application.MessageBox(PChar(E.Message), PChar(Self.Caption), MB_OK + MB_ICONERROR);
      Exit;
    end;
  end;

  rg_ent_tipo_pessoa.ItemIndex := 0;
  rg_ent_tipo_pessoaClick(Self);

  tsConsulta.TabVisible := False;
  tsCadastro.TabVisible := True;

  proc_limpa_campos;

  v_operacao := statInserindo;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSEntidades);

  c_enderecos := TEnderecos.Create;
  try
    c_enderecos.proc_consultar(FDQEnderecos, CDSEnderecos, StrToInt(ed_ent_codigo.Text), TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo);
  finally
    FreeAndNil(c_enderecos);
  end;

  c_contatos  := TContatos.Create;
  try
    c_contatos.proc_consultar(FDQContatos, CDSContatos, StrToInt(ed_ent_codigo.Text), TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo);
  finally
    FreeAndNil(c_contatos);
  end;

  c_contratos := TContratos.Create;
  try
    c_contratos.proc_consultar(FDQContratos, CDSContratos, 0, StrToInt(ed_ent_codigo.Text), TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo);
  finally
    FreeAndNil(c_contratos);
  end;

  try
    c_funcionarios := TFuncionarios.Create;
    c_funcionarios.proc_consultar(FDQFuncionarios, CDSFuncionarios, 0, 0);
  finally
    FreeAndNil(c_funcionarios);
  end;

  c_formas_pagamentos := TFormasPagamentosContratos.Create;
  try
    c_formas_pagamentos.proc_consultar(FDQFormasPagamentosContratos, CDSFormasPagamentosContratos, 0, 0, 0, 0);
  finally
    FreeAndNil(c_formas_pagamentos);
  end;

  proc_limpa_campos_endereco(False);
  proc_limpa_campos_contato(False);
  proc_limpa_campos_contratos(False);

  v_operacao_enderecos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesEndereco, v_operacao_enderecos, CDSEnderecos);

  v_operacao_contatos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesContatos, v_operacao_contatos, CDSContatos);

  v_operacao_contratos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesContratos, v_operacao_contratos, CDSContratos);

  cmb_ent_sit_codigo.ItemIndex := 0;

  PCAdicionais.ActivePageIndex := tsEnderecos.TabIndex;

  tsCadastro.TabVisible := True;
  tsConsulta.TabVisible := False;

  rg_ent_tipo_pessoa.SetFocus;
end;

procedure TFrmCad_Entidades.btnNovoConClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos_contato(True);

  v_operacao_contatos := statInserindo;
  proc_atualiza_operacao_query(pnlBotoesContatos, v_operacao_contatos, CDSContatos);

  DBGContatos.Enabled := False;

  ed_con_contato.SetFocus;
end;

procedure TFrmCad_Entidades.btnNovoContrClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos_contratos(True);

  b_novo_contrato := True;

  v_operacao_contratos := statInserindo;
  proc_atualiza_operacao_query(pnlBotoesContratos, v_operacao_contratos, CDSContratos);

  DBGContratos.Enabled := False;

  cmb_con_pro_codigo.SetFocus;
end;

procedure TFrmCad_Entidades.btnNovoEndClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos_endereco(True);

  v_operacao_enderecos := statInserindo;
  proc_atualiza_operacao_query(pnlBotoesEndereco, v_operacao_enderecos, CDSEnderecos);

  DBGEnderecos.Enabled := False;

  cmb_end_tipo.SetFocus;
end;

procedure TFrmCad_Entidades.btnOpcoesClick(Sender: TObject);
var vPonto : TPoint;
begin
  inherited;
  vPonto := btnOpcoes.ClientToScreen(Point(0, btnOpcoes.Height));
  PopOpcoes.Popup(vPonto.X, vPonto.Y);
end;

procedure TFrmCad_Entidades.btnRenovarClick(Sender: TObject);
var c_produtos     : TProdutos;
    c_funcionarios : TEntidades;

    i_nro : Integer;
begin
  inherited;
  proc_limpa_campos_contratos(True);

  b_novo_contrato := True;

  try
    c_produtos := TProdutos.Create;
    cmb_con_pro_codigo.ItemIndex := cmb_con_pro_codigo.Items.IndexOf(c_produtos.func_retorna_nome_combo(CDSContratoscon_pro_codigo.AsInteger, CDSContratoscon_pro_emp_codigo.AsInteger));
  finally
    FreeAndNil(c_produtos);
  end;

  try
    c_funcionarios := TEntidades.Create;
    cmb_con_ent_codigo_funcionario.ItemIndex := cmb_con_ent_codigo_funcionario.Items.IndexOf(c_funcionarios.func_retorna_nome_combo(CDSContratoscon_ent_codigo_funcionario.AsInteger, CDSContratoscon_ent_emp_codigo_funcionario.AsInteger));
  finally
    FreeAndNil(c_funcionarios);
  end;

  ed_con_descricao.Text            := CDSContratoscon_descricao.AsString;
  ed_con_vencimento_pagamento.Text := CDSContratoscon_vencimento_pagamento.AsString;
  cmb_con_tipo.ItemIndex           := CDSContratoscon_tipo.AsInteger;
  ed_con_horas_mes.Text            := CDSContratoscon_horas_mes.AsString;
  ed_con_vencimento_contrato.Date  := CDSContratoscon_vencimento_contrato.AsDateTime;
  cmb_con_situacao.ItemIndex       := CDSContratoscon_situacao.AsInteger;
  ed_con_valor.Text                := CDSContratoscon_valor.AsString;

  try
    CDSFormasPagamentosContratos.Filtered := False;
    CDSFormasPagamentosContratos.Filter   := ' fpc_con_codigo         = ' + CDSContratoscon_codigo.AsString     +   ' and ' +
                                             ' fpc_con_ent_codigo     = ' + CDSContratoscon_ent_codigo.AsString +   ' and ' +
                                             ' fpc_con_ent_emp_codigo = ' + CDSContratoscon_ent_emp_codigo.AsString;
    CDSFormasPagamentosContratos.Filtered := True;
    CDSFormasPagamentosContratos.First;

    while not CDSFormasPagamentosContratos.Eof do begin
      ckl_formas_pagamentos.Checked[CDSFormasPagamentosContratosfpc_codigo.AsInteger] := True;

      CDSFormasPagamentosContratos.Next;
    end;
  finally
    CDSFormasPagamentosContratos.Filtered := False;
    CDSFormasPagamentosContratos.Filter   := '';

    CDSFormasPagamentosContratos.First;
  end;

  v_operacao_contratos := statInserindo;
  proc_atualiza_operacao_query(pnlBotoesContratos, v_operacao_contratos, CDSContratos);

  DBGContratos.Enabled := False;

  cmb_con_pro_codigo.SetFocus;
end;

procedure TFrmCad_Entidades.btnTransferirHorasClick(Sender: TObject);
var c_entidades : TEntidades;
begin
  inherited;
  try
    c_entidades := TEntidades.Create;
    c_entidades.proc_carregar_combo(cmbDestinoHoras, 0);
  finally
    FreeAndNil(c_entidades);
  end;

  pnlTransferir.Left    := 0;
  pnlTransferir.Top     := 0;
  pnlTransferir.Visible := True;
end;

procedure TFrmCad_Entidades.CDSContratosAfterScroll(DataSet: TDataSet);
begin
  inherited;
  btnRenovar.Enabled := (not CDSContratos.Eof);
end;

procedure TFrmCad_Entidades.CDSContratosCalcFields(DataSet: TDataSet);
var c_produtos  : TProdutos;
    c_entidades : TEntidades;
begin
  inherited;
  c_produtos  := TProdutos.Create;
  c_entidades := TEntidades.Create;
  try
    c_produtos.func_recuperar(CDSContratoscon_pro_codigo.AsInteger, CDSContratoscon_pro_emp_codigo.AsInteger);
    c_entidades.func_recuperar(CDSContatoscon_ent_codigo.AsInteger, CDSContatoscon_ent_emp_codigo.AsInteger);

    CDSContratosc_pro_descricao.AsString    := c_produtos.pro_descricao;
    CDSContratosc_ent_razao_social.AsString := c_entidades.ent_razao_social;
  finally
    FreeAndNil(c_produtos);
    FreeAndNil(c_entidades);
  end;

  case CDSContratoscon_tipo.AsInteger of
    0 : CDSContratosc_con_tipo.AsString := 'HORAL';
    1 : CDSContratosc_con_tipo.AsString := 'SEMANAL';
    2 : CDSContratosc_con_tipo.AsString := 'MENSAL';
    3 : CDSContratosc_con_tipo.AsString := 'QUINZENAL';
  end;
end;

procedure TFrmCad_Entidades.CDSEnderecosCalcFields(DataSet: TDataSet);
var c_cidades : TCidades;
begin
  inherited;
  case CDSEnderecosend_tipo.AsInteger of
    0 : CDSEnderecosend_descricao_tipo.AsString := 'PRINCIPAL';
    1 : CDSEnderecosend_descricao_tipo.AsString := 'ENTREGA';
    2 : CDSEnderecosend_descricao_tipo.AsString := 'COBRAN�A';
    3 : CDSEnderecosend_descricao_tipo.AsString := 'OUTROS';
  end;

  try
    c_cidades := TCidades.Create;
    if c_cidades.func_recuperar(CDSEnderecosend_cid_codigo.AsInteger) then begin
      CDSEnderecoscid_nome.AsString := c_cidades.cid_nome;
    end;
  finally
    FreeAndNil(c_cidades);
  end;
end;

procedure TFrmCad_Entidades.CDSFuncionariosBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (CDSFuncionarios.State = dsEdit) or (CDSFuncionarios.State = dsInsert) then begin
    CDSFuncionariosfun_ent_codigo.AsInteger     := StrToInt(ed_ent_codigo.Text);
    CDSFuncionariosfun_ent_emp_codigo.AsInteger := TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo;
  end;
end;

procedure TFrmCad_Entidades.ckl_ent_tipoClick(Sender: TObject);
begin
  inherited;
  ckl_ent_tipo.Checked[0] := True;

  tsFuncionarios.TabVisible := False;
  if ckl_ent_tipo.Checked[2] then begin
    tsFuncionarios.TabVisible := True;
  end;
end;

procedure TFrmCad_Entidades.cmb_end_uf_siglaChange(Sender: TObject);
var c_cidades : TCidades;
begin
  inherited;
  if cmb_end_uf_sigla.ItemIndex >= 0 then begin
    try
      c_cidades := TCidades.Create;
      c_cidades.proc_carregar_combo(cmb_end_cid_codigo, cmb_end_uf_sigla.Items.Strings[cmb_end_uf_sigla.ItemIndex]);
    finally
      FreeAndNil(c_cidades);
    end;
  end;
end;

procedure TFrmCad_Entidades.DBGContatosDblClick(Sender: TObject);
begin
  inherited;
  btnEditarCon.Click;
end;

procedure TFrmCad_Entidades.DBGContatosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if (not Odd(CDSContatos.RecNo)) then begin
    DBGContatos.Canvas.Brush.Color := clSilver;
  end else begin
    DBGContatos.Canvas.Brush.Color := clWindow;
  end;

  if (gdSelected in State) then begin
    DBGContatos.Canvas.Brush.Color := $00FFA74F;
  end;

  DBGContatos.Canvas.FillRect(Rect);
  DBGContatos.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TFrmCad_Entidades.DBGEnderecosDblClick(Sender: TObject);
begin
  inherited;
  btnEditarEnd.Click;
end;

procedure TFrmCad_Entidades.DBGEnderecosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if (not Odd(CDSEnderecos.RecNo)) then begin
    DBGEnderecos.Canvas.Brush.Color := clSilver;
  end else begin
    DBGEnderecos.Canvas.Brush.Color := clWindow;
  end;

  if (gdSelected in State) then begin
    DBGEnderecos.Canvas.Brush.Color := $00FFA74F;
  end;

  DBGEnderecos.Canvas.FillRect(Rect);
  DBGEnderecos.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TFrmCad_Entidades.dbgEntidadesDblClick(Sender: TObject);
begin
  inherited;
  btnEditar.Click;
end;

procedure TFrmCad_Entidades.dbgEntidadesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if (not Odd(CDSEntidades.RecNo)) then begin
    dbgEntidades.Canvas.Brush.Color := clSilver;
  end else begin
    dbgEntidades.Canvas.Brush.Color := clWindow;
  end;

  if (gdSelected in State) then begin
    dbgEntidades.Canvas.Brush.Color := $00FFA74F;
  end;

  dbgEntidades.Canvas.FillRect(Rect);
  dbgEntidades.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TFrmCad_Entidades.ed_consultaChange(Sender: TObject);
var c_entidades : TEntidades;
begin
  inherited;
  try
    c_entidades := TEntidades.Create;
    c_entidades.proc_consultar(FDQEntidades, CDSEntidades, cmbCampos, cmbConStatus, ed_consulta.Text);
  finally
    FreeAndNil(c_entidades);
  end;
end;

procedure TFrmCad_Entidades.ed_con_horas_mesKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if ((not (Key in ['0'..'9'])) and (not (Key in [','])) and (Word(Key) <> VK_BACK)) or ((Key in [',']) and (Pos(',', TEdit(Sender).Text) > 0)) then begin
    Key := #0;
  end else begin
    if (Key in [',']) then begin
      if Length(TEdit(Sender).Text) = 0 then begin
        TEdit(Sender).Text := '0,';
        TEdit(Sender).SelStart := Length(TEdit(Sender).Text);
        Abort;
      end;
    end;
  end;
end;

procedure TFrmCad_Entidades.ed_con_valorKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if ((not (Key in ['0'..'9'])) and (not (Key in [','])) and (Word(Key) <> VK_BACK)) or ((Key in [',']) and (Pos(',', TEdit(Sender).Text) > 0)) then begin
    Key := #0;
  end else begin
    if (Key in [',']) then begin
      if Length(TEdit(Sender).Text) = 0 then begin
        TEdit(Sender).Text := '0,';
        TEdit(Sender).SelStart := Length(TEdit(Sender).Text);
        Abort;
      end;
    end;
  end;
end;

procedure TFrmCad_Entidades.ed_ent_cpfExit(Sender: TObject);
begin
  inherited;
  if func_limpa_mascara(ed_ent_cpf.Text) <> '' then begin
    if (Length(func_limpa_mascara(ed_ent_cpf.Text)) < 11) or (not func_validaCPF(func_limpa_mascara(ed_ent_cpf.Text))) then begin
      Application.MessageBox(PChar('CPF informado � inv�lido, favor informar CPF v�lido.'), PChar(Self.Caption), MB_OK + MB_ICONWARNING);
      ed_ent_cpf.SetFocus;
      ed_ent_cpf.SelectAll;
      Exit;
    end;
  end;
end;

procedure TFrmCad_Entidades.ed_ent_data_nascimentoExit(Sender: TObject);
begin
  inherited;
  tsResponsaveis.TabVisible := False;
  if ed_ent_data_nascimento.Date > (Date - 6570) then begin
    tsResponsaveis.TabVisible := True;
  end;
end;

procedure TFrmCad_Entidades.ed_ent_observacoesEnter(Sender: TObject);
begin
  inherited;
  Application.OnMessage := nil;
end;

procedure TFrmCad_Entidades.ed_ent_observacoesExit(Sender: TObject);
begin
  inherited;
  Application.OnMessage := ProcessKey;
end;

procedure TFrmCad_Entidades.ed_fun_salarioExit(Sender: TObject);
var d_valor : Double;
begin
  inherited;
  if Trim(ed_fun_salario.Text) <> ',' then begin
    try
      d_valor := iif((Trim(ed_fun_salario.Text) <> ','), StrToFloat(Trim(ed_fun_salario.Text)), 0);
    except
      on E : Exception do begin
        Application.MessageBox(PChar('Valor informado � inv�lido.'), PChar(Self.Caption), MB_OK + MB_ICONWARNING);
        ed_fun_salario.SetFocus;
        ed_fun_salario.SelectAll;
        Exit;
      end;
    end;
  end;
end;

procedure TFrmCad_Entidades.ed_res_cpfExit(Sender: TObject);
begin
  inherited;
  if func_limpa_mascara(ed_res_cpf.Text) <> '' then begin
    if (Length(func_limpa_mascara(ed_res_cpf.Text)) < 11) or (not func_validaCPF(func_limpa_mascara(ed_res_cpf.Text))) then begin
      Application.MessageBox(PChar('CPF informado � inv�lido, favor informar CPF v�lido.'), PChar(Self.Caption), MB_OK + MB_ICONWARNING);
      ed_res_cpf.SetFocus;
      ed_res_cpf.SelectAll;
      Exit;
    end;
  end;
end;

procedure TFrmCad_Entidades.FormActivate(Sender: TObject);
var c_status    : TSituacoesEntidade;
    c_empresas  : TEmpresas;
    c_estados   : TEstados;
    c_entidades : TEntidades;
    c_produtos  : TProdutos;
begin
  inherited;
  try
    c_status    := TSituacoesEntidade.Create;
    c_empresas  := TEmpresas.Create;
    c_estados   := TEstados.Create;
    c_entidades := TEntidades.Create;
    c_produtos  := TProdutos.Create;

    c_estados.proc_carregar_combo(cmb_end_uf_sigla);
    c_status.proc_carregar_combobox(cmbConStatus);
    c_status.proc_carregar_combobox(cmb_ent_sit_codigo, 1);
    c_empresas.proc_carregar_combo(cmb_ent_emp_codigo);
    c_entidades.proc_carregar_combo(cmb_con_ent_codigo_funcionario);
    c_produtos.proc_carregar_combo(cmb_con_pro_codigo);

    cmbConStatus.ItemIndex := cmbConStatus.Items.Count - 1;
  finally
    FreeAndNil(c_status);
    FreeAndNil(c_empresas);
    FreeAndNil(c_estados);
    FreeAndNil(c_entidades);
    FreeAndNil(c_produtos);
  end;

  ed_consulta.Clear;

  btnCancelar.Click;
end;

procedure TFrmCad_Entidades.FormCreate(Sender: TObject);
begin
  inherited;
  tsCadastro.TabVisible := False;
  tsConsulta.TabVisible := True;
end;

procedure TFrmCad_Entidades.FormDestroy(Sender: TObject);
begin
  inherited;
  proc_limpa_combo(cmbConStatus);
  proc_limpa_combo(cmb_ent_emp_codigo);
  proc_limpa_combo(cmb_end_uf_sigla);
  proc_limpa_combo(cmb_end_cid_codigo);
  proc_limpa_combo(cmb_ent_sit_codigo);
  proc_limpa_combo(cmb_con_ent_codigo_funcionario);
  proc_limpa_combo(cmb_con_pro_codigo);
end;

procedure TFrmCad_Entidades.ImprimirSaldoemContratosxSaldoemAulas1Click(
  Sender: TObject);
var s_caminho : String;
begin
  inherited;
  s_caminho := ExtractFilePath(Application.ExeName) + 'fast\RelSaldoContXSaldoAulas.fr3';

  FDQSaldoContratos.Connection := Conexao.FDConexao;
  FDQSaldoAulasCont.Connection := Conexao.FDConexao;

  FDQSaldoContratos.Close;
  FDQSaldoContratos.SQL.Clear;
  FDQSaldoContratos.SQL.Add(' select c.con_descricao descricao, c.con_valor valor,                    '+
                            '        date_format(c.con_vencimento_contrato, ''%d/%m/%Y'') vencimento, '+
                            '        c.con_horas_mes horas, e.ent_razao_social                        '+
                            ' from contratos c                                                        '+
                            ' inner join entidades e on e.ent_codigo     = c.con_ent_codigo and       '+
                            '                           e.ent_emp_codigo = c.con_ent_emp_codigo       '+
                            ' where c.con_ent_codigo = :p_con_ent_codigo                              '+
                            ' order by c.con_vencimento_contrato                                      ');
  FDQSaldoContratos.ParamByName('p_con_ent_codigo').AsInteger := CDSEntidadesent_codigo.AsInteger;
  FDQSaldoContratos.Open;

  FDQSaldoAulasCont.Close;
  FDQSaldoAulasCont.SQL.Clear;
  FDQSaldoAulasCont.SQL.Add(' select e.ent_razao_social professor, date_format(a.agd_data, ''%d/%m/%Y'') dia, '+
                            '        a.agd_qtde_horas horas                                                   '+
                            ' from agendamentos_entidade ae                                                   '+
                            ' inner join agendamentos a on a.agd_codigo     = ae.agd_codigo and               '+
                            '                a.agd_emp_codigo = ae.agd_emp_codigo                             '+
                            ' inner join entidades e on e.ent_codigo     = a.agd_fun_ent_codigo and           '+
                            '                           e.ent_emp_codigo = a.agd_fun_ent_emp_codigo           '+
                            ' where ae.ent_codigo = :p_ent_codigo                                             '+
                            ' order by e.ent_razao_social, a.agd_data                                         ');
  FDQSaldoAulasCont.ParamByName('p_ent_codigo').AsInteger := CDSEntidadesent_codigo.AsInteger;
  FDQSaldoAulasCont.Open;

  Relatorio.LoadFromFile(s_caminho);
  Relatorio.ShowReport;
end;

procedure TFrmCad_Entidades.meImprimirSaldoDeAulasClick(Sender: TObject);
var s_caminho : String;
begin
  inherited;
  s_caminho := ExtractFilePath(Application.ExeName) + 'fast\RelSaldoAulas.fr3';

  FDQSaldoAulas.Connection := Conexao.FDConexao;

  FDQSaldoAulas.Close;
  FDQSaldoAulas.SQL.Clear;
  FDQSaldoAulas.SQL.Add(' select x.ent_codigo, x.ent_emp_codigo, x.ent_razao_social,                                 '+
                        '        x.agd_qtde_horas, x.con_horas_mes,                                                  '+
                        '        (x.con_horas_mes - x.agd_qtde_horas) horas_faltantes,                               '+
                        '        x.sit_descricao                                                                     '+
                        ' from (  select e.ent_codigo, e.ent_emp_codigo, e.ent_razao_social,                         '+
                        '                se.sit_descricao,                                                           '+
                        '                sum(a.agd_qtde_horas) agd_qtde_horas,                                       '+
                        '                (select sum(c.con_horas_mes)                                                '+
                        '                 from contratos c                                                           '+
                        '                 where c.con_ent_codigo     = ae.ent_codigo                                 '+
                        '                   and c.con_ent_emp_codigo = ae.ent_emp_codigo                             '+
                        '                   and c.con_situacao       > 0) con_horas_mes                              '+
                        '         from agendamentos a                                                                '+
                        '         inner join agendamentos_entidade ae on ae.agd_codigo     = a.agd_codigo and        '+
                        '                                                ae.agd_emp_codigo = a.agd_emp_codigo        '+
                        '         inner join entidades e on e.ent_codigo     = ae.ent_codigo and                     '+
                        '                                   e.ent_emp_codigo = ae.ent_emp_codigo                     '+
                        '         inner join situacoes_entidade se on se.sit_codigo = e.ent_sit_codigo               '+
                        '         where a.agd_data < sysdate()                                                       '+
                        '         group by e.ent_codigo, e.ent_emp_codigo, e.ent_razao_social, se.sit_descricao      '+
                        '         order by e.ent_razao_social ) x                                                    '+
                        ' where x.sit_descricao <> ''INATIVO''                                                       ');
  FDQSaldoAulas.Open;

  Relatorio.LoadFromFile(s_caminho);
  Relatorio.ShowReport;
end;

procedure TFrmCad_Entidades.proc_limpa_campos;
var i_nro : Integer;
begin
  ed_ent_data_nascimento.Date := Date - 2555;
  rg_ent_tipo_pessoa.ItemIndex := 0;

  ed_ent_razao_social_f.Clear;
  ed_ent_rg.Clear;
  ed_ent_cpf.Clear;
  ed_ent_ocupacao.Clear;
  ed_ent_razao_social_j.Clear;
  ed_ent_nome_fantasia.Clear;
  ed_ent_cnpj.Clear;
  ed_ent_ie.Clear;
  ed_ent_im.Clear;
  ed_res_nome.Clear;
  ed_res_rg.Clear;
  ed_res_cpf.Clear;
  ed_res_data_nascimento.Date := Date - 6570;

  ckl_ent_tipo.Checked[0] := True;
  for i_nro := 1 to ckl_ent_tipo.Items.Count -1 do begin
    ckl_ent_tipo.Checked[i_nro] := False;
  end;

  ed_fun_ct.Clear;
  ed_fun_pis.Clear;
  ed_fun_funcao.Clear;
  cmb_fun_modo.ItemIndex := -1;
  cmb_fun_situacao.ItemIndex := 0;

  tsResponsaveis.TabVisible := False;
  tsFuncionarios.TabVisible := False;

  cdsExclusaoContrato.Close;
  cdsExclusaoContrato.CreateDataSet;
end;

procedure TFrmCad_Entidades.proc_limpa_campos(PTipoPessoa: Integer);
begin
  ed_ent_razao_social_f.Clear;
  ed_ent_rg.Clear;
  ed_ent_cpf.Clear;
  ed_ent_ocupacao.Clear;
  ed_ent_razao_social_j.Clear;
  ed_ent_nome_fantasia.Clear;
  ed_ent_cnpj.Clear;
  ed_ent_ie.Clear;
  ed_ent_im.Clear;
end;

procedure TFrmCad_Entidades.proc_limpa_campos_contato(PStatus: Boolean);
begin
  lbl_con_email.Enabled      := PStatus;
  lbl_con_contato.Enabled    := PStatus;
  lbl_con_telefone.Enabled   := PStatus;
  lbl_con_observacao.Enabled := PStatus;
  ed_con_email.Enabled       := PStatus;
  ed_con_contato.Enabled     := PStatus;
  ed_con_telefone.Enabled    := PStatus;
  ed_con_observacao.Enabled  := PStatus;

  ed_con_email.Clear;
  ed_con_contato.Clear;
  ed_con_telefone.Clear;
  ed_con_observacao.Clear;
end;

procedure TFrmCad_Entidades.proc_limpa_campos_contratos(PStatus: Boolean);
begin
  cmb_con_pro_codigo.Enabled             := PStatus;
  cmb_con_ent_codigo_funcionario.Enabled := PStatus;
  ed_con_descricao.Enabled               := PStatus;
  ed_con_valor.Enabled                   := PStatus;
  ed_con_vencimento_pagamento.Enabled    := PStatus;
  ed_con_vencimento_contrato.Enabled     := PStatus;
  cmb_con_tipo.Enabled                   := PStatus;
  ed_con_horas_mes.Enabled               := PStatus;
  ckl_formas_pagamentos.Enabled          := PStatus;
//  cmb_con_situacao.Enabled               := PStatus;

  lbl_con_pro_codigo.Enabled             := PStatus;
  lbl_con_ent_codigo_funcionario.Enabled := PStatus;
  lbl_con_descricao.Enabled              := PStatus;
  lbl_con_valor.Enabled                  := PStatus;
  lbl_con_vencimento_pagamento.Enabled   := PStatus;
  lbl_con_vencimento_contrato.Enabled    := PStatus;
  lbl_con_tipo.Enabled                   := PStatus;
  lbl_con_horas_mes.Enabled              := PStatus;
  lbl_con_situacao.Enabled               := PStatus;
  gb_formas_pagamentos.Enabled           := PStatus;

  cmb_con_pro_codigo.ItemIndex             := -1;
  cmb_con_ent_codigo_funcionario.ItemIndex := -1;
  ed_con_descricao.Clear;
  ed_con_valor.Clear;
  ed_con_vencimento_pagamento.Clear;
  ed_con_vencimento_contrato.Date := Date;
  cmb_con_tipo.ItemIndex := 0;
  ed_con_horas_mes.Clear;
  ckl_formas_pagamentos.CheckAll(cbUnchecked);
  cmb_con_situacao.ItemIndex := 0;
  ck_indicacao.Checked := False;
end;

procedure TFrmCad_Entidades.proc_limpa_campos_endereco(PStatus : Boolean);
begin
  lbl_end_tipo.Enabled        := PStatus;
  lbl_end_cep.Enabled         := PStatus;
  lbl_end_uf_sigla.Enabled    := PStatus;
  lbl_end_cid_codigo.Enabled  := PStatus;
  lbl_end_logradouro.Enabled  := PStatus;
  lbl_end_numero.Enabled      := PStatus;
  lbl_end_complemento.Enabled := PStatus;
  cmb_end_tipo.Enabled        := PStatus;
  ed_end_cep.Enabled          := PStatus;
  cmb_end_uf_sigla.Enabled    := PStatus;
  cmb_end_cid_codigo.Enabled  := PStatus;
  ed_end_logradouro.Enabled   := PStatus;
  ed_end_numero.Enabled       := PStatus;
  ed_end_complemento.Enabled  := PStatus;
  btnAddEnd.Enabled           := PStatus;

  cmb_end_tipo.ItemIndex := 0;
  ed_end_cep.Clear;
  cmb_end_uf_sigla.ItemIndex   := -1;
  cmb_end_cid_codigo.ItemIndex := -1;
  proc_limpa_combo(cmb_end_cid_codigo);
  ed_end_logradouro.Clear;
  ed_end_numero.Clear;
  ed_end_complemento.Clear;
end;

procedure TFrmCad_Entidades.rg_ent_tipo_pessoaClick(Sender: TObject);
begin
  inherited;
  case rg_ent_tipo_pessoa.ItemIndex of
    0 : begin
          tsFisica.TabVisible   := True;
          tsJuridica.TabVisible := False;
        end;

    1 : begin
          tsFisica.TabVisible   := False;
          tsJuridica.TabVisible := True;
        end;
  end;
end;

end.
