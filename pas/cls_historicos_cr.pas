unit cls_historicos_cr;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Classes, DB;

type THistoricosCR = Class(TObject)
  private
    { Private declarations }
    f_hist_cr_codigo     : Integer;
    f_hist_cr_emp_codigo : Integer;
    f_hist_cr_sequencia  : Integer;
    f_hist_cr_forma      : Integer;
    f_hist_cr_valor      : Double;
    f_hist_nsu           : String;
    f_hist_cartao        : String;
    f_hist_data_pgto     : TDateTime;
  public
    { Public declarations }
    procedure proc_gravar(PCodigo, PEmpresa, PSequencia : Integer);
    function func_recuperar(PCodigo, PEmpresa, PSequencia : Integer) : Boolean;
  published
    { Published declarations }
    property hist_cr_codigo     : Integer   read f_hist_cr_codigo     write f_hist_cr_codigo;
    property hist_cr_emp_codigo : Integer   read f_hist_cr_emp_codigo write f_hist_cr_emp_codigo;
    property hist_cr_sequencia  : Integer   read f_hist_cr_sequencia  write f_hist_cr_sequencia;
    property hist_cr_forma      : Integer   read f_hist_cr_forma      write f_hist_cr_forma;
    property hist_cr_valor      : Double    read f_hist_cr_valor      write f_hist_cr_valor;
    property hist_nsu           : String    read f_hist_nsu           write f_hist_nsu;
    property hist_cartao        : String    read f_hist_cartao        write f_hist_cartao;
    property hist_data_pgto     : TDateTime read f_hist_data_pgto     write f_hist_data_pgto;
  end;

implementation

{ THistoricosCR }

function THistoricosCR.func_recuperar(PCodigo, PEmpresa,
  PSequencia: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from historicos_cr h                               '+
                   ' where h.hist_cr_codigo     = :p_hist_cr_codigo     '+
                   '   and h.hist_cr_emp_codigo = :p_hist_cr_emp_codigo '+
                   '   and h.hist_cr_sequencia  = :p_hist_cr_sequencia  ');
    qryAux.ParamByName('p_hist_cr_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_hist_cr_emp_codigo').AsInteger := PEmpresa;
    qryAux.ParamByName('p_hist_cr_sequencia').AsInteger  := PSequencia;
    qryAux.Open;

    Result := (not qryAux.Eof);

    hist_cr_codigo     := qryAux.FieldByName('hist_cr_codigo').AsInteger;
    hist_cr_emp_codigo := qryAux.FieldByName('hist_cr_emp_codigo').AsInteger;
    hist_cr_sequencia  := qryAux.FieldByName('hist_cr_sequencia').AsInteger;
    hist_cr_forma      := qryAux.FieldByName('hist_cr_forma').AsInteger;
    hist_cr_valor      := qryAux.FieldByName('hist_cr_valor').AsFloat;
    hist_nsu           := qryAux.FieldByName('hist_nsu').AsString;
    hist_cartao        := qryAux.FieldByName('hist_cartao').AsString;
    hist_data_pgto     := qryAux.FieldByName('hist_data_pgto').AsDateTime;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure THistoricosCR.proc_gravar(PCodigo, PEmpresa, PSequencia: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from historicos_cr h                               '+
                   ' where h.hist_cr_codigo     = :p_hist_cr_codigo     '+
                   '   and h.hist_cr_emp_codigo = :p_hist_cr_emp_codigo '+
                   '   and h.hist_cr_sequencia  = :p_hist_cr_sequencia  ');
    qryAux.ParamByName('p_hist_cr_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_hist_cr_emp_codigo').AsInteger := PEmpresa;
    qryAux.ParamByName('p_hist_cr_sequencia').AsInteger  := PSequencia;
    qryAux.Open;

    if qryAux.Eof then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into historicos_cr( '+
                     '   hist_cr_codigo    ,      '+
                     '   hist_cr_emp_codigo,      '+
                     '   hist_cr_sequencia ,      '+
                     '   hist_cr_forma     ,      '+
                     '   hist_cr_valor     ,      '+
                     '   hist_nsu          ,      '+
                     '   hist_cartao       ,      '+
                     '   hist_data_pgto           '+
                     ' )values(                   '+
                     '   :p_hist_cr_codigo    ,   '+
                     '   :p_hist_cr_emp_codigo,   '+
                     '   :p_hist_cr_sequencia ,   '+
                     '   :p_hist_cr_forma     ,   '+
                     '   :p_hist_cr_valor     ,   '+
                     '   :p_hist_nsu          ,   '+
                     '   :p_hist_cartao       ,   '+
                     '   :p_hist_data_pgto    )   ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update historicos_cr                               '+
                     ' set hist_cr_forma  = :p_hist_cr_forma ,            '+
                     '     hist_cr_valor  = :p_hist_cr_valor ,            '+
                     '     hist_nsu       = :p_hist_nsu      ,            '+
                     '     hist_cartao    = :p_hist_cartao   ,            '+
                     '     hist_data_pgto = :p_hist_data_pgto             '+
                     ' where h.hist_cr_codigo     = :p_hist_cr_codigo     '+
                     '   and h.hist_cr_emp_codigo = :p_hist_cr_emp_codigo '+
                     '   and h.hist_cr_sequencia  = :p_hist_cr_sequencia  ');
    end;

    qryAux.ParamByName('p_hist_cr_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_hist_cr_emp_codigo').AsInteger := PEmpresa;
    qryAux.ParamByName('p_hist_cr_sequencia').AsInteger  := PSequencia;
    qryAux.ParamByName('p_hist_cr_forma').AsInteger      := hist_cr_forma;
    qryAux.ParamByName('p_hist_cr_valor').AsFloat        := hist_cr_valor;
    qryAux.ParamByName('p_hist_nsu').AsString            := hist_nsu;
    qryAux.ParamByName('p_hist_cartao').AsString         := hist_cartao;
    qryAux.ParamByName('p_hist_data_pgto').AsDateTime    := hist_data_pgto;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
