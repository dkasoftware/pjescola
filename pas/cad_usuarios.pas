unit cad_usuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ExtCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ComCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  Datasnap.Provider, Datasnap.DBClient, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, cls_utils;

type
  TFrmCad_Usuarios = class(TFrmAux_Padrao)
    PC: TPageControl;
    tsConsulta: TTabSheet;
    tsCadastro: TTabSheet;
    pnlConsulta: TPanel;
    Panel1: TPanel;
    cmbCampos: TComboBox;
    Label1: TLabel;
    dbgUsuarios: TDBGrid;
    FDQUsuarios: TFDQuery;
    CDSUsuarios: TClientDataSet;
    PROUsuarios: TDataSetProvider;
    DSUsuarios: TDataSource;
    FDQUsuariosusu_codigo: TIntegerField;
    FDQUsuariosusu_nome: TStringField;
    FDQUsuariosusu_login: TStringField;
    FDQUsuariosusu_senha: TStringField;
    FDQUsuariosusu_nivel: TIntegerField;
    FDQUsuariosusu_status: TIntegerField;
    CDSUsuariosusu_codigo: TIntegerField;
    CDSUsuariosusu_nome: TStringField;
    CDSUsuariosusu_login: TStringField;
    CDSUsuariosusu_senha: TStringField;
    CDSUsuariosusu_nivel: TIntegerField;
    CDSUsuariosusu_status: TIntegerField;
    ed_consulta: TEdit;
    cmbConStatus: TComboBox;
    Label2: TLabel;
    pnlCadastro: TPanel;
    gbCodigo: TGroupBox;
    gbInformacoes: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    cmb_usu_nivel: TComboBox;
    ed_usu_nome: TEdit;
    ed_usu_codigo: TEdit;
    gbSenha: TGroupBox;
    Label5: TLabel;
    ed_usu_login: TEdit;
    Label6: TLabel;
    ed_usu_senha: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    ed_repetir_senha: TEdit;
    rg_usu_status: TRadioGroup;
    Label10: TLabel;
    ed_nova_senha: TEdit;
    btnRestarSenha: TBitBtn;
    procedure ed_consultaChange(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure ed_usu_senhaKeyPress(Sender: TObject; var Key: Char);
    procedure ed_repetir_senhaExit(Sender: TObject);
  private
    { Private declarations }
    v_operacao : TOperacaoQuery;
    procedure proc_limpa_campos;
  public
    { Public declarations }
  end;

var
  FrmCad_Usuarios: TFrmCad_Usuarios;

implementation

{$R *.dfm}

uses dm_conexao, cls_usuarios;

procedure TFrmCad_Usuarios.btnCancelarClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos;

  tsCadastro.TabVisible := False;
  tsConsulta.TabVisible := True;

  ed_consulta.Clear;
  ed_consulta.SetFocus;

  proc_atualiza_operacao_query(TForm(Self), statNavegando, CDSUsuarios);
end;

procedure TFrmCad_Usuarios.btnEditarClick(Sender: TObject);
begin
  inherited;
  tsCadastro.TabVisible := True;
  tsConsulta.TabVisible := False;

  ed_usu_codigo.Text      := CDSUsuariosusu_codigo.AsString;
  ed_usu_nome.Text        := CDSUsuariosusu_nome.AsString;
  ed_usu_login.Text       := CDSUsuariosusu_login.AsString;
  ed_usu_senha.Text       := '';//func_descriptografar(CDSUsuariosusu_senha.AsString, K_KEY_ENCRYPTION);
  cmb_usu_nivel.ItemIndex := CDSUsuariosusu_nivel.AsInteger;
  rg_usu_status.ItemIndex := CDSUsuariosusu_status.AsInteger;

  ed_usu_nome.SetFocus;
  ed_usu_nome.SelectAll;

  v_operacao := statEditando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSUsuarios);
end;

procedure TFrmCad_Usuarios.btnGravarClick(Sender: TObject);
var c_usuarios : TUsuarios;
begin
  inherited;
  try
    c_usuarios := TUsuarios.Create;

    c_usuarios.usu_codigo := iif((v_operacao = statInserindo), c_usuarios.func_prox_codigo, CDSUsuariosusu_codigo.AsInteger);
    c_usuarios.usu_nome   := ed_usu_nome.Text;
    c_usuarios.usu_login  := ed_usu_login.Text;
    c_usuarios.usu_senha  := func_base64encode(ed_usu_senha.Text);
    c_usuarios.usu_nivel  := cmb_usu_nivel.ItemIndex;
    c_usuarios.usu_status := rg_usu_status.ItemIndex;

    try
      Conexao.FDConexao.StartTransaction;

      c_usuarios.proc_gravar(c_usuarios.usu_codigo);

      Conexao.FDConexao.Commit;

      btnCancelar.Click;
      CDSUsuarios.Locate('usu_codigo', c_usuarios.usu_codigo, [loCaseInsensitive]);
    except
      on E : Exception do begin
        Conexao.FDConexao.Rollback;
        Application.MessageBox(PChar(E.Message), PChar(Self.Caption), MB_OK + MB_ICONERROR);
        Exit;
      end;
    end;
  finally
    FreeAndNil(c_usuarios);
  end;
end;

procedure TFrmCad_Usuarios.btnNovoClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos;

  tsCadastro.TabVisible := True;
  tsConsulta.TabVisible := False;

  ed_usu_nome.SetFocus;

  v_operacao := statInserindo;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSUsuarios);
end;

procedure TFrmCad_Usuarios.ed_consultaChange(Sender: TObject);
var c_usuarios : TUsuarios;
begin
  inherited;
  try
    c_usuarios := TUsuarios.Create;
    c_usuarios.proc_consultar(FDQUsuarios, CDSUsuarios, cmbCampos, ed_consulta.Text, cmbConStatus.ItemIndex);
  finally
    FreeAndNil(c_usuarios);
  end;
end;

procedure TFrmCad_Usuarios.ed_repetir_senhaExit(Sender: TObject);
begin
  inherited;
//  if Trim(ed then

end;

procedure TFrmCad_Usuarios.ed_usu_senhaKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (Key = #22) or (Key = #3) then Key := #0;
end;

procedure TFrmCad_Usuarios.FormActivate(Sender: TObject);
begin
  inherited;
  btnCancelar.Click;
end;

procedure TFrmCad_Usuarios.proc_limpa_campos;
begin
  ed_usu_codigo.Clear;
  ed_usu_nome.Clear;
  ed_usu_login.Clear;
  ed_usu_senha.Clear;

  cmb_usu_nivel.ItemIndex := 0;
end;

end.
