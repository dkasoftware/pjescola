unit aux_backup;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, ShellAPI, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, dm_conexao, cls_utils;

type
  TFrmAux_Backup = class(TFrmAux_Padrao)
    ed_destino: TEdit;
    btnBuscaDestrino: TBitBtn;
    Label1: TLabel;
    btnBackup: TBitBtn;
    Memo: TMemo;
    lProgress: TLabel;
    ProgressBar: TProgressBar;
    SD: TSaveDialog;
    procedure BitBtn1Click(Sender: TObject);
    procedure btnBackupClick(Sender: TObject);
    procedure btnBuscaDestrinoClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    function GetDosOutput(CommandLine: string; Work: string = 'C:\'): string;
  public
    { Public declarations }
  end;

var
  FrmAux_Backup: TFrmAux_Backup;

implementation

{$R *.dfm}

{ TFrmAux_Backup }

procedure TFrmAux_Backup.BitBtn1Click(Sender: TObject);
var aux : String;
begin
  inherited;
end;

procedure TFrmAux_Backup.btnBackupClick(Sender: TObject);
var qryAux      : TFDQuery;
    s_comando   : String;
    s_diretorio : String;
begin
  inherited;
  if Trim(ed_destino.Text) = '' then begin
    Application.MessageBox(PChar('Informe o diret�rio de destino.'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
    ed_destino.SetFocus;
    Exit;
  end;

  if not DirectoryExists(ed_destino.Text) then begin
    ForceDirectories(ed_destino.Text);
  end;

  qryAux := TFDQuery.Create(nil);
  try
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select table_name              '+
                   ' from information_schema.tables '+
                   ' where table_schema = :p_schema ');
    qryAux.ParamByName('p_schema').AsString := Conexao.db_schema;
    qryAux.Open;

    ProgressBar.Position := 0;
    ProgressBar.Max      := qryAux.RecordCount;

    lProgress.Caption := '0%';

    s_diretorio := Trim(ed_destino.Text);
    Memo.Lines.Clear;

    while (not qryAux.Eof) do begin
      ProgressBar.Position := ProgressBar.Position + 1;
      lProgress.Caption    := IntToStr(Trunc((ProgressBar.Position * 100) / ProgressBar.Max)) + '%';
      Application.ProcessMessages;

      Memo.Lines.Add('>> ' + qryAux.FieldByName('table_name').AsString + ' <<');
      Memo.Lines.Add(' ');

      s_comando := 'mysqldump --user=' + Conexao.db_user;
      s_comando := s_comando + ' --password=' + func_base64decode(Conexao.db_pass);
      s_comando := s_comando + ' --host=' + Conexao.db_host;
      s_comando := s_comando + ' --protocol=tcp --port=3306 --default-character-set=utf8';
      s_comando := s_comando + ' --skip-triggers --column-statistics=0';
      s_comando := s_comando + ' "' + Conexao.db_schema + '" "' + qryAux.FieldByName('table_name').AsString + '"';
      s_comando := s_comando + ' > ' + s_diretorio + Conexao.db_schema + '_' + qryAux.FieldByName('table_name').AsString + '.sql';

      Memo.Lines.Add(' ');
      Memo.Lines.Add(s_comando);
      Memo.Lines.Add(' ');

      Memo.Lines.Add(GetDosOutput(s_comando, s_diretorio));

      Memo.Lines.Add(' ');
      Memo.Lines.Add(' ---------------------------------------------------------- ');
      Memo.Lines.Add(' ');

      qryAux.Next;
    end;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TFrmAux_Backup.btnBuscaDestrinoClick(Sender: TObject);
begin
  inherited;
  SD.InitialDir := ExtractFilePath(Application.ExeName);
  SD.FileName   := 'dump.sql';
  sd.Execute;
  ed_destino.Text := ExtractFilePath(SD.FileName);
end;

procedure TFrmAux_Backup.FormActivate(Sender: TObject);
begin
  inherited;
  ed_destino.Text := ExtractFilePath(Application.ExeName) + 'backup\dump' + FormatDateTime('ddmmyyyyHHMM', Now) + '\';
  Memo.Lines.Clear;

  ed_destino.SetFocus;
end;

function TFrmAux_Backup.GetDosOutput(CommandLine: string; Work: string = 'C:\'): string;
var
  SA: TSecurityAttributes;
  SI: TStartupInfo;
  PI: TProcessInformation;
  StdOutPipeRead, StdOutPipeWrite: THandle;
  WasOK: Boolean;
  Buffer: array[0..255] of AnsiChar;
  BytesRead: Cardinal;
  WorkDir: string;
  Handle: Boolean;
begin
  Result := '';
  with SA do begin
    nLength := SizeOf(SA);
    bInheritHandle := True;
    lpSecurityDescriptor := nil;
  end;
  CreatePipe(StdOutPipeRead, StdOutPipeWrite, @SA, 0);
  try
    with SI do
    begin
      FillChar(SI, SizeOf(SI), 0);
      cb := SizeOf(SI);
      dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
      wShowWindow := SW_HIDE;
      hStdInput := GetStdHandle(STD_INPUT_HANDLE); // don't redirect stdin
      hStdOutput := StdOutPipeWrite;
      hStdError := StdOutPipeWrite;
    end;
    WorkDir := Work;
    Handle := CreateProcess(nil, PChar('cmd.exe /C ' + CommandLine),
                            nil, nil, True, 0, nil,
                            PChar(WorkDir), SI, PI);
    CloseHandle(StdOutPipeWrite);
    if Handle then
      try
        repeat
          WasOK := ReadFile(StdOutPipeRead, Buffer, 255, BytesRead, nil);
          if BytesRead > 0 then
          begin
            Buffer[BytesRead] := #0;
            Result := Result + Buffer;
          end;
        until not WasOK or (BytesRead = 0);
        WaitForSingleObject(PI.hProcess, INFINITE);
      finally
        CloseHandle(PI.hThread);
        CloseHandle(PI.hProcess);
      end;
  finally
    CloseHandle(StdOutPipeRead);
  end;
end;

end.
