unit menu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus, Vcl.StdCtrls, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Datasnap.Provider, Datasnap.DBClient, Data.DB, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, DateUtils, frxClass, frxDBSet;

type
  TfrmMenu = class(TForm)
    MainMenu: TMainMenu;
    meCadastros: TMenuItem;
    PCMenu: TPageControl;
    tsCadastros: TTabSheet;
    Panel1: TPanel;
    btnCadEntidades: TBitBtn;
    SB: TStatusBar;
    meEntidades: TMenuItem;
    meCadastroEntidades: TMenuItem;
    N1: TMenuItem;
    meContratos: TMenuItem;
    btnAgendaHide: TSpeedButton;
    FDQAgendamentos: TFDQuery;
    DSAgendamentos: TDataSource;
    CDSAgendamentos: TClientDataSet;
    PROAgendamentos: TDataSetProvider;
    Movimentaes1: TMenuItem;
    meAgendamentos: TMenuItem;
    TSMovimentacoes: TTabSheet;
    Panel2: TPanel;
    btnMovAgendamentos: TBitBtn;
    FDQAgendamentossegunda: TStringField;
    FDQAgendamentosterca: TStringField;
    FDQAgendamentosquarta: TStringField;
    FDQAgendamentosquinta: TStringField;
    FDQAgendamentossexta: TStringField;
    FDQAgendamentossabado: TStringField;
    FDQAgendamentosdomingo: TStringField;
    CDSAgendamentossegunda: TStringField;
    CDSAgendamentosterca: TStringField;
    CDSAgendamentosquarta: TStringField;
    CDSAgendamentosquinta: TStringField;
    CDSAgendamentossexta: TStringField;
    CDSAgendamentossabado: TStringField;
    CDSAgendamentosdomingo: TStringField;
    FDQAgendamentoshora: TTimeField;
    CDSAgendamentoshora: TTimeField;
    PC: TPageControl;
    tsPorDia: TTabSheet;
    Panel3: TPanel;
    DBGAgendamentosDia: TDBGrid;
    ed_data_por_dia: TDateTimePicker;
    lblDiaSemana: TLabel;
    FDQAgendamentosDia: TFDQuery;
    PROAgendamentosDia: TDataSetProvider;
    CDSAgendamentosDia: TClientDataSet;
    FDQAgendamentosDiahora: TTimeField;
    FDQAgendamentosDiasala1: TStringField;
    FDQAgendamentosDiasala2: TStringField;
    FDQAgendamentosDiasala3: TStringField;
    FDQAgendamentosDiasala4: TStringField;
    FDQAgendamentosDiasala5: TStringField;
    DSAgendamentosDia: TDataSource;
    CDSAgendamentosDiahora: TTimeField;
    CDSAgendamentosDiasala1: TStringField;
    CDSAgendamentosDiasala2: TStringField;
    CDSAgendamentosDiasala3: TStringField;
    CDSAgendamentosDiasala4: TStringField;
    CDSAgendamentosDiasala5: TStringField;
    gbDia: TGroupBox;
    pnlConfirmaAula: TPanel;
    ed_professor: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    ed_alunos: TMemo;
    ed_sala: TEdit;
    Label3: TLabel;
    ed_data: TEdit;
    Label4: TLabel;
    ed_hora: TEdit;
    Label5: TLabel;
    btnCancelarConfirmacao: TBitBtn;
    btnConfirmarAula: TBitBtn;
    btnMovContas_a_Receber: TBitBtn;
    N2: TMenuItem;
    meContasReceber: TMenuItem;
    btnListagemAulas: TBitBtn;
    pnlRelListagemAulas: TPanel;
    dt_relListaIni: TDateTimePicker;
    dt_relListaFin: TDateTimePicker;
    lbl_con_ent_codigo_funcionario: TLabel;
    cmb_ent_codigo_funcionario: TComboBox;
    btnImprimirListaAulas: TBitBtn;
    btnFecharListaAulas: TBitBtn;
    Relatorio: TfrxReport;
    frxDBListagemAulas: TfrxDBDataset;
    FDQListagemAulas: TFDQuery;
    FDQListagemAulasent_razao_social: TStringField;
    FDQListagemAulasagd_qtde_horas: TFMTBCDField;
    CDSAgendamentosDiaonline: TStringField;
    FDQAgendamentosDiaonline: TStringField;
    pnlAgendamentoOnLine: TPanel;
    ed_agenda_online: TMemo;
    ed_data_online: TEdit;
    btnAgendamentoOnLine: TBitBtn;
    Financeiro1: TMenuItem;
    Cadastro1: TMenuItem;
    Bancos1: TMenuItem;
    ContasBancarias1: TMenuItem;
    FerramentasAdministrativas1: TMenuItem;
    Backup1: TMenuItem;
    gbConfirmacao: TGroupBox;
    gbAgendamentoON: TGroupBox;
    gbPeriodo: TGroupBox;
    p1: TPanel;
    l1: TLabel;
    Relatrios1: TMenuItem;
    RelatrioAulaspor1: TMenuItem;
    procedure btnCadEntidadesClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure meCadastroEntidadesClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure btnAgendaHideClick(Sender: TObject);
    procedure CDSAgendamentosCalcFields(DataSet: TDataSet);
    procedure btnMovAgendamentosClick(Sender: TObject);
    procedure meAgendamentosClick(Sender: TObject);
    procedure ed_data_por_diaChange(Sender: TObject);
    procedure DBGAgendamentosDiaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGAgendamentosDiaDblClick(Sender: TObject);
    procedure btnCancelarConfirmacaoClick(Sender: TObject);
    procedure btnConfirmarAulaClick(Sender: TObject);
    procedure meContasReceberClick(Sender: TObject);
    procedure btnMovContas_a_ReceberClick(Sender: TObject);
    procedure btnListagemAulasClick(Sender: TObject);
    procedure btnFecharListaAulasClick(Sender: TObject);
    procedure btnImprimirListaAulasClick(Sender: TObject);
    procedure btnAgendamentoOnLineClick(Sender: TObject);
    procedure Bancos1Click(Sender: TObject);
    procedure ContasBancarias1Click(Sender: TObject);
    procedure Backup1Click(Sender: TObject);
    procedure meContratosClick(Sender: TObject);
    procedure RelatrioAulaspor1Click(Sender: TObject);
  private
    { Private declarations }
    b_activate : Boolean;

    f_codigo_usuario : Integer;
    f_nome_usuario   : String;
    f_login_usuario  : String;
    f_nivel_usuario  : Integer;
    f_codigo_empresa : Integer;
    f_razao_empresa  : String;

    i_func_codigo   : Integer;
    i_func_empresa  : Integer;
    i_aluno_codigo  : Integer;
    i_aluno_empresa : Integer;
    s_dia           : String;
    s_hora          : String;
  public
    { Public declarations }

  published
    { Published declarations }
    property codigo_usuario : Integer read f_codigo_usuario write f_codigo_usuario;
    property nome_usuario   : String  read f_nome_usuario   write f_nome_usuario;
    property login_usuario  : String  read f_login_usuario  write f_login_usuario;
    property nivel_usuario  : Integer read f_nivel_usuario  write f_nivel_usuario;
    property codigo_empresa : Integer read f_codigo_empresa write f_codigo_empresa;
    property razao_empresa  : String  read f_razao_empresa  write f_razao_empresa;
  end;

var
  frmMenu: TfrmMenu;

implementation

{$R *.dfm}

uses cad_entidades, login, cls_usuarios_logins, cls_utils, cls_entidades, cls_produtos,
     cls_salas, mov_agendamentos, cls_agendamentos, cls_historicos_agendamentos,
     cls_tabelacodigos, dm_conexao, mov_contas_a_receber, cad_bancos, cad_contasbancarias,
     aux_backup, cad_contratos, Aux_RelAulasProfessorData;

procedure TfrmMenu.Backup1Click(Sender: TObject);
var f_backup : TFrmAux_Backup;
begin
  try
    f_backup := TFrmAux_Backup.Create(nil);
    f_backup.ShowModal;
  finally
    FreeAndNil(f_backup);
  end;
end;

procedure TfrmMenu.Bancos1Click(Sender: TObject);
var f_bancos : TFrmCad_Bancos;
begin
  try
    f_bancos := TFrmCad_Bancos.Create(nil);
    f_bancos.ShowModal;
  finally
    FreeAndNil(f_bancos);
  end;
end;

procedure TfrmMenu.btnAgendaHideClick(Sender: TObject);
begin
  if PC.Width > 0 then begin
    PC.Width       := 0;
    btnAgendaHide.Caption := '<';
  end else begin
    PC.Width       := Self.Width - btnAgendaHide.Width - 6;
    btnAgendaHide.Caption := '>';
  end;

  Application.ProcessMessages;
end;

procedure TfrmMenu.btnAgendamentoOnLineClick(Sender: TObject);
begin
  pnlAgendamentoOnLine.Visible := False;
end;

procedure TfrmMenu.btnCadEntidadesClick(Sender: TObject);
begin
  meCadastroEntidades.Click;
end;

procedure TfrmMenu.btnCancelarConfirmacaoClick(Sender: TObject);
begin
  pnlConfirmaAula.Visible := False;
end;

procedure TfrmMenu.btnConfirmarAulaClick(Sender: TObject);
var c_historicoAgendamentos : THistoricos_Agendamentos;
    c_agendamentos          : TAgendamentos;
    c_tabela                : TTabelaCodigos;
begin
  if (Application.MessageBox(PChar('Deseja mesmo confirmar a aula?'), PChar('Aten��o'), MB_YESNO + MB_ICONQUESTION) = mrYes) then begin
    c_historicoAgendamentos := THistoricos_Agendamentos.Create;
    c_agendamentos          := TAgendamentos.Create;
    c_tabela                := TTabelaCodigos.Create;
    try
      if c_agendamentos.func_recuperar_aluno_professor(i_func_codigo, i_func_empresa, i_aluno_codigo, i_aluno_empresa, StrToDateTime(s_dia + ' ' + s_hora)) then begin
//        c_historicoAgendamentos.hagd_codigo         := c_tabela.func_gerar_codigo('historicos_agendamentos', IntToStr(codigo_empresa));
//        c_historicoAgendamentos.hagd_emp_codigo     := codigo_empresa;
//        c_historicoAgendamentos.hagd_agd_codigo     := c_agendamentos.agd_codigo;
//        c_historicoAgendamentos.hagd_agd_emp_codigo := c_agendamentos.agd_emp_codigo;
//        c_historicoAgendamentos.hagd_status         := 1;

        try
          Conexao.FDConexao.StartTransaction;

//          c_historicoAgendamentos.proc_gravar(c_historicoAgendamentos.hagd_codigo, c_historicoAgendamentos.hagd_emp_codigo);

          Conexao.FDConexao.Commit;

          btnCancelarConfirmacao.Click;
        except
          on E : Exception do begin
            Conexao.FDConexao.Rollback;
            Application.MessageBox(PChar(E.Message), PChar('Aten��o'), MB_OK + MB_ICONERROR);
          end;
        end;
      end;
    finally
      FreeAndNil(c_historicoAgendamentos);
      FreeAndNil(c_agendamentos);
      FreeAndNil(c_tabela);
    end;
  end;
end;

procedure TfrmMenu.btnFecharListaAulasClick(Sender: TObject);
begin
  pnlRelListagemAulas.Visible := False;
end;

procedure TfrmMenu.btnImprimirListaAulasClick(Sender: TObject);
var s_caminho : String;
begin
  s_caminho := ExtractFilePath(Application.ExeName) + 'fast\RelListagemAulas.fr3';

  FDQListagemAulas.Connection := Conexao.FDConexao;

  FDQListagemAulas.Close;
  FDQListagemAulas.SQL.Clear;
  FDQListagemAulas.SQL.Add(' select e.ent_razao_social, sum(a.agd_qtde_horas) agd_qtde_horas                        '+
                           ' from personal_english.agendamentos a                                                   '+
                           ' inner join personal_english.entidades e on e.ent_codigo     = a.agd_fun_ent_codigo and '+
                           '                                            e.ent_emp_codigo = a.agd_fun_ent_emp_codigo '+
                           ' where a.agd_data >= date(:p_data_ini)                                                  '+
                           '   and a.agd_data <= date(:p_data_fin)                                                  ');

  if cmb_ent_codigo_funcionario.ItemIndex > 0 then begin
    FDQListagemAulas.SQL.Add('   and a.agd_fun_ent_codigo     = :p_agd_fun_ent_codigo     '+
                             '   and a.agd_fun_ent_emp_codigo = :p_agd_fun_ent_emp_codigo ');
    FDQListagemAulas.ParamByName('p_agd_fun_ent_codigo').AsInteger     := TEntidadeCombo(cmb_ent_codigo_funcionario.Items.Objects[cmb_ent_codigo_funcionario.ItemIndex]).ent_codigo;
    FDQListagemAulas.ParamByName('p_agd_fun_ent_emp_codigo').AsInteger := TEntidadeCombo(cmb_ent_codigo_funcionario.Items.Objects[cmb_ent_codigo_funcionario.ItemIndex]).ent_emp_codigo;
  end;

  FDQListagemAulas.SQL.Add(' group by e.ent_razao_social '+
                           ' order by e.ent_razao_social ');
  FDQListagemAulas.ParamByName('p_data_ini').AsDateTime := dt_relListaIni.Date;
  FDQListagemAulas.ParamByName('p_data_fin').AsDateTime := dt_relListaFin.Date;
  FDQListagemAulas.Open;

  Relatorio.LoadFromFile(s_caminho);
  Relatorio.Variables['DT_INI'] := QuotedStr(FormatDateTime('dd/mm/yyyy', dt_relListaIni.Date));
  Relatorio.Variables['DT_FIN'] := QuotedStr(FormatDateTime('dd/mm/yyyy', dt_relListaFin.Date));
  Relatorio.ShowReport;
end;

procedure TfrmMenu.btnListagemAulasClick(Sender: TObject);
var c_entidades    : TEntidades;
begin
  inherited;
  c_entidades    := TEntidades.Create;
  try
    c_entidades.proc_carregar_combo(cmb_ent_codigo_funcionario, 2, True);
  finally
    FreeAndNil(c_entidades);
  end;

  dt_relListaIni.DateTime := StartOfTheMonth(Now);
  dt_relListaFin.DateTime := EndOfTheMonth(Now);

  pnlRelListagemAulas.Left    := Trunc((Self.Width / 2) - (pnlRelListagemAulas.Width / 2));
  pnlRelListagemAulas.Top     := Trunc((Self.Height/ 2) - (pnlRelListagemAulas.Height/ 2));
  pnlRelListagemAulas.Visible := True;
end;

procedure TfrmMenu.btnMovAgendamentosClick(Sender: TObject);
begin
  meAgendamentos.Click;
end;

procedure TfrmMenu.btnMovContas_a_ReceberClick(Sender: TObject);
begin
  meContasReceber.Click;
end;

procedure TfrmMenu.CDSAgendamentosCalcFields(DataSet: TDataSet);
begin
//  CDSAgendamentosdia_semana.AsString := func_dia_da_semana(DayOfWeek(CDSAgendamentosagd_data.AsDateTime));
end;

procedure TfrmMenu.ContasBancarias1Click(Sender: TObject);
var f_contas_bancarias : TFrmCad_ContasBancarias;
begin
  f_contas_bancarias := TFrmCad_ContasBancarias.Create(nil);
  try
    f_contas_bancarias.ShowModal;
  finally
    FreeAndNil(f_contas_bancarias);
  end;
end;

procedure TfrmMenu.meContasReceberClick(Sender: TObject);
var f_contas_receber : TFrmMov_Contas_a_Receber;
begin
  f_contas_receber := TFrmMov_Contas_a_Receber.Create(nil);
  try
    f_contas_receber.ShowModal;
  finally
    FreeAndNil(f_contas_receber);
  end;
end;

procedure TfrmMenu.meContratosClick(Sender: TObject);
var f_contratos : TFrmCad_Contratos;
begin
  f_contratos := TFrmCad_Contratos.Create(nil);
  try
    f_contratos.ShowModal;
  finally
    FreeAndNil(f_contratos);
  end;
end;

procedure TfrmMenu.RelatrioAulaspor1Click(Sender: TObject);
var c_relAulaProfessorData : TFrmAux_RelAulasProfessorData;
begin
  c_relAulaProfessorData := TFrmAux_RelAulasProfessorData.Create(nil);
  try
    c_relAulaProfessorData.ShowModal;
  finally
    FreeAndNil(c_relAulaProfessorData);
  end;
end;

procedure TfrmMenu.DBGAgendamentosDiaDblClick(Sender: TObject);
var l_registro     : TStringList;
    c_entidades    : TEntidades;
    c_agendamentos : TAgendamentos;
begin
  if TDBGrid(Sender).SelectedField.FieldName = 'online' then begin
    c_agendamentos := TAgendamentos.Create;
    try
      c_agendamentos.proc_carrega_agenda_online(ed_agenda_online, ed_data_por_dia.Date, CDSAgendamentosDiahora.AsString);
    finally
      FreeAndNil(c_agendamentos);
    end;

    ed_data_online.Text := FormatDateTime('dd/mm/yyyy', ed_data_por_dia.Date);

    pnlAgendamentoOnLine.Left    := Trunc((Self.Width / 2) - (pnlAgendamentoOnLine.Width / 2));
    pnlAgendamentoOnLine.Top     := Trunc((Self.Height / 2) - (pnlAgendamentoOnLine.Height / 2));
    pnlAgendamentoOnLine.Visible := True;
  end else begin
    l_registro := TStringList.Create;
    try
      l_registro.Delimiter     := '|';
      l_registro.DelimitedText := StringReplace(TDBGrid(Sender).SelectedField.Value, ' ', '', [rfReplaceAll]);

      i_func_codigo   := StrToInt(l_registro.Strings[1]);
      i_func_empresa  := StrToInt(l_registro.Strings[2]);
      i_aluno_codigo  := StrToInt(l_registro.Strings[3]);
      i_aluno_empresa := StrToInt(l_registro.Strings[4]);
      s_hora          := l_registro.Strings[5];
      s_dia           := DateToStr(ed_data_por_dia.Date);
    finally
      FreeAndNil(l_registro);
    end;

    c_entidades := TEntidades.Create;
    try
      c_entidades.func_recuperar(i_func_codigo, i_func_empresa);
      ed_professor.Text := c_entidades.ent_razao_social;

      c_entidades.func_recuperar(i_aluno_codigo, i_aluno_empresa);
      ed_alunos.Text := c_entidades.ent_razao_social;

      ed_sala.Text := TDBGrid(Sender).SelectedField.FieldName;
      ed_data.Text := FormatDateTime('dd/mm/yyyy', ed_data_por_dia.Date);
      ed_hora.Text := s_hora;

      pnlConfirmaAula.Left    := Trunc((Self.Width / 2) - (pnlConfirmaAula.Width / 2));
      pnlConfirmaAula.Top     := Trunc((Self.Height / 2) - (pnlConfirmaAula.Height / 2));
      pnlConfirmaAula.Visible := True;
    finally
      FreeAndNil(c_entidades);
    end;
  end;
end;

procedure TfrmMenu.DBGAgendamentosDiaDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var l_registro     : TStringList;
    c_agendamentos : TAgendamentos;
begin
  if (not Odd(CDSAgendamentosDia.RecNo)) then begin
    dbgAgendamentosDia.Canvas.Brush.Color := $00FDD7D7;
  end else begin
    dbgAgendamentosDia.Canvas.Brush.Color := clWindow;
  end;

  if Column.FieldName = 'hora' then begin
    dbgAgendamentosDia.Canvas.Brush.Color := clGray;
    dbgAgendamentosDia.Canvas.font.Color  := clWhite;
  end;

  if (gdSelected in State) then begin
    dbgAgendamentosDia.Canvas.Brush.Color := $00FFA74F;
  end;

  dbgAgendamentosDia.Canvas.FillRect(Rect);
  dbgAgendamentosDia.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TfrmMenu.ed_data_por_diaChange(Sender: TObject);
var c_agendamentos : TAgendamentos;
begin
  case DayOfWeek(ed_data_por_dia.Date) of
    1 : lblDiaSemana.Caption := 'Domingo';
    2 : lblDiaSemana.Caption := 'Segunda';
    3 : lblDiaSemana.Caption := 'Ter�a';
    4 : lblDiaSemana.Caption := 'Quarta';
    5 : lblDiaSemana.Caption := 'Quinta';
    6 : lblDiaSemana.Caption := 'Sexta';
    7 : lblDiaSemana.Caption := 'S�bado';
  end;

  c_agendamentos := TAgendamentos.Create;
  try
    c_agendamentos.proc_consultar_agenda_dia(FDQAgendamentosDia, CDSAgendamentosDia, codigo_empresa, ed_data_por_dia.Date);
  finally
    FreeAndNil(c_agendamentos);
  end;
end;

procedure TfrmMenu.FormActivate(Sender: TObject);
var c_agendamentos : TAgendamentos;
    c_entidades    : TEntidades;
begin
  b_activate := True;

  ed_data_por_dia.Date := Date;

  SB.Panels[0].Text := FormatDateTime('dddddd', Now);

  b_activate := False;

  ed_data_por_diaChange(Self);
end;

procedure TfrmMenu.FormCreate(Sender: TObject);
var f_login         : TfrmLogin;
    c_usuario_login : TUsuarios_Logins;
begin
  try
    f_login := TfrmLogin.Create(nil);

    f_login.ShowModal;
  finally
    FreeAndNil(f_login);
  end;

  if login_usuario <> '' then begin
    {try
      c_usuario_login := TUsuarios_Logins.Create;

      c_usuario_login.ul_usu_codigo := frmMenu.codigo_usuario;
      c_usuario_login.ul_data_hora  := Now;
      c_usuario_login.ul_maquina    := func_getcomputer;

      c_usuario_login.proc_gravar;
    finally
      FreeAndNil(c_usuario_login);
    end;}

    Self.Show;
  end;
end;

procedure TfrmMenu.FormResize(Sender: TObject);
begin
  btnAgendaHide.Click;
end;

procedure TfrmMenu.meAgendamentosClick(Sender: TObject);
var f_agendamentos : TFrmMov_Agendamentos;
    c_agendamentos : TAgendamentos;
    c_entidades    : TEntidades;
begin
  f_agendamentos := TFrmMov_Agendamentos.Create(nil);
  try
    f_agendamentos.ShowModal;

    c_agendamentos := TAgendamentos.Create;
    c_entidades    := TEntidades.Create;
    try
      c_agendamentos.proc_consultar_agenda_dia(FDQAgendamentosDia, CDSAgendamentosDia, codigo_empresa, ed_data_por_dia.Date);
    finally
      FreeAndNil(c_agendamentos);
      FreeAndNil(c_entidades);
    end;
  finally
    FreeAndNil(f_agendamentos);
  end;
end;

procedure TfrmMenu.meCadastroEntidadesClick(Sender: TObject);
var f_entidades : TFrmCad_Entidades;
begin
  try
    f_entidades := TFrmCad_Entidades.Create(nil);
    f_entidades.ShowModal;
  finally
    FreeAndNil(f_entidades);
  end;
end;

end.
