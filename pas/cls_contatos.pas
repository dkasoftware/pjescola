unit cls_contatos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type TContatos = Class(TObject)
  private
    { Private declarations }
    f_con_codigo         : Integer;
    f_con_ent_codigo     : Integer;
    f_con_ent_emp_codigo : Integer;
    f_con_email          : String;
    f_con_contato        : String;
    f_con_telefone       : String;
    f_con_observacao     : String;
  public
    { Public declarations }
    procedure proc_gravar(PCodigo, PCodigoEntidade, PCodigoEmpresa : Integer);
    procedure proc_limpar_gravacao(PCodigoEntidade, PCodigoEmpresa : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PCodigoEntidade, PCodigoEmpresa : Integer);

    function func_recuperar(PCodigo, PCodigoEntidade, PCodigoEmpresa : Integer) : Boolean;
  published
    { Published declarations }
    property con_codigo         : Integer read f_con_codigo         write f_con_codigo;
    property con_ent_codigo     : Integer read f_con_ent_codigo     write f_con_ent_codigo;
    property con_ent_emp_codigo : Integer read f_con_ent_emp_codigo write f_con_ent_emp_codigo;
    property con_email          : String  read f_con_email          write f_con_email;
    property con_contato        : String  read f_con_contato        write f_con_contato;
    property con_telefone       : String  read f_con_telefone       write f_con_telefone;
    property con_observacao     : String  read f_con_observacao     write f_con_observacao;
  end;

implementation

{ TContatos }

function TContatos.func_recuperar(PCodigo, PCodigoEntidade,
  PCodigoEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from contatos c                                    '+
                   ' where c.con_codigo         = :p_con_codigo         '+
                   '   and c.con_ent_codigo     = :p_con_ent_codigo     '+
                   '   and c.con_ent_emp_codigo = :p_con_ent_emp_codigo ');
    qryAux.ParamByName('p_con_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    con_codigo         := qryAux.FieldByName('con_codigo').AsInteger;
    con_ent_codigo     := qryAux.FieldByName('con_ent_codigo').AsInteger;
    con_ent_emp_codigo := qryAux.FieldByName('con_ent_emp_codigo').AsInteger;
    con_email          := qryAux.FieldByName('con_email').AsString;
    con_contato        := qryAux.FieldByName('con_contato').AsString;
    con_telefone       := qryAux.FieldByName('con_telefone').AsString;
    con_observacao     := qryAux.FieldByName('con_observacao').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TContatos.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PCodigoEntidade, PCodigoEmpresa: Integer);
begin
  PQuery.Close;
  PClient.Close;

  PQuery.Connection := Conexao.FDConexao;

  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                                           '+
                 ' from contatos c                                    '+
                 ' where c.con_ent_codigo     = :p_con_ent_codigo     '+
                 '   and c.con_ent_emp_codigo = :p_con_ent_emp_codigo '+
                 ' order by c.con_codigo                              ');
  PQuery.ParamByName('p_con_ent_codigo').AsInteger     := PCodigoEntidade;
  PQuery.ParamByName('p_con_ent_emp_codigo').AsInteger := PCodigoEmpresa;
  PQuery.Open;
  PClient.Open;
end;

procedure TContatos.proc_gravar(PCodigo, PCodigoEntidade,
  PCodigoEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from contatos c                                    '+
                   ' where c.con_codigo         = :p_con_codigo         '+
                   '   and c.con_ent_codigo     = :p_con_ent_codigo     '+
                   '   and c.con_ent_emp_codigo = :p_con_ent_emp_codigo ');
    qryAux.ParamByName('p_con_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into contatos (  '+
                     '   con_codigo        ,    '+
                     '   con_ent_codigo    ,    '+
                     '   con_ent_emp_codigo,    '+
                     '   con_email         ,    '+
                     '   con_contato       ,    '+
                     '   con_telefone      ,    '+
                     '   con_observacao         '+
                     '  ) values (              '+
                     '   :p_con_codigo        , '+
                     '   :p_con_ent_codigo    , '+
                     '   :p_con_ent_emp_codigo, '+
                     '   :p_con_email         , '+
                     '   :p_con_contato       , '+
                     '   :p_con_telefone      , '+
                     '   :p_con_observacao    ) ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update contatos                                  '+
                     ' set con_email      = :p_con_email     ,          '+
                     '     con_contato    = :p_con_contato   ,          '+
                     '     con_telefone   = :p_con_telefone  ,          '+
                     '     con_observacao = :p_con_observacao           '+
                     ' where con_codigo         = :p_con_codigo         '+
                     '   and con_ent_codigo     = :p_con_ent_codigo     '+
                     '   and con_ent_emp_codigo = :p_con_ent_emp_codigo ');
    end;

    qryAux.ParamByName('p_con_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.ParamByName('p_con_email').AsString           := con_email;
    qryAux.ParamByName('p_con_contato').AsString         := con_contato;
    qryAux.ParamByName('p_con_telefone').AsString        := con_telefone;
    qryAux.ParamByName('p_con_observacao').AsString      := con_observacao;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TContatos.proc_limpar_gravacao(PCodigoEntidade,
  PCodigoEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' delete from contatos                             '+
                   ' where con_ent_codigo     = :p_con_ent_codigo     '+
                   '   and con_ent_emp_codigo = :p_con_ent_emp_codigo ');
    qryAux.ParamByName('p_con_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_con_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
