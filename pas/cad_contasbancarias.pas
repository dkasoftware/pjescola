unit cad_contasbancarias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, Datasnap.DBClient, Datasnap.Provider, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, cls_utils;

type
  TFrmCad_ContasBancarias = class(TFrmAux_Padrao)
    PC: TPageControl;
    tsConsulta: TTabSheet;
    tsCadastro: TTabSheet;
    dbgContasBancarias: TDBGrid;
    lbl_con_pro_codigo: TLabel;
    cmb_con_bancos: TComboBox;
    ed_con_cb_numero: TEdit;
    ed_con_cb_agencia_numero: TEdit;
    FDQContasBancarias: TFDQuery;
    PROContasBancarias: TDataSetProvider;
    CDSContasBancarias: TClientDataSet;
    DSContasBancarias: TDataSource;
    FDQContasBancariascb_codigo: TIntegerField;
    FDQContasBancariascb_ban_codigo: TIntegerField;
    FDQContasBancariascb_ban_emp_codigo: TIntegerField;
    FDQContasBancariascb_ag_numero: TStringField;
    FDQContasBancariascb_ag_digito: TStringField;
    FDQContasBancariascb_numero: TStringField;
    FDQContasBancariascb_digito: TStringField;
    FDQContasBancariascb_tipo: TIntegerField;
    FDQContasBancariascb_ativo: TBooleanField;
    CDSContasBancariascb_codigo: TIntegerField;
    CDSContasBancariascb_ban_codigo: TIntegerField;
    CDSContasBancariascb_ban_emp_codigo: TIntegerField;
    CDSContasBancariascb_ag_numero: TStringField;
    CDSContasBancariascb_ag_digito: TStringField;
    CDSContasBancariascb_numero: TStringField;
    CDSContasBancariascb_digito: TStringField;
    CDSContasBancariascb_tipo: TIntegerField;
    CDSContasBancariascb_ativo: TBooleanField;
    cmb_cb_ban_codigo: TComboBox;
    gb_ent_codigo: TGroupBox;
    ed_cb_codigo: TEdit;
    cmb_cb_ativo: TComboBox;
    ed_cb_numero: TEdit;
    ed_cb_ag_numero: TEdit;
    ed_cb_ag_digito: TEdit;
    ed_cb_digito: TEdit;
    cmb_cb_tipo: TComboBox;
    procedure FormActivate(Sender: TObject);
    procedure dbgContasBancariasDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure cmb_con_bancosClick(Sender: TObject);
    procedure ed_con_cb_agencia_numeroChange(Sender: TObject);
    procedure ed_con_cb_numeroChange(Sender: TObject);
  private
    { Private declarations }
    v_operacao : TOperacaoQuery;

    procedure proc_limpa_campos;
    procedure proc_carrega_cadastro;
  public
    { Public declarations }
  end;

var
  FrmCad_ContasBancarias: TFrmCad_ContasBancarias;

implementation

uses dm_conexao, cls_bancos, cls_tabelacodigos, cls_empresas, menu, cls_contas_bancarias;

{$R *.dfm}

procedure TFrmCad_ContasBancarias.btnCancelarClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos;

  tsCadastro.TabVisible := False;
  tsConsulta.TabVisible := True;

  cmb_con_bancos.SetFocus;

  proc_carrega_cadastro;

  v_operacao := statNavegando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSContasBancarias);

  btnExcluir.Enabled := False;
end;

procedure TFrmCad_ContasBancarias.btnEditarClick(Sender: TObject);
var c_bancos : TBancos;
begin
  inherited;
  ed_cb_codigo.Text := CDSContasBancariascb_codigo.AsString;

  try
    c_bancos := TBancos.Create;
    cmb_cb_ban_codigo.ItemIndex := cmb_cb_ban_codigo.Items.IndexOf(c_bancos.func_retorna_razao_combo(CDSContasBancariascb_ban_codigo.AsInteger, CDSContasBancariascb_ban_emp_codigo.AsInteger));
  finally
    FreeAndNil(c_bancos);
  end;

  ed_cb_numero.Text    := CDSContasBancariascb_numero.AsString;
  ed_cb_digito.Text    := CDSContasBancariascb_digito.AsString;
  ed_cb_ag_numero.Text := CDSContasBancariascb_ag_numero.AsString;
  ed_cb_ag_digito.Text := CDSContasBancariascb_ag_digito.AsString;

  if CDSContasBancariascb_ativo.AsBoolean then begin
    cmb_cb_ativo.ItemIndex := 1;
  end else begin
    cmb_cb_ativo.ItemIndex := 0;
  end;

  v_operacao := statEditando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSContasBancarias);

  tsConsulta.TabVisible := False;
  tsCadastro.TabVisible := True;

  ed_cb_ag_numero.SetFocus;
end;

procedure TFrmCad_ContasBancarias.btnGravarClick(Sender: TObject);
var c_contas_bancarias : TContasBancarias;
begin
  inherited;
  if cmb_cb_ban_codigo.ItemIndex = 0 then begin
    Application.MessageBox(PChar('Selecione um banco.'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
    cmb_cb_ban_codigo.SetFocus;
    Exit;
  end;

  c_contas_bancarias := TContasBancarias.Create;
  try
    try
      Conexao.FDConexao.StartTransaction;

      c_contas_bancarias.cb_codigo         := StrToInt(ed_cb_codigo.Text);
      c_contas_bancarias.cb_ban_codigo     := TBancoCombo(cmb_cb_ban_codigo.Items.Objects[cmb_cb_ban_codigo.ItemIndex]).ban_codigo;
      c_contas_bancarias.cb_ban_emp_codigo := TBancoCombo(cmb_cb_ban_codigo.Items.Objects[cmb_cb_ban_codigo.ItemIndex]).ban_emp_codigo;
      c_contas_bancarias.cb_ag_numero      := ed_cb_numero.Text;
      c_contas_bancarias.cb_ag_digito      := ed_cb_digito.Text;
      c_contas_bancarias.cb_numero         := ed_cb_ag_numero.Text;
      c_contas_bancarias.cb_digito         := ed_cb_ag_digito.Text;
      c_contas_bancarias.cb_tipo           := cmb_cb_tipo.ItemIndex;
      c_contas_bancarias.cb_ativo          := CDSContasBancariascb_ativo.AsBoolean;

      c_contas_bancarias.proc_gravar(c_contas_bancarias.cb_codigo, c_contas_bancarias.cb_ban_codigo, c_contas_bancarias.cb_ban_emp_codigo);

      Conexao.FDConexao.Commit;
    except
      on E : Exception do begin
        Conexao.FDConexao.Rollback;
        Application.MessageBox(PChar(E.Message), PChar('Aten��o'), MB_OK + MB_ICONINFORMATION);
      end;
    end;
  finally
    FreeAndNil(c_contas_bancarias);
  end;

  btnCancelar.Click;
end;

procedure TFrmCad_ContasBancarias.btnNovoClick(Sender: TObject);
var c_tabela : TTabelaCodigos;
begin
  inherited;
  c_tabela := TTabelaCodigos.Create;
  try
    try
      Conexao.FDConexao.StartTransaction;

      ed_cb_codigo.Text := IntToStr(c_tabela.func_gerar_codigo('contas_bancarias', IntToStr(frmMenu.codigo_empresa)));

      Conexao.FDConexao.Commit;
    except
      on E : Exception do begin
        Conexao.FDConexao.Rollback;
        Application.MessageBox(PChar(E.Message), PChar(Self.Caption), MB_OK + MB_ICONERROR);
        Exit;
      end;
    end;

    tsConsulta.TabVisible := False;
    tsCadastro.TabVisible := True;

    proc_limpa_campos;

    v_operacao := statInserindo;
    proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSContasBancarias);

    ed_cb_ag_numero.SetFocus;
  finally
    FreeAndNil(c_tabela)
  end;
end;

procedure TFrmCad_ContasBancarias.cmb_con_bancosClick(Sender: TObject);
begin
  inherited;
  proc_carrega_cadastro;
end;

procedure TFrmCad_ContasBancarias.dbgContasBancariasDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if (not Odd(CDSContasBancarias.RecNo)) then begin
    dbgContasBancarias.Canvas.Brush.Color := clSilver;
  end else begin
    dbgContasBancarias.Canvas.Brush.Color := clWindow;
  end;

  if (gdSelected in State) then begin
    dbgContasBancarias.Canvas.Brush.Color := $00FFA74F;
  end;

  dbgContasBancarias.Canvas.FillRect(Rect);
  dbgContasBancarias.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TFrmCad_ContasBancarias.ed_con_cb_agencia_numeroChange(
  Sender: TObject);
begin
  inherited;
  proc_carrega_cadastro;
end;

procedure TFrmCad_ContasBancarias.ed_con_cb_numeroChange(Sender: TObject);
begin
  inherited;
  proc_carrega_cadastro;
end;

procedure TFrmCad_ContasBancarias.FormActivate(Sender: TObject);
var c_bancos : TBancos;
begin
  inherited;
  c_bancos := TBancos.Create;
  try
    c_bancos.proc_carregar_combo(cmb_con_bancos);
    c_bancos.proc_carregar_combo(cmb_cb_ban_codigo);
  finally
    FreeAndNil(c_bancos);
  end;

  proc_carrega_cadastro;

  btnCancelar.Click;
end;

procedure TFrmCad_ContasBancarias.proc_carrega_cadastro;
var c_contas_bancarias : TContasBancarias;
begin
  c_contas_bancarias := TContasBancarias.Create;
  try
    if cmb_con_bancos.ItemIndex > 0 then begin
      c_contas_bancarias.proc_consultar(FDQContasBancarias, CDSContasBancarias, TBancoCombo(cmb_con_bancos.Items.Objects[cmb_con_bancos.ItemIndex]).ban_codigo,
                                        TBancoCombo(cmb_con_bancos.Items.Objects[cmb_con_bancos.ItemIndex]).ban_emp_codigo, ed_con_cb_agencia_numero.Text,
                                        ed_con_cb_numero.Text);
    end else begin
      c_contas_bancarias.proc_consultar(FDQContasBancarias, CDSContasBancarias, 0, 0, ed_con_cb_agencia_numero.Text, ed_con_cb_numero.Text);
    end;
  finally
    FreeAndNil(c_contas_bancarias);
  end;
end;

procedure TFrmCad_ContasBancarias.proc_limpa_campos;
begin
  cmb_cb_tipo.ItemIndex  := 0;
  cmb_cb_ativo.ItemIndex := 1;
  ed_cb_numero.Clear;
  ed_cb_ag_numero.Clear;
  ed_cb_ag_digito.Clear;
  ed_cb_digito.Clear;
end;

end.
