unit mov_agendamentos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf,
  FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.Provider, Data.DB,
  Datasnap.DBClient, DateUtils, cls_utils, Vcl.Menus;

type
  TFrmMov_Agendamentos = class(TFrmAux_Padrao)
    PC: TPageControl;
    tsConsulta: TTabSheet;
    tsCadastro: TTabSheet;
    gbPeriodo: TGroupBox;
    ed_data_inicial: TDateTimePicker;
    Label1: TLabel;
    ed_data_final: TDateTimePicker;
    Label2: TLabel;
    DSAgendamentos: TDataSource;
    CDSAgendamentos: TClientDataSet;
    CDSAgendamentosagd_codigo: TIntegerField;
    CDSAgendamentosagd_emp_codigo: TIntegerField;
    CDSAgendamentosagd_ent_codigo: TIntegerField;
    CDSAgendamentosagd_ent_emp_codigo: TIntegerField;
    CDSAgendamentosagd_fun_ent_codigo: TIntegerField;
    CDSAgendamentosagd_fun_ent_emp_codigo: TIntegerField;
    CDSAgendamentosagd_pro_codigo: TIntegerField;
    CDSAgendamentosagd_pro_emp_codigo: TIntegerField;
    CDSAgendamentosagd_sala_codigo: TIntegerField;
    CDSAgendamentosagd_sala_emp_codigo: TIntegerField;
    CDSAgendamentosagd_data: TDateField;
    CDSAgendamentosagd_hora: TStringField;
    CDSAgendamentosagd_qtde_horas: TBCDField;
    CDSAgendamentosagd_situacao: TIntegerField;
    CDSAgendamentosagd_observacoes: TStringField;
    CDSAgendamentosnome_aluno: TStringField;
    CDSAgendamentosnome_professor: TStringField;
    CDSAgendamentosnome_produto: TStringField;
    CDSAgendamentosdescricao_sala: TStringField;
    PROAgendamentos: TDataSetProvider;
    FDQAgendamentos: TFDQuery;
    FDQAgendamentosagd_codigo: TIntegerField;
    FDQAgendamentosagd_emp_codigo: TIntegerField;
    FDQAgendamentosagd_ent_codigo: TIntegerField;
    FDQAgendamentosagd_ent_emp_codigo: TIntegerField;
    FDQAgendamentosagd_fun_ent_codigo: TIntegerField;
    FDQAgendamentosagd_fun_ent_emp_codigo: TIntegerField;
    FDQAgendamentosagd_pro_codigo: TIntegerField;
    FDQAgendamentosagd_pro_emp_codigo: TIntegerField;
    FDQAgendamentosagd_sala_codigo: TIntegerField;
    FDQAgendamentosagd_sala_emp_codigo: TIntegerField;
    FDQAgendamentosagd_data: TDateField;
    FDQAgendamentosagd_hora: TStringField;
    FDQAgendamentosagd_qtde_horas: TBCDField;
    FDQAgendamentosagd_situacao: TIntegerField;
    FDQAgendamentosagd_observacoes: TStringField;
    dbgAgendamentos: TDBGrid;
    ed_consulta: TEdit;
    Label3: TLabel;
    CDSAgendamentosdia_da_semana: TStringField;
    cmb_agd_emp_codigo: TComboBox;
    gb_ent_codigo: TGroupBox;
    ed_agd_codigo: TEdit;
    GroupBox1: TGroupBox;
    lbl_con_ent_codigo_funcionario: TLabel;
    lbl_con_pro_codigo: TLabel;
    cmb_ent_codigo_funcionario: TComboBox;
    cmb_pro_codigo: TComboBox;
    lbl_con_descricao: TLabel;
    ed_agd_observacoes: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    cmb_sala_codigo: TComboBox;
    mc_agd_data: TMonthCalendar;
    cmb_agd_hora: TComboBox;
    cmb_agd_qtde_hora: TComboBox;
    popOpcoesGrid: TPopupMenu;
    RepetirAgendamento1: TMenuItem;
    pnlRepetirAgendamento: TPanel;
    cmbQtdeRepeticao: TComboBox;
    btnRepetirAgendamento: TBitBtn;
    btnFecharRepeticao: TBitBtn;
    cmbComoAgendar: TComboBox;
    FDQAgendamentosnome_aluno: TStringField;
    FDQAgendamentosnome_professor: TStringField;
    FDQAgendamentosnome_produto: TStringField;
    FDQAgendamentosdescricao_sala: TStringField;
    ed_ent_codigo: TEdit;
    dbgEntidades: TDBGrid;
    cdsEntidades: TClientDataSet;
    dsEntidades: TDataSource;
    cdsEntidadesent_codigo: TIntegerField;
    cdsEntidadesent_emp_codigo: TIntegerField;
    cdsEntidadesent_razao_social: TStringField;
    FDQBuscaEntidades: TFDQuery;
    pnlBuscaEntidades: TPanel;
    dbgBuscaEntdades: TDBGrid;
    dsBuscaEntidades: TDataSource;
    FDQBuscaEntidadesent_codigo: TIntegerField;
    FDQBuscaEntidadesent_emp_codigo: TIntegerField;
    FDQBuscaEntidadesent_razao_social: TStringField;
    FDQBuscaEntidadesent_nome_fantasia: TStringField;
    FDQBuscaEntidadesent_tipo_pessoa: TIntegerField;
    FDQBuscaEntidadesent_cnpj: TStringField;
    FDQBuscaEntidadesent_ie: TStringField;
    FDQBuscaEntidadesent_im: TStringField;
    FDQBuscaEntidadesent_ir: TStringField;
    FDQBuscaEntidadesent_suframa: TStringField;
    FDQBuscaEntidadesent_ramo_codigo: TIntegerField;
    FDQBuscaEntidadesent_cpf: TStringField;
    FDQBuscaEntidadesent_rg: TStringField;
    FDQBuscaEntidadesent_data_nascimento: TDateField;
    FDQBuscaEntidadesent_ocupacao: TStringField;
    FDQBuscaEntidadesent_estado_civil: TStringField;
    FDQBuscaEntidadesent_sit_codigo: TIntegerField;
    FDQBuscaEntidadesent_observacoes: TBlobField;
    btnAddEntidade: TBitBtn;
    btnRetEntidade: TBitBtn;
    procedure CDSAgendamentosCalcFields(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure ed_data_inicialChange(Sender: TObject);
    procedure ed_consultaChange(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnGravarClick(Sender: TObject);
    procedure ed_data_finalChange(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
    procedure mc_agd_dataClick(Sender: TObject);
    procedure cmb_ent_codigo_funcionarioChange(Sender: TObject);
    procedure cmb_sala_codigoChange(Sender: TObject);
    procedure dbgAgendamentosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure RepetirAgendamento1Click(Sender: TObject);
    procedure btnFecharRepeticaoClick(Sender: TObject);
    procedure btnRepetirAgendamentoClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure dbgBuscaEntdadesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dbgEntidadesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure dsEntidadesDataChange(Sender: TObject; Field: TField);
    procedure dbgBuscaEntdadesDblClick(Sender: TObject);
    procedure ed_ent_codigoChange(Sender: TObject);
    procedure btnAddEntidadeClick(Sender: TObject);
  private
    { Private declarations }
    v_operacao : TOperacaoQuery;

    procedure proc_carrega_horarios;
    procedure proc_limpa_campos;
  public
    { Public declarations }
  end;

var
  FrmMov_Agendamentos: TFrmMov_Agendamentos;

implementation

{$R *.dfm}

uses cls_entidades, cls_produtos, cls_salas, cls_agendamentos, Menu, cls_tabelacodigos,
     dm_conexao, cls_empresas, cls_agendamentos_entidade, cls_cc_entidade_aulas;

procedure TFrmMov_Agendamentos.btnAddEntidadeClick(Sender: TObject);
begin
  inherited;
  if (Trim(ed_ent_codigo.Text) <> '') then begin
    cdsEntidades.Insert;
    cdsEntidadesent_codigo.AsInteger      := FDQBuscaEntidadesent_codigo.AsInteger;
    cdsEntidadesent_emp_codigo.AsInteger  := FDQBuscaEntidadesent_emp_codigo.AsInteger;
    cdsEntidadesent_razao_social.AsString := FDQBuscaEntidadesent_razao_social.AsString;
    cdsEntidades.Post;

    ed_ent_codigo.Clear;
  end;
end;

procedure TFrmMov_Agendamentos.btnCancelarClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos;

  tsCadastro.TabVisible := False;
  tsConsulta.TabVisible := True;

  ed_data_inicial.SetFocus;
  ed_data_inicialChange(Self);

  v_operacao := statNavegando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSAgendamentos);
end;

procedure TFrmMov_Agendamentos.btnEditarClick(Sender: TObject);
var c_entidades             : TEntidades;
    c_produtos              : TProdutos;
    c_salas                 : TSalas;
    c_empresas              : TEmpresas;
    c_agendamentos_entidade : TAgendamentos_entidade;
begin
  inherited;
  c_empresas              := TEmpresas.Create;
  c_entidades             := TEntidades.Create;
  c_produtos              := TProdutos.Create;
  c_salas                 := TSalas.Create;
  c_agendamentos_entidade := TAgendamentos_entidade.Create;

  try
    ed_agd_codigo.Text                   := CDSAgendamentosagd_codigo.AsString;
    cmb_agd_emp_codigo.ItemIndex         := cmb_agd_emp_codigo.Items.IndexOf(c_empresas.func_retorna_razao_combo(CDSAgendamentosagd_emp_codigo.AsInteger));
    cmb_ent_codigo_funcionario.ItemIndex := cmb_ent_codigo_funcionario.Items.IndexOf(c_entidades.func_retorna_nome_combo(CDSAgendamentosagd_fun_ent_codigo.AsInteger, CDSAgendamentosagd_fun_ent_emp_codigo.AsInteger));
    cmb_pro_codigo.ItemIndex             := cmb_pro_codigo.Items.IndexOf(c_produtos.func_retorna_nome_combo(CDSAgendamentosagd_pro_codigo.AsInteger, CDSAgendamentosagd_pro_emp_codigo.AsInteger));
    cmb_sala_codigo.ItemIndex            := cmb_sala_codigo.Items.IndexOf(c_salas.func_retorna_nome_combo(CDSAgendamentosagd_sala_codigo.AsInteger, CDSAgendamentosagd_sala_emp_codigo.AsInteger));
    ed_agd_observacoes.Text              := CDSAgendamentosagd_observacoes.AsString;
    mc_agd_data.Date                     := CDSAgendamentosagd_data.AsDateTime;
    cmb_agd_hora.ItemIndex               := cmb_agd_hora.Items.IndexOf(CDSAgendamentosagd_hora.AsString);
    cmb_agd_qtde_hora.Text               := CDSAgendamentosagd_qtde_horas.AsString;

    c_agendamentos_entidade.proc_carrega_edicao(CDSAgendamentosagd_codigo.AsInteger, CDSAgendamentosagd_emp_codigo.AsInteger, cdsEntidades);
  finally
    FreeAndNil(c_empresas);
    FreeAndNil(c_entidades);
    FreeAndNil(c_produtos);
    FreeAndNil(c_salas);
    FreeAndNil(c_agendamentos_entidade);
  end;

  tsCadastro.TabVisible := True;
  tsConsulta.TabVisible := False;

  v_operacao := statEditando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSAgendamentos);
end;

procedure TFrmMov_Agendamentos.btnExcluirClick(Sender: TObject);
var c_agendamentos : TAgendamentos;
begin
  inherited;
  if Application.MessageBox(PChar('Deseja realmente excluir!'), PChar('Aten��o'), MB_YESNO + MB_ICONQUESTION) = mrNo then begin
    Exit;
  end;

  c_agendamentos := TAgendamentos.Create;
  try
    try
      Conexao.FDConexao.StartTransaction;
      c_agendamentos.proc_excluir(CDSAgendamentosagd_codigo.AsInteger, CDSAgendamentosagd_emp_codigo.AsInteger);
      Conexao.FDConexao.Commit;

      Application.MessageBox(PChar('Agendamento exclu�do!'), PChar('Aten��o'), MB_OK + MB_ICONINFORMATION);
      ed_data_inicialChange(Self);
    except
      on E : Exception do begin
        Conexao.FDConexao.Rollback;
        Application.MessageBox(PChar(E.Message), PChar('Aten��o'), MB_OK + MB_ICONERROR);
      end;
    end;
  finally
    FreeAndNil(c_agendamentos);
  end;
end;

procedure TFrmMov_Agendamentos.btnFecharRepeticaoClick(Sender: TObject);
begin
  inherited;
  pnlRepetirAgendamento.Visible := False;
  dbgAgendamentos.Enabled       := True;
end;

procedure TFrmMov_Agendamentos.btnGravarClick(Sender: TObject);
var c_agendamentos          : TAgendamentos;
    c_agendamentos_entidade : TAgendamentos_entidade;
    c_cc_entidade_aulas     : TCC_entidade_aulas;
begin
  inherited;
  if cmb_ent_codigo_funcionario.ItemIndex = 0 then begin
    Application.MessageBox(PChar('Favor informar o usu�rio da sala!'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
    cmb_ent_codigo_funcionario.SetFocus;
    Exit;
  end;

  c_agendamentos          := TAgendamentos.Create;
  c_agendamentos_entidade := TAgendamentos_entidade.Create;
  c_cc_entidade_aulas     := TCC_entidade_aulas.Create;

  try
    Conexao.FDConexao.StartTransaction;

    try
      c_agendamentos.agd_codigo     := StrToInt(ed_agd_codigo.Text);
      c_agendamentos.agd_emp_codigo := TEmpresasCombo(cmb_agd_emp_codigo.Items.Objects[cmb_agd_emp_codigo.ItemIndex]).emp_codigo;

      if (cmb_ent_codigo_funcionario.ItemIndex > 0) then begin
        c_agendamentos.agd_fun_ent_codigo     := TEntidadeCombo(cmb_ent_codigo_funcionario.Items.Objects[cmb_ent_codigo_funcionario.ItemIndex]).ent_codigo;
        c_agendamentos.agd_fun_ent_emp_codigo := TEntidadeCombo(cmb_ent_codigo_funcionario.Items.Objects[cmb_ent_codigo_funcionario.ItemIndex]).ent_emp_codigo;
      end;

      if (cmb_pro_codigo.ItemIndex > 0) then begin
        c_agendamentos.agd_pro_codigo     := TProdutosCombo(cmb_pro_codigo.Items.Objects[cmb_pro_codigo.ItemIndex]).pro_codigo;
        c_agendamentos.agd_pro_emp_codigo := TProdutosCombo(cmb_pro_codigo.Items.Objects[cmb_pro_codigo.ItemIndex]).pro_emp_codigo;
      end;

      if (cmb_sala_codigo.ItemIndex > 0) then begin
        c_agendamentos.agd_sala_codigo     := TSalaCombo(cmb_sala_codigo.Items.Objects[cmb_sala_codigo.ItemIndex]).sala_codigo;
        c_agendamentos.agd_sala_emp_codigo := TSalaCombo(cmb_sala_codigo.Items.Objects[cmb_sala_codigo.ItemIndex]).sala_emp_codigo;
      end;

      c_agendamentos.agd_data        := mc_agd_data.Date;
      c_agendamentos.agd_hora        := cmb_agd_hora.Items.Strings[cmb_agd_hora.ItemIndex];
      c_agendamentos.agd_qtde_horas  := StrToFloatDef(cmb_agd_qtde_hora.Items.Strings[cmb_agd_qtde_hora.ItemIndex], 0);
      c_agendamentos.agd_situacao    := 0;
      c_agendamentos.agd_observacoes := Trim(ed_agd_observacoes.Text);
      c_agendamentos.agd_hora_final  := FormatDateTime('HH:MM', StrToDateTime(FormatDateTime('DD/MM/YYYY', mc_agd_data.Date) + ' ' + cmb_agd_hora.Items.Strings[cmb_agd_hora.ItemIndex]) + (c_agendamentos.agd_qtde_horas / 24));
      c_agendamentos.proc_gravar(c_agendamentos.agd_codigo, c_agendamentos.agd_emp_codigo);

      cdsEntidades.First;
      while (not cdsEntidades.Eof) do begin
        c_agendamentos_entidade.agd_codigo     := c_agendamentos.agd_codigo;
        c_agendamentos_entidade.agd_emp_codigo := c_agendamentos.agd_emp_codigo;
        c_agendamentos_entidade.ent_codigo     := cdsEntidadesent_codigo.AsInteger;
        c_agendamentos_entidade.ent_emp_codigo := cdsEntidadesent_emp_codigo.AsInteger;

        c_agendamentos_entidade.proc_gravar;

        c_cc_entidade_aulas.func_recuperar(cdsEntidadesent_codigo.AsInteger, cdsEntidadesent_emp_codigo.AsInteger, c_agendamentos.agd_pro_codigo, c_agendamentos.agd_pro_emp_codigo);

        c_cc_entidade_aulas.cca_ent_codigo     := cdsEntidadesent_codigo.AsInteger;
        c_cc_entidade_aulas.cca_ent_emp_codigo := cdsEntidadesent_emp_codigo.AsInteger;
        c_cc_entidade_aulas.cca_pro_codigo     := c_agendamentos.agd_pro_codigo;
        c_cc_entidade_aulas.cca_pro_emp_codigo := c_agendamentos.agd_pro_emp_codigo;
        c_cc_entidade_aulas.cca_saldo_horas    := c_cc_entidade_aulas.cca_saldo_horas - c_agendamentos.agd_qtde_horas;

        c_cc_entidade_aulas.proc_gravar;

        cdsEntidades.Next;
      end;
    finally
      FreeAndNil(c_agendamentos);
    end;

    Conexao.FDConexao.Commit;

    Application.MessageBox(PChar('Agendamento realizado com sucesso!'), PChar('Aten��o'), MB_OK + MB_ICONINFORMATION);

    btnCancelar.Click;
  except
    on E : Exception do begin
      Conexao.FDConexao.Rollback;
      Application.MessageBox(PChar(E.Message), PChar('Aten��o'), MB_OK + MB_ICONERROR);
    end;
  end;
end;

procedure TFrmMov_Agendamentos.btnNovoClick(Sender: TObject);
var c_tabela : TTabelaCodigos;
begin
  inherited;
  try
    Conexao.FDConexao.StartTransaction;

    ed_agd_codigo.Text := c_tabela.func_gerar_codigo('agendamentos', IntToStr(TEmpresasCombo(cmb_agd_emp_codigo.Items.Objects[cmb_agd_emp_codigo.ItemIndex]).emp_codigo)).ToString;
    cmb_agd_emp_codigo.ItemIndex := cmb_agd_emp_codigo.Items.IndexOf(frmMenu.razao_empresa);

    Conexao.FDConexao.Commit;
  except
    on E : Exception do begin
      Conexao.FDConexao.Rollback;
      Application.MessageBox(PChar(E.Message), PChar(Self.Caption), MB_OK + MB_ICONERROR);
      Exit;
    end;
  end;

  tsConsulta.TabVisible := False;
  tsCadastro.TabVisible := True;

  proc_limpa_campos;

  v_operacao := statInserindo;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSAgendamentos);
end;

procedure TFrmMov_Agendamentos.btnRepetirAgendamentoClick(Sender: TObject);
var c_agendamentos : TAgendamentos;
begin
  inherited;
  if Application.MessageBox(PChar('Deseja realmente repetir o agendamento selecionado?'), PChar('Aten��o'), MB_YESNO + MB_ICONQUESTION) = mrYes then begin
    c_agendamentos := TAgendamentos.Create;
    try
      try
        Conexao.FDConexao.StartTransaction;

        c_agendamentos.proc_repetir_agendamento(CDSAgendamentosagd_codigo.AsInteger, CDSAgendamentosagd_emp_codigo.AsInteger, cmbComoAgendar.ItemIndex, StrToIntDef(cmbQtdeRepeticao.Items.Strings[cmbQtdeRepeticao.ItemIndex], 0));

        Conexao.FDConexao.Commit;

        Application.MessageBox(PChar('Agendamentos gravados com sucesso!'), PChar('Aten��o'), MB_YESNO + MB_ICONINFORMATION);
      except
        on E : Exception do begin
          Conexao.FDConexao.Rollback;
          Application.MessageBox(PChar(E.Message), PChar('Aten��o'), MB_YESNO + MB_ICONINFORMATION);
        end;
      end;
    finally
      FreeAndNil(c_agendamentos);
    end;
  end;
end;

procedure TFrmMov_Agendamentos.CDSAgendamentosCalcFields(DataSet: TDataSet);
begin
  inherited;
  CDSAgendamentosdia_da_semana.AsString := func_dia_da_semana(DayOfWeek(CDSAgendamentosagd_data.AsDateTime));
end;

procedure TFrmMov_Agendamentos.cmb_ent_codigo_funcionarioChange(
  Sender: TObject);
begin
  inherited;
  mc_agd_dataClick(Self);
end;

procedure TFrmMov_Agendamentos.cmb_sala_codigoChange(Sender: TObject);
begin
  inherited;
  mc_agd_dataClick(Self);
end;

procedure TFrmMov_Agendamentos.dbgAgendamentosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if (not Odd(CDSAgendamentos.RecNo)) then begin
    dbgAgendamentos.Canvas.Brush.Color := clSilver;
  end else begin
    dbgAgendamentos.Canvas.Brush.Color := clWindow;
  end;

  if (gdSelected in State) then begin
    dbgAgendamentos.Canvas.Brush.Color := $00FFA74F;
  end;

  dbgAgendamentos.Canvas.FillRect(Rect);
  dbgAgendamentos.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TFrmMov_Agendamentos.dbgBuscaEntdadesDblClick(Sender: TObject);
begin
  inherited;
  ed_ent_codigo.Text := FDQBuscaEntidadesent_razao_social.AsString;

  btnAddEntidade.Click;
end;

procedure TFrmMov_Agendamentos.dbgBuscaEntdadesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if (not Odd(FDQBuscaEntidades.RecNo)) then begin
    dbgBuscaEntdades.Canvas.Brush.Color := clSilver;
  end else begin
    dbgBuscaEntdades.Canvas.Brush.Color := clWindow;
  end;

  if (gdSelected in State) then begin
    dbgBuscaEntdades.Canvas.Brush.Color := $00FFA74F;
  end;

  dbgBuscaEntdades.Canvas.FillRect(Rect);
  dbgBuscaEntdades.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TFrmMov_Agendamentos.dbgEntidadesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  inherited;
  if (not Odd(cdsEntidades.RecNo)) then begin
    dbgEntidades.Canvas.Brush.Color := clSilver;
  end else begin
    dbgEntidades.Canvas.Brush.Color := clWindow;
  end;

  if (gdSelected in State) then begin
    dbgEntidades.Canvas.Brush.Color := $00FFA74F;
  end;

  dbgEntidades.Canvas.FillRect(Rect);
  dbgEntidades.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TFrmMov_Agendamentos.dsEntidadesDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  btnRetEntidade.Enabled := (cdsEntidades.RecordCount > 0);
end;

procedure TFrmMov_Agendamentos.ed_consultaChange(Sender: TObject);
var c_agendamentos : TAgendamentos;
begin
  inherited;
  c_agendamentos := TAgendamentos.Create;
  try
    c_agendamentos.proc_consultar(FDQAgendamentos, CDSAgendamentos, frmMenu.codigo_empresa, 0, 0, 0, 0, 0, 0, 0, 0, ed_data_inicial.Date, ed_data_final.Date, ed_consulta.Text);
  finally
    FreeAndNil(c_agendamentos);
  end;
end;

procedure TFrmMov_Agendamentos.ed_data_finalChange(Sender: TObject);
begin
  inherited;
  ed_data_inicialChange(Self);
end;

procedure TFrmMov_Agendamentos.ed_data_inicialChange(Sender: TObject);
var c_agendamentos : TAgendamentos;
begin
  inherited;
  if ed_data_final.Date < ed_data_inicial.Date then begin
    ed_data_final.Date := ed_data_inicial.Date;
  end;

  c_agendamentos := TAgendamentos.Create;
  try
    c_agendamentos.proc_consultar(FDQAgendamentos, CDSAgendamentos, frmMenu.codigo_empresa, 0, 0, 0, 0, 0, 0, 0, 0, Trunc(ed_data_inicial.Date), Trunc(ed_data_final.Date), ed_consulta.Text);
  finally
    FreeAndNil(c_agendamentos);
  end;

  v_operacao := statNavegando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSAgendamentos);
end;

procedure TFrmMov_Agendamentos.ed_ent_codigoChange(Sender: TObject);
begin
  inherited;
  if Trim(ed_ent_codigo.Text) <> '' then begin
    pnlBuscaEntidades.Top     := ed_ent_codigo.Top + ed_ent_codigo.Height + 1;
    pnlBuscaEntidades.Left    := ed_ent_codigo.Left;
    pnlBuscaEntidades.Visible := True;

    FDQBuscaEntidades.Filtered := False;
    FDQBuscaEntidades.Filter   := 'ent_razao_social like ' + QuotedStr('%' + Trim(ed_ent_codigo.Text) + '%');
    FDQBuscaEntidades.Filtered := True;
  end else begin
    pnlBuscaEntidades.Visible  := False;
    FDQBuscaEntidades.Filtered := False;
    FDQBuscaEntidades.Filter   := '';
  end;
end;

procedure TFrmMov_Agendamentos.FormActivate(Sender: TObject);
var c_agendamentos : TAgendamentos;
    c_entidades    : TEntidades;
    c_produtos     : TProdutos;
    c_empresas     : TEmpresas;
    c_salas        : TSalas;

    i_idx : Integer;

    dt_aux : TDateTime;
begin
  inherited;
  c_empresas     := TEmpresas.Create;
  c_agendamentos := TAgendamentos.Create;
  c_entidades    := TEntidades.Create;
  c_produtos     := TProdutos.Create;
  c_salas        := TSalas.Create;

  try
    c_empresas.proc_carregar_combo(cmb_agd_emp_codigo);
    c_agendamentos.proc_consultar(FDQAgendamentos, CDSAgendamentos, frmMenu.codigo_empresa, 0, 0, 0, 0, 0, 0, 0, 0, ed_data_inicial.Date, ed_data_final.Date, ed_consulta.Text);
    c_entidades.proc_carregar_combo(cmb_ent_codigo_funcionario, 2, True);
    c_produtos.proc_carregar_combo(cmb_pro_codigo);
    c_salas.proc_carregar_combo(cmb_sala_codigo);
  finally
    FreeAndNil(c_empresas);
    FreeAndNil(c_agendamentos);
    FreeAndNil(c_entidades);
    FreeAndNil(c_produtos);
    FreeAndNil(c_empresas);
    FreeAndNil(c_salas);
  end;

  proc_carrega_horarios;

  ed_data_inicial.Date := Date;
  ed_data_final.Date   := Date;

//  if Now > StrToDateTime(FormatDateTime('dd/mm/yyyy ', Now) + '20:00') then begin
//    mc_agd_data.MinDate := Date + 1;
//  end else begin
//    mc_agd_data.MinDate := Date;
//  end;

  btnCancelar.Click;
 end;

procedure TFrmMov_Agendamentos.mc_agd_dataClick(Sender: TObject);
begin
  inherited;
  proc_carrega_horarios;
end;

procedure TFrmMov_Agendamentos.proc_carrega_horarios;
var i_idx  : Integer;
    dt_aux : TDateTime;
    s_aux  : String;

    c_agendamentos : TAgendamentos;
begin
  cmb_agd_hora.Items.Clear;

  if Date = mc_agd_data.Date then begin
    s_aux := FormatDateTime('HH24:', Now) + '00';
    if StrToInt(FormatDateTime('MM', Now)) < 30 then begin
      s_aux := FormatDateTime('HH24:', Now) + '30';
    end;
  end else begin
    s_aux := '06:00';
  end;

  dt_aux := StrToDAteTime(FormatDateTime('dd/mm/yyyy', mc_agd_data.Date) + s_aux);
  cmb_agd_hora.Items.Add(s_aux);
  for i_idx := 1 to 56 do begin
    dt_aux := dt_aux + ((30/60)/24);
    cmb_agd_hora.Items.Add(FormatDateTime('HH:MM', dt_aux));

    if FormatDateTime('HH:MM', dt_aux) = '22:00' then begin
      Break;
    end;
  end;

  c_agendamentos := TAgendamentos.Create;
  try
    if (cmb_ent_codigo_funcionario.ItemIndex > 0) and (cmb_sala_codigo.ItemIndex > 0) then begin
      c_agendamentos.func_valida_data(cmb_agd_hora, TEmpresasCombo(cmb_agd_emp_codigo.Items.Objects[cmb_agd_emp_codigo.ItemIndex]).emp_codigo, dt_aux,
                                      iif(cmb_ent_codigo_funcionario.ItemIndex > 0, TEntidadeCombo(cmb_ent_codigo_funcionario.Items.Objects[cmb_ent_codigo_funcionario.ItemIndex]).ent_codigo, 0),
                                      iif(cmb_ent_codigo_funcionario.ItemIndex > 0, TEntidadeCombo(cmb_ent_codigo_funcionario.Items.Objects[cmb_ent_codigo_funcionario.ItemIndex]).ent_emp_codigo, 0),
                                      iif(cmb_sala_codigo.ItemIndex > 0, TSalaCombo(cmb_sala_codigo.Items.Objects[cmb_sala_codigo.ItemIndex]).sala_codigo, 0),
                                      iif(cmb_sala_codigo.ItemIndex > 0, TSalaCombo(cmb_sala_codigo.Items.Objects[cmb_sala_codigo.ItemIndex]).sala_emp_codigo, 0));
    end else begin
      if (cmb_ent_codigo_funcionario.ItemIndex > 0) and (cmb_sala_codigo.ItemIndex <= 0) then begin
        c_agendamentos.func_valida_data(cmb_agd_hora, TEmpresasCombo(cmb_agd_emp_codigo.Items.Objects[cmb_agd_emp_codigo.ItemIndex]).emp_codigo, dt_aux,
                                        iif(cmb_ent_codigo_funcionario.ItemIndex > 0, TEntidadeCombo(cmb_ent_codigo_funcionario.Items.Objects[cmb_ent_codigo_funcionario.ItemIndex]).ent_codigo, 0),
                                        iif(cmb_ent_codigo_funcionario.ItemIndex > 0, TEntidadeCombo(cmb_ent_codigo_funcionario.Items.Objects[cmb_ent_codigo_funcionario.ItemIndex]).ent_emp_codigo, 0),
                                        0, 0);
      end else begin
        if (cmb_ent_codigo_funcionario.ItemIndex <= 0) and (cmb_sala_codigo.ItemIndex > 0) then begin
          c_agendamentos.func_valida_data(cmb_agd_hora, TEmpresasCombo(cmb_agd_emp_codigo.Items.Objects[cmb_agd_emp_codigo.ItemIndex]).emp_codigo, dt_aux, 0, 0,
                                          iif(cmb_sala_codigo.ItemIndex > 0, TSalaCombo(cmb_sala_codigo.Items.Objects[cmb_sala_codigo.ItemIndex]).sala_codigo, 0),
                                          iif(cmb_sala_codigo.ItemIndex > 0, TSalaCombo(cmb_sala_codigo.Items.Objects[cmb_sala_codigo.ItemIndex]).sala_emp_codigo, 0));
        end;
      end;
    end;
  finally
    FreeAndNil(c_agendamentos);
  end;
end;

procedure TFrmMov_Agendamentos.proc_limpa_campos;
var c_entidades : TEntidades;
begin
  cdsEntidades.Close;
  cdsEntidades.CreateDataSet;

  c_entidades := TEntidades.Create;
  try
    c_entidades.func_carregar_busca(FDQBuscaEntidades);
  finally
    FreeAndNil(c_entidades);
  end;

  ed_ent_codigo.Clear;
  cmb_pro_codigo.ItemIndex := 0;
  cmb_ent_codigo_funcionario.ItemIndex := 0;
  cmb_sala_codigo.ItemIndex := 0;
  ed_agd_observacoes.Clear;
  mc_agd_data.Date := Date;
  cmb_agd_hora.ItemIndex := 0;
  cmb_agd_qtde_hora.ItemIndex := 1;

  pnlRepetirAgendamento.Visible := False;
  dbgAgendamentos.Enabled := True;
end;

procedure TFrmMov_Agendamentos.RepetirAgendamento1Click(Sender: TObject);
begin
  inherited;
  dbgAgendamentos.Enabled := False;

  pnlRepetirAgendamento.Top  := Trunc((Self.Height / 2) - (pnlRepetirAgendamento.Height / 2));
  pnlRepetirAgendamento.Left := Trunc((Self.Width / 2) - (pnlRepetirAgendamento.Width / 2));
  pnlRepetirAgendamento.Visible := True;
end;

end.
