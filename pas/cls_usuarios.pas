unit cls_usuarios;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils;

type
  TUsuarios = Class(TObject)
  private
    { Private declarations }
    f_usu_codigo : Integer;
    f_usu_nome   : String;
    f_usu_login  : String;
    f_usu_senha  : String;
    f_usu_nivel  : Integer;
    f_usu_status : Integer;
  public
    { Public declarations }
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PComboCampos : TComboBox; PConsulta : String; PStatus : Integer);
    procedure proc_gravar(PCodigo : Integer);
    function func_prox_codigo : Integer;
    function func_recuperar(PCodigo : Integer) : Boolean;
    function func_recuperar_login(PLogin : String) : Boolean;
  published
    { Published declarations }
    property usu_codigo : Integer read f_usu_codigo write f_usu_codigo;
    property usu_nome   : String  read f_usu_nome   write f_usu_nome;
    property usu_login  : String  read f_usu_login  write f_usu_login;
    property usu_senha  : String  read f_usu_senha  write f_usu_senha;
    property usu_nivel  : Integer read f_usu_nivel  write f_usu_nivel;
    property usu_status : Integer read f_usu_status write f_usu_status;
  end;


implementation

{ TUsuarios }

procedure TUsuarios.proc_gravar(PCodigo: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                           '+
                   ' from usuarios u                    '+
                   ' where u.usu_codigo = :p_usu_codigo ');
    qryAux.ParamByName('p_usu_codigo').AsInteger := PCodigo;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into usuarios ( '+
                     '   usu_codigo,          '+
                     '   usu_nome  ,          '+
                     '   usu_login ,          '+
                     '   usu_senha ,          '+
                     '   usu_nivel ,          '+
                     '   usu_status           '+
                     ' ) values (             '+
                     '   :p_usu_codigo,       '+
                     '   :p_usu_nome  ,       '+
                     '   :p_usu_login ,       '+
                     '   :p_usu_senha ,       '+
                     '   :p_usu_nivel ,       '+
                     '   :p_usu_status)       ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update usuarios                  '+
                     ' set usu_nome   = :p_usu_nome  ,  '+
                     '     usu_login  = :p_usu_login ,  '+
                     '     usu_senha  = :p_usu_senha ,  '+
                     '     usu_nivel  = :p_usu_nivel ,  '+
                     '     usu_status = :p_usu_status   '+
                     ' where usu_codigo = :p_usu_codigo ');
    end;

    qryAux.ParamByName('p_usu_codigo').AsInteger := PCodigo;
    qryAux.ParamByName('p_usu_nome').AsString    := usu_nome;
    qryAux.ParamByName('p_usu_login').AsString   := usu_login;
    qryAux.ParamByName('p_usu_senha').AsString   := usu_senha;
    qryAux.ParamByName('p_usu_nivel').AsInteger  := usu_nivel;
    qryAux.ParamByName('p_usu_status').AsInteger := usu_status;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

function TUsuarios.func_prox_codigo: Integer;
var qryAux : TFDQuery;
begin
  Result := 0;

  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select coalesce(max(u.usu_codigo), 0) usu_codigo '+
                   ' from usuarios u                                  ');
    qryAux.Open;

    Result := qryAux.FieldByName('usu_codigo').AsInteger + 1;
  finally
    FreeAndNil(qryAux);
  end;
end;

function TUsuarios.func_recuperar(PCodigo: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                           '+
                   ' from usuarios u                    '+
                   ' where u.usu_codigo = :p_usu_codigo ');
    qryAux.ParamByName('p_usu_codigo').AsInteger := PCodigo;
    qryAux.Open;

    Result := (not qryAux.Eof);

    usu_codigo := qryAux.FieldByName('usu_codigo').AsInteger;
    usu_nome   := qryAux.FieldByName('usu_nome').AsString;
    usu_login  := qryAux.FieldByName('usu_login').AsString;
    usu_senha  := qryAux.FieldByName('usu_senha').AsString;
    usu_nivel  := qryAux.FieldByName('usu_nivel').AsInteger;
    usu_status := qryAux.FieldByName('usu_status').AsInteger;
  finally
    FreeAndNil(qryAux);
  end;
end;

function TUsuarios.func_recuperar_login(PLogin: String): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select u.usu_codigo              '+
                   ' from usuarios u                  '+
                   ' where u.usu_login = :p_usu_login ');
    qryAux.ParamByName('p_usu_login').AsString := PLogin;
    qryAux.Open;

    Result := func_recuperar(qryAux.FieldByName('usu_codigo').AsInteger);
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TUsuarios.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PComboCampos: TComboBox; PConsulta: String;
  PStatus: Integer);
begin
  PQuery.Connection := Conexao.FDConexao;

  PClient.Close;
  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *        '+
                 ' from usuarios u '+
                 ' where           ');

  case PComboCampos.ItemIndex of
    0 : PQuery.SQL.Add(' usu_codigo like concat(''%'', :p_consulta, ''%'') ');
    1 : PQuery.SQL.Add(' usu_nome like concat(''%'', :p_consulta, ''%'') ');
    2 : PQuery.SQL.Add(' usu_login like concat(''%'', :p_consulta, ''%'') ');
  end;

  PQuery.SQL.Add(' and (u.usu_status = :p_usu_status or :p_usu_status = 2) ');
  PQuery.ParamByName('p_consulta').AsString    := PConsulta;
  PQuery.ParamByName('p_usu_status').AsInteger := PStatus;
  PClient.Open;
end;

end.
