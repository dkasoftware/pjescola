unit cls_utils;

interface

uses Aux_Padrao, Datasnap.DBClient, Forms, Variants, SysUtils, StrUtils,
     IdCoder, IdCoder3to4, IdCoderMIME, Controls, Winapi.Windows,
     Winapi.Messages, Registry, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons;

type TOperacaoQuery = (statInserindo, statEditando, statNavegando);

procedure proc_atualiza_operacao_query(var PForm : TForm; POperacao : TOperacaoQuery; PClient : TClientDataSet); Overload;
procedure proc_drawcontrol(PControl : TWinControl);
procedure proc_limpa_combo(var PComboBox : TComboBox);
procedure proc_atualiza_operacao_query(var PPanel : TPanel; POperacao : TOperacaoQuery; PClient : TClientDataSet); Overload;

function iif(PCondicao : Boolean; PResultadoVerdadeiro : Variant; PResultadoFalso : Variant) : Variant; Overload;
function iif(PCondicao : Boolean; PResultadoVerdadeiro : Variant) : Variant; OVerload;
function func_base64decode(const PString : AnsiString) : AnsiString;
function func_base64encode(const PString : AnsiString) : AnsiString;
function func_getcomputer : String;
function func_validaCPF(PCPF : String) : Boolean;
function func_limpa_mascara(PTexto : String) : String;
function func_dia_da_semana(PDia : Integer) : String;
function func_valor_por_extenso(vlr : Real) : String;

implementation

procedure proc_atualiza_operacao_query(var PForm : TForm; POperacao : TOperacaoQuery; PClient : TClientDataSet); Overload
begin
  TFrmAux_Padrao(PForm).btnNovo.Enabled     := (POperacao = statNavegando);
  TFrmAux_Padrao(PForm).btnGravar.Enabled   := (POperacao = statInserindo) or (POperacao = statEditando);
  TFrmAux_Padrao(PForm).btnEditar.Enabled   := (POperacao = statNavegando) and (not PClient.Eof);
  TFrmAux_Padrao(PForm).btnCancelar.Enabled := (POperacao = statInserindo) or (POperacao = statEditando);
  TFrmAux_Padrao(PForm).btnExcluir.Enabled  := (POperacao = statNavegando) and (not PClient.Eof);
end;

function iif(PCondicao : Boolean; PResultadoVerdadeiro : Variant; PResultadoFalso : Variant) : Variant;
begin
  if PCondicao then begin
    Result := PResultadoVerdadeiro;
  end else begin
    Result := PResultadoFalso;
  end;
end;

function iif(PCondicao : Boolean; PResultadoVerdadeiro : Variant) : Variant; OVerload;
begin
  Result := Null;
  if PCondicao then begin
    Result := PResultadoVerdadeiro;
  end;
end;

function func_base64decode(const PString : AnsiString) : AnsiString;
var x_decoder : TIdDecoderMime;
begin
  x_decoder := TIdDecoderMime.Create(nil);

  try
    Result := x_decoder.DecodeString(PString);
  finally
    FreeAndNil(x_decoder)
  end
end;

function func_base64encode(const PString : AnsiString) : AnsiString;
var x_encoder : TIdEncoderMime;
begin
  x_encoder := TIdEncoderMime.Create(nil);

  try
    Result := x_encoder.EncodeString(PString);
  finally
    FreeAndNil(x_encoder);
  end
end;

procedure proc_drawcontrol(PControl : TWinControl);
var x_rect : TRect;
    x_hrgn : HRGN;
begin
  x_rect := PControl.ClientRect;
  x_hrgn := CreateRoundRectRgn(x_rect.Left, x_rect.Top, x_rect.Right, x_rect.Bottom, 10, 10) ;

  PControl.Perform(EM_GETRECT, 0, lParam(@x_rect)) ;
  InflateRect(x_rect, - 4, - 4) ;
  PControl.Perform(EM_SETRECTNP, 0, lParam(@x_rect)) ;
  SetWindowRgn(PControl.Handle, x_hrgn, True) ;
  PControl.Invalidate;
end;

function func_getcomputer : String;
var ch_computer : PChar;
    w_size      : DWORD;
begin
  ch_computer := #0;
  w_size      := MAX_COMPUTERNAME_LENGTH + 1;

  try
    GetMem(ch_computer, w_size);
    if Winapi.Windows.GetComputerName(ch_computer, w_size) then begin
      Result := ch_computer;
    end;
  finally
    FreeMem(ch_computer, w_size);
  end;
end;

procedure proc_limpa_combo(var PComboBox : TComboBox);
var i_nro : Integer;
begin
  for i_nro := 0 to PComboBox.Items.Count - 1 do begin
    if PComboBox.Items.Objects[i_nro] <> nil then begin
      PComboBox.Items.Objects[i_nro].Destroy;
    end;
  end;
  PComboBox.Items.Clear;
end;

procedure proc_atualiza_operacao_query(var PPanel : TPanel; POperacao : TOperacaoQuery; PClient : TClientDataSet); Overload;
var i_nro : Integer;
begin
  for i_nro := 0 to PPanel.ControlCount - 1 do begin
    if (Pos('Novo'    , TBitBtn(PPanel.Controls[i_nro]).Name) > 0) then TBitBtn(PPanel.Controls[i_nro]).Enabled := (POperacao = statNavegando)                               else
    if (Pos('Gravar'  , TBitBtn(PPanel.Controls[i_nro]).Name) > 0) then TBitBtn(PPanel.Controls[i_nro]).Enabled := (POperacao = statInserindo) or (POperacao = statEditando) else
    if (Pos('Editar'  , TBitBtn(PPanel.Controls[i_nro]).Name) > 0) then TBitBtn(PPanel.Controls[i_nro]).Enabled := (POperacao = statNavegando) and (not PClient.Eof)         else
    if (Pos('Cancelar', TBitBtn(PPanel.Controls[i_nro]).Name) > 0) then TBitBtn(PPanel.Controls[i_nro]).Enabled := (POperacao = statInserindo) or (POperacao = statEditando) else
    if (Pos('Excluir' , TBitBtn(PPanel.Controls[i_nro]).Name) > 0) then TBitBtn(PPanel.Controls[i_nro]).Enabled := (POperacao = statNavegando) and (not PClient.Eof);
  end;
end;

function func_validaCPF(PCPF : String) : Boolean;
var w     : array[0..1] of Word;
    b_CPF : array[0..10] of Byte;
    b     : Byte;
begin
  try
    for b := 1 to 11 do begin
      b_CPF[b - 1] := StrToInt(PCPF[b]);
    end;

    w[0] := 10 * b_CPF[0] + 9 * b_CPF[1] + 8 * b_CPF[2];
    w[0] := w[0] + 7 * b_CPF[3] + 6 * b_CPF[4] + 5 * b_CPF[5];
    w[0] := w[0] + 4 * b_CPF[6] + 3 * b_CPF[7] + 2 * b_CPF[8];
    w[0] := 11 - w[0] mod 11;
    w[0] := iif(w[0] >= 10, 0, w[0]);

    w[1] := 11 * b_CPF[0] + 10 * b_CPF[1] + 9 * b_CPF[2];
    w[1] := w[1] + 8 * b_CPF[3] + 7 * b_CPF[4] + 6 * b_CPF[5];
    w[1] := w[1] + 5 * b_CPF[6] + 4 * b_CPF[7] + 3 * b_CPF[8];
    w[1] := w[1] + 2 * w[0];
    w[1] := 11 - w[1] mod 11;
    w[1] := iif(w[1] >= 10, 0, w[1]);

    Result :=  ((w[0] = b_CPF[9]) and (w[1] = b_CPF[10]));
  except
    on E: Exception do begin
      Result := False;
    end;
  end;
end;

function func_limpa_mascara(PTexto : String) : String;
begin
  Result := StringReplace(PTexto, '.', '', [rfReplaceAll]);
  Result := StringReplace(Result, '-', '', [rfReplaceAll]);
  Result := StringReplace(Result, ' ', '', [rfReplaceAll]);
  Result := StringReplace(Result, ',', '', [rfReplaceAll]);
  Result := StringReplace(Result, '_', '', [rfReplaceAll]);
end;

function func_dia_da_semana(PDia : Integer) : String;
begin
  case PDia of
    1 : Result := 'Domingo';
    2 : Result := 'Segunda-Feira';
    3 : Result := 'Ter�a-Feira';
    4 : Result := 'Quarta-Feira';
    5 : Result := 'Quinta-Feira';
    6 : Result := 'Sexta-Feira';
    7 : Result := 'S�bado';
  end;
end;

function func_valor_por_extenso(vlr : Real) : String;
const
  unidade: array[1..19] of string = ('um', 'dois', 'tr�s', 'quatro', 'cinco',
             'seis', 'sete', 'oito', 'nove', 'dez', 'onze',
             'doze', 'treze', 'quatorze', 'quinze', 'dezesseis',
             'dezessete', 'dezoito', 'dezenove');
  centena: array[1..9] of string = ('cento', 'duzentos', 'trezentos',
             'quatrocentos', 'quinhentos', 'seiscentos',
             'setecentos', 'oitocentos', 'novecentos');
  dezena: array[2..9] of string = ('vinte', 'trinta', 'quarenta', 'cinquenta',
             'sessenta', 'setenta', 'oitenta', 'noventa');
  qualificaS: array[0..4] of string = ('', 'mil', 'milh�o', 'bilh�o', 'trilh�o');
  qualificaP: array[0..4] of string = ('', 'mil', 'milh�es', 'bilh�es', 'trilh�es');
var
                        inteiro: Int64;
                          resto: real;
  vlrS, s, saux, vlrP, centavos: string;
     n, unid, dez, cent, tam, i: integer;
                    umReal, tem: boolean;
begin
  if (vlr = 0)
     then begin
            func_valor_por_extenso := 'zero';
            exit;
          end;

  inteiro := trunc(vlr); // parte inteira do valor
  resto := vlr - inteiro; // parte fracion�ria do valor
  vlrS := inttostr(inteiro);
  if (length(vlrS) > 15)
     then begin
            func_valor_por_extenso := 'Erro: valor superior a 999 trilh�es.';
            exit;
          end;

  s := '';
  centavos := inttostr(round(resto * 100));

// definindo o extenso da parte inteira do valor
  i := 0;
  umReal := false; tem := false;
  while (vlrS <> '0') do
  begin
    tam := length(vlrS);
// retira do valor a 1a. parte, 2a. parte, por exemplo, para 123456789:
// 1a. parte = 789 (centena)
// 2a. parte = 456 (mil)
// 3a. parte = 123 (milh�es)
    if (tam > 3)
       then begin
              vlrP := copy(vlrS, tam-2, tam);
              vlrS := copy(vlrS, 1, tam-3);
            end
    else begin // �ltima parte do valor
           vlrP := vlrS;
           vlrS := '0';
         end;
    if (vlrP <> '000')
       then begin
              saux := '';
              if (vlrP = '100')
                 then saux := 'cem'
              else begin
                     n := strtoint(vlrP);        // para n = 371, tem-se:
                     cent := n div 100;          // cent = 3 (centena trezentos)
                     dez := (n mod 100) div 10;  // dez  = 7 (dezena setenta)
                     unid := (n mod 100) mod 10; // unid = 1 (unidade um)
                     if (cent <> 0)
                        then saux := centena[cent];
                     if ((dez <> 0) or (unid <> 0))
                        then begin
                               if ((n mod 100) <= 19)
                                  then begin
                                         if (length(saux) <> 0)
                                            then saux := saux + ' e ' + unidade[n mod 100]
                                         else saux := unidade[n mod 100];
                                       end
                               else begin
                                      if (length(saux) <> 0)
                                         then saux := saux + ' e ' + dezena[dez]
                                      else saux := dezena[dez];
                                      if (unid <> 0)
                                         then if (length(saux) <> 0)
                                                 then saux := saux + ' e ' + unidade[unid]
                                              else saux := unidade[unid];
                                    end;
                             end;
                   end;
              if ((vlrP = '1') or (vlrP = '001'))
                 then begin
                        if (i = 0) // 1a. parte do valor (um real)
                           then umReal := true
                        else saux := saux + ' ' + qualificaS[i];
                      end
              else if (i <> 0)
                      then saux := saux + ' ' + qualificaP[i];
              if (length(s) <> 0)
                 then s := saux + ', ' + s
              else s := saux;
            end;
    if (((i = 0) or (i = 1)) and (length(s) <> 0))
       then tem := true; // tem centena ou mil no valor
    i := i + 1; // pr�ximo qualificador: 1- mil, 2- milh�o, 3- bilh�o, ...
  end;

  if (length(s) <> 0)
     then begin
            if (umReal)
               then s := s + ' real'
            else if (tem)
                    then s := s + ' reais'
                 else s := s + ' de reais';
          end;
// definindo o extenso dos centavos do valor
  if (centavos <> '0') // valor com centavos
     then begin
            if (length(s) <> 0) // se n�o � valor somente com centavos
               then s := s + ' e ';
            if (centavos = '1')
               then s := s + 'um centavo'
            else begin
                   n := strtoint(centavos);
                   if (n <= 19)
                      then s := s + unidade[n]
                   else begin                 // para n = 37, tem-se:
                          unid := n mod 10;   // unid = 37 % 10 = 7 (unidade sete)
                          dez := n div 10;    // dez  = 37 / 10 = 3 (dezena trinta)
                          s := s + dezena[dez];
                          if (unid <> 0)
                             then s := s + ' e ' + unidade[unid];
                       end;
                   s := s + ' centavos';
                 end;
          end;
  func_valor_por_extenso := s;
end;

end.
