unit mov_contas_a_receber;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Datasnap.Provider,
  Datasnap.DBClient, Math, menu, frxClass, frxDBSet;

type
  TFrmMov_Contas_a_Receber = class(TFrmAux_Padrao)
    Label4: TLabel;
    cmb_ent_codigo: TComboBox;
    gbPeriodo: TGroupBox;
    ed_data_inicial: TDateTimePicker;
    ed_data_final: TDateTimePicker;
    L1: TLabel;
    rgSituacao: TRadioGroup;
    btnBuscar: TBitBtn;
    DBGContasReceber: TDBGrid;
    DSContas: TDataSource;
    FDQContas: TFDQuery;
    CDSContas: TClientDataSet;
    PROContas: TDataSetProvider;
    FDQContascr_codigo: TIntegerField;
    FDQContascr_emp_codigo: TIntegerField;
    FDQContascr_ent_codigo: TIntegerField;
    FDQContascr_ent_emp_codigo: TIntegerField;
    FDQContascr_con_codigo: TIntegerField;
    FDQContascr_data: TDateField;
    FDQContascr_observacao: TBlobField;
    FDQContascr_situacao: TIntegerField;
    FDQContascr_valor: TBCDField;
    CDSContascr_codigo: TIntegerField;
    CDSContascr_emp_codigo: TIntegerField;
    CDSContascr_ent_codigo: TIntegerField;
    CDSContascr_ent_emp_codigo: TIntegerField;
    CDSContascr_con_codigo: TIntegerField;
    CDSContascr_data: TDateField;
    CDSContascr_observacao: TBlobField;
    CDSContascr_situacao: TIntegerField;
    CDSContascr_valor: TBCDField;
    CDSContasNOME: TStringField;
    CDSContasOBSERVACAO: TStringField;
    CDSContasSITUACAO: TStringField;
    pnlReceber: TPanel;
    ed_valor: TEdit;
    Label1: TLabel;
    cmbFormaPagamento: TComboBox;
    ed_nsu: TEdit;
    ed_cartao: TEdit;
    btnReceberPgto: TBitBtn;
    btnCancelarPgto: TBitBtn;
    l_cartao: TLabel;
    l_nsu: TLabel;
    dbgRecebimentos: TDBGrid;
    btnAdicionarRecebimento: TBitBtn;
    btnEditarRecebimento: TBitBtn;
    cdsRecebimentos: TClientDataSet;
    dsRecebimentos: TDataSource;
    cdsRecebimentosFORMA: TIntegerField;
    cdsRecebimentosCARTAO: TStringField;
    cdsRecebimentosNSU: TStringField;
    cdsRecebimentosRECEBIDO: TFloatField;
    cdsRecebimentosFORMA_DESC: TStringField;
    ed_recebimento: TEdit;
    btnExcluirRecebimento: TBitBtn;
    Relatorio: TfrxReport;
    frxDBContas: TfrxDBDataset;
    CDSContascr_valor_extenso: TStringField;
    CDSContasDATA_ATUAL: TDateField;
    btnRecibo: TBitBtn;
    CDSContasHORAS: TFloatField;
    procedure FormActivate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure CDSContasCalcFields(DataSet: TDataSet);
    procedure DBGContasReceberDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ed_valorKeyPress(Sender: TObject; var Key: Char);
    procedure cmbFormaPagamentoChange(Sender: TObject);
    procedure btnReceberPgtoClick(Sender: TObject);
    procedure ed_recebidoExit(Sender: TObject);
    procedure cdsRecebimentosCalcFields(DataSet: TDataSet);
    procedure btnAdicionarRecebimentoClick(Sender: TObject);
    procedure btnExcluirRecebimentoClick(Sender: TObject);
    procedure btnEditarRecebimentoClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnCancelarPgtoClick(Sender: TObject);
    procedure btnReciboClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmMov_Contas_a_Receber: TFrmMov_Contas_a_Receber;

implementation

uses dm_conexao, cls_entidades, cls_contas_a_receber, cls_historicos_cr,
     cls_tabelacodigos, cls_contratos, cls_utils;

{$R *.dfm}

procedure TFrmMov_Contas_a_Receber.btnAdicionarRecebimentoClick(
  Sender: TObject);
var d_valor : Double;
begin
  inherited;
  d_valor := StrToFloatDef(ed_recebimento.Text, 0);

  if (d_valor > 0) or (cmbFormaPagamento.Items.Strings[cmbFormaPagamento.ItemIndex] = 'INDICA��O') then begin
    cdsRecebimentos.Insert;
    cdsRecebimentosFORMA.AsInteger  := cmbFormaPagamento.ItemIndex;
    cdsRecebimentosRECEBIDO.AsFloat := d_valor;
    cdsRecebimentosCARTAO.AsString  := ed_cartao.Text;
    cdsRecebimentosNSU.AsString     := ed_nsu.Text;
    cdsRecebimentos.Post;

    ed_recebimento.Clear;
    ed_nsu.Clear;
    ed_cartao.Clear;
  end else begin
    Application.MessageBox(PChar('Informe um valor a ser recebido.'), PChar('Aten��o'), MB_OK + MB_ICONINFORMATION);
    ed_recebimento.SetFocus;
    ed_recebimento.SelectAll;
    Exit;
  end;
end;

procedure TFrmMov_Contas_a_Receber.btnBuscarClick(Sender: TObject);
var c_contas_receber : TContasReceber;
begin
  inherited;
  c_contas_receber := TContasReceber.Create;
  try
    if cmb_ent_codigo.ItemIndex > 0 then begin
      c_contas_receber.proc_consultar(FDQContas, CDSContas, TEntidadeCombo(cmb_ent_codigo.Items.Objects[cmb_ent_codigo.ItemIndex]).ent_codigo, TEntidadeCombo(cmb_ent_codigo.Items.Objects[cmb_ent_codigo.ItemIndex]).ent_emp_codigo, rgSituacao.ItemIndex, ed_data_inicial.Date, ed_data_final.Date);
    end else begin
      c_contas_receber.proc_consultar(FDQContas, CDSContas, 0, 0, rgSituacao.ItemIndex, ed_data_inicial.Date, ed_data_final.Date);
    end;
  finally
    FreeAndNil(c_contas_receber);
  end;
end;

procedure TFrmMov_Contas_a_Receber.btnCancelarPgtoClick(Sender: TObject);
begin
  inherited;
  pnlReceber.Visible := False;
end;

procedure TFrmMov_Contas_a_Receber.btnEditarRecebimentoClick(Sender: TObject);
begin
  inherited;
  ed_recebimento.Text         := cdsRecebimentosRECEBIDO.AsString;
  ed_nsu.Text                 := cdsRecebimentosNSU.AsString;
  ed_cartao.Text              := cdsRecebimentosCARTAO.AsString;
  cmbFormaPagamento.ItemIndex := cdsRecebimentosFORMA.AsInteger;
end;

procedure TFrmMov_Contas_a_Receber.btnExcluirRecebimentoClick(Sender: TObject);
begin
  inherited;
  cdsRecebimentos.Delete;
end;

procedure TFrmMov_Contas_a_Receber.btnNovoClick(Sender: TObject);
begin
  inherited;
  cdsRecebimentos.Close;
  cdsRecebimentos.CreateDataSet;

  cmbFormaPagamento.ItemIndex := 0;
  cmbFormaPagamentoChange(Self);

  ed_valor.Text := FloatToStr(CDSContascr_valor.AsFloat);

  pnlReceber.Left    := 0;
  pnlReceber.Top     := 0;
  pnlReceber.Visible := True;

  cmbFormaPagamento.SetFocus;
end;

procedure TFrmMov_Contas_a_Receber.btnReceberPgtoClick(Sender: TObject);
var d_total   : Double;
    d_receber : Double;

    c_contaReceber : TContasReceber;
    c_recebimentos : THistoricosCR;
    c_tabela       : TTabelaCodigos;
    c_contratos    : TContratos;
begin
  inherited;
  d_total   := StrToFloatDef(ed_valor.Text, 0);
  d_receber := 0;

  try
    cdsRecebimentos.DisableControls;

    cdsRecebimentos.First;
    while (not cdsRecebimentos.Eof) do begin
      d_receber := d_receber + cdsRecebimentosRECEBIDO.AsFloat;

      cdsRecebimentos.Next;
    end;
  finally
    cdsRecebimentos.First;
    cdsRecebimentos.EnableControls;
  end;

  if RoundTo(d_total, -2) <> RoundTo(d_receber, -2) then begin
    Application.MessageBox(PChar('O valor recebido dever� ser o total da fatura.'), PChar('Aten��o'), MB_OK + MB_ICONINFORMATION);
    Exit;
  end;

  c_recebimentos := THistoricosCR.Create;
  c_tabela       := TTabelaCodigos.Create;
  c_contaReceber := TContasReceber.Create;
  c_contratos    := TContratos.Create;
  try
    try
      Conexao.FDConexao.StartTransaction;

      cdsRecebimentos.DisableControls;

      cdsRecebimentos.First;
      while not cdsRecebimentos.Eof do begin
        c_recebimentos.hist_cr_codigo     := CDSContascr_codigo.AsInteger;
        c_recebimentos.hist_cr_emp_codigo := CDSContascr_emp_codigo.AsInteger;
        c_recebimentos.hist_cr_sequencia  := c_tabela.func_gerar_codigo('historicos_cr', CDSContascr_codigo.AsString + CDSContascr_emp_codigo.AsString);
        c_recebimentos.hist_cr_forma      := cdsRecebimentosFORMA.AsInteger;
        c_recebimentos.hist_cr_valor      := cdsRecebimentosRECEBIDO.AsFloat;
        c_recebimentos.hist_nsu           := cdsRecebimentosNSU.AsString;
        c_recebimentos.hist_cartao        := cdsRecebimentosCARTAO.AsString;
        c_recebimentos.hist_data_pgto     := Date;

        c_recebimentos.proc_gravar(c_recebimentos.hist_cr_codigo, c_recebimentos.hist_cr_emp_codigo, c_recebimentos.hist_cr_sequencia);

        cdsRecebimentos.Next;
      end;

      c_contaReceber.func_recuperar(CDSContascr_codigo.AsInteger, CDSContascr_emp_codigo.AsInteger);
      c_contaReceber.cr_situacao := 1;
      c_contaReceber.proc_gravar(CDSContascr_codigo.AsInteger, CDSContascr_emp_codigo.AsInteger);

      c_contratos.func_recuperar(CDSContascr_con_codigo.AsInteger, CDSContascr_ent_codigo.AsInteger, CDSContascr_ent_emp_codigo.AsInteger);
      c_contratos.con_situacao := 1;
      c_contratos.proc_gravar(CDSContascr_con_codigo.AsInteger, CDSContascr_ent_codigo.AsInteger, CDSContascr_ent_emp_codigo.AsInteger);

      Conexao.FDConexao.Commit;

      Application.MessageBox(PChar('Recebimento foi efetuado com sucesso!'), PChar(Self.Caption), MB_OK + MB_ICONINFORMATION);
      btnCancelarPgto.Click;
      btnBuscar.Click;
    except
      on E : Exception do begin
        Conexao.FDConexao.Rollback;
        Application.MessageBox(PChar(E.Message), PChar(Self.Caption), MB_OK + MB_ICONERROR);
        Exit;
      end;
    end;
  finally
    FreeAndNil(c_recebimentos);
    FreeAndNil(c_tabela);

    cdsRecebimentos.EnableControls;
  end;
end;

procedure TFrmMov_Contas_a_Receber.btnReciboClick(Sender: TObject);
var s_caminho : String;
begin
  inherited;
  s_caminho := ExtractFilePath(Application.ExeName) + 'fast\RelRecibo.fr3';

  try
    CDSContas.DisableControls;

    CDSContas.Filter   := 'cr_codigo = ' + CDSContascr_codigo.AsString + ' and cr_emp_codigo = ' + CDSContascr_emp_codigo.AsString;
    CDSContas.Filtered := True;

    Relatorio.LoadFromFile(s_caminho);
    Relatorio.ShowReport;
  finally
    CDSContas.Filter   := '';
    CDSContas.Filtered := False;

    CDSContas.EnableControls;
  end;
end;

procedure TFrmMov_Contas_a_Receber.CDSContasCalcFields(DataSet: TDataSet);
var c_entidades : TEntidades;
    c_contratos : TContratos;
begin
  inherited;
  c_entidades := TEntidades.Create;
  try
    c_entidades.func_recuperar(CDSContascr_ent_codigo.AsInteger, CDSContascr_ent_emp_codigo.AsInteger);
    CDSContasNOME.AsString := c_entidades.ent_razao_social;
  finally
    FreeAndNil(c_entidades);
  end;

  CDSContasOBSERVACAO.AsString := CDSContascr_observacao.AsString;
  case CDSContascr_situacao.AsInteger of
    0 : CDSContasSITUACAO.AsString := 'Aberto';
    1 : CDSContasSITUACAO.AsString := 'Pago';
  end;

  CDSContascr_valor_extenso.AsString := func_valor_por_extenso(CDSContascr_valor.AsFloat);
  CDSContasDATA_ATUAL.AsDateTime     := Date;

  c_contratos := TContratos.Create;
  try
    c_contratos.func_recuperar(CDSContascr_con_codigo.AsInteger, CDSContascr_ent_codigo.AsInteger, CDSContascr_ent_emp_codigo.AsInteger);
    CDSContasHORAS.AsFloat := c_contratos.con_horas_mes;
  finally
    FreeAndNil(c_contratos);
  end;
end;

procedure TFrmMov_Contas_a_Receber.cdsRecebimentosCalcFields(DataSet: TDataSet);
begin
  inherited;
  case cdsRecebimentosFORMA.AsInteger of
    0 : cdsRecebimentosFORMA_DESC.AsString := 'DINHEIRO';
    1 : cdsRecebimentosFORMA_DESC.AsString := 'CHEQUE';
    2 : cdsRecebimentosFORMA_DESC.AsString := 'CART�O DE CR�DITO';
    3 : cdsRecebimentosFORMA_DESC.AsString := 'CART�O DE D�BITO';
    4 : cdsRecebimentosFORMA_DESC.AsString := 'PERMUTA';
    5 : cdsRecebimentosFORMA_DESC.AsString := 'BOLETO';
    6 : cdsRecebimentosFORMA_DESC.AsString := 'DEP�SITO BANC�RIO';
    7 : cdsRecebimentosFORMA_DESC.AsString := 'TRANSFER�NCIA (TED)';
    8 : cdsRecebimentosFORMA_DESC.AsString := 'PIX';
    9 : cdsRecebimentosFORMA_DESC.AsString := 'INDICA��O';
  end;
end;

procedure TFrmMov_Contas_a_Receber.cmbFormaPagamentoChange(Sender: TObject);
begin
  inherited;
  l_cartao.Enabled  := False;
  ed_cartao.Enabled := False;
  l_nsu.Enabled     := False;
  ed_nsu.Enabled    := False;

  case cmbFormaPagamento.ItemIndex of
//    0 :
//    1 :

    2, 3 : begin
             l_cartao.Enabled  := True;
             ed_cartao.Enabled := True;
             l_nsu.Enabled     := True;
             ed_nsu.Enabled    := True;
           end;

    4, 5 : ed_recebimento.Text := ed_valor.Text;


  end;
end;

procedure TFrmMov_Contas_a_Receber.DBGContasReceberDrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  inherited;
  if (not Odd(CDSContas.RecNo)) then begin
    DBGContasReceber.Canvas.Brush.Color := clSilver;
  end else begin
    DBGContasReceber.Canvas.Brush.Color := clWindow;
  end;

  if (gdSelected in State) then begin
    DBGContasReceber.Canvas.Brush.Color := $00FFA74F;
  end;

  DBGContasReceber.Canvas.FillRect(Rect);
  DBGContasReceber.DefaultDrawDataCell(Rect, Column.Field, State);
end;

procedure TFrmMov_Contas_a_Receber.ed_recebidoExit(Sender: TObject);
begin
  inherited;
  if Trim(ed_recebimento.Text) = '' then begin
    ed_recebimento.Text := '0,00';
  end;
end;

procedure TFrmMov_Contas_a_Receber.ed_valorKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  if ((not (Key in ['0'..'9'])) and (not (Key in [','])) and (Word(Key) <> VK_BACK)) or ((Key in [',']) and (Pos(',', TEdit(Sender).Text) > 0)) then begin
    Key := #0;
  end else begin
    if (Key in [',']) then begin
      if Length(TEdit(Sender).Text) = 0 then begin
        TEdit(Sender).Text := '0,';
        TEdit(Sender).SelStart := Length(TEdit(Sender).Text);
        Abort;
      end;
    end;
  end;
end;

procedure TFrmMov_Contas_a_Receber.FormActivate(Sender: TObject);
var c_entidades : TEntidades;
begin
  inherited;
  c_entidades := TEntidades.Create;
  try
    c_entidades.proc_carregar_combo(cmb_ent_codigo, 0);
  finally
    FreeAndNil(c_entidades);
  end;

  ed_data_inicial.Date := Date;
  ed_data_final.Date   := Date;

  btnBuscar.Click;
end;

end.
