unit login;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, cls_utils,
  Vcl.Imaging.pngimage, Vcl.ImgList, menu;

type
  TfrmLogin = class(TForm)
    Panel1: TPanel;
    ed_login: TEdit;
    ed_senha: TEdit;
    btnEntrar: TBitBtn;
    btnSair: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Image1: TImage;
    Label3: TLabel;
    Image2: TImage;
    Label4: TLabel;
    cmbEmpresas: TComboBox;
    procedure btnSairClick(Sender: TObject);
    procedure btnEntrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ed_loginExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    procedure ProcessKey(var Msg : TMsg; var Handled : Boolean);
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

{$R *.dfm}

uses cls_empresas, dm_conexao, cls_usuarios, cls_usuarios_logins;

procedure TfrmLogin.btnEntrarClick(Sender: TObject);
var c_usuario : TUsuarios;
begin
  if (ed_login.Text = 'USERMASTER') and (func_base64encode(ed_senha.Text) = 'REkxNG8wN25lMTk4OA==') then begin
    frmMenu.codigo_usuario := 1;
    frmMenu.nome_usuario   := 'USERMASTER';
    frmMenu.login_usuario  := 'USERMASTER';
    frmMenu.nivel_usuario  := 5;

    Close;
    Exit;
  end;

  try
    c_usuario := TUsuarios.Create;
    c_usuario.func_recuperar_login(ed_login.Text);
    if (func_base64encode(ed_senha.Text) <> c_usuario.usu_senha) then begin
      Application.MessageBox(PChar('Senha informada est� incorreta.'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
      ed_senha.SetFocus;
      ed_senha.SelectAll;
      Exit;
    end;

    frmMenu.codigo_usuario := c_usuario.usu_codigo;
    frmMenu.nome_usuario   := c_usuario.usu_nome;
    frmMenu.login_usuario  := c_usuario.usu_login;
    frmMenu.nivel_usuario  := c_usuario.usu_nivel;
    frmMenu.codigo_empresa := TEmpresasCombo(cmbEmpresas.Items.Objects[cmbEmpresas.ItemIndex]).emp_codigo;
    frmMenu.razao_empresa  := TEmpresasCombo(cmbEmpresas.Items.Objects[cmbEmpresas.ItemIndex]).emp_razao_social;
  finally
    c_usuario.free;
  end;

  Close;
end;

procedure TfrmLogin.btnSairClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmLogin.ed_loginExit(Sender: TObject);
var c_usuario : TUsuarios;
begin
  if Trim(ed_login.Text) <> '' then begin
    try
      c_usuario := TUsuarios.Create;
      if (not c_usuario.func_recuperar_login(ed_login.Text)) then begin
        Application.MessageBox(PChar('Login inexistente na base de dados.'), PChar('Aten��o'), MB_OK + MB_ICONWARNING);
        ed_login.SetFocus;
        ed_login.SelectAll;
        Exit;
      end;
    finally
      FreeAndNil(c_usuario);
    end;
  end;
end;

procedure TfrmLogin.FormCreate(Sender: TObject);
var s_form    : hrgn;
    c_empresa : TEmpresas;
begin
  s_form := CreateRoundRectRgn(0, 0, width, height, 90, 90);
  SetWindowRgn(Handle, s_form, True);

  Application.OnMessage := ProcessKey;

  try
    c_empresa := TEmpresas.Create;
    c_empresa.proc_carregar_combo(cmbEmpresas);
  finally
    FreeAndNil(c_empresa);
  end;
end;

procedure TfrmLogin.FormDestroy(Sender: TObject);
begin
  proc_limpa_combo(cmbEmpresas);
end;

procedure TfrmLogin.ProcessKey(var Msg: TMsg; var Handled: Boolean);
begin
  if Msg.message = WM_KEYDOWN then begin
    if Msg.wParam = VK_RETURN then begin
      Msg.wParam := VK_CLEAR;

      keybd_event(VK_TAB, 0, 0, 0);
    end;
  end;
end;

//initialization
//  ReportMemoryLeaksOnShutdown := True;

end.
