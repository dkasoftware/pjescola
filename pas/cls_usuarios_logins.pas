unit cls_usuarios_logins;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils;

type
  TUsuarios_Logins = Class(TObject)
  private
    { Private declarations }
    f_ul_usu_codigo : Integer;
    f_ul_data_hora  : TDateTime;
    f_ul_sequencia  : Integer;
    f_ul_maquina    : String;
  public
    { Public declarations }
    procedure proc_gravar;
    procedure proc_desconecta_acesso;
    function func_proxima_sequencia : Integer;
  published
    { Published declarations }
    property ul_usu_codigo : Integer   read f_ul_usu_codigo write f_ul_usu_codigo;
    property ul_data_hora  : TDateTime read f_ul_data_hora  write f_ul_data_hora;
    property ul_sequencia  : Integer   read f_ul_sequencia  write f_ul_sequencia;
    property ul_maquina    : String    read f_ul_maquina    write f_ul_maquina;
  end;

implementation

{ TUsuarios_Logins }

function TUsuarios_Logins.func_proxima_sequencia: Integer;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select coalesce(max(u.ul_sequencia), 0) sequencia '+
                   ' from usuarios_logins u                            '+
                   ' where u.ul_usu_codigo = :p_ul_usu_codigo          ');
    qryAux.ParamByName('p_ul_usu_codigo').AsInteger := ul_usu_codigo;
    qryAux.Open;

    Result := qryAux.FieldByName('sequencia').AsInteger + 1;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TUsuarios_Logins.proc_desconecta_acesso;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' delete from usuarios_logins u            '+
                   ' where u.ul_usu_codigo = :p_ul_usu_codigo '+
                   '   and u.ul_sequencia  = :p_ul_sequencia  ');
    qryAux.ParamByName('p_ul_usu_codigo').AsInteger := ul_usu_codigo;
    qryAux.ParamByName('p_ul_sequencia').AsInteger  := ul_sequencia;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TUsuarios_Logins.proc_gravar;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' insert into usuarios_logins( '+
                   '   ul_usu_codigo,             '+
                   '   ul_sequencia ,             '+
                   '   ul_data_hora ,             '+
                   '   ul_maquina                 '+
                   ' )values(                     '+
                   '   :p_ul_usu_codigo,          '+
                   '   :p_ul_sequencia ,          '+
                   '   :p_ul_data_hora ,          '+
                   '   :p_ul_maquina   )          ');
    qryAux.ParamByName('p_ul_usu_codigo').AsInteger := ul_usu_codigo;
    qryAux.ParamByName('p_ul_sequencia').AsInteger  := func_proxima_sequencia;
    qryAux.ParamByName('p_ul_data_hora').AsDateTime := ul_data_hora;
    qryAux.ParamByName('p_ul_maquina').AsString     := ul_maquina;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
