unit cls_contas_bancarias;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type
  TContasBancarias = Class(TObject)

  private
    { Private declarations }
    f_cb_codigo         : Integer;
    f_cb_ban_codigo     : Integer;
    f_cb_ban_emp_codigo : Integer;
    f_cb_ag_numero      : String;
    f_cb_ag_digito      : String;
    f_cb_numero         : String;
    f_cb_digito         : String;
    f_cb_tipo           : Integer;
    f_cb_ativo          : Boolean;
  public
    { Public declarations }
    procedure proc_gravar(PCodigo, PBanCodigo, PBanEmpresa : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PBanCodigo, PBanEmpresa : Integer; PAgencia, PNumeroConta : String);

    function func_recuperar(PCodigo, PBanCodigo, PBanEmpresa : Integer) : Boolean;
  published
    { Published declarations }
    property cb_codigo         : Integer read f_cb_codigo         write f_cb_codigo;
    property cb_ban_codigo     : Integer read f_cb_ban_codigo     write f_cb_ban_codigo;
    property cb_ban_emp_codigo : Integer read f_cb_ban_emp_codigo write f_cb_ban_emp_codigo;
    property cb_ag_numero      : String  read f_cb_ag_numero      write f_cb_ag_numero;
    property cb_ag_digito      : String  read f_cb_ag_digito      write f_cb_ag_digito;
    property cb_numero         : String  read f_cb_numero         write f_cb_numero;
    property cb_digito         : String  read f_cb_digito         write f_cb_digito;
    property cb_tipo           : Integer read f_cb_tipo           write f_cb_tipo;
    property cb_ativo          : Boolean read f_cb_ativo          write f_cb_ativo;
  end;

implementation

{ TContasBancarias }

function TContasBancarias.func_recuperar(PCodigo, PBanCodigo,
  PBanEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                         '+
                   ' from contas_bancarias c                          '+
                   ' where c.cb_codigo         = :p_cb_codigo         '+
                   '   and c.cb_ban_codigo     = :p_cb_ban_codigo     '+
                   '   and c.cb_ban_emp_codigo = :p_cb_ban_emp_codigo ');
    qryAux.ParamByName('p_cb_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_cb_ban_codigo').AsInteger     := PBanCodigo;
    qryAux.ParamByName('p_cb_ban_emp_codigo').AsInteger := PBanEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    cb_codigo         := qryAux.FieldByName('cb_codigo').AsInteger;
    cb_ban_codigo     := qryAux.FieldByName('cb_ban_codigo').AsInteger;
    cb_ban_emp_codigo := qryAux.FieldByName('cb_ban_emp_codigo').AsInteger;
    cb_ag_numero      := qryAux.FieldByName('cb_ag_numero').AsString;
    cb_ag_digito      := qryAux.FieldByName('cb_ag_digito').AsString;
    cb_numero         := qryAux.FieldByName('cb_numero').AsString;
    cb_digito         := qryAux.FieldByName('cb_digito').AsString;
    cb_tipo           := qryAux.FieldByName('cb_tipo').AsInteger;
    cb_ativo          := qryAux.FieldByName('cb_ativo').AsBoolean;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TContasBancarias.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PBanCodigo, PBanEmpresa: Integer; PAgencia,
  PNumeroConta: String);
begin
  PQuery.Connection := Conexao.FDConexao;

  PClient.Close;
  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                        '+
                 ' from contas_bancarias c         '+
                 ' where c.cb_codigo = c.cb_codigo ');

  if PBanCodigo > 0 then begin
    PQuery.SQL.Add(' and c.cb_ban_codigo     = :p_cb_ban_codigo     '+
                   ' and c.cb_ban_emp_codigo = :p_cb_ban_emp_codigo ');
    PQuery.ParamByName('p_cb_ban_codigo').AsInteger     := PBanCodigo;
    PQuery.ParamByName('p_cb_ban_emp_codigo').AsInteger := PBanEmpresa;
  end;

  PQuery.SQL.Add('     and c.cb_ag_numero like ''%' + PAgencia + '%''     '+
                 '     and c.cb_numero    like ''%' + PNumeroConta + '%'' '+
                 ' order by c.cb_ban_codigo, c.cb_ag_numero, c.cb_numero  ');
  PQuery.Open;
  PClient.Open;
end;

procedure TContasBancarias.proc_gravar(PCodigo, PBanCodigo,
  PBanEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                         '+
                   ' from contas_bancarias c                          '+
                   ' where c.cb_codigo         = :p_cb_codigo         '+
                   '   and c.cb_ban_codigo     = :p_cb_ban_codigo     '+
                   '   and c.cb_ban_emp_codigo = :p_cb_ban_emp_codigo ');
    qryAux.ParamByName('p_cb_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_cb_ban_codigo').AsInteger     := PBanCodigo;
    qryAux.ParamByName('p_cb_ban_emp_codigo').AsInteger := PBanEmpresa;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into contas_bancarias( '+
                     '   cb_codigo        ,          '+
                     '   cb_ban_codigo    ,          '+
                     '   cb_ban_emp_codigo,          '+
                     '   cb_ag_numero     ,          '+
                     '   cb_ag_digito     ,          '+
                     '   cb_numero        ,          '+
                     '   cb_digito        ,          '+
                     '   cb_tipo          ,          '+
                     '   cb_ativo                    '+
                     ' ) values (                    '+
                     '   :p_cb_codigo        ,       '+
                     '   :p_cb_ban_codigo    ,       '+
                     '   :p_cb_ban_emp_codigo,       '+
                     '   :p_cb_ag_numero     ,       '+
                     '   :p_cb_ag_digito     ,       '+
                     '   :p_cb_numero        ,       '+
                     '   :p_cb_digito        ,       '+
                     '   :p_cb_tipo          ,       '+
                     '   :p_cb_ativo         )       ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update contas_bancarias                        '+
                     ' set cb_ag_numero = :p_cb_ag_numero,            '+
                     '     cb_ag_digito = :p_cb_ag_digito,            '+
                     '     cb_numero    = :p_cb_numero   ,            '+
                     '     cb_digito    = :p_cb_digito   ,            '+
                     '     cb_tipo      = :p_cb_tipo     ,            '+
                     '     cb_ativo     = :p_cb_ativo                 '+
                     ' where cb_codigo         = :p_cb_codigo         '+
                     '   and cb_ban_codigo     = :p_cb_ban_codigo     '+
                     '   and cb_ban_emp_codigo = :p_cb_ban_emp_codigo ');
    end;

    qryAux.ParamByName('p_cb_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_cb_ban_codigo').AsInteger     := PBanCodigo;
    qryAux.ParamByName('p_cb_ban_emp_codigo').AsInteger := PBanEmpresa;
    qryAux.ParamByName('p_cb_ag_numero').AsString       := cb_ag_numero;
    qryAux.ParamByName('p_cb_ag_digito').AsString       := cb_ag_digito;
    qryAux.ParamByName('p_cb_numero').AsString          := cb_numero;
    qryAux.ParamByName('p_cb_digito').AsString          := cb_digito;
    qryAux.ParamByName('p_cb_tipo').AsInteger           := cb_tipo;
    qryAux.ParamByName('p_cb_ativo').AsBoolean          := cb_ativo;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
