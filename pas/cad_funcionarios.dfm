inherited FrmCad_Funcionarios: TFrmCad_Funcionarios
  Caption = 'Cadastro de Funcion'#225'rios'
  ClientHeight = 479
  ClientWidth = 894
  OnActivate = FormActivate
  ExplicitWidth = 900
  ExplicitHeight = 508
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 894
    Height = 460
    ExplicitWidth = 894
    ExplicitHeight = 460
    inherited pnlButtons: TPanel
      Top = 424
      Width = 888
      ExplicitTop = 424
      ExplicitWidth = 888
      inherited btnCancelar: TBitBtn
        OnClick = btnCancelarClick
      end
      inherited btnEditar: TBitBtn
        OnClick = btnEditarClick
      end
      inherited btnNovo: TBitBtn
        OnClick = btnNovoClick
      end
      inherited btnSair: TBitBtn
        Left = 798
        ExplicitLeft = 798
      end
    end
    object PC: TPageControl
      Left = 3
      Top = 3
      Width = 888
      Height = 421
      ActivePage = tsConsulta
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object tsConsulta: TTabSheet
        Caption = 'Consulta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        object pnlConsulta: TPanel
          Left = 0
          Top = 0
          Width = 880
          Height = 390
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Padding.Left = 3
          Padding.Top = 3
          Padding.Right = 3
          Padding.Bottom = 3
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          object dbgEntidades: TDBGrid
            Left = 3
            Top = 37
            Width = 874
            Height = 350
            Align = alClient
            DataSource = DSFuncionarios
            GradientEndColor = 16754511
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'fun_codigo'
                Title.Alignment = taCenter
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'fun_nome'
                Title.Caption = 'Nome'
                Width = 765
                Visible = True
              end>
          end
          object Panel1: TPanel
            Left = 3
            Top = 3
            Width = 874
            Height = 34
            Align = alTop
            BevelInner = bvLowered
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            DesignSize = (
              874
              34)
            object Label1: TLabel
              Left = 9
              Top = 9
              Width = 38
              Height = 16
              Alignment = taRightJustify
              Caption = 'Buscar'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object Label2: TLabel
              Left = 661
              Top = 9
              Width = 36
              Height = 16
              Alignment = taRightJustify
              Anchors = [akTop, akRight]
              Caption = 'Status'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object cmbCampos: TComboBox
              Left = 54
              Top = 5
              Width = 86
              Height = 24
              Style = csDropDownList
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              Items.Strings = (
                'C'#243'digo'
                'Nome')
            end
            object ed_consulta: TEdit
              Left = 142
              Top = 5
              Width = 510
              Height = 24
              Anchors = [akLeft, akTop, akRight]
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object cmbConStatus: TComboBox
              Left = 703
              Top = 5
              Width = 167
              Height = 24
              Style = csDropDownList
              Anchors = [akTop, akRight]
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ItemIndex = 2
              ParentFont = False
              TabOrder = 2
              Text = 'TODOS'
              Items.Strings = (
                'ATIVO'
                'INATIVO'
                'TODOS')
            end
          end
        end
      end
      object tsCadastro: TTabSheet
        Caption = 'Cadastro'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object pnlCadastro: TPanel
          Left = 0
          Top = 0
          Width = 880
          Height = 390
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 880
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            Padding.Left = 5
            Padding.Right = 5
            TabOrder = 0
            object gb_ent_codigo: TGroupBox
              Left = 5
              Top = 0
              Width = 133
              Height = 49
              Align = alLeft
              Caption = 'C'#243'digo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object ed_fun_codigo: TEdit
                Left = 6
                Top = 18
                Width = 121
                Height = 24
                TabStop = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                NumbersOnly = True
                ParentFont = False
                ReadOnly = True
                TabOrder = 0
              end
            end
            object TGroupBox
              Left = 138
              Top = 0
              Width = 608
              Height = 49
              Align = alLeft
              Caption = 'Empresa'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object cmb_fun_emp_codigo: TComboBox
                Left = 7
                Top = 18
                Width = 593
                Height = 24
                Style = csDropDownList
                TabOrder = 0
              end
            end
            object TGroupBox
              Left = 746
              Top = 0
              Width = 129
              Height = 49
              Align = alClient
              Caption = 'Situa'#231#227'o'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              object cmb_fun_situacao: TComboBox
                Left = 7
                Top = 18
                Width = 114
                Height = 24
                Style = csDropDownList
                ItemIndex = 0
                TabOrder = 0
                Text = 'ATIVO'
                Items.Strings = (
                  'ATIVO'
                  'INATIVO')
              end
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 49
            Width = 880
            Height = 191
            Align = alTop
            BevelOuter = bvNone
            Padding.Left = 5
            Padding.Right = 5
            TabOrder = 1
            object GroupBox1: TGroupBox
              Left = 5
              Top = 0
              Width = 870
              Height = 191
              Align = alClient
              Caption = 'Funcion'#225'rio'
              TabOrder = 0
              object Label4: TLabel
                Left = 43
                Top = 22
                Width = 33
                Height = 16
                Alignment = taRightJustify
                Caption = 'Nome'
              end
              object Label5: TLabel
                Left = 60
                Top = 47
                Width = 16
                Height = 16
                Alignment = taRightJustify
                Caption = 'RG'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label6: TLabel
                Left = 321
                Top = 48
                Width = 22
                Height = 16
                Alignment = taRightJustify
                Caption = 'CPF'
              end
              object Label7: TLabel
                Left = 647
                Top = 48
                Width = 66
                Height = 16
                Alignment = taRightJustify
                Caption = 'Nascimento'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label8: TLabel
                Left = 11
                Top = 74
                Width = 65
                Height = 16
                Alignment = taRightJustify
                Caption = 'Estado Civil'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label3: TLabel
                Left = 58
                Top = 110
                Width = 16
                Height = 16
                Alignment = taRightJustify
                Caption = 'CT'
              end
              object Label10: TLabel
                Left = 323
                Top = 110
                Width = 19
                Height = 16
                Alignment = taRightJustify
                Caption = 'PIS'
              end
              object Label11: TLabel
                Left = 33
                Top = 136
                Width = 41
                Height = 16
                Alignment = taRightJustify
                Caption = 'Fun'#231#227'o'
              end
              object Label12: TLabel
                Left = 312
                Top = 162
                Width = 31
                Height = 16
                Alignment = taRightJustify
                Caption = 'Modo'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label13: TLabel
                Left = 34
                Top = 162
                Width = 40
                Height = 16
                Alignment = taRightJustify
                Caption = 'Sal'#225'rio'
              end
              object ed_fun_nome: TEdit
                Left = 82
                Top = 18
                Width = 780
                Height = 24
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                MaxLength = 150
                ParentFont = False
                TabOrder = 0
              end
              object ed_fun_rg: TEdit
                Left = 82
                Top = 44
                Width = 169
                Height = 24
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                MaxLength = 20
                NumbersOnly = True
                ParentFont = False
                TabOrder = 1
              end
              object ed_fun_data_nascimento: TDateTimePicker
                Left = 718
                Top = 44
                Width = 144
                Height = 24
                Date = 43720.996172847220000000
                Time = 43720.996172847220000000
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
              end
              object cmb_fun_estado_civil: TComboBox
                Left = 82
                Top = 70
                Width = 169
                Height = 24
                Style = csDropDownList
                TabOrder = 3
                Items.Strings = (
                  'SOLTEIRO'
                  'CASADO'
                  'DIVORCIADO')
              end
              object ed_fun_cpf: TMaskEdit
                Left = 350
                Top = 44
                Width = 169
                Height = 24
                EditMask = '999.999.999-99;1; '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                MaxLength = 14
                ParentFont = False
                TabOrder = 4
                Text = '   .   .   -  '
              end
              object ed_fun_ct: TMaskEdit
                Left = 82
                Top = 106
                Width = 169
                Height = 24
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                Text = ''
              end
              object ed_fun_pis: TMaskEdit
                Left = 350
                Top = 106
                Width = 169
                Height = 24
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 6
                Text = ''
              end
              object ed_fun_funcao: TMaskEdit
                Left = 82
                Top = 132
                Width = 780
                Height = 24
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 7
                Text = ''
              end
              object cmb_fun_modo: TComboBox
                Left = 350
                Top = 158
                Width = 169
                Height = 24
                Style = csDropDownList
                TabOrder = 8
                Items.Strings = (
                  'DI'#193'RIO'
                  'HORAL'
                  'SEMANAL'
                  'MENSAL')
              end
              object ed_fun_salario: TMaskEdit
                Left = 82
                Top = 158
                Width = 169
                Height = 24
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                TabOrder = 9
                Text = ''
              end
            end
          end
          object TPanel
            Left = 0
            Top = 240
            Width = 880
            Height = 150
            Align = alClient
            BevelOuter = bvNone
            Padding.Left = 5
            Padding.Top = 5
            Padding.Right = 5
            Padding.Bottom = 5
            TabOrder = 2
            object PCAdicionais: TPageControl
              Left = 5
              Top = 5
              Width = 870
              Height = 140
              ActivePage = tsEnderecos
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object tsEnderecos: TTabSheet
                Caption = 'Endere'#231'os'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 862
                  Height = 109
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Padding.Left = 2
                  Padding.Top = 2
                  Padding.Right = 2
                  Padding.Bottom = 2
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object lbl_end_cep: TLabel
                    Left = 66
                    Top = 19
                    Width = 22
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'CEP'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object lbl_end_uf_sigla: TLabel
                    Left = 278
                    Top = 19
                    Width = 15
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'UF'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object lbl_end_cid_codigo: TLabel
                    Left = 384
                    Top = 19
                    Width = 39
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Cidade'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object lbl_end_logradouro: TLabel
                    Left = 23
                    Top = 45
                    Width = 65
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Logradouro'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object lbl_end_numero: TLabel
                    Left = 764
                    Top = 45
                    Width = 14
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'N'#186
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object lbl_end_complemento: TLabel
                    Left = 9
                    Top = 71
                    Width = 79
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Complemento'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                  end
                  object ed_fun_cep: TEdit
                    Left = 94
                    Top = 15
                    Width = 170
                    Height = 24
                    CharCase = ecUpperCase
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 0
                  end
                  object cmb_fun_uf_sigla: TComboBox
                    Left = 299
                    Top = 15
                    Width = 68
                    Height = 24
                    Style = csDropDownList
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 1
                  end
                  object cmb_fun_cid_codigo: TComboBox
                    Left = 434
                    Top = 15
                    Width = 390
                    Height = 24
                    Style = csDropDownList
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 2
                  end
                  object btnAddEnd: TBitBtn
                    Left = 825
                    Top = 15
                    Width = 24
                    Height = 24
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = [fsBold]
                    Glyph.Data = {
                      36060000424D3606000000000000360000002800000020000000100000000100
                      18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
                      FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF029B100099000599
                      10FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                      C6C6C6C6C6C6C6C68C8C8C8A8A8A8D8A8DFF00FFFF00FFFF00FFFF00FFFF00FF
                      26B7FF22B4FF34BDFF64D4FF77DCFF6EDCFF61DDFF67E1FF00990025B2280099
                      0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C4C4CACACADBDBDBE0E0E0DF
                      DFDFDFDFDFE2E2E28A8A8A9696968A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FF4EC8FF64D4FF9EE8FFB8F0FFA5EEFF8CECFF90EEFF00990044C0460099
                      0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D2D2D2DBDBDBEAEAEAF0F0F0EE
                      EEEEEAEAEAECECEC8A8A8AA2A2A28A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FF73D6FF61D3FF9AE6FFB5EFFF0A9E1000990000990000990061CE620099
                      00009900009900009900FF00FFFF00FFC6C6C6DDDDDDDADADAE9E9E9F0F0F08D
                      8D8D8A8A8A8A8A8A8A8A8AB0B0B08A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                      26B7FF8CDEFF5ED3FF96E5FFB0EDFF009900AAECABADEEADA0ED9F80E57F61D0
                      6037B63718A618009900FF00FFFF00FFC6C6C6E3E3E3D9D9D9E8E8E8EFEFEF8A
                      8A8AD7D7D7D9D9D9D4D4D4C5C5C5B0B0B09B9B9B9090908A8A8AFF00FFFF00FF
                      26B7FFA5E5FF59D2FF91E4FFABEAFF099E100099000099000099009EEB9F0099
                      00009900009900009900FF00FFFF00FFC6C6C6EAEAEAD8D8D8E7E7E7ECECEC8D
                      8D8D8A8A8A8A8A8A8A8A8AD3D3D38A8A8A8A8A8A8A8A8A8A8A8AFF00FFFF00FF
                      26B7FFBEECFF5ED4FF8CE3FFA6EAFF85E1FF59D7FF58D6FF009900A6EBA60099
                      0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6F0F0F0DADADAE6E6E6ECECECE4
                      E4E4DBDBDBDADADA8A8A8AD6D6D68A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FF96DBFFC2EDFFD2F4FFD5F5FFD4F5FFD2F4FFD2F4FF0099009CE79E0099
                      0026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E4E1E4F0F0F0F6F6F6F6F6F6F6
                      F6F6F6F6F6F6F6F68A8A8AD0D0D08A8A8AC6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FF22B2FF2FBDFF4ACBFF58D3FF56D5FF51D8FF57DCFF059D10009900039B
                      1026B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6C4C2C4CACACAD3D3D3D9D9D9DA
                      DADADBDBDBDDDDDD8C8C8C8A8A8A8C8C8CC6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FF47C4FF62D3FF9DE8FFB9F0FFA6EEFF8EECFF92EFFF8FEDFF71E2FF33C3
                      FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6D0D0D0DADADAEAEAEAF0F0F0EE
                      EEEEEAEAEAEDEDEDEBEBEBE3E3E3CDCDCDC6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FF70D5FF60D3FF9AE6FFB6EFFFA0ECFF83E7FF85E9FF82E8FF68DEFF32C1
                      FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6DCDCDCDADADAE9E9E9F0F0F0EC
                      ECECE7E7E7E9E9E9E8E8E8E0E0E0CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FF88DCFF5DD2FF96E5FFB1EDFF99E9FF77E2FF76E3FF74E2FF5ED9FF2EBE
                      FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E2E2E2D9D9D9E8E8E8EFEFEFEA
                      EAEAE3E3E3E3E3E3E3E3E3DDDDDDCACACAC6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FFA1E4FF59D2FF91E3FFACEBFF90E5FF69DDFF68DDFF67DDFF53D3FF29BA
                      FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6E9E9E9D8D8D8E6E6E6EDEDEDE7
                      E7E7DFDFDFDFDFDFDFDFDFD8D8D8C8C8C8C6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FFBAEBFF54D2FF8CE3FFA7E9FF88E2FF5CD7FF5AD7FF58D7FF48CEFF39C0
                      FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EFEFEFD8D8D8E6E6E6EBEBEBE5
                      E5E5DBDBDBDBDBDBDBDBDBD5D5D5CCCCCCC6C6C6FF00FFFF00FFFF00FFFF00FF
                      26B7FFAAE5FFC6F0FFC1F0FFCBF3FFC1F0FFB3ECFFB2ECFFB1ECFFBDEEFF9FE4
                      FF26B7FFFF00FFFF00FFFF00FFFF00FFC6C6C6EAEAEAF2F2F2F1F1F1F4F4F4F1
                      F1F1EEEEEEEEEEEEEEEEEEF0F0F0E9E9E9C6C6C6FF00FFFF00FFFF00FFFF00FF
                      FF00FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7FF26B7
                      FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC6C6C6C6C6C6C6C6C6C6C6C6C6
                      C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6C6FF00FFFF00FFFF00FF}
                    NumGlyphs = 2
                    ParentFont = False
                    TabOrder = 3
                  end
                  object ed_fun_logradouro: TEdit
                    Left = 94
                    Top = 41
                    Width = 655
                    Height = 24
                    CharCase = ecUpperCase
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 150
                    ParentFont = False
                    TabOrder = 4
                  end
                  object ed_fun_numero: TEdit
                    Left = 784
                    Top = 41
                    Width = 65
                    Height = 24
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    NumbersOnly = True
                    ParentFont = False
                    TabOrder = 5
                  end
                  object ed_fun_complemento: TEdit
                    Left = 94
                    Top = 67
                    Width = 755
                    Height = 24
                    CharCase = ecUpperCase
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 150
                    ParentFont = False
                    TabOrder = 6
                  end
                end
              end
              object tsObservacoes: TTabSheet
                Caption = 'Observa'#231#245'es'
                ImageIndex = 3
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 862
                  Height = 109
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object TGroupBox
                    Left = 1
                    Top = -71
                    Width = 860
                    Height = 179
                    Align = alBottom
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    Padding.Left = 2
                    Padding.Top = 2
                    Padding.Right = 2
                    Padding.Bottom = 2
                    ParentFont = False
                    TabOrder = 0
                    object ed_fun_observacoes: TRichEdit
                      Left = 4
                      Top = 76
                      Width = 852
                      Height = 99
                      Align = alBottom
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -13
                      Font.Name = 'Tahoma'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      WantReturns = False
                    end
                  end
                end
              end
              object tsContatos: TTabSheet
                Caption = 'Contatos'
                ImageIndex = 2
                object TPanel
                  Left = 0
                  Top = 0
                  Width = 862
                  Height = 109
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -13
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  Padding.Left = 2
                  Padding.Top = 2
                  Padding.Right = 2
                  Padding.Bottom = 2
                  ParentBackground = False
                  ParentFont = False
                  TabOrder = 0
                  object Label9: TLabel
                    Left = 37
                    Top = 42
                    Width = 35
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'e-Mail'
                  end
                  object lbl_con_telefone: TLabel
                    Left = 22
                    Top = 17
                    Width = 50
                    Height = 16
                    Alignment = taRightJustify
                    Caption = 'Telefone'
                  end
                  object ed_fun_email: TEdit
                    Left = 78
                    Top = 38
                    Width = 769
                    Height = 24
                    CharCase = ecUpperCase
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    ParentFont = False
                    TabOrder = 0
                  end
                  object ed_fun_telefone: TMaskEdit
                    Left = 78
                    Top = 12
                    Width = 169
                    Height = 24
                    EditMask = '+99 (99) 99999-9999;1; '
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -13
                    Font.Name = 'Tahoma'
                    Font.Style = []
                    MaxLength = 19
                    ParentFont = False
                    TabOrder = 1
                    Text = '+   (  )      -    '
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  inherited SB: TStatusBar
    Top = 460
    Width = 894
    ExplicitTop = 460
    ExplicitWidth = 894
  end
  object FDQFuncionarios: TFDQuery
    SQL.Strings = (
      'select *'
      'from funcionarios')
    Left = 639
    Top = 78
    object FDQFuncionariosfun_codigo: TIntegerField
      FieldName = 'fun_codigo'
      Origin = 'fun_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQFuncionariosfun_emp_codigo: TIntegerField
      FieldName = 'fun_emp_codigo'
      Origin = 'fun_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQFuncionariosfun_nome: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_nome'
      Origin = 'fun_nome'
      Size = 60
    end
    object FDQFuncionariosfun_cpf: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_cpf'
      Origin = 'fun_cpf'
    end
    object FDQFuncionariosfun_rg: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_rg'
      Origin = 'fun_rg'
    end
    object FDQFuncionariosfun_data_nascimento: TDateField
      AutoGenerateValue = arDefault
      FieldName = 'fun_data_nascimento'
      Origin = 'fun_data_nascimento'
    end
    object FDQFuncionariosfun_estado_civil: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_estado_civil'
      Origin = 'fun_estado_civil'
    end
    object FDQFuncionariosfun_ct: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_ct'
      Origin = 'fun_ct'
    end
    object FDQFuncionariosfun_pis: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_pis'
      Origin = 'fun_pis'
    end
    object FDQFuncionariosfun_funcao: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_funcao'
      Origin = 'fun_funcao'
      Size = 150
    end
    object FDQFuncionariosfun_salario: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'fun_salario'
      Origin = 'fun_salario'
      Precision = 15
    end
    object FDQFuncionariosfun_modo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'fun_modo'
      Origin = 'fun_modo'
    end
    object FDQFuncionariosfun_telefone: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_telefone'
      Origin = 'fun_telefone'
    end
    object FDQFuncionariosfun_email: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_email'
      Origin = 'fun_email'
      Size = 150
    end
    object FDQFuncionariosfun_cep: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_cep'
      Origin = 'fun_cep'
    end
    object FDQFuncionariosfun_uf_sigla: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_uf_sigla'
      Origin = 'fun_uf_sigla'
      Size = 3
    end
    object FDQFuncionariosfun_cid_codigo: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'fun_cid_codigo'
      Origin = 'fun_cid_codigo'
    end
    object FDQFuncionariosfun_logradouro: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_logradouro'
      Origin = 'fun_logradouro'
      Size = 150
    end
    object FDQFuncionariosfun_numero: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'fun_numero'
      Origin = 'fun_numero'
    end
    object FDQFuncionariosfun_complemento: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'fun_complemento'
      Origin = 'fun_complemento'
      Size = 150
    end
    object FDQFuncionariosfun_observacoes: TBlobField
      AutoGenerateValue = arDefault
      FieldName = 'fun_observacoes'
      Origin = 'fun_observacoes'
    end
    object FDQFuncionariosfun_situacao: TIntegerField
      AutoGenerateValue = arDefault
      FieldName = 'fun_situacao'
      Origin = 'fun_situacao'
    end
  end
  object CDSFuncionarios: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'PROFuncionarios'
    Left = 639
    Top = 134
    object CDSFuncionariosfun_codigo: TIntegerField
      FieldName = 'fun_codigo'
      Required = True
    end
    object CDSFuncionariosfun_emp_codigo: TIntegerField
      FieldName = 'fun_emp_codigo'
      Required = True
    end
    object CDSFuncionariosfun_nome: TStringField
      FieldName = 'fun_nome'
      Size = 60
    end
    object CDSFuncionariosfun_cpf: TStringField
      FieldName = 'fun_cpf'
    end
    object CDSFuncionariosfun_rg: TStringField
      FieldName = 'fun_rg'
    end
    object CDSFuncionariosfun_data_nascimento: TDateField
      FieldName = 'fun_data_nascimento'
    end
    object CDSFuncionariosfun_estado_civil: TStringField
      FieldName = 'fun_estado_civil'
    end
    object CDSFuncionariosfun_ct: TStringField
      FieldName = 'fun_ct'
    end
    object CDSFuncionariosfun_pis: TStringField
      FieldName = 'fun_pis'
    end
    object CDSFuncionariosfun_funcao: TStringField
      FieldName = 'fun_funcao'
      Size = 150
    end
    object CDSFuncionariosfun_salario: TBCDField
      FieldName = 'fun_salario'
      Precision = 15
    end
    object CDSFuncionariosfun_modo: TIntegerField
      FieldName = 'fun_modo'
    end
    object CDSFuncionariosfun_telefone: TStringField
      FieldName = 'fun_telefone'
    end
    object CDSFuncionariosfun_email: TStringField
      FieldName = 'fun_email'
      Size = 150
    end
    object CDSFuncionariosfun_cep: TStringField
      FieldName = 'fun_cep'
    end
    object CDSFuncionariosfun_uf_sigla: TStringField
      FieldName = 'fun_uf_sigla'
      Size = 3
    end
    object CDSFuncionariosfun_cid_codigo: TIntegerField
      FieldName = 'fun_cid_codigo'
    end
    object CDSFuncionariosfun_logradouro: TStringField
      FieldName = 'fun_logradouro'
      Size = 150
    end
    object CDSFuncionariosfun_numero: TIntegerField
      FieldName = 'fun_numero'
    end
    object CDSFuncionariosfun_complemento: TStringField
      FieldName = 'fun_complemento'
      Size = 150
    end
    object CDSFuncionariosfun_observacoes: TBlobField
      FieldName = 'fun_observacoes'
    end
    object CDSFuncionariosfun_situacao: TIntegerField
      FieldName = 'fun_situacao'
    end
  end
  object DSFuncionarios: TDataSource
    DataSet = CDSFuncionarios
    Left = 639
    Top = 162
  end
  object PROFuncionarios: TDataSetProvider
    DataSet = FDQFuncionarios
    Left = 639
    Top = 106
  end
end
