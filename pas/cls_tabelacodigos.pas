unit cls_tabelacodigos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils;

type
  TTabelaCodigos = Class(TObject)
  private
    { Private declarations }
    f_tab_nome       : String;
    f_tab_composicao : String;
    f_tab_codigo     : Integer;
  public
    { Public declarations }
    function func_gerar_codigo(PTabela, PComposicao : String) : Integer;
  published
    { Published declarations }
    property tab_nome       : String  read f_tab_nome       write f_tab_nome;
    property tab_composicao : String  read f_tab_composicao write f_tab_composicao;
    property tab_codigo     : Integer read f_tab_codigo     write f_tab_codigo;
  end;

implementation

{ TTabelaCodigos }

function TTabelaCodigos.func_gerar_codigo(PTabela, PComposicao : String): Integer;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select (coalesce(max(t.tab_codigo), 0) + 1) codigo '+
                   ' from tabelacodigos t                               '+
                   ' where t.tab_nome       = :p_tab_nome               '+
                   '   and t.tab_composicao = :p_tab_composicao         ');
    qryAux.ParamByName('p_tab_nome').AsString       := PTabela;
    qryAux.ParamByName('p_tab_composicao').AsString := PComposicao;
    qryAux.Open;

    Result := qryAux.FieldByName('codigo').AsInteger;

    if Result = 1 then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into tabelacodigos values ( '+
                     '   :p_tab_nome      ,               '+
                     '   :p_tab_composicao,               '+
                     '   :p_tab_codigo    )               ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update tabelacodigos                     '+
                     ' set tab_codigo = :p_tab_codigo           '+
                     ' where tab_nome       = :p_tab_nome       '+
                     '   and tab_composicao = :p_tab_composicao ');
    end;

    qryAux.ParamByName('p_tab_nome').AsString       := PTabela;
    qryAux.ParamByName('p_tab_composicao').AsString := PComposicao;
    qryAux.ParamByName('p_tab_codigo').AsInteger    := Result;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
