unit cls_enderecos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type TEnderecos = Class(TObject)
  private
    { Private declarations }
    f_end_codigo         : Integer;
    f_end_ent_codigo     : Integer;
    f_end_ent_emp_codigo : Integer;
    f_end_tipo           : Integer;
    f_end_cep            : String;
    f_end_uf_sigla       : String;
    f_end_cid_codigo     : Integer;
    f_end_logradouro     : String;
    f_end_numero         : Integer;
    f_end_complemento    : String;

  public
    { Public declarations }
    procedure proc_gravar(PCodigo, PCodigoEntidade, PCodigoEmpresa : Integer);
    procedure proc_limpar_gravacao(PCodigoEntidade, PCodigoEmpresa : Integer);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PCodigoEntidade, PCodigoEmpresa : Integer);

    function func_recuperar(PCodigo, PCodigoEntidade, PCodigoEmpresa : Integer) : Boolean;
  published
    { Published declarations }
    property end_codigo         : Integer read f_end_codigo         write f_end_codigo;
    property end_ent_codigo     : Integer read f_end_ent_codigo     write f_end_ent_codigo;
    property end_ent_emp_codigo : Integer read f_end_ent_emp_codigo write f_end_ent_emp_codigo;
    property end_tipo           : Integer read f_end_tipo           write f_end_tipo;
    property end_cep            : String  read f_end_cep            write f_end_cep;
    property end_uf_sigla       : String  read f_end_uf_sigla       write f_end_uf_sigla;
    property end_cid_codigo     : Integer read f_end_cid_codigo     write f_end_cid_codigo;
    property end_logradouro     : String  read f_end_logradouro     write f_end_logradouro;
    property end_numero         : Integer read f_end_numero         write f_end_numero;
    property end_complemento    : String  read f_end_complemento    write f_end_complemento;
  end;

implementation

{ TEnderecos }

function TEnderecos.func_recuperar(PCodigo, PCodigoEntidade,
  PCodigoEmpresa: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from enderecos e                                   '+
                   ' where e.end_codigo         = :p_end_codigo         '+
                   '   and e.end_ent_codigo     = :p_end_ent_codigo     '+
                   '   and e.end_ent_emp_codigo = :p_end_ent_emp_codigo ');
    qryAux.ParamByName('p_end_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_end_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_end_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.Open;

    Result := (not qryAux.Eof);

    end_codigo         := qryAux.FieldByName('end_codigo').AsInteger;
    end_ent_codigo     := qryAux.FieldByName('end_ent_codigo').AsInteger;
    end_ent_emp_codigo := qryAux.FieldByName('end_ent_emp_codigo').AsInteger;
    end_tipo           := qryAux.FieldByName('end_tipo').AsInteger;
    end_cep            := qryAux.FieldByName('end_cep').AsString;
    end_uf_sigla       := qryAux.FieldByName('end_uf_sigla').AsString;
    end_cid_codigo     := qryAux.FieldByName('end_cid_codigo').AsInteger;
    end_logradouro     := qryAux.FieldByName('end_logradouro').AsString;
    end_numero         := qryAux.FieldByName('end_numero').AsInteger;
    end_complemento    := qryAux.FieldByName('end_complemento').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TEnderecos.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PCodigoEntidade, PCodigoEmpresa: Integer);
begin
  PQuery.Close;
  PClient.Close;

  PQuery.Connection := Conexao.FDConexao;

  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                                           '+
                 ' from enderecos e                                   '+
                 ' where e.end_ent_codigo     = :p_end_ent_codigo     '+
                 '   and e.end_ent_emp_codigo = :p_end_ent_emp_codigo '+
                 ' order by e.end_codigo                              ');
  PQuery.ParamByName('p_end_ent_codigo').AsInteger     := PCodigoEntidade;
  PQuery.ParamByName('p_end_ent_emp_codigo').AsInteger := PCodigoEmpresa;
  PQuery.Open;
  PClient.Open;
end;

procedure TEnderecos.proc_gravar(PCodigo, PCodigoEntidade,
  PCodigoEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                           '+
                   ' from enderecos e                                   '+
                   ' where e.end_codigo         = :p_end_codigo         '+
                   '   and e.end_ent_codigo     = :p_end_ent_codigo     '+
                   '   and e.end_ent_emp_codigo = :p_end_ent_emp_codigo ');
    qryAux.ParamByName('p_end_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_end_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_end_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into enderecos (  '+
                     '   end_codigo        ,    '+
                     '   end_ent_codigo    ,    '+
                     '   end_ent_emp_codigo,    '+
                     '   end_tipo          ,    '+
                     '   end_cep           ,    '+
                     '   end_uf_sigla      ,    '+
                     '   end_cid_codigo    ,    '+
                     '   end_logradouro    ,    '+
                     '   end_numero        ,    '+
                     '   end_complemento        '+
                     '  ) values (              '+
                     '   :p_end_codigo        , '+
                     '   :p_end_ent_codigo    , '+
                     '   :p_end_ent_emp_codigo, '+
                     '   :p_end_tipo          , '+
                     '   :p_end_cep           , ');

      if (end_uf_sigla = '') then begin
        qryAux.SQL.Add(' NULL, ');
      end else begin
        qryAux.SQL.Add(' :p_end_uf_sigla, ');
      end;

      if (end_cid_codigo = 0) then begin
        qryAux.SQL.Add(' NULL, ');
      end else begin
        qryAux.SQL.Add(' :p_end_cid_codigo, ');
      end;

      qryAux.SQL.Add('   :p_end_logradouro    , '+
                     '   :p_end_numero        , '+
                     '   :p_end_complemento   ) ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update enderecos            '+
                     ' set end_tipo = :p_end_tipo, '+
                     '     end_cep  = :p_end_cep , ');

      if (end_uf_sigla = '') then begin
        qryAux.SQL.Add(' end_uf_sigla = NULL, ');
      end else begin
        qryAux.SQL.Add(' end_uf_sigla = :p_end_uf_sigla, ');
      end;

      if (end_cid_codigo = 0) then begin
        qryAux.SQL.Add(' end_cid_codigo = NULL, ');
      end else begin
        qryAux.SQL.Add(' end_cid_codigo = :p_end_cid_codigo, ');
      end;

      qryAux.SQL.Add('     end_logradouro  = :p_end_logradouro ,        '+
                     '     end_numero      = :p_end_numero     ,        '+
                     '     end_complemento = :p_end_complemento         '+
                     ' where end_codigo         = :p_end_codigo         '+
                     '   and end_ent_codigo     = :p_end_ent_codigo     '+
                     '   and end_ent_emp_codigo = :p_end_ent_emp_codigo ');
    end;

    qryAux.ParamByName('p_end_codigo').AsInteger         := PCodigo;
    qryAux.ParamByName('p_end_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_end_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.ParamByName('p_end_tipo').AsInteger           := end_tipo;
    qryAux.ParamByName('p_end_cep').AsString             := end_cep;

    if (end_uf_sigla <> '') then begin
      qryAux.ParamByName('p_end_uf_sigla').AsString := end_uf_sigla;
    end;

    if (end_cid_codigo <> 0) then begin
      qryAux.ParamByName('p_end_cid_codigo').AsInteger := end_cid_codigo;
    end;

    qryAux.ParamByName('p_end_logradouro').AsString  := end_logradouro;
    qryAux.ParamByName('p_end_numero').AsInteger     := end_numero;
    qryAux.ParamByName('p_end_complemento').AsString := end_complemento;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TEnderecos.proc_limpar_gravacao(PCodigoEntidade,
  PCodigoEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' delete from enderecos                            '+
                   ' where end_ent_codigo     = :p_end_ent_codigo     '+
                   '   and end_ent_emp_codigo = :p_end_ent_emp_codigo ');
    qryAux.ParamByName('p_end_ent_codigo').AsInteger     := PCodigoEntidade;
    qryAux.ParamByName('p_end_ent_emp_codigo').AsInteger := PCodigoEmpresa;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
