unit cls_agendamentos_entidade;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Classes, DB;

type TAgendamentos_entidade = Class(TObject)
  private
    { Private declarations }
    f_agd_codigo     : Integer;
    f_agd_emp_codigo : Integer;
    f_ent_codigo     : Integer;
    f_ent_emp_codigo : Integer;
  public
    { Public declarations }
    procedure proc_gravar;
    procedure proc_eliminar;
    procedure proc_carrega_edicao(PAgdCodigo, PAgdEmpCodigo : Integer; PCDS : TClientDataSet);
  published
    { Published declarations }
    property agd_codigo     : Integer read f_agd_codigo     write f_agd_codigo;
    property agd_emp_codigo : Integer read f_agd_emp_codigo write f_agd_emp_codigo;
    property ent_codigo     : Integer read f_ent_codigo     write f_ent_codigo;
    property ent_emp_codigo : Integer read f_ent_emp_codigo write f_ent_emp_codigo;
  end;


implementation

{ TAgendamentos_entidade }

procedure TAgendamentos_entidade.proc_carrega_edicao(PAgdCodigo,
  PAgdEmpCodigo: Integer; PCDS: TClientDataSet);
var qryAux : TFDQuery;
begin
  qryAux := TFDQuery.Create(nil);
  try
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select e.ent_razao_social, a.*                                '+
                   ' from agendamentos_entidade a                                  '+
                   ' inner join entidades e on e.ent_codigo     = a.ent_codigo and '+
                   '                           e.ent_emp_codigo = a.ent_emp_codigo '+
                   ' where a.agd_codigo     = :p_agd_codigo                        '+
                   '   and a.agd_emp_codigo = :p_agd_emp_codigo                    ');
    qryAux.ParamByName('p_agd_codigo').AsInteger     := PAgdCodigo;
    qryAux.ParamByName('p_agd_emp_codigo').AsInteger := PAgdEmpCodigo;
    qryAux.Open;

    PCDS.Close;
    PCDS.CreateDataSet;

    while (not qryAux.Eof) do begin
      PCDS.Insert;

      PCDS.FieldByName('ent_codigo').AsInteger      := qryAux.FieldByName('ent_codigo').AsInteger;
      PCDS.FieldByName('ent_emp_codigo').AsInteger  := qryAux.FieldByName('ent_emp_codigo').AsInteger;
      PCDS.FieldByName('ent_razao_social').AsString := qryAux.FieldByName('ent_razao_social').AsString;

      PCDS.Post;

      qryAux.Next;
    end;





  finally
    FreeAndNil(qryAux);
  end;

end;

procedure TAgendamentos_entidade.proc_eliminar;
var qryAux : TFDQuery;
begin
  qryAux := TFDQuery.Create(nil);
  try
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' delete from agendamentos_entidade         '+
                   ' where agd_codigo     := :p_agd_codigo     '+
                   '   and agd_emp_codigo := :p_agd_emp_codigo '+
                   '   and ent_codigo     := :p_ent_codigo     '+
                   '   and ent_emp_codigo := :p_ent_emp_codigo ');
    qryAux.ParamByName('p_agd_codigo').AsInteger     := agd_codigo;
    qryAux.ParamByName('p_agd_emp_codigo').AsInteger := agd_emp_codigo;
    qryAux.ParamByName('p_ent_codigo').AsInteger     := ent_codigo;
    qryAux.ParamByName('p_ent_emp_codigo').AsInteger := ent_emp_codigo;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TAgendamentos_entidade.proc_gravar;
var qryAux : TFDQuery;
begin
  qryAux := TFDQuery.Create(nil);
  try
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' insert into agendamentos_entidade ( '+
                   '   agd_codigo    ,                   '+
                   '   agd_emp_codigo,                   '+
                   '   ent_codigo    ,                   '+
                   '   ent_emp_codigo                    '+
                   '  ) values (                         '+
                   '   :p_agd_codigo    ,                '+
                   '   :p_agd_emp_codigo,                '+
                   '   :p_ent_codigo    ,                '+
                   '   :p_ent_emp_codigo)                ');
    qryAux.ParamByName('p_agd_codigo').AsInteger     := agd_codigo;
    qryAux.ParamByName('p_agd_emp_codigo').AsInteger := agd_emp_codigo;
    qryAux.ParamByName('p_ent_codigo').AsInteger     := ent_codigo;
    qryAux.ParamByName('p_ent_emp_codigo').AsInteger := ent_emp_codigo;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
