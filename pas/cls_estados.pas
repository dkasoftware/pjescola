unit cls_estados;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type TEstados = Class(TObject)
  private
    { Private declarations }
    f_uf_sigla    : String;
    f_uf_nome     : String;
    f_uf_regiao   : String;
    f_uf_cod_ibge : String;
  public
    { Public declarations }
    procedure proc_gravar(PUFSigla : String);
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PUFSigla, PNome : String);
    procedure proc_carregar_combo(var PComboBox : TComboBox);

    function func_recuperar(PUFSigla : String) : Boolean;
  published
    { Published declarations }
    property uf_sigla    : String read f_uf_sigla    write f_uf_sigla;
    property uf_nome     : String read f_uf_nome     write f_uf_nome;
    property uf_regiao   : String read f_uf_regiao   write f_uf_regiao;
    property uf_cod_ibge : String read f_uf_cod_ibge write f_uf_cod_ibge;
  end;

type
  TUFCombo = TEstados;

implementation

{ TEstados }

function TEstados.func_recuperar(PUFSigla: String): Boolean;
begin

end;

procedure TEstados.proc_carregar_combo(var PComboBox: TComboBox);
var qryAux    : TFDQuery;
    c_estados : TUFCombo;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *            '+
                   ' from estados e      '+
                   ' order by e.uf_sigla ');
    qryAux.Open;

    proc_limpa_combo(PComboBox);

    while (not qryAux.Eof) do begin
      c_estados := TUFCombo.Create;

      c_estados.uf_sigla := qryAux.FieldByName('uf_sigla').AsString;
      c_estados.uf_nome  := qryAux.FieldByName('uf_nome').AsString;

      PComboBox.Items.AddObject(c_estados.uf_sigla, c_estados);

      qryAux.Next;
    end;

    PComboBox.ItemIndex := -1;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TEstados.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PUFSigla, PNome: String);
begin

end;

procedure TEstados.proc_gravar(PUFSigla: String);
begin

end;

end.
