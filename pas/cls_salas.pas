unit cls_salas;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Classes, DB;

type TSalas = Class(TObject)
  private
    { Private declarations }
    f_sala_codigo     : Integer;
    f_sala_emp_codigo : integer;
    f_sala_descricao  : String;
  public
    { Public declarations }
    procedure proc_carregar_combo(var PComboBox: TComboBox);

    function func_recuperar(PCodigo, PEmpCodigo : Integer) : Boolean;
    function func_retorna_nome_combo(PCodigo, PEmpresa : Integer) : String;
  published
    { Published declarations }
    property sala_codigo     : Integer read f_sala_codigo     write f_sala_codigo;
    property sala_emp_codigo : integer read f_sala_emp_codigo write f_sala_emp_codigo;
    property sala_descricao  : String  read f_sala_descricao  write f_sala_descricao;
  end;

type
  TSalaCombo = TSalas;

implementation

{ TSalas }

function TSalas.func_recuperar(PCodigo, PEmpCodigo: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                     '+
                   ' from salas s                                 '+
                   ' where s.sala_codigo     = :p_sala_codigo     '+
                   '   and s.sala_emp_codigo = :p_sala_emp_codigo ');
    qryAux.ParamByName('p_sala_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_sala_emp_codigo').AsInteger := PEmpCodigo;
    qryAux.Open;

    Result := (not qryAux.Eof);

    sala_codigo      := qryAux.FieldByName('sala_codigo').AsInteger;
    sala_emp_codigo  := qryAux.FieldByName('sala_emp_codigo').AsInteger;
    sala_descricao   := qryAux.FieldByName('sala_descricao').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

function TSalas.func_retorna_nome_combo(PCodigo, PEmpresa: Integer): String;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select sala_descricao                      '+
                   ' from salas                                 '+
                   ' where sala_codigo     = :p_sala_codigo     '+
                   '   and sala_emp_codigo = :p_sala_emp_codigo ');
    qryAux.ParamByName('p_sala_codigo').AsInteger     := PCodigo;
    qryAux.ParamByName('p_sala_emp_codigo').AsInteger := PEmpresa;
    qryAux.Open;

    Result := qryAux.FieldByName('sala_descricao').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TSalas.proc_carregar_combo(var PComboBox: TComboBox);
var qryAux  : TFDQuery;
    c_salas : TSalaCombo;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *               '+
                   ' from salas s           '+
                   ' order by s.sala_codigo ');
    qryAux.Open;

    proc_limpa_combo(PComboBox);
    PComboBox.Items.Add('');
    while (not qryAux.Eof) do begin
      c_salas := TSalaCombo.Create;

      c_salas.sala_codigo     := qryAux.FieldByName('sala_codigo').AsInteger;
      c_salas.sala_emp_codigo := qryAux.FieldByName('sala_emp_codigo').AsInteger;
      c_salas.sala_descricao  := qryAux.FieldByName('sala_descricao').AsString;

      PComboBox.Items.AddObject(c_salas.sala_descricao, c_salas);

      qryAux.Next;
    end;

    PComboBox.ItemIndex := 0;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
