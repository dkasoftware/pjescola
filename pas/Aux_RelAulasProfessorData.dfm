inherited FrmAux_RelAulasProfessorData: TFrmAux_RelAulasProfessorData
  Caption = 'Relat'#243'rio Aulas Por Professor (Data)'
  ClientHeight = 194
  ClientWidth = 429
  OnActivate = FormActivate
  ExplicitWidth = 435
  ExplicitHeight = 223
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 429
    Height = 175
    object lbl_con_ent_codigo_funcionario: TLabel [0]
      Left = 18
      Top = 107
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = 'Professor'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited pnlButtons: TPanel
      Top = 139
      Width = 423
      inherited btnExcluir: TBitBtn
        Visible = False
      end
      inherited btnCancelar: TBitBtn
        Visible = False
      end
      inherited btnEditar: TBitBtn
        Visible = False
      end
      inherited btnGravar: TBitBtn
        Visible = False
      end
      inherited btnNovo: TBitBtn
        Caption = 'Imprimir'
        Glyph.Data = {
          36060000424D3606000000000000360000002800000020000000100000000100
          18000000000000060000120B0000120B00000000000000000000FF00FFFF00FF
          FF00FFFF00FF868484868484FF00FFFF00FF868484868484FF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF989898989898FF00FFFF
          00FF989898989898FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FF868484BCBABAB5B3B3868484868484868484EBEAEA8684848684848684
          84FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF989898BFBFBFBABABA98989898
          9898989898E1E1E1989898989898989898FF00FFFF00FFFF00FFFF00FFFF00FF
          868484E3E2E2B5B3B3B5B3B3B5B3B35150504F4F4F868787CDCDCDE8E9E9C7C6
          C6868484868484868484FF00FFFF00FF989898DBDBDBBABABABABABABABABA72
          7272717171999999CCCCCCDFDFDFC7C7C7989898989898989898FF00FF868484
          DEDDDDD6D6D6A4A3A3A4A3A3A4A3A35655551615161212121818181212129393
          93CACACA868484FF00FFFF00FF989898D7D7D7D3D3D3AEAEAEAEAEAEAEAEAE76
          7676474747454545494949454545A2A2A2CACACA989898FF00FF868484D6D6D6
          D6D6D6A4A3A3E0DEDED9D7D7CDCBCBC2C0C0B6B4B49F9D9D7976771212121414
          141313138684848F8C8C989898D3D3D3D3D3D3AEAEAED9D9D9D3D3D3CBCBCBC3
          C3C3BABABAAAAAAA8E8E8E4545454747474646469898989D9D9D868484D6D6D6
          A4A3A3F3F2F2FFFEFEFCFBFBEAE7E8E6E6E6E6E5E5DAD9D9CCCBCBBFBDBDA2A0
          A07371719391918E8C8C989898D3D3D3AEAEAEE6E6E6EFEFEFEDEDEDDFDFDFDE
          DEDEDDDDDDD4D4D4CACACAC1C1C1ACACAC8A8A8AA1A1A19D9D9D868484A4A3A3
          FFFFFFFEFDFDFBFBFBDFDEDFADA7A9B4ADAEC3BDBED1CECFE0E0E0E1E1E1D4D3
          D3C7C6C6A7A5A5868383989898AEAEAEEFEFEFEFEFEFEDEDEDD8D8D8B3B3B3B7
          B7B7C2C2C2CDCDCDDADADADBDBDBD0D0D0C7C7C7AFAFAF979797FF00FF868484
          868484F1F0F0C2B9BA93898BA19B9FABA8AAABA6A7B1ACADAFA9AAB2ADAECAC9
          C9DCDCDCD0D0D0868484FF00FF989898989898E6E6E6C1C1C19E9E9EAAAAAAB2
          B2B2B1B1B1B6B6B6B4B4B4B7B7B7C9C9C9D7D7D7CECECE989898FF00FFFF00FF
          FF00FF868484CAA097BA9E87A2897E95817B897C7F928C92A5A2A4BBB6B7D7D6
          D6CFCFCF868484FF00FFFF00FFFF00FFFF00FF989898A7A7A79999998A8A8A9A
          9A9A9696969F9F9FAEAEAEBDBDBDD3D3D3CDCDCD989898FF00FFFF00FFFF00FF
          FF00FFFF00FFCC9A99FFEAC4FFDDB3EEC399D5AE8CC9A786CC9A999895968684
          84868484FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9D3D3D3CCCCCCB8
          B8B8A7A7A79F9F9FA9A9A9A5A5A5989898989898FF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFCC9A99FFE7C8FFDDBAFFDBB1FFD9A6FFD39FCC9A99FF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9D5D5D5CFCFCFCB
          CBCBC6C6C6C3C3C3A9A9A9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFCE9D9BFFEDDAFFE7CEFFE2C3FFDDB8FFDBAECC9A99FF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFABABABDDDDDDD8D8D8D3D3D3CE
          CECECACACAA9A9A9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFCC9A99FEF0E1FFECD8FFE6CCFFE1C2FEDDB7CC9A99FF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9E0E0E0DCDCDCD7D7D7D3
          D3D3CDCDCDA9A9A9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          CC9A99FFFFF5FFFFF5FFF0E1FFEBD6FFE8CCF6D4BACC9A99FF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9EAEAEAEAEAEAE1E1E1DCDCDCD7
          D7D7CBCBCBA9A9A9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          CC9A99CC9A99CC9A99FCF3E9FCEADAFCE5D0CC9A99FF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9A9A9A9A9A9A9E3E3E3DCDCDCD8
          D8D8A9A9A9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFCC9A99CC9A99CC9A99FF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA9A9A9A9A9A9A9
          A9A9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
        OnClick = btnNovoClick
      end
      inherited btnSair: TBitBtn
        Left = 333
      end
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 8
      Width = 201
      Height = 89
      Caption = 'Per'#237'odo'
      TabOrder = 1
      object Label1: TLabel
        Left = 23
        Top = 25
        Width = 30
        Height = 16
        Alignment = taRightJustify
        Caption = 'In'#237'cio'
      end
      object Label2: TLabel
        Left = 26
        Top = 56
        Width = 27
        Height = 16
        Alignment = taRightJustify
        Caption = 'Final'
      end
      object ed_data_inicial: TDateTimePicker
        Left = 62
        Top = 21
        Width = 120
        Height = 24
        Date = 43852.284125532410000000
        Time = 43852.284125532410000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object ed_data_final: TDateTimePicker
        Left = 62
        Top = 51
        Width = 120
        Height = 24
        Date = 43852.284125532410000000
        Time = 43852.284125532410000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
    end
    object cmb_ent_codigo_funcionario: TComboBox
      Left = 78
      Top = 103
      Width = 335
      Height = 24
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  inherited SB: TStatusBar
    Top = 175
    Width = 429
  end
  object FDQAulasProfessor: TFDQuery
    SQL.Strings = (
      
        '  select e.ent_razao_social aluno, DATE_FORMAT(a.agd_data, '#39'%d/%' +
        'm/%Y'#39') dia,'
      '       a.agd_qtde_horas horas, f.ent_razao_social professor'
      'from agendamentos_entidade ae'
      
        'inner join agendamentos a on a.agd_codigo     = ae.agd_codigo an' +
        'd'
      #9#9#9#9#9#9#9' a.agd_emp_codigo = ae.agd_emp_codigo'
      
        'inner join entidades f on f.ent_codigo     = a.agd_fun_ent_codig' +
        'o and'
      
        '                          f.ent_emp_codigo = a.agd_fun_ent_emp_c' +
        'odigo'
      'inner join entidades e on e.ent_codigo     = ae.ent_codigo and'
      '                          e.ent_emp_codigo = ae.ent_emp_codigo'
      'where a.agd_fun_ent_codigo  = 124'
      '  and date(a.agd_data)     >= date('#39'2022-01-01'#39')'
      '  and date(a.agd_data)     <= date('#39'2022-01-30'#39')'
      'order by a.agd_data')
    Left = 259
    Top = 16
    object FDQAulasProfessoraluno: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'aluno'
      Origin = 'aluno'
      Size = 150
    end
    object FDQAulasProfessordia: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'dia'
      Origin = 'dia'
      Size = 10
    end
    object FDQAulasProfessorhoras: TBCDField
      AutoGenerateValue = arDefault
      FieldName = 'horas'
      Origin = 'horas'
      Precision = 15
    end
    object FDQAulasProfessorprofessor: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'professor'
      Origin = 'professor'
      Size = 150
    end
  end
  object Relatorio: TfrxReport
    Version = '4.13.2'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44749.851786238400000000
    ReportOptions.LastChange = 44749.860550347220000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 232
    Top = 16
    Datasets = <
      item
        DataSet = frxDBAulasProfessor
        DataSetName = 'frxDBAulasProfessor'
      end>
    Variables = <
      item
        Name = ' VAR'
        Value = ''
      end
      item
        Name = 'DATAINI'
        Value = ''
      end
      item
        Name = 'DATAFIN'
        Value = ''
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo2: TfrxMemoView
          Align = baCenter
          Width = 718.110700000000000000
          Height = 22.677167800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'RELA'#199#195'O DE AULAS POR PROFESSOR (DATA)')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 22.677180000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'PER'#205'ODO:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Align = baCenter
          Top = 41.574793390000000000
          Width = 718.110700000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object DATAINI: TfrxMemoView
          Left = 98.267780000000000000
          Top = 22.677180000000000000
          Width = 619.842920000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '  [DATAINI]   '#192'   [DATAFIN]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 18.897650000000000000
        Top = 222.992270000000000000
        Width = 718.110700000000000000
        DataSet = frxDBAulasProfessor
        DataSetName = 'frxDBAulasProfessor'
        RowCount = 0
        object frxDBAulasProfessoraluno: TfrxMemoView
          Width = 548.031850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'aluno'
          DataSet = frxDBAulasProfessor
          DataSetName = 'frxDBAulasProfessor'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDBAulasProfessor."aluno"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBAulasProfessordia: TfrxMemoView
          Left = 548.031850000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'dia'
          DataSet = frxDBAulasProfessor
          DataSetName = 'frxDBAulasProfessor'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDBAulasProfessor."dia"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBAulasProfessorhoras: TfrxMemoView
          Left = 642.520100000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'horas'
          DataSet = frxDBAulasProfessor
          DataSetName = 'frxDBAulasProfessor'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDBAulasProfessor."horas"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -7
        Font.Name = 'Arial'
        Font.Style = []
        Height = 11.338582677165400000
        ParentFont = False
        Top = 343.937230000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Align = baCenter
          Width = 718.110700000000000000
          Height = 11.338582680000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'RELA'#199#195'O DE AULAS POR PROFESSOR (DATA)')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 15.118120000000000000
        Top = 86.929190000000000000
        Width = 718.110700000000000000
        object SysMemo1: TfrxSysMemoView
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          ShowHint = False
          AutoWidth = True
          DisplayFormat.FormatStr = 'dddddd'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[DATE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line2: TfrxLineView
          Align = baCenter
          Top = 11.338590000000000000
          Width = 718.110700000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object SysMemo2: TfrxSysMemoView
          Left = 623.622450000000000000
          Width = 94.488250000000000000
          Height = 11.338582680000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'g. [PAGE#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Height = 37.795300000000000000
        Top = 162.519790000000000000
        Width = 718.110700000000000000
        object Memo4: TfrxMemoView
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'PROFESSOR:')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDBAulasProfessorprofessor: TfrxMemoView
          Left = 98.267780000000000000
          Width = 449.764070000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'professor'
          DataSet = frxDBAulasProfessor
          DataSetName = 'frxDBAulasProfessor'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDBAulasProfessor."professor"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Top = 18.897650000000000000
          Width = 548.031850000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'ALUNO:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'DIA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 642.520100000000000000
          Top = 18.897650000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'HORAS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        Height = 18.897650000000000000
        Top = 264.567100000000000000
        Width = 718.110700000000000000
        object Memo8: TfrxMemoView
          Left = 642.520100000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          DataSet = frxDBAulasProfessor
          DataSetName = 'frxDBAulasProfessor'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDBAulasProfessor."horas">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 548.031850000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Color = clSilver
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDBAulasProfessor: TfrxDBDataset
    UserName = 'frxDBAulasProfessor'
    CloseDataSource = False
    FieldAliases.Strings = (
      'aluno=aluno'
      'dia=dia'
      'horas=horas'
      'professor=professor')
    DataSet = FDQAulasProfessor
    BCDToCurrency = False
    Left = 259
    Top = 44
  end
end
