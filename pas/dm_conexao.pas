unit dm_conexao;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.VCLUI.Wait,
  FireDAC.Comp.UI, Data.DB, FireDAC.Comp.Client, FireDAC.Phys.MySQL, IniFiles,
  Forms, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet;

type
  TConexao = class(TDataModule)
    FDConexao: TFDConnection;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDTransaction: TFDTransaction;
    FDPhysMySQLDriverLink: TFDPhysMySQLDriverLink;
    FDQAux: TFDQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    f_db_host   : String;
    f_db_user   : String;
    f_db_pass   : String;
    f_db_schema : String;
  public
    { Public declarations }
    procedure proc_conectar;

  published
    { Published declarations }
    property db_host    : String  read f_db_host    write f_db_host;
    property db_user    : String  read f_db_user    write f_db_user;
    property db_pass    : String  read f_db_pass    write f_db_pass;
    property db_schema  : String  read f_db_schema  write f_db_schema;
  end;

var
  Conexao: TConexao;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses cls_utils;

{$R *.dfm}

{ TConexao }

procedure TConexao.DataModuleCreate(Sender: TObject);
begin
  proc_conectar;
end;

procedure TConexao.DataModuleDestroy(Sender: TObject);
begin
  FDConexao.Connected := False;
  FDConexao.Params.Clear;
end;

procedure TConexao.proc_conectar;
var ex_config : TIniFile;
begin
  if (not FileExists(ExtractFilePath(Application.ExeName) + 'dkasystem.ini')) then begin
    Application.MessageBox(PChar('Favor configurar o arquivo dkasystem.ini'), PChar('Aten��o'), 0 + 48);
    Application.Terminate;
  end;

  ex_config := TIniFile.Create(ExtractFilePath(Application.ExeName) + 'dkasystem.ini');
  try
    FDConexao.Params.Clear;

    FDConexao.Params.Add('DriverID=MySQL');
    FDConexao.Params.Add('Server='    + ex_config.ReadString('LOCAL', 'server', ''));
    FDConexao.Params.Add('Database='  + ex_config.ReadString('LOCAL', 'database', ''));
    FDConexao.Params.Add('User_Name=' + ex_config.ReadString('LOCAL', 'user_name', ''));
    FDConexao.Params.Add('Password='  + func_base64decode(ex_config.ReadString('LOCAL', 'password', '')));
    FDConexao.Params.Add('Port='      + ex_config.ReadString('LOCAL', 'port', ''));

    FDConexao.Params.Add('UseSSL=False');
    FDConexao.Params.Add('Pooled=False');
    FDConexao.Params.Add('Compress=True');
    FDConexao.Params.Add('ResultMode=Store');
    FDConexao.Params.Add('TinyIntFormat=Boolean');

    FDPhysMySQLDriverLink.VendorLib := ex_config.ReadString('LOCAL', 'vendorlib', ExtractFilePath(Application.ExeName) + 'libmySQL.dll');

    try
      FDConexao.Connected := True;

      db_host   := ex_config.ReadString('LOCAL', 'server', '');
      db_user   := ex_config.ReadString('LOCAL', 'user_name', '');
      db_pass   := ex_config.ReadString('LOCAL', 'password', '');
      db_schema := ex_config.ReadString('LOCAL', 'database', '');
    except
      FDConexao.Params.Clear;

      FDConexao.Params.Add('DriverID=MySQL');
      FDConexao.Params.Add('Server='    + ex_config.ReadString('CLOUD', 'server', ''));
      FDConexao.Params.Add('Database='  + ex_config.ReadString('CLOUD', 'database', ''));
      FDConexao.Params.Add('User_Name=' + ex_config.ReadString('CLOUD', 'user_name', ''));
      FDConexao.Params.Add('Password='  + func_base64decode(ex_config.ReadString('CLOUD', 'password', '')));
      FDConexao.Params.Add('Port='      + ex_config.ReadString('CLOUD', 'port', ''));

      FDConexao.Params.Add('UseSSL=False');
      FDConexao.Params.Add('Pooled=False');
      FDConexao.Params.Add('Compress=True');
      FDConexao.Params.Add('ResultMode=Store');
      FDConexao.Params.Add('TinyIntFormat=Boolean');

      FDPhysMySQLDriverLink.VendorLib := ex_config.ReadString('CLOUD', 'vendorlib', ExtractFilePath(Application.ExeName) + 'libmySQL.dll');

      try
        FDConexao.Connected := True;

        db_host   := ex_config.ReadString('CLOUD', 'server', '');
        db_user   := ex_config.ReadString('CLOUD', 'user_name', '');
        db_pass   := ex_config.ReadString('CLOUD', 'password', '');
        db_schema := ex_config.ReadString('CLOUD', 'database', '');
      except
        on E : Exception do begin
          Application.MessageBox(PChar(E.Message), PChar('Aten��o'), 0 + 48);
        end;
      end;
    end;
  finally
    FreeAndNil(ex_config);
  end;
end;

end.
