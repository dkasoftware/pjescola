unit cad_funcionarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.Mask, Vcl.Grids, Vcl.DBGrids,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  FireDAC.Stan.Async, FireDAC.DApt, Datasnap.Provider, Data.DB,
  Datasnap.DBClient, FireDAC.Comp.DataSet, FireDAC.Comp.Client, cls_utils;

type
  TFrmCad_Funcionarios = class(TFrmAux_Padrao)
    PC: TPageControl;
    tsConsulta: TTabSheet;
    pnlConsulta: TPanel;
    dbgEntidades: TDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    cmbCampos: TComboBox;
    ed_consulta: TEdit;
    cmbConStatus: TComboBox;
    tsCadastro: TTabSheet;
    pnlCadastro: TPanel;
    Panel7: TPanel;
    gb_ent_codigo: TGroupBox;
    ed_fun_codigo: TEdit;
    cmb_fun_emp_codigo: TComboBox;
    Panel8: TPanel;
    PCAdicionais: TPageControl;
    tsEnderecos: TTabSheet;
    tsObservacoes: TTabSheet;
    ed_fun_observacoes: TRichEdit;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    ed_fun_nome: TEdit;
    ed_fun_rg: TEdit;
    ed_fun_data_nascimento: TDateTimePicker;
    cmb_fun_estado_civil: TComboBox;
    ed_fun_cpf: TMaskEdit;
    cmb_fun_situacao: TComboBox;
    Label3: TLabel;
    ed_fun_ct: TMaskEdit;
    Label10: TLabel;
    ed_fun_pis: TMaskEdit;
    tsContatos: TTabSheet;
    Label9: TLabel;
    ed_fun_email: TEdit;
    lbl_con_telefone: TLabel;
    ed_fun_telefone: TMaskEdit;
    Label11: TLabel;
    ed_fun_funcao: TMaskEdit;
    Label12: TLabel;
    cmb_fun_modo: TComboBox;
    Label13: TLabel;
    ed_fun_salario: TMaskEdit;
    lbl_end_cep: TLabel;
    lbl_end_uf_sigla: TLabel;
    lbl_end_cid_codigo: TLabel;
    lbl_end_logradouro: TLabel;
    lbl_end_numero: TLabel;
    lbl_end_complemento: TLabel;
    ed_fun_cep: TEdit;
    cmb_fun_uf_sigla: TComboBox;
    cmb_fun_cid_codigo: TComboBox;
    btnAddEnd: TBitBtn;
    ed_fun_logradouro: TEdit;
    ed_fun_numero: TEdit;
    ed_fun_complemento: TEdit;
    FDQFuncionarios: TFDQuery;
    CDSFuncionarios: TClientDataSet;
    DSFuncionarios: TDataSource;
    PROFuncionarios: TDataSetProvider;
    FDQFuncionariosfun_codigo: TIntegerField;
    FDQFuncionariosfun_emp_codigo: TIntegerField;
    FDQFuncionariosfun_nome: TStringField;
    FDQFuncionariosfun_cpf: TStringField;
    FDQFuncionariosfun_rg: TStringField;
    FDQFuncionariosfun_data_nascimento: TDateField;
    FDQFuncionariosfun_estado_civil: TStringField;
    FDQFuncionariosfun_ct: TStringField;
    FDQFuncionariosfun_pis: TStringField;
    FDQFuncionariosfun_funcao: TStringField;
    FDQFuncionariosfun_salario: TBCDField;
    FDQFuncionariosfun_modo: TIntegerField;
    FDQFuncionariosfun_telefone: TStringField;
    FDQFuncionariosfun_email: TStringField;
    FDQFuncionariosfun_cep: TStringField;
    FDQFuncionariosfun_uf_sigla: TStringField;
    FDQFuncionariosfun_cid_codigo: TIntegerField;
    FDQFuncionariosfun_logradouro: TStringField;
    FDQFuncionariosfun_numero: TIntegerField;
    FDQFuncionariosfun_complemento: TStringField;
    FDQFuncionariosfun_observacoes: TBlobField;
    FDQFuncionariosfun_situacao: TIntegerField;
    CDSFuncionariosfun_codigo: TIntegerField;
    CDSFuncionariosfun_emp_codigo: TIntegerField;
    CDSFuncionariosfun_nome: TStringField;
    CDSFuncionariosfun_cpf: TStringField;
    CDSFuncionariosfun_rg: TStringField;
    CDSFuncionariosfun_data_nascimento: TDateField;
    CDSFuncionariosfun_estado_civil: TStringField;
    CDSFuncionariosfun_ct: TStringField;
    CDSFuncionariosfun_pis: TStringField;
    CDSFuncionariosfun_funcao: TStringField;
    CDSFuncionariosfun_salario: TBCDField;
    CDSFuncionariosfun_modo: TIntegerField;
    CDSFuncionariosfun_telefone: TStringField;
    CDSFuncionariosfun_email: TStringField;
    CDSFuncionariosfun_cep: TStringField;
    CDSFuncionariosfun_uf_sigla: TStringField;
    CDSFuncionariosfun_cid_codigo: TIntegerField;
    CDSFuncionariosfun_logradouro: TStringField;
    CDSFuncionariosfun_numero: TIntegerField;
    CDSFuncionariosfun_complemento: TStringField;
    CDSFuncionariosfun_observacoes: TBlobField;
    CDSFuncionariosfun_situacao: TIntegerField;
    procedure FormActivate(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
    procedure btnEditarClick(Sender: TObject);
  private
    { Private declarations }
    v_operacao : TOperacaoQuery;

    procedure proc_limpa_campos;
  public
    { Public declarations }
  end;

var
  FrmCad_Funcionarios: TFrmCad_Funcionarios;

implementation

{$R *.dfm}

uses dm_conexao, cls_empresas, cls_estados, cls_funcionarios, cls_tabelacodigos,
  menu;

procedure TFrmCad_Funcionarios.btnCancelarClick(Sender: TObject);
begin
  inherited;
  proc_limpa_campos;

  tsCadastro.TabVisible := False;
  tsConsulta.TabVisible := True;

  ed_consulta.SetFocus;

  CDSFuncionarios.Close;
  FDQFuncionarios.Close;

  v_operacao := statNavegando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSFuncionarios);

  btnExcluir.Enabled := False;
end;

procedure TFrmCad_Funcionarios.btnEditarClick(Sender: TObject);
var c_empresa      : TEmpresas;
    c_funcionarios : TFuncionarios;
begin
  inherited;
  ed_fun_codigo.Text := CDSFuncionariosfun_codigo.AsString;

  try
    c_empresa := TEmpresas.Create;
    cmb_fun_emp_codigo.ItemIndex := cmb_fun_emp_codigo.Items.IndexOf(c_empresa.func_retorna_razao_combo(CDSFuncionariosfun_emp_codigo.AsInteger));
  finally
    FreeAndNil(c_empresa);
  end;

  ed_fun_nome.Text               := CDSFuncionariosfun_nome.AsString;
  ed_fun_cpf.Text                := CDSFuncionariosfun_cpf.AsString;
  ed_fun_rg.Text                 := CDSFuncionariosfun_rg.AsString;
  ed_fun_data_nascimento.Date    := CDSFuncionariosfun_data_nascimento.AsDateTime;
  cmb_fun_estado_civil.ItemIndex := cmb_fun_estado_civil.Items.IndexOf(CDSFuncionariosfun_estado_civil.AsString);
  ed_fun_ct.Text                 := CDSFuncionariosfun_ct.AsString;
  ed_fun_pis.Text                := CDSFuncionariosfun_pis.AsString;
  ed_fun_funcao.Text             := CDSFuncionariosfun_funcao.AsString;
  ed_fun_salario.Text            := CDSFuncionariosfun_salario.AsString;
  cmb_fun_modo.ItemIndex         := CDSFuncionariosfun_modo.AsInteger;

  c_funcionarios := TFuncionarios.Create;

  {try
    if c_funcionarios.func_recuperar_observacoes(CDSEntidadesent_codigo.AsInteger, CDSEntidadesent_emp_codigo.AsInteger) then begin
      c_entidades.ent_observacoes.Position := 0;
      ed_ent_observacoes.Lines.LoadFromStream(c_entidades.ent_observacoes);
    end;
  finally
    c_entidades.ent_observacoes.Free;
    FreeAndNil(c_entidades);
  end;

  try
    c_status  := TSituacoesEntidade.Create;
    cmb_ent_sit_codigo.ItemIndex := cmb_ent_sit_codigo.Items.IndexOf(c_status.func_retorna_status_combo(CDSEntidadesent_sit_codigo.AsInteger));
  finally
    FreeAndNil(c_status);
  end;

  v_operacao := statEditando;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSEntidades);

  c_enderecos := TEnderecos.Create;
  try
    c_enderecos.proc_consultar(FDQEnderecos, CDSEnderecos, StrToInt(ed_ent_codigo.Text), TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo);
  finally
    FreeAndNil(c_enderecos);
  end;

  c_contatos := TContatos.Create;
  try
    c_contatos.proc_consultar(FDQContatos, CDSContatos, StrToInt(ed_ent_codigo.Text), TEmpresasCombo(cmb_ent_emp_codigo.Items.Objects[cmb_ent_emp_codigo.ItemIndex]).emp_codigo);
  finally
    FreeAndNil(c_contatos);
  end;

  c_responsaveis := TResponsaveis.Create;
  try
    if c_responsaveis.func_recuperar(CDSEntidadesent_codigo.AsInteger, CDSEntidadesent_emp_codigo.AsInteger) then begin
      ed_res_nome.Text            := c_responsaveis.res_nome;
      ed_res_rg.Text              := c_responsaveis.res_rg;
      ed_res_cpf.Text             := c_responsaveis.res_cpf;
      ed_res_data_nascimento.Date := c_responsaveis.res_data_nascimento;
    end;
  finally
    FreeAndNil(c_responsaveis);
  end;

  v_operacao_enderecos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesEndereco, v_operacao_enderecos, CDSEnderecos);

  v_operacao_contatos := statNavegando;
  proc_atualiza_operacao_query(pnlBotoesContatos, v_operacao_contatos, CDSContatos);

  proc_limpa_campos_endereco(False);
  proc_limpa_campos_contato(False);

  PCAdicionais.ActivePageIndex := tsEnderecos.TabIndex;

  tsConsulta.TabVisible := False;
  tsCadastro.TabVisible := True;

  rg_ent_tipo_pessoa.SetFocus;}
end;

procedure TFrmCad_Funcionarios.btnNovoClick(Sender: TObject);
var c_funcionarios : TFuncionarios;
    c_tabela       : TTabelaCodigos;
begin
  inherited;
  try
    Conexao.FDConexao.StartTransaction;

    ed_fun_codigo.Text := c_tabela.func_gerar_codigo('funcionarios', IntToStr(TEmpresasCombo(cmb_fun_emp_codigo.Items.Objects[cmb_fun_emp_codigo.ItemIndex]).emp_codigo)).ToString;
    cmb_fun_emp_codigo.ItemIndex := cmb_fun_emp_codigo.Items.IndexOf(frmMenu.razao_empresa);

    Conexao.FDConexao.Commit;
  except
    on E : Exception do begin
      Conexao.FDConexao.Rollback;
      Application.MessageBox(PChar(E.Message), PChar(Self.Caption), MB_OK + MB_ICONERROR);
      Exit;
    end;
  end;

  tsConsulta.TabVisible := False;
  tsCadastro.TabVisible := True;

  proc_limpa_campos;

  v_operacao := statInserindo;
  proc_atualiza_operacao_query(TForm(Self), v_operacao, CDSFuncionarios);

  cmb_fun_situacao.ItemIndex := 0;

  PCAdicionais.ActivePageIndex := tsEnderecos.TabIndex;

  tsCadastro.TabVisible := True;
  tsConsulta.TabVisible := False;

  ed_fun_nome.SetFocus;
end;

procedure TFrmCad_Funcionarios.FormActivate(Sender: TObject);
var c_empresas : TEmpresas;
    c_estados  : TEstados;
begin
  inherited;
  try
    c_empresas := TEmpresas.Create;
    c_estados  := TEstados.Create;

    c_estados.proc_carregar_combo(cmb_fun_uf_sigla);
    c_empresas.proc_carregar_combo(cmb_fun_emp_codigo);

    cmbConStatus.ItemIndex := cmbConStatus.Items.Count - 1;
  finally
    FreeAndNil(c_empresas);
    FreeAndNil(c_estados);
  end;

  ed_consulta.Clear;

  btnCancelar.Click;
end;

procedure TFrmCad_Funcionarios.proc_limpa_campos;
begin
  ed_fun_codigo.Clear;
  ed_fun_nome.Clear;
  ed_fun_cpf.Clear;
  ed_fun_rg.Clear;
  ed_fun_data_nascimento.Date := Date - 2555;
  cmb_fun_estado_civil.ItemIndex := -1;
  ed_fun_ct.Clear;
  ed_fun_pis.Clear;
  ed_fun_funcao.Clear;
  ed_fun_salario.Clear;
  cmb_fun_modo.ItemIndex := 0;
  ed_fun_telefone.Clear;
  ed_fun_email.Clear;
  ed_fun_cep.Clear;
  cmb_fun_uf_sigla.ItemIndex := -1;
  cmb_fun_cid_codigo.Items.Clear;
  ed_fun_logradouro.Clear;
  ed_fun_numero.Clear;
  ed_fun_complemento.Clear;
  ed_fun_observacoes.Lines.Clear;
  cmb_fun_situacao.ItemIndex := 0;
end;

end.
