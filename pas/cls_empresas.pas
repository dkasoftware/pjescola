unit cls_empresas;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type TEmpresas = Class(TObject)
  private
    { Private declarations }
    f_emp_codigo        : Integer;
    f_emp_razao_social  : String;
    f_emp_nome_fantasia : String;
    f_emp_cnpj          : String;
    f_emp_ie            : String;
    f_emp_im            : String;
    f_emp_telefone      : String;
    f_emp_email         : String;
    f_emp_data_abertura : TDateTime;
    f_emp_observacoes   : String;
    f_emp_cep           : String;
    f_emp_logradouro    : String;
    f_emp_numero        : Integer;
    f_emp_complemento   : String;
    f_emp_uf_sigla      : String;
    f_emp_cid_codigo    : Integer;
    f_emp_situacao      : Integer;
  public
    { Public declarations }
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PComboCampos : TComboBox; PConsulta : String);
    procedure proc_gravar(PCodigo : Integer);
    procedure proc_carregar_combo(var PComboBox : TComboBox);

    function func_retorna_razao_combo(PCodigo : Integer) : String;
  published
    { Published declarations }
    property emp_codigo        : Integer   read f_emp_codigo        write f_emp_codigo;
    property emp_razao_social  : String    read f_emp_razao_social  write f_emp_razao_social;
    property emp_nome_fantasia : String    read f_emp_nome_fantasia write f_emp_nome_fantasia;
    property emp_cnpj          : String    read f_emp_cnpj          write f_emp_cnpj;
    property emp_ie            : String    read f_emp_ie            write f_emp_ie;
    property emp_im            : String    read f_emp_im            write f_emp_im;
    property emp_telefone      : String    read f_emp_telefone      write f_emp_telefone;
    property emp_email         : String    read f_emp_email         write f_emp_email;
    property emp_data_abertura : TDateTime read f_emp_data_abertura write f_emp_data_abertura;
    property emp_observacoes   : String    read f_emp_observacoes   write f_emp_observacoes;
    property emp_cep           : String    read f_emp_cep           write f_emp_cep;
    property emp_logradouro    : String    read f_emp_logradouro    write f_emp_logradouro;
    property emp_numero        : Integer   read f_emp_numero        write f_emp_numero;
    property emp_complemento   : String    read f_emp_complemento   write f_emp_complemento;
    property emp_uf_sigla      : String    read f_emp_uf_sigla      write f_emp_uf_sigla;
    property emp_cid_codigo    : Integer   read f_emp_cid_codigo    write f_emp_cid_codigo;
    property emp_situacao      : Integer   read f_emp_situacao      write f_emp_situacao;
  end;

type
  TEmpresasCombo = TEmpresas;

implementation

{ TEmpresas }

function TEmpresas.func_retorna_razao_combo(PCodigo: Integer): String;
var qryAux     : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select e.emp_razao_social          '+
                   ' from empresas e                    '+
                   ' where e.emp_codigo = :p_emp_codigo ');
    qryAux.ParamByName('p_emp_codigo').AsInteger := PCodigo;
    qryAux.Open;

    Result := qryAux.FieldByName('emp_razao_social').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TEmpresas.proc_carregar_combo(var PComboBox : TComboBox);
var qryAux     : TFDQuery;
    c_empresas : TEmpresasCombo;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                 '+
                   ' from empresas e          '+
                   ' where e.emp_situacao = 0 '+
                   ' order by e.emp_codigo    ');
    qryAux.Open;

    proc_limpa_combo(PComboBox);
    while (not qryAux.Eof) do begin
      c_empresas := TEmpresasCombo.Create;

      c_empresas.emp_codigo       := qryAux.FieldByName('emp_codigo').AsInteger;
      c_empresas.emp_razao_social := qryAux.FieldByName('emp_razao_social').AsString;

      PComboBox.Items.AddObject(c_empresas.emp_razao_social, c_empresas);

      qryAux.Next;
    end;

    PComboBox.ItemIndex := 0;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TEmpresas.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PComboCampos: TComboBox; PConsulta: String);
begin
  //aqui a consulta
end;

procedure TEmpresas.proc_gravar(PCodigo: Integer);
begin
  //aqui o gravar
end;

end.
