inherited FrmCad_Contratos: TFrmCad_Contratos
  Caption = 'Contratos'
  ClientHeight = 621
  ClientWidth = 1018
  OnActivate = FormActivate
  ExplicitWidth = 1024
  ExplicitHeight = 650
  PixelsPerInch = 96
  TextHeight = 16
  inherited mainPainel: TPanel
    Width = 1018
    Height = 602
    ExplicitWidth = 1018
    ExplicitHeight = 602
    inherited pnlButtons: TPanel
      Top = 566
      Width = 1012
      ExplicitTop = 566
      ExplicitWidth = 1012
      inherited btnSair: TBitBtn
        Left = 922
        ExplicitLeft = 922
      end
    end
    object PC: TPageControl
      Left = 3
      Top = 3
      Width = 1012
      Height = 563
      ActivePage = tsCadastro
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object tsConsulta: TTabSheet
        Caption = 'Consulta'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        object pnlConsulta: TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 532
          Align = alClient
          BevelOuter = bvNone
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Padding.Left = 3
          Padding.Top = 3
          Padding.Right = 3
          Padding.Bottom = 3
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          object dbgEntidades: TDBGrid
            Left = 3
            Top = 37
            Width = 998
            Height = 492
            Align = alClient
            DataSource = dsAlunos
            GradientEndColor = 16754511
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -13
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnDblClick = dbgEntidadesDblClick
            Columns = <
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'ent_codigo'
                Title.Alignment = taCenter
                Title.Caption = 'C'#243'digo'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ent_razao_social'
                Title.Caption = 'Nome'
                Width = 880
                Visible = True
              end>
          end
          object Panel1: TPanel
            Left = 3
            Top = 3
            Width = 998
            Height = 34
            Align = alTop
            BevelInner = bvLowered
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            DesignSize = (
              998
              34)
            object Label1: TLabel
              Left = 14
              Top = 9
              Width = 33
              Height = 16
              Alignment = taRightJustify
              Caption = 'Nome'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
            end
            object ed_consulta: TEdit
              Left = 53
              Top = 5
              Width = 940
              Height = 24
              Anchors = [akLeft, akTop, akRight]
              CharCase = ecUpperCase
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Tahoma'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              OnChange = ed_consultaChange
            end
          end
        end
      end
      object tsCadastro: TTabSheet
        Caption = 'Contratos'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ImageIndex = 1
        ParentFont = False
        object pnlCadastro: TPanel
          Left = 0
          Top = 0
          Width = 1004
          Height = 532
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object TPanel
            Left = 0
            Top = 0
            Width = 1004
            Height = 532
            Align = alClient
            BevelOuter = bvNone
            Padding.Left = 5
            Padding.Top = 5
            Padding.Right = 5
            Padding.Bottom = 5
            TabOrder = 0
            object TGroupBox
              Left = 5
              Top = 5
              Width = 994
              Height = 54
              Align = alTop
              Caption = 'Cliente'
              Enabled = False
              TabOrder = 0
              object ed_ent_codigo: TEdit
                Left = 8
                Top = 20
                Width = 75
                Height = 24
                TabOrder = 0
              end
              object ed_ent_emp_codigo: TEdit
                Left = 85
                Top = 20
                Width = 900
                Height = 24
                TabOrder = 1
              end
            end
            object TGroupBox
              Left = 5
              Top = 304
              Width = 994
              Height = 223
              Align = alBottom
              Caption = 'Hist'#243'rico'
              TabOrder = 1
              object DBGrid1: TDBGrid
                Left = 2
                Top = 18
                Width = 990
                Height = 203
                Align = alClient
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -13
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
              end
            end
          end
        end
      end
    end
  end
  inherited SB: TStatusBar
    Top = 602
    Width = 1018
    ExplicitTop = 602
    ExplicitWidth = 1018
  end
  object FDQAlunos: TFDQuery
    Connection = Conexao.FDConexao
    SQL.Strings = (
      'select e.ent_codigo, e.ent_emp_codigo, e.ent_razao_social '#10
      'from entidades e'#10
      'where e.ent_sit_codigo = 1'#10
      'order by e.ent_razao_social')
    Left = 496
    Top = 312
    object FDQAlunosent_codigo: TIntegerField
      FieldName = 'ent_codigo'
      Origin = 'ent_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQAlunosent_emp_codigo: TIntegerField
      FieldName = 'ent_emp_codigo'
      Origin = 'ent_emp_codigo'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQAlunosent_razao_social: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'ent_razao_social'
      Origin = 'ent_razao_social'
      Size = 150
    end
  end
  object dsAlunos: TDataSource
    DataSet = FDQAlunos
    Left = 496
    Top = 340
  end
end
