unit cls_situacoes_entidade;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils;

type
  TSituacoesEntidade = Class(TObject)
  private
    { Private declarations }
    f_sit_codigo    : Integer;
    f_sit_descricao : String;
  public
    { Public declarations }
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PComboCampos : TComboBox; PConsulta : String);
    procedure proc_gravar(PCodigo : Integer);
    procedure proc_carregar_combobox(var PComboBox : TComboBox; PTipoCombo : Integer = 0);

    function func_retorna_status_combo(PCodigo : Integer) : String;
  published
    { Published declarations }
    property sit_codigo    : Integer read f_sit_codigo    write f_sit_codigo;
    property sit_descricao : String  read f_sit_descricao write f_sit_descricao;
  end;

type
  TSituacoesCombo = TSituacoesEntidade;

implementation

{ TSituacoesEntidade }

function TSituacoesEntidade.func_retorna_status_combo(PCodigo: Integer): String;
var qryAux      : TFDQuery;
    c_situacoes : TSituacoesCombo;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                           '+
                   ' from situacoes_entidade s          '+
                   ' where s.sit_codigo = :p_sit_codigo ');
    qryAux.ParamByName('p_sit_codigo').AsInteger := PCodigo;
    qryAux.Open;

    Result := qryAux.FieldByName('sit_descricao').AsString;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TSituacoesEntidade.proc_carregar_combobox(
  var PComboBox : TComboBox; PTipoCombo : Integer = 0);
var qryAux      : TFDQuery;
    c_situacoes : TSituacoesCombo;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                  '+
                   ' from situacoes_entidade s '+
                   ' order by s.sit_codigo     ');
    qryAux.Open;

    proc_limpa_combo(PComboBox);
    while (not qryAux.Eof) do begin
      c_situacoes := TSituacoesCombo.Create;

      c_situacoes.sit_codigo    := qryAux.FieldByName('sit_codigo').AsInteger;
      c_situacoes.sit_descricao := qryAux.FieldByName('sit_descricao').AsString;

      PComboBox.Items.AddObject(c_situacoes.sit_descricao, c_situacoes);

      qryAux.Next;
    end;

    if (PComboBox.Items.Count > 0) and (PTipoCombo = 0) then begin
      c_situacoes := TSituacoesCombo.Create;

      c_situacoes.sit_codigo    := PComboBox.Items.Count;
      c_situacoes.sit_descricao := 'TODOS';

      PComboBox.Items.AddObject(c_situacoes.sit_descricao, c_situacoes);
    end;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TSituacoesEntidade.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PComboCampos: TComboBox; PConsulta: String);
begin
  PQuery.Connection := Conexao.FDConexao;

  PClient.Close;
  PQuery.Close;
  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                  '+
                 ' from situacoes_entidade s '+
                 ' where                     ');

  case PComboCampos.ItemIndex of
    0 : PQuery.SQL.Add(' sit_codigo like concat(''%'', :p_consulta, ''%'') ');
    1 : PQuery.SQL.Add(' sit_descricao like concat(''%'', :p_consulta, ''%'') ');
  end;

  PQuery.ParamByName('p_consulta').AsString := PConsulta;
  PClient.Open;
end;

procedure TSituacoesEntidade.proc_gravar(PCodigo: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                           '+
                   ' from situacoes_entidade s          '+
                   ' where s.sit_codigo = :p_sit_codigo ');
    qryAux.ParamByName('p_sit_codigo').AsInteger := PCodigo;
    qryAux.Open;

    if (qryAux.Eof) then begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' insert into situacoes_entidade ( '+
                     '   sit_descricao                  '+
                     ' ) values (                       '+
                     '   :p_sit_descricao)              ');
    end else begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add(' update situacoes_entidade            '+
                     ' set sit_descricao = :p_sit_descricao '+
                     ' where sit_codigo = :p_sit_codigo     ');
      qryAux.ParamByName('p_sit_codigo').AsInteger := PCodigo;
    end;

    qryAux.ParamByName('p_sit_descricao').AsString := sit_descricao;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
