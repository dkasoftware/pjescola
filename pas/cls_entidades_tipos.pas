unit cls_entidades_tipos;

interface

uses dm_conexao, FireDAC.Comp.Client, Datasnap.DBClient, Vcl.StdCtrls,
     SysUtils, Variants, cls_utils, Classes, DB, Vcl.CheckLst;

type TEntidadesTipos = Class(TObject)
  private
    { Private declarations }
    f_etp_ent_codigo     : Integer;
    f_etp_ent_emp_codigo : Integer;
    f_etp_tipo           : Integer;
  public
    { Public declarations }
    procedure proc_consultar(var PQuery : TFDQuery; var PClient : TClientDataSet; PEntCodigo, PEntEmpresa : Integer);
    procedure proc_carrega_check_list(var PLista : TCheckListBox; PEntCodigo, PEntEmpresa : Integer);
    procedure proc_gravar(PEntCodigo, PEntEmpresa, PTipo : Integer);
    procedure proc_limpar_gravacao(PEntCodigo, PEntEmpresa : Integer);

    function func_recuperar(PEntCodigo, PEntEmpresa, PTipo : Integer) : Boolean;
  published
    { Published declarations }
    property etp_ent_codigo     : Integer read f_etp_ent_codigo     write f_etp_ent_codigo;
    property etp_ent_emp_codigo : Integer read f_etp_ent_emp_codigo write f_etp_ent_emp_codigo;
    property etp_tipo           : Integer read f_etp_tipo           write f_etp_tipo;
  end;

implementation

{ TEntidades }

function TEntidadesTipos.func_recuperar(PEntCodigo, PEntEmpresa, PTipo: Integer): Boolean;
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' select *                                         '+
                   ' from entidades_tipos                             '+
                   ' where etp_ent_codigo     = :p_etp_ent_codigo     '+
                   '   and etp_ent_emp_codigo = :p_etp_ent_emp_codigo '+
                   '   and etp_tipo           = :p_etp_tipo           ');
    qryAux.ParamByName('p_etp_ent_codigo').AsInteger     := PEntCodigo;
    qryAux.ParamByName('p_etp_ent_emp_codigo').AsInteger := PEntEmpresa;
    qryAux.ParamByName('p_etp_tipo').AsInteger           := PTipo;
    qryAux.Open;

    Result := (not qryAux.Eof);

    etp_ent_codigo     := qryAux.FieldByName('etp_ent_codigo').AsInteger;
    etp_ent_emp_codigo := qryAux.FieldByName('etp_ent_emp_codigo').AsInteger;
    etp_tipo           := qryAux.FieldByName('etp_tipo').AsInteger;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TEntidadesTipos.proc_carrega_check_list(var PLista: TCheckListBox;
  PEntCodigo, PEntEmpresa: Integer);
var i_nro : Integer;
begin
  for i_nro := 0 to PLista.Items.Count -1 do begin
    PLista.Checked[i_nro] := func_recuperar(PEntCodigo, PEntEmpresa, i_nro);
  end;
end;

procedure TEntidadesTipos.proc_consultar(var PQuery: TFDQuery;
  var PClient: TClientDataSet; PEntCodigo, PEntEmpresa: Integer);
begin
  PQuery.Close;
  PClient.Close;

  PQuery.Connection := Conexao.FDConexao;

  PQuery.SQL.Clear;
  PQuery.SQL.Add(' select *                                         '+
                 ' from entidades_tipos                             '+
                 ' where etp_ent_codigo     = :p_etp_ent_codigo     '+
                 '   and etp_ent_emp_codigo = :p_etp_ent_emp_codigo ');
  PQuery.ParamByName('p_etp_ent_codigo').AsInteger     := PEntCodigo;
  PQuery.ParamByName('p_etp_ent_emp_codigo').AsInteger := PEntEmpresa;
  PQuery.Open;
  PClient.Open;
end;

procedure TEntidadesTipos.proc_gravar(PEntCodigo, PEntEmpresa, PTipo: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' insert into entidades_tipos( '+
                   '   etp_ent_codigo    ,        '+
                   '   etp_ent_emp_codigo,        '+
                   '   etp_tipo                   '+
                   ' )values(                     '+
                   '   :p_etp_ent_codigo    ,     '+
                   '   :p_etp_ent_emp_codigo,     '+
                   '   :p_etp_tipo          )     ');
    qryAux.ParamByName('p_etp_ent_codigo').AsInteger     := PEntCodigo;
    qryAux.ParamByName('p_etp_ent_emp_codigo').AsInteger := PEntEmpresa;
    qryAux.ParamByName('p_etp_tipo').AsInteger           := PTipo;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

procedure TEntidadesTipos.proc_limpar_gravacao(PEntCodigo,
  PEntEmpresa: Integer);
var qryAux : TFDQuery;
begin
  try
    qryAux := TFDQuery.Create(nil);
    qryAux.Connection := Conexao.FDConexao;

    qryAux.SQL.Add(' delete from entidades_tipos                      '+
                   ' where etp_ent_codigo     = :p_etp_ent_codigo     '+
                   '   and etp_ent_emp_codigo = :p_etp_ent_emp_codigo ');
    qryAux.ParamByName('p_etp_ent_codigo').AsInteger     := PEntCodigo;
    qryAux.ParamByName('p_etp_ent_emp_codigo').AsInteger := PEntEmpresa;
    qryAux.ExecSQL;
  finally
    FreeAndNil(qryAux);
  end;
end;

end.
