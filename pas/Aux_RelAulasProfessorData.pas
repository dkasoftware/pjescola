unit Aux_RelAulasProfessorData;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Aux_Padrao, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, cls_entidades, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  frxClass, frxDBSet, Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client,
  dm_conexao;

type
  TFrmAux_RelAulasProfessorData = class(TFrmAux_Padrao)
    GroupBox1: TGroupBox;
    ed_data_inicial: TDateTimePicker;
    ed_data_final: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    lbl_con_ent_codigo_funcionario: TLabel;
    cmb_ent_codigo_funcionario: TComboBox;
    FDQAulasProfessor: TFDQuery;
    Relatorio: TfrxReport;
    frxDBAulasProfessor: TfrxDBDataset;
    FDQAulasProfessoraluno: TStringField;
    FDQAulasProfessordia: TStringField;
    FDQAulasProfessorhoras: TBCDField;
    FDQAulasProfessorprofessor: TStringField;
    procedure btnSairClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btnNovoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrmAux_RelAulasProfessorData: TFrmAux_RelAulasProfessorData;

implementation

{$R *.dfm}

procedure TFrmAux_RelAulasProfessorData.btnNovoClick(Sender: TObject);
var s_caminho : String;
begin
  inherited;
  s_caminho := ExtractFilePath(Application.ExeName) + 'fast\RelAulasProfessorData.fr3';

  FDQAulasProfessor.Connection := Conexao.FDConexao;

  FDQAulasProfessor.Close;
  FDQAulasProfessor.SQL.Clear;
  FDQAulasProfessor.SQL.Add(' select e.ent_razao_social aluno, date_format(a.agd_data, ''%d/%m/%Y'') dia, '+
                            '        a.agd_qtde_horas horas, f.ent_razao_social professor                 '+
                            ' from agendamentos_entidade ae                                               '+
                            ' inner join agendamentos a on a.agd_codigo     = ae.agd_codigo and           '+
                            '                a.agd_emp_codigo = ae.agd_emp_codigo                         '+
                            ' inner join entidades f on f.ent_codigo     = a.agd_fun_ent_codigo and       '+
                            '                           f.ent_emp_codigo = a.agd_fun_ent_emp_codigo       '+
                            ' inner join entidades e on e.ent_codigo     = ae.ent_codigo and              '+
                            '                           e.ent_emp_codigo = ae.ent_emp_codigo              '+
                            ' where a.agd_fun_ent_codigo  = :p_agd_fun_ent_codigo                         '+
                            '   and date(a.agd_data)     >= date(:p_data_ini)                             '+
                            '   and date(a.agd_data)     <= date(:p_data_fin)                             '+
                            ' order by a.agd_data                                                         ');
  FDQAulasProfessor.ParamByName('p_agd_fun_ent_codigo').AsInteger := TEntidadeCombo(cmb_ent_codigo_funcionario.Items.Objects[cmb_ent_codigo_funcionario.ItemIndex]).ent_codigo;
  FDQAulasProfessor.ParamByName('p_data_ini').AsDateTime          := ed_data_inicial.Date;
  FDQAulasProfessor.ParamByName('p_data_fin').AsDateTime          := ed_data_final.Date;
  FDQAulasProfessor.Open;

  Relatorio.LoadFromFile(s_caminho);

  Relatorio.Variables['DATAINI'] := QuotedStr(FormatDateTime('dd/mm/yyyy', ed_data_inicial.Date));
  Relatorio.Variables['DATAFIN'] := QuotedStr(FormatDateTime('dd/mm/yyyy', ed_data_final.Date));

  Relatorio.ShowReport;
end;

procedure TFrmAux_RelAulasProfessorData.btnSairClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TFrmAux_RelAulasProfessorData.FormActivate(Sender: TObject);
var c_entidades : TEntidades;
begin
  inherited;
  ed_data_inicial.Date := Date;
  ed_data_final.Date   := Date;

  c_entidades := TEntidades.Create;

  try
    c_entidades.proc_carregar_combo(cmb_ent_codigo_funcionario, 2, True);
  finally
    FreeAndNil(c_entidades);
  end;
end;

end.
